﻿/*
*********************************************************************************************************
*                                                 uC/OS-III
*                                          The Real-Time Kernel
*                                               PORT Windows
*
*
*												Benjamin Heinen
*                                  Ecole Polytechnique de Montreal, Qc, CANADA
*                                                  05/2019
*
* File : exo5.c
*
*********************************************************************************************************
*/

// Main include of µC-III
#include  <cpu.h>
#include  <lib_mem.h>
#include  <os.h>

#include  "os_app_hooks.h"
#include  "app_cfg.h"
/*
*********************************************************************************************************
*                                              CONSTANTS
*********************************************************************************************************
*/

#define TASK_STK_SIZE       16384            // Size of each task's stacks (# of WORDs)

#define ROBOT_A_PRIO        13                // Defining Priority of each task
#define ROBOT_B_PRIO        15
#define CONTROLLER_PRIO     22
#define MUT_PREP_TIME_PRIO      6
#define MUT_CONTROLLER_PRIO 5

#define ROBOT_A_T2_PRIO        12
#define ROBOT_B_T2_PRIO       14

/*
*********************************************************************************************************
*                                             VARIABLES
*********************************************************************************************************
*/

CPU_STK           prepRobotAStk[TASK_STK_SIZE];  //Stack of each task
CPU_STK           prepRobotBStk[TASK_STK_SIZE];
CPU_STK           transportStk[TASK_STK_SIZE];
CPU_STK           controllerStk[TASK_STK_SIZE];

CPU_STK           prepRobotAStk_T2[TASK_STK_SIZE];  //Stack of each task
CPU_STK           prepRobotBStk_T2[TASK_STK_SIZE];


OS_TCB robotATCB;
OS_TCB robotBTCB;
OS_TCB controllerTCB;

OS_TCB robotATCB_T2;
OS_TCB robotBTCB_T2;

/*
*********************************************************************************************************
*                                           SHARED  VARIABLES
*********************************************************************************************************
*/
OS_SEM sem_robot_B_to_robot_A_T1;

OS_SEM sem_robot_B_to_robot_A_T2;

OS_MUTEX total_prep_time_mutex;;
OS_MUTEX controller_done_mutex;

OS_Q Q_controller_to_robot_A;
void* A_prep_time_msg[10];

OS_Q Q_robot_A_to_robot_B_T1;
void* B_T1_prep_time_msg[10];

OS_Q Q_robot_A_to_robot_B_T2;
void* B_T2_prep_time_msg[10];

volatile int total_prep_time = 0;
int controller_done = 0;
/*
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*/
void    robotA(void* data);
void    robotB(void* data);
void    controller(void* data);
void    errMsg(OS_ERR err, char* errMSg);
int     readCurrentTotalPrepTime();
void    writeCurrentTotalPrepTime(int qty);

/*
*********************************************************************************************************
*                                             STRUCTURES
*********************************************************************************************************
*/

typedef struct work_data {
	int work_data_a;
	int work_data_b;
} work_data;

/*
*********************************************************************************************************
*                                                  MAIN
*********************************************************************************************************
*/

void main(void)
{
	OS_ERR err;


	/* TODO : Initialiser, creer les taches et demarer */

	CPU_IntInit();

	Mem_Init();                                                 // Initialize Memory Managment Module                   
	CPU_IntDis();                                               // Disable all Interrupts                               
	CPU_Init();                                                 // Initialize the uC/CPU services                       

	OSInit(&err);
	if (err != OS_ERR_NONE) {
		while (1);
	}

	App_OS_SetAllHooks();                                       // Set all applications hooks                      

	//Creating Mutexes
	OSMutexCreate(&total_prep_time_mutex, "mutex", &err);
	errMsg(err, "error while creating Mutex total_prep_time_mutex");

	OSMutexCreate(&controller_done_mutex, "mutex", &err);
	errMsg(err, "error while creating Mutex controller_done_mutex");

	//Creating Queues
	OSQCreate(&Q_controller_to_robot_A, "Q_controller_to_robot_A", sizeof(work_data), &err);
	errMsg(err, "error while creating the queue Q_controller_to_robot_A");

	OSQCreate(&Q_robot_A_to_robot_B_T1, "Q_robot_A_to_robot_B_T1", sizeof(work_data), &err);
	errMsg(err, "error while creating the queue Q_robot_A_to_robot_B_T1");

	OSQCreate(&Q_robot_A_to_robot_B_T2, "Q_robot_A_to_robot_B_T2", sizeof(work_data), &err);
	errMsg(err, "error while creating the queue Q_robot_A_to_robot_B_T2");
	
	//Creating Sems
	OSSemCreate(&sem_robot_B_to_robot_A_T1, "sem_robot_B_to_robot_A_T1", 0, &err);
	errMsg(err, "error while creating sem_robot_B_to_robot_A_T1");

	OSSemCreate(&sem_robot_B_to_robot_A_T2, "sem_robot_B_to_robot_A_T2", 0, &err);
	errMsg(err, "error while creating sem_robot_B_to_robot_A_T2");

	//Creating Tasks
	OSTaskCreate(&controllerTCB, "controllerTCB", controller, 0, CONTROLLER_PRIO, &controllerStk[0], TASK_STK_SIZE / 2, TASK_STK_SIZE, 0, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);
	errMsg(err, "error while creating the task controllerTCB");

	OSTaskCreate(&robotATCB, "robotATCB", robotA, "T1", ROBOT_A_PRIO, &prepRobotAStk[0], TASK_STK_SIZE / 2, TASK_STK_SIZE, 0, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);
	errMsg(err, "error while creating the task robotATCB");

	OSTaskCreate(&robotBTCB, "robotBTCB", robotB, "T1", ROBOT_B_PRIO, &prepRobotBStk[0], TASK_STK_SIZE / 2, TASK_STK_SIZE, 0, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);
	errMsg(err, "error while creating the task robotBTCB");

	OSTaskCreate(&robotATCB_T2, "robotATCB_T2", robotA, "T2", ROBOT_A_T2_PRIO, &prepRobotAStk_T2[0], TASK_STK_SIZE / 2, TASK_STK_SIZE, 0, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);
	errMsg(err, "error while creating the task robotATCB_T2");

	OSTaskCreate(&robotBTCB_T2, "robotBTCB_T2", robotB, "T2", ROBOT_B_T2_PRIO, &prepRobotBStk_T2[0], TASK_STK_SIZE / 2, TASK_STK_SIZE, 0, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);
	errMsg(err, "error while creating the task robotBTCB_T2");

	OSStart(&err);
	errMsg(err, "error while starting the program");

	while (DEF_ON); // Vous ne devriez pas rentrer dans cette boucle

	return;
}

/*
*********************************************************************************************************
*                                            TASK FUNCTIONS
*********************************************************************************************************
*/

void doWorkRobotA(work_data workData, int orderNumber, int startTime, char* pData)
{
	OS_ERR err;
	CPU_TS ts;
	//Avertir Robot B
	/*TODO : Envoi de message*/
	if (pData == "T1") {

		OSSemPost(&sem_robot_B_to_robot_A_T1, OS_OPT_POST_1, &err);
		errMsg(err, "error Post Sem : sem_robot_B_to_robot_A_T1");

		OSQPost(&Q_robot_A_to_robot_B_T1, &workData.work_data_b, sizeof(int), OS_OPT_POST_FIFO, &err);
		errMsg(err, "error Post Queue : Q_robot_A_to_robot_B_T1");

		/* TODO : Proteger ce bloc avec un mutex*/
		OSMutexPend(&total_prep_time_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
		errMsg(err, "error Pend Mutex : total_prep_time_mutex");

		//On met à jour le temps consacré à la préparation

		int currentTotalPrepTime = readCurrentTotalPrepTime();
		writeCurrentTotalPrepTime(currentTotalPrepTime + workData.work_data_a);


		printf("%d: TACHE PREP_ROBOT_A COMMANDE #%d @ %d : Debut preparation robot A_%s.\n", OSPrioCur, workData.work_data_a, OSTimeGet(&err) - startTime, pData);
		errMsg(err, "Error print");

		/* TODO : Faites un delai d'une duree de work_data_a */
		OSTimeDlyHMSM(0, 0, 0, workData.work_data_a, OS_OPT_TIME_HMSM_STRICT, &err);
		errMsg(err, "error delay");

		orderNumber++;

		OSMutexPost(&total_prep_time_mutex, OS_OPT_POST_NONE, &err);
		errMsg(err, "error Post Mutex : total_prep_time_mutex");
	}
	else {

		OSSemPost(&sem_robot_B_to_robot_A_T2, OS_OPT_POST_1, &err);
		errMsg(err, "error Post Sem : sem_robot_B_to_robot_A_T2");

		OSQPost(&Q_robot_A_to_robot_B_T2, &workData.work_data_b, sizeof(int), OS_OPT_POST_FIFO, &err);
		errMsg(err, "error Post Queue : Q_robot_A_to_robot_B_T2");

		/* TODO : Proteger ce bloc avec un mutex*/
		OSMutexPend(&total_prep_time_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
		errMsg(err, "error Pend Mutex : total_prep_time_mutex");

		//On met à jour le temps consacré à la préparation

		int currentTotalPrepTime = readCurrentTotalPrepTime();
		writeCurrentTotalPrepTime(currentTotalPrepTime + workData.work_data_a);


		printf("%d: TACHE PREP_ROBOT_A COMMANDE #%d @ %d : Debut preparation robot A_%s.\n", OSPrioCur, workData.work_data_a, OSTimeGet(&err) - startTime, pData);
		errMsg(err, "Error print");

		/* TODO : Faites un delai d'une duree de work_data_a */
		OSTimeDlyHMSM(0, 0, 0, workData.work_data_a, OS_OPT_TIME_HMSM_STRICT, &err);
		errMsg(err, "error delay");

		orderNumber++;

		OSMutexPost(&total_prep_time_mutex, OS_OPT_POST_NONE, &err);
		errMsg(err, "error Post Mutex : total_prep_time_mutex");

	}
}


void robotA(void* data)
{
	OS_ERR err;
	CPU_TS ts;
	int startTime = 0;
	int orderNumber = 1;
	work_data* workData;
	OS_MSG_SIZE msg_size;
	printf("%d : TACHE PREP_ROBOT_A @ %d : DEBUT. \n", OSPrioCur, OSTimeGet(&err) - startTime);
	errMsg(err, "Error print");

	char* pData = (char*)data;

	while (1)
	{
		startTime = OSTimeGet(&err);
		errMsg(err, "error OSTimeGet");

		workData = (work_data*)malloc(sizeof(work_data));

		//Acquiert mutex protection variable controller_done
		OSMutexPend(&controller_done_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
		errMsg(err, "error Post Mutex : controller_done_mutex");

		/* TODO : Recuperer la quantite*/
		workData = (work_data*)OSQPend(&Q_controller_to_robot_A, 0, OS_OPT_PEND_BLOCKING, &msg_size, &ts, &err);
		errMsg(err, "error Pend Queue : Q_controller_to_robot_A");

		doWorkRobotA(*workData, orderNumber, startTime, pData);

		/* TODO : Liberer la memoire*/
		free(workData);

		orderNumber++;

		OSMutexPost(&controller_done_mutex, OS_OPT_POST_NONE, &err);
		errMsg(err, "error Post Mutex : controller_done_mutex");

	}
}

void robotB(void* data)
{
	OS_ERR err;
	CPU_TS ts;
	OS_MSG_SIZE msg_size;

	work_data* workData;

	int startTime = 0;
	int orderNumber = 1;
	int* robotBPrepTime;

	printf("%d : TACHE PREP_ROBOT_B @ %d : DEBUT. \n", OSPrioCur, OSTimeGet(&err) - startTime);
	errMsg(err, "Error print");
	char* pData = (char*)data;

	while (1)
	{
		startTime = OSTimeGet(&err);
		errMsg(err, "error OSTimeGet");

		if (pData == "T1") {
			/* TODO : Recuperer la quantite */
			workData = OSQPend(&Q_robot_A_to_robot_B_T1, 0, OS_OPT_PEND_BLOCKING, &msg_size, &ts, &err);
			errMsg(err, "error Pend Queue : Q_robot_A_to_robot_B_T1");
			
			robotBPrepTime = &workData->work_data_b;

			/* TODO : On met à jour le temps consacré à la préparation */

			printf("%d: TACHE PREP_ROBOT_B COMMANDE #%d @ %d : Debut preparation robot B_%s.\n", OSPrioCur, *robotBPrepTime, OSTimeGet(&err) - startTime, pData);
			errMsg(err, "Error print");


			/* TODO : Faite un delqi d'une duree de robotBPrepTime*/
			OSTimeDlyHMSM(0, 0, 0, *robotBPrepTime, OS_OPT_TIME_HMSM_STRICT, &err);
			errMsg(err, "error delay");

			/* TODO : Gerer la synchronisation*/

			startTime += *robotBPrepTime;

			orderNumber++;

			OSSemPend(&sem_robot_B_to_robot_A_T1, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
			errMsg(err, "error Pend Sem : sem_robot_B_to_robot_A_T1");

		}
		else {
			/* TODO : Recuperer la quantite */
			workData = OSQPend(&Q_robot_A_to_robot_B_T2, 0, OS_OPT_PEND_BLOCKING, &msg_size, &ts, &err);
			errMsg(err, "error Pend Queue : Q_robot_A_to_robot_B_T2");


			robotBPrepTime = &workData->work_data_b;

			/* TODO : On met à jour le temps consacré à la préparation */

			printf("%d: TACHE PREP_ROBOT_B COMMANDE #%d @ %d : Debut preparation robot B_%s.\n", OSPrioCur, *robotBPrepTime, OSTimeGet(&err) - startTime, pData);
			errMsg(err, "Error print");


			/* TODO : Faite un delqi d'une duree de robotBPrepTime*/
			OSTimeDlyHMSM(0, 0, 0, *robotBPrepTime, OS_OPT_TIME_HMSM_STRICT, &err);
			errMsg(err, "error delay");

			/* TODO : Gerer la synchronisation*/

			startTime += *robotBPrepTime;

			orderNumber++;

			OSSemPend(&sem_robot_B_to_robot_A_T2, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
			errMsg(err, "error Pend Sem : sem_robot_B_to_robot_A_T2");

		}
	}
}

void controller(void* data)
{
	OS_ERR err;
	CPU_TS ts;

	int startTime = 0;
	int randomTime = 0;
	work_data* workData;
	printf("TACHE CONTROLLER @ %d : DEBUT. \n", OSTimeGet(&err) - startTime);
	errMsg(err, "Error print");

	while (1)
	{
		for (int i = 1; i < 12; i++)
		{
			/* TODO : Création d'une commande */

			workData = (work_data*)malloc(sizeof(work_data));
			workData->work_data_a = (rand() % 9 + 5) * 10;
			workData->work_data_b = (rand() % 9 + 5) * 10;

			printf("TACHE CONTROLLER @ %d : COMMANDE #%d. \n prep time A = %d, prep time B = %d\n", OSTimeGet(&err) - startTime, i, workData->work_data_a, workData->work_data_b);
			errMsg(err, "Error print");


			/* TODO :  Envoi de la commande aux différentes tâches */
			OSQPost(&Q_controller_to_robot_A, workData, sizeof(work_data), OS_OPT_POST_FIFO, &err);
			errMsg(err, "error Post Sem : Q_controller_to_robot_A");


			/* TODO :  Délai aléatoire avant nouvelle commande */
			randomTime = (rand() % 9 + 5) * 10;
			OSTimeDlyHMSM(0, 0, 0, randomTime, OS_OPT_TIME_HMSM_STRICT, &err);
			errMsg(err, "error delay");

		}


		/* TODO : Protection de ce bloc avec un mutex.*/
		OSMutexPend(&controller_done_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
		errMsg(err, "error Pend Mutex: controller_done_mutex");

		controller_done = 1;
		printf("FIN DE L'ENVOI DE COMMANDE @ %d\n", OSTimeGet(&err) - startTime);
		errMsg(err, "Error print");


		OSTaskSuspend(&controllerTCB, &err);
		OSMutexPost(&controller_done_mutex, OS_OPT_POST_NONE, &err);
		errMsg(err, "error Post Mutex: controller_done_mutex");

	}
}

int readCurrentTotalPrepTime()
{
	OS_ERR err;

	/*TODO : Effectuer un delai d'une duree de 2 ticks*/
	OSTimeDly(2, OS_OPT_TIME_DLY, &err);
	errMsg(err, "error delay");
	return total_prep_time;
}

void writeCurrentTotalPrepTime(int newCount)
{
	OS_ERR err;

	/*TODO : Effectuer un delai d'une duree de 2 ticks*/
	OSTimeDly(2, OS_OPT_TIME_DLY, &err);
	errMsg(err, "error delay");
	total_prep_time = newCount;
}

void errMsg(OS_ERR err, char* errMsg)
{
	if (err != OS_ERR_NONE)
	{
		printf(errMsg);
		exit(1);
	}
}