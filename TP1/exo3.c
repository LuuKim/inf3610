﻿///*
//*********************************************************************************************************
//*                                                 uC/OS-III
//*                                          The Real-Time Kernel
//*                                               PORT Windows
//*
//*
//*												Benjamin Heinen
//*                                  Ecole Polytechnique de Montreal, Qc, CANADA
//*                                                  05/2019
//*
//* File : exo3.c
//*
//*********************************************************************************************************
//*/
//
//// Main include of �C-III
//#include  <cpu.h>
//#include  <lib_mem.h>
//#include  <os.h>
//
//#include  "os_app_hooks.h"
//#include  "app_cfg.h"
///*
//*********************************************************************************************************
//*                                              CONSTANTS
//*********************************************************************************************************
//*/
//
//#define TASK_STK_SIZE       16384            // Size of each task's stacks (# of WORDs)
//
//#define ROBOT_A_PRIO        10               // Defining Priority of each task
//#define ROBOT_B_PRIO        9
//#define CONTROLLER_PRIO     7
//#define TRANSPORT_PRIO      8
//#define QTY_MUT_PRIO        6
//#define PEND_MUT_PRIO       5
//
//typedef enum {
//	ROBOT_A_DONE = 1 << 0,
//	ROBOT_B_DONE = 1 << 1,
//	COMMAND_READY = 1 << 2,
//	ROBOT_A_READY = 1 << 3,  //Ready to receive new command
//	ROBOT_B_READY = 1 << 4
//} EV_FLAGS;
//
///*
//*********************************************************************************************************
//*                                             VARIABLES
//*********************************************************************************************************
//*/
//
//CPU_STK           robotAStk[TASK_STK_SIZE];  //Stack of each task
//CPU_STK           robotBStk[TASK_STK_SIZE];
//CPU_STK           controllerStk[TASK_STK_SIZE];
//
//
//OS_TCB robotATCB;
//OS_TCB robotBTCB;
//OS_TCB controllerTCB;
//
///*
//*********************************************************************************************************
//*                                           SHARED  VARIABLES
//*********************************************************************************************************
//*/
//
//OS_MUTEX item_count_mutex;
//OS_MUTEX pending_mutex;
//
//OS_FLAG_GRP flags;
//
//volatile int total_item_count = 0;
//volatile int waiting_list = 0;
//
//
///*
//*********************************************************************************************************
//*                                         FUNCTION PROTOTYPES
//*********************************************************************************************************
//*/
//void    prep_robot_A(void* data);
//void    prep_robot_B(void* data);
//void    controller(void* data);
//void    errMsg(OS_ERR err, char* errMSg);
//int     readCurrentTotalItemCount();
//void    writeCurrentTotalItemCount(int qty);
//
///*
//*********************************************************************************************************
//*                                                  MAIN
//*********************************************************************************************************
//*/
//
//
//void main(void)
//{
//	OS_ERR err;
//	/* TODO : Initialiser, creer les taches et demarer */
//
//	CPU_IntInit();
//
//	Mem_Init();                                                 // Initialize Memory Managment Module                   
//	CPU_IntDis();                                               // Disable all Interrupts                               
//	CPU_Init();                                                 // Initialize the uC/CPU services                       
//
//	OSInit(&err);
//	if (err != OS_ERR_NONE) {
//		while (1);
//	}
//
//	App_OS_SetAllHooks();                                       // Set all applications hooks                      
//
//	OSFlagCreate(&flags, "flag", (OS_FLAGS)ROBOT_A_READY, &err);
//	errMsg(err, "error while creating Flag flag");
//	
//	OSMutexCreate(&pending_mutex, "mutex", &err);
//	errMsg(err, "error while creating Mutex mutex");
//
//	OSTaskCreate(&controllerTCB, "controllerTCB", controller, 0, CONTROLLER_PRIO, &controllerStk[0], TASK_STK_SIZE / 2, TASK_STK_SIZE, 0, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);
//	errMsg(err, "error while creating the task controllerTCB");
//
//	OSTaskCreate(&robotATCB, "robotATCB", prep_robot_A, 0, ROBOT_A_PRIO, &robotAStk[0], TASK_STK_SIZE / 2, TASK_STK_SIZE, 0, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);
//	errMsg(err, "error while creating the task robotATCB");
//
//	OSTaskCreate(&robotBTCB, "robotBTCB", prep_robot_B, 0, ROBOT_B_PRIO, &robotBStk[0], TASK_STK_SIZE / 2, TASK_STK_SIZE, 0, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);
//	errMsg(err, "error while creating the task robotBTCB");
//	
//	OSStart(&err);
//	errMsg(err, "error while starting the program");
//	
//	while (DEF_ON); // Vous ne devriez pas rentrer dans cette boucle
//
//	return;
//}
//
//
//
///*
//*********************************************************************************************************
//*                                            TASK FUNCTIONS
//*********************************************************************************************************
//*/
//
//void prep_robot_A(void* data)
//{
//	OS_ERR err;
//	CPU_TS ts;
//	int startTime = 0;
//	int orderNumber = 1;
//	int itemCount;
//	printf("ROBOT A @ %d : DEBUT.\n", OSTimeGet(&err) - startTime);
//	errMsg(err, "Error print");
//
//	OSFlagPost(&flags, ROBOT_A_READY, OS_OPT_POST_FLAG_SET, &err);
//
//	while (DEF_ON)
//	{
//		/* TODO : Remplisser la boucle afin de permettre l'utilisation des flags. N'oubliez pas de proteger vos donnees*/
//		OSFlagPend(&flags, ROBOT_A_READY + COMMAND_READY, 0, OS_OPT_PEND_BLOCKING + OS_OPT_PEND_FLAG_SET_ALL, &ts, &err);
//		errMsg(err, "error FlagPend");
//
//		OSFlagPost(&flags, COMMAND_READY, OS_OPT_POST_FLAG_CLR, &err);
//		errMsg(err, "error FlagPost");
//
//		OSFlagPost(&flags, ROBOT_A_READY, OS_OPT_POST_FLAG_CLR, &err);
//		errMsg(err, "error FlagPost");
//
//		OSMutexPend(&pending_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
//		errMsg(err, "error Pend Mutex: pending_mutex");
//		
//		itemCount = (rand() % 7 + 1) * 10;
//
//		//On met � jour le temps consacr� � la pr�paration
//		int current = readCurrentTotalItemCount();
//		writeCurrentTotalItemCount(current + itemCount);
//
//		int counter = 0;
//		while (counter < itemCount * 1000) { counter++; }
//		printf("ROBOT A COMMANDE #%d avec %d items @ %d.\n", orderNumber, itemCount, OSTimeGet(&err) - startTime);
//		errMsg(err, "Error print");
//
//
//		--waiting_list;
//		if (waiting_list == 0) {
//		}
//		OSMutexPost(&pending_mutex, OS_OPT_POST_NONE, &err);
//		errMsg(err, "error Post Mutex: pending_mutex");
//
//		OSFlagPost(&flags, ROBOT_A_READY, OS_OPT_POST_FLAG_SET, &err);
//		errMsg(err, "error FlagPend");
//
//
//		orderNumber++;
//	}
//}
//
//void prep_robot_B(void* data)
//{
//	OS_ERR err;
//	CPU_TS ts;
//
//	int startTime = 0;
//	int orderNumber = 1;
//	int itemCount;
//	printf("ROBOT B @ %d : DEBUT. \n", OSTimeGet(&err) - startTime);
//	errMsg(err, "Error print");
//
//	OSFlagPost(&flags, ROBOT_B_READY, OS_OPT_POST_FLAG_SET, &err);
//
//	while (1)
//	{
//		itemCount = (rand() % 6 + 2) * 10;
//
//		/* TODO : Remplisser la boucle afin de permettre l'utilisation des flags. N'oubliez pas de proteger vos donnees*/
//		OSFlagPend(&flags, ROBOT_B_READY + COMMAND_READY, 0, OS_OPT_PEND_BLOCKING + OS_OPT_PEND_FLAG_SET_ALL, &ts, &err);
//		errMsg(err, "error FlagPend");
//
//		OSFlagPost(&flags, COMMAND_READY, OS_OPT_POST_FLAG_CLR, &err);
//		errMsg(err, "error FlagPost");
//
//		OSFlagPost(&flags, ROBOT_B_READY, OS_OPT_POST_FLAG_CLR, &err);
//		errMsg(err, "error FlagPost");
//			   
//		OSMutexPend(&pending_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
//		errMsg(err, "error Pend Mutex: pending_mutex");
//
//
//		//On met � jour le temps consacr� � la pr�paration
//		int current = readCurrentTotalItemCount();
//		writeCurrentTotalItemCount(current + itemCount);
//
//		int counter = 0;
//		while (counter < itemCount * 1000) { counter++; }
//		printf("ROBOT B COMMANDE #%d avec %d items @ %d.\n", orderNumber, itemCount, OSTimeGet(&err) - startTime);
//		errMsg(err, "Error print");
//
//
//		OSMutexPost(&pending_mutex, OS_OPT_POST_NONE, &err);
//		errMsg(err, "error Post Mutex: pending_mutex");
//		
//		orderNumber++;
//		OSFlagPost(&flags, ROBOT_B_READY, OS_OPT_POST_FLAG_SET, &err);
//		errMsg(err, "error FlagPost");
//
//
//	}
//}
//
//void controller(void* data)
//{
//	OS_ERR err;
//	CPU_TS ts;
//
//	int startTime = 0;
//	int randomTime = 0;
//	printf("CONTROLLER @ %d : DEBUT. \n", OSTimeGet(&err) - startTime);
//	errMsg(err, "Error print");
//
//	for (int i = 1; i < 11; i++)
//	{
//		OSFlagPost(&flags, COMMAND_READY, OS_OPT_POST_FLAG_SET, &err);
//		errMsg(err, "error FlagPost");
//		
//		randomTime = (rand() % 9 + 5) * 10;
//		/*TODO : Effectuer un delai d'une duree de randomTime*/
//		OSTimeDlyHMSM(0, 0, 0, randomTime, OS_OPT_TIME_HMSM_STRICT, &err);
//		errMsg(err, "error delay");
//
//
//		/* TODO : Remplisser la boucle afin de permettre l'utilisation des flags. N'oubliez pas de proteger vos donnees*/
//
//		printf("CONTROLLER @ %d : COMMANDE #%d. \n", OSTimeGet(&err) - startTime, i);
//		errMsg(err, "error print");
//
//
//		OSMutexPend(&pending_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
//		errMsg(err, "error Pend Mutex: pending_mutex");
//
//		++waiting_list;
//		OSMutexPost(&pending_mutex, OS_OPT_POST_NONE, &err);
//		errMsg(err, "error Post Mutex: pending_mutex");
//
//
//	}
//}
//
//int readCurrentTotalItemCount()
//{
//	OS_ERR err;
//
//	/*TODO : Effectuer un delai d'une duree de 2 ticks*/
//	OSTimeDly(2, OS_OPT_TIME_DLY, &err);
//	errMsg(err, "error delay");
//	
//	return total_item_count;
//}
//void writeCurrentTotalItemCount(int newCount)
//{
//	OS_ERR err;
//	/*TODO : Effectuer un delai d'une duree de 2 ticks*/
//	OSTimeDly(2, OS_OPT_TIME_DLY, &err);
//	errMsg(err, "error delay");
//	
//	total_item_count = newCount;
//}
//
//void errMsg(OS_ERR err, char* errMsg)
//{
//	if (err != OS_ERR_NONE)
//	{
//		printf(errMsg);
//		exit(1);
//	}
//}