#include "Filter.h"
#include <string.h>
#include <math.h>

#define ABS(x)          ((x>0)? x : -x)

typedef union {
	uint8_t pix[4];
	unsigned full;
} OneToFourPixels;


unsigned image[3][3];


uint8_t filter_operator(const int fullIndex, uint8_t * image)
{
#pragma HLS inline			// Inliner la fonction lui permet d'etre "copie-coller" lorsqu elle est appelle
							// et ainsi faciliter le pipelinage de la boucle principale
	/*la fonction peut avoir 3 signatures diff�rentes, selon vos diff�rentes modifications:
	 * uint8_t filter_operator(const int fullIndex, uint8_t * image)
	 * uint8_t filter_operator(const int fullIndex, uint8_t image[IMG_HEIGHT * IMG_WIDTH])
	 * uint8_t filter_operator(const int col, const int row, uint8_t image[IMG_HEIGHT][IMG_WIDTH])
	 *
	 * Les deux premieres sont assez equivalentes, mais la derniere permet d'acceder � l'image comme un
	 * tableau 2D. Par contre, un tableau 2D doit alors lui etre passe, ce qui n'est pas evident considerant
	 * que les entrees de la fonction filtrer() sont 1D. Cependant, si pour une raison ou une autre
	 * un buffer-cache intermediaire etait utilise, celui-ci pourrait etre 2D...
	 */
	return 0;
}


unsigned convert(uint8_t inter_pix)
{
	OneToFourPixels fourWide;
	for(int j = 0; j < 4; ++j) {
		fourWide.pix[j] = inter_pix;
	}
	return fourWide.full;

}

void simple_box_blur(uint8_t * pixel, unsigned * pixel_out) {
	double kernel[3][3] = {{1.0/9.0, 1.0/9.0, 1.0/9.0}, {1.0/9.0, 1.0/9.0, 1.0/9.0}, {1.0/9.0, 1.0/9.0, 1.0/9.0}};
	int translation = 1;
	for(int i = 0; i < IMG_WIDTH; i++) {
		for(int j = 0; j < IMG_HEIGHT; j++) {
			int R = 0;
			int shiftW = i - translation ;
			for(int k = 0; k <= 3; k++) {
				int shiftH = 0;
				for(int l = 0; l <= 3; l++) {
					R += kernel[k][l]*pixel[shiftW+shiftH*IMG_WIDTH];
					shiftW++;
				}
				shiftH++;
		}
			pixel_out[i+j*IMG_WIDTH] = convert((uint8_t) R);
		}
	}
}

void gaussian_blur(uint8_t * pixel, unsigned * pixel_out) {
	double kernel[7][7] = {{ 0, 0, 0, 5.0/1068.0, 0, 0, 0},
		{ 0, 5.0/1068.0, 18.0/1068.0, 32.0/1068.0, 18.0/1068.0, 5.0/1068.0, 0},
		{ 0, 18.0/1068.0, 64.0/1068.0, 100.0/1068.0, 64.0/1068.0, 18.0/1068.0, 0},
		{5.0/1068.0, 32.0/1068.0, 100.0/1068.0, 100.0/1068.0, 100.0/1068.0, 32.0/1068.0, 5.0/1068.0},
		{ 0, 18.0/1068.0,  64.0/1068.0, 100.0/1068.0,  64.0/1068.0, 18.0/1068.0, 0},
		{ 0,  5.0/1068.0,  18.0/1068.0, 32.0/1068.0, 18.0/1068.0, 5.0/1068.0, 0},
		{ 0, 0, 0, 5.0/1068.0, 0, 0, 0}};
	for(int i = 0; i < IMG_WIDTH; i++) {
		for(int j = 0; j < IMG_HEIGHT; j++) {

			int R = 0;
			int kernelW = 0;
			for(int k = i-3; k <= i+4; k++) {
				int kernelH = 0;
				for(int l = j-3; l <= j+4; l++) {
					R += kernel[kernelW][kernelH]*pixel[k+l*IMG_WIDTH];
					kernelH++;
				}
				kernelW++;
		}
			pixel_out[i+j*IMG_WIDTH] = convert((uint8_t) R);
	}
	}

}

 void sobel(uint8_t * pixel, unsigned * pixel_out) {
	double kernel1[3][3] = {{1, 2, 1}, {0, 0, 0}, {-1, -2, -1}};
	double kernel2[3][3] = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}};
		for(int i = 0; i < IMG_WIDTH; i++) {
			for(int j = 0; j < IMG_HEIGHT; j++) {
				int R = 0;
				for (int s = 0; s < 2; s++) {
					int acc = 0;
					int kernelW = 0;
					for(int k = i-1; k <= i+1; k++) {
						int kernelH = 0;
						for(int l = j-1; l <= j+1; l++) {
							if (s == 0) {
								acc += kernel1[kernelW][kernelH]*pixel[k+l*IMG_WIDTH];
							} else {
								acc += kernel2[kernelW][kernelH]*pixel[k+l*IMG_WIDTH];
							}
							kernelH++;
						}
						kernelW++;
					}
					R += abs(acc);
				}
				pixel_out[i+j*IMG_WIDTH] = convert((uint8_t) (255 - R));
			}
		}

}

void laplacian_operator(uint8_t * pixel, unsigned * pixel_out) {
	double kernel[3][3] = {{0, -1, 0}, {-1, 4, -1}, {0, -1, 0}};
	for(int i = 0; i < IMG_WIDTH; i++) {
		for(int j = 0; j < IMG_HEIGHT; j++) {

			int R = 0;
			int kernelW = 0;
			for(int k = i-1; k <= i+2; k++) {
				int kernelH = 0;
				for(int l = j-1; l <= j+2; l++) {
					R += kernel[kernelW][kernelH]*pixel[k+l*IMG_WIDTH];
					kernelH++;
				}
				kernelW++;
		}
			pixel_out[i+j*IMG_WIDTH] = convert((uint8_t) R);
	}
	}
}

void laplacien_of_gaussian(uint8_t * pixel, unsigned * pixel_out) {
	double kernel[5][5] = {{0, 0, -1, 0, 0}, {0, -1, -2, -1, 0}, {-1, -2, 16, -2, -1}, {0, -1, -2, -1, 0}, {0, 0, -1, 0, 0}};
		for(int i = 0; i < IMG_WIDTH; i++) {
			for(int j = 0; j < IMG_HEIGHT; j++) {

				int R = 0;
				int kernelW = 0;
				for(int k = i-2; k <= i+3; k++) {
					int kernelH = 0;
					for(int l = j-2; l <= j+3; l++) {
						R += kernel[kernelW][kernelH]*pixel[k+l*IMG_WIDTH];
						kernelH++;
					}
					kernelW++;
			}
				pixel_out[i+j*IMG_WIDTH] = convert((uint8_t) R);
		}
		}
}


