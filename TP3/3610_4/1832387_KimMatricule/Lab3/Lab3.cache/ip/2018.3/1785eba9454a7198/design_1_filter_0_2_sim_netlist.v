// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
// Date        : Sat Dec 14 19:47:04 2019
// Host        : L3712-09 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_filter_0_2_sim_netlist.v
// Design      : design_1_filter_0_2
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_filter_0_2,filter,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "HLS" *) 
(* x_core_info = "filter,Vivado 2018.3" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s_axi_AXILiteS_AWADDR,
    s_axi_AXILiteS_AWVALID,
    s_axi_AXILiteS_AWREADY,
    s_axi_AXILiteS_WDATA,
    s_axi_AXILiteS_WSTRB,
    s_axi_AXILiteS_WVALID,
    s_axi_AXILiteS_WREADY,
    s_axi_AXILiteS_BRESP,
    s_axi_AXILiteS_BVALID,
    s_axi_AXILiteS_BREADY,
    s_axi_AXILiteS_ARADDR,
    s_axi_AXILiteS_ARVALID,
    s_axi_AXILiteS_ARREADY,
    s_axi_AXILiteS_RDATA,
    s_axi_AXILiteS_RRESP,
    s_axi_AXILiteS_RVALID,
    s_axi_AXILiteS_RREADY,
    ap_clk,
    ap_rst_n,
    interrupt,
    m_axi_gmem0_AWADDR,
    m_axi_gmem0_AWLEN,
    m_axi_gmem0_AWSIZE,
    m_axi_gmem0_AWBURST,
    m_axi_gmem0_AWLOCK,
    m_axi_gmem0_AWREGION,
    m_axi_gmem0_AWCACHE,
    m_axi_gmem0_AWPROT,
    m_axi_gmem0_AWQOS,
    m_axi_gmem0_AWVALID,
    m_axi_gmem0_AWREADY,
    m_axi_gmem0_WDATA,
    m_axi_gmem0_WSTRB,
    m_axi_gmem0_WLAST,
    m_axi_gmem0_WVALID,
    m_axi_gmem0_WREADY,
    m_axi_gmem0_BRESP,
    m_axi_gmem0_BVALID,
    m_axi_gmem0_BREADY,
    m_axi_gmem0_ARADDR,
    m_axi_gmem0_ARLEN,
    m_axi_gmem0_ARSIZE,
    m_axi_gmem0_ARBURST,
    m_axi_gmem0_ARLOCK,
    m_axi_gmem0_ARREGION,
    m_axi_gmem0_ARCACHE,
    m_axi_gmem0_ARPROT,
    m_axi_gmem0_ARQOS,
    m_axi_gmem0_ARVALID,
    m_axi_gmem0_ARREADY,
    m_axi_gmem0_RDATA,
    m_axi_gmem0_RRESP,
    m_axi_gmem0_RLAST,
    m_axi_gmem0_RVALID,
    m_axi_gmem0_RREADY,
    m_axi_gmem1_AWADDR,
    m_axi_gmem1_AWLEN,
    m_axi_gmem1_AWSIZE,
    m_axi_gmem1_AWBURST,
    m_axi_gmem1_AWLOCK,
    m_axi_gmem1_AWREGION,
    m_axi_gmem1_AWCACHE,
    m_axi_gmem1_AWPROT,
    m_axi_gmem1_AWQOS,
    m_axi_gmem1_AWVALID,
    m_axi_gmem1_AWREADY,
    m_axi_gmem1_WDATA,
    m_axi_gmem1_WSTRB,
    m_axi_gmem1_WLAST,
    m_axi_gmem1_WVALID,
    m_axi_gmem1_WREADY,
    m_axi_gmem1_BRESP,
    m_axi_gmem1_BVALID,
    m_axi_gmem1_BREADY,
    m_axi_gmem1_ARADDR,
    m_axi_gmem1_ARLEN,
    m_axi_gmem1_ARSIZE,
    m_axi_gmem1_ARBURST,
    m_axi_gmem1_ARLOCK,
    m_axi_gmem1_ARREGION,
    m_axi_gmem1_ARCACHE,
    m_axi_gmem1_ARPROT,
    m_axi_gmem1_ARQOS,
    m_axi_gmem1_ARVALID,
    m_axi_gmem1_ARREADY,
    m_axi_gmem1_RDATA,
    m_axi_gmem1_RRESP,
    m_axi_gmem1_RLAST,
    m_axi_gmem1_RVALID,
    m_axi_gmem1_RREADY);
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_AXILiteS AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axi_AXILiteS, ADDR_WIDTH 5, DATA_WIDTH 32, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, ID_WIDTH 0, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK3, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [4:0]s_axi_AXILiteS_AWADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_AXILiteS AWVALID" *) input s_axi_AXILiteS_AWVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_AXILiteS AWREADY" *) output s_axi_AXILiteS_AWREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_AXILiteS WDATA" *) input [31:0]s_axi_AXILiteS_WDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_AXILiteS WSTRB" *) input [3:0]s_axi_AXILiteS_WSTRB;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_AXILiteS WVALID" *) input s_axi_AXILiteS_WVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_AXILiteS WREADY" *) output s_axi_AXILiteS_WREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_AXILiteS BRESP" *) output [1:0]s_axi_AXILiteS_BRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_AXILiteS BVALID" *) output s_axi_AXILiteS_BVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_AXILiteS BREADY" *) input s_axi_AXILiteS_BREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_AXILiteS ARADDR" *) input [4:0]s_axi_AXILiteS_ARADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_AXILiteS ARVALID" *) input s_axi_AXILiteS_ARVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_AXILiteS ARREADY" *) output s_axi_AXILiteS_ARREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_AXILiteS RDATA" *) output [31:0]s_axi_AXILiteS_RDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_AXILiteS RRESP" *) output [1:0]s_axi_AXILiteS_RRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_AXILiteS RVALID" *) output s_axi_AXILiteS_RVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_AXILiteS RREADY" *) input s_axi_AXILiteS_RREADY;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 ap_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axi_AXILiteS:m_axi_gmem0:m_axi_gmem1, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK3, INSERT_VIP 0" *) input ap_clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 ap_rst_n RST" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, INSERT_VIP 0" *) input ap_rst_n;
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 interrupt INTERRUPT" *) (* x_interface_parameter = "XIL_INTERFACENAME interrupt, SENSITIVITY LEVEL_HIGH, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {INTERRUPT {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, PortWidth 1" *) output interrupt;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME m_axi_gmem0, ADDR_WIDTH 32, MAX_BURST_LENGTH 256, NUM_READ_OUTSTANDING 16, NUM_WRITE_OUTSTANDING 16, MAX_READ_BURST_LENGTH 16, MAX_WRITE_BURST_LENGTH 16, PROTOCOL AXI4, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, FREQ_HZ 100000000, ID_WIDTH 0, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, HAS_LOCK 1, HAS_PROT 1, HAS_CACHE 1, HAS_QOS 1, HAS_REGION 1, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK3, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) output [31:0]m_axi_gmem0_AWADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 AWLEN" *) output [7:0]m_axi_gmem0_AWLEN;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 AWSIZE" *) output [2:0]m_axi_gmem0_AWSIZE;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 AWBURST" *) output [1:0]m_axi_gmem0_AWBURST;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 AWLOCK" *) output [1:0]m_axi_gmem0_AWLOCK;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 AWREGION" *) output [3:0]m_axi_gmem0_AWREGION;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 AWCACHE" *) output [3:0]m_axi_gmem0_AWCACHE;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 AWPROT" *) output [2:0]m_axi_gmem0_AWPROT;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 AWQOS" *) output [3:0]m_axi_gmem0_AWQOS;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 AWVALID" *) output m_axi_gmem0_AWVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 AWREADY" *) input m_axi_gmem0_AWREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 WDATA" *) output [31:0]m_axi_gmem0_WDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 WSTRB" *) output [3:0]m_axi_gmem0_WSTRB;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 WLAST" *) output m_axi_gmem0_WLAST;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 WVALID" *) output m_axi_gmem0_WVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 WREADY" *) input m_axi_gmem0_WREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 BRESP" *) input [1:0]m_axi_gmem0_BRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 BVALID" *) input m_axi_gmem0_BVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 BREADY" *) output m_axi_gmem0_BREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 ARADDR" *) output [31:0]m_axi_gmem0_ARADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 ARLEN" *) output [7:0]m_axi_gmem0_ARLEN;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 ARSIZE" *) output [2:0]m_axi_gmem0_ARSIZE;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 ARBURST" *) output [1:0]m_axi_gmem0_ARBURST;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 ARLOCK" *) output [1:0]m_axi_gmem0_ARLOCK;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 ARREGION" *) output [3:0]m_axi_gmem0_ARREGION;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 ARCACHE" *) output [3:0]m_axi_gmem0_ARCACHE;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 ARPROT" *) output [2:0]m_axi_gmem0_ARPROT;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 ARQOS" *) output [3:0]m_axi_gmem0_ARQOS;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 ARVALID" *) output m_axi_gmem0_ARVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 ARREADY" *) input m_axi_gmem0_ARREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 RDATA" *) input [31:0]m_axi_gmem0_RDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 RRESP" *) input [1:0]m_axi_gmem0_RRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 RLAST" *) input m_axi_gmem0_RLAST;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 RVALID" *) input m_axi_gmem0_RVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem0 RREADY" *) output m_axi_gmem0_RREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME m_axi_gmem1, ADDR_WIDTH 32, MAX_BURST_LENGTH 256, NUM_READ_OUTSTANDING 16, NUM_WRITE_OUTSTANDING 16, MAX_READ_BURST_LENGTH 16, MAX_WRITE_BURST_LENGTH 16, PROTOCOL AXI4, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, FREQ_HZ 100000000, ID_WIDTH 0, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, HAS_LOCK 1, HAS_PROT 1, HAS_CACHE 1, HAS_QOS 1, HAS_REGION 1, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK3, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) output [31:0]m_axi_gmem1_AWADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 AWLEN" *) output [7:0]m_axi_gmem1_AWLEN;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 AWSIZE" *) output [2:0]m_axi_gmem1_AWSIZE;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 AWBURST" *) output [1:0]m_axi_gmem1_AWBURST;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 AWLOCK" *) output [1:0]m_axi_gmem1_AWLOCK;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 AWREGION" *) output [3:0]m_axi_gmem1_AWREGION;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 AWCACHE" *) output [3:0]m_axi_gmem1_AWCACHE;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 AWPROT" *) output [2:0]m_axi_gmem1_AWPROT;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 AWQOS" *) output [3:0]m_axi_gmem1_AWQOS;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 AWVALID" *) output m_axi_gmem1_AWVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 AWREADY" *) input m_axi_gmem1_AWREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 WDATA" *) output [31:0]m_axi_gmem1_WDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 WSTRB" *) output [3:0]m_axi_gmem1_WSTRB;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 WLAST" *) output m_axi_gmem1_WLAST;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 WVALID" *) output m_axi_gmem1_WVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 WREADY" *) input m_axi_gmem1_WREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 BRESP" *) input [1:0]m_axi_gmem1_BRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 BVALID" *) input m_axi_gmem1_BVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 BREADY" *) output m_axi_gmem1_BREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 ARADDR" *) output [31:0]m_axi_gmem1_ARADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 ARLEN" *) output [7:0]m_axi_gmem1_ARLEN;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 ARSIZE" *) output [2:0]m_axi_gmem1_ARSIZE;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 ARBURST" *) output [1:0]m_axi_gmem1_ARBURST;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 ARLOCK" *) output [1:0]m_axi_gmem1_ARLOCK;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 ARREGION" *) output [3:0]m_axi_gmem1_ARREGION;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 ARCACHE" *) output [3:0]m_axi_gmem1_ARCACHE;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 ARPROT" *) output [2:0]m_axi_gmem1_ARPROT;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 ARQOS" *) output [3:0]m_axi_gmem1_ARQOS;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 ARVALID" *) output m_axi_gmem1_ARVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 ARREADY" *) input m_axi_gmem1_ARREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 RDATA" *) input [31:0]m_axi_gmem1_RDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 RRESP" *) input [1:0]m_axi_gmem1_RRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 RLAST" *) input m_axi_gmem1_RLAST;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 RVALID" *) input m_axi_gmem1_RVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m_axi_gmem1 RREADY" *) output m_axi_gmem1_RREADY;

  wire ap_clk;
  wire ap_rst_n;
  wire interrupt;
  wire [31:0]m_axi_gmem0_ARADDR;
  wire [1:0]m_axi_gmem0_ARBURST;
  wire [3:0]m_axi_gmem0_ARCACHE;
  wire [7:0]m_axi_gmem0_ARLEN;
  wire [1:0]m_axi_gmem0_ARLOCK;
  wire [2:0]m_axi_gmem0_ARPROT;
  wire [3:0]m_axi_gmem0_ARQOS;
  wire m_axi_gmem0_ARREADY;
  wire [3:0]m_axi_gmem0_ARREGION;
  wire [2:0]m_axi_gmem0_ARSIZE;
  wire m_axi_gmem0_ARVALID;
  wire [31:0]m_axi_gmem0_AWADDR;
  wire [1:0]m_axi_gmem0_AWBURST;
  wire [3:0]m_axi_gmem0_AWCACHE;
  wire [7:0]m_axi_gmem0_AWLEN;
  wire [1:0]m_axi_gmem0_AWLOCK;
  wire [2:0]m_axi_gmem0_AWPROT;
  wire [3:0]m_axi_gmem0_AWQOS;
  wire m_axi_gmem0_AWREADY;
  wire [3:0]m_axi_gmem0_AWREGION;
  wire [2:0]m_axi_gmem0_AWSIZE;
  wire m_axi_gmem0_AWVALID;
  wire m_axi_gmem0_BREADY;
  wire [1:0]m_axi_gmem0_BRESP;
  wire m_axi_gmem0_BVALID;
  wire [31:0]m_axi_gmem0_RDATA;
  wire m_axi_gmem0_RLAST;
  wire m_axi_gmem0_RREADY;
  wire [1:0]m_axi_gmem0_RRESP;
  wire m_axi_gmem0_RVALID;
  wire [31:0]m_axi_gmem0_WDATA;
  wire m_axi_gmem0_WLAST;
  wire m_axi_gmem0_WREADY;
  wire [3:0]m_axi_gmem0_WSTRB;
  wire m_axi_gmem0_WVALID;
  wire [31:0]m_axi_gmem1_ARADDR;
  wire [1:0]m_axi_gmem1_ARBURST;
  wire [3:0]m_axi_gmem1_ARCACHE;
  wire [7:0]m_axi_gmem1_ARLEN;
  wire [1:0]m_axi_gmem1_ARLOCK;
  wire [2:0]m_axi_gmem1_ARPROT;
  wire [3:0]m_axi_gmem1_ARQOS;
  wire m_axi_gmem1_ARREADY;
  wire [3:0]m_axi_gmem1_ARREGION;
  wire [2:0]m_axi_gmem1_ARSIZE;
  wire m_axi_gmem1_ARVALID;
  wire [31:0]m_axi_gmem1_AWADDR;
  wire [1:0]m_axi_gmem1_AWBURST;
  wire [3:0]m_axi_gmem1_AWCACHE;
  wire [7:0]m_axi_gmem1_AWLEN;
  wire [1:0]m_axi_gmem1_AWLOCK;
  wire [2:0]m_axi_gmem1_AWPROT;
  wire [3:0]m_axi_gmem1_AWQOS;
  wire m_axi_gmem1_AWREADY;
  wire [3:0]m_axi_gmem1_AWREGION;
  wire [2:0]m_axi_gmem1_AWSIZE;
  wire m_axi_gmem1_AWVALID;
  wire m_axi_gmem1_BREADY;
  wire [1:0]m_axi_gmem1_BRESP;
  wire m_axi_gmem1_BVALID;
  wire [31:0]m_axi_gmem1_RDATA;
  wire m_axi_gmem1_RLAST;
  wire m_axi_gmem1_RREADY;
  wire [1:0]m_axi_gmem1_RRESP;
  wire m_axi_gmem1_RVALID;
  wire [31:0]m_axi_gmem1_WDATA;
  wire m_axi_gmem1_WLAST;
  wire m_axi_gmem1_WREADY;
  wire [3:0]m_axi_gmem1_WSTRB;
  wire m_axi_gmem1_WVALID;
  wire [4:0]s_axi_AXILiteS_ARADDR;
  wire s_axi_AXILiteS_ARREADY;
  wire s_axi_AXILiteS_ARVALID;
  wire [4:0]s_axi_AXILiteS_AWADDR;
  wire s_axi_AXILiteS_AWREADY;
  wire s_axi_AXILiteS_AWVALID;
  wire s_axi_AXILiteS_BREADY;
  wire [1:0]s_axi_AXILiteS_BRESP;
  wire s_axi_AXILiteS_BVALID;
  wire [31:0]s_axi_AXILiteS_RDATA;
  wire s_axi_AXILiteS_RREADY;
  wire [1:0]s_axi_AXILiteS_RRESP;
  wire s_axi_AXILiteS_RVALID;
  wire [31:0]s_axi_AXILiteS_WDATA;
  wire s_axi_AXILiteS_WREADY;
  wire [3:0]s_axi_AXILiteS_WSTRB;
  wire s_axi_AXILiteS_WVALID;
  wire [0:0]NLW_U0_m_axi_gmem0_ARID_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_gmem0_ARUSER_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_gmem0_AWID_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_gmem0_AWUSER_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_gmem0_WID_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_gmem0_WUSER_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_gmem1_ARID_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_gmem1_ARUSER_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_gmem1_AWID_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_gmem1_AWUSER_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_gmem1_WID_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_gmem1_WUSER_UNCONNECTED;

  (* C_M_AXI_GMEM0_ADDR_WIDTH = "32" *) 
  (* C_M_AXI_GMEM0_ARUSER_WIDTH = "1" *) 
  (* C_M_AXI_GMEM0_AWUSER_WIDTH = "1" *) 
  (* C_M_AXI_GMEM0_BUSER_WIDTH = "1" *) 
  (* C_M_AXI_GMEM0_CACHE_VALUE = "3" *) 
  (* C_M_AXI_GMEM0_DATA_WIDTH = "32" *) 
  (* C_M_AXI_GMEM0_ID_WIDTH = "1" *) 
  (* C_M_AXI_GMEM0_PROT_VALUE = "0" *) 
  (* C_M_AXI_GMEM0_RUSER_WIDTH = "1" *) 
  (* C_M_AXI_GMEM0_USER_VALUE = "0" *) 
  (* C_M_AXI_GMEM0_WUSER_WIDTH = "1" *) 
  (* C_M_AXI_GMEM1_ADDR_WIDTH = "32" *) 
  (* C_M_AXI_GMEM1_ARUSER_WIDTH = "1" *) 
  (* C_M_AXI_GMEM1_AWUSER_WIDTH = "1" *) 
  (* C_M_AXI_GMEM1_BUSER_WIDTH = "1" *) 
  (* C_M_AXI_GMEM1_CACHE_VALUE = "3" *) 
  (* C_M_AXI_GMEM1_DATA_WIDTH = "32" *) 
  (* C_M_AXI_GMEM1_ID_WIDTH = "1" *) 
  (* C_M_AXI_GMEM1_PROT_VALUE = "0" *) 
  (* C_M_AXI_GMEM1_RUSER_WIDTH = "1" *) 
  (* C_M_AXI_GMEM1_USER_VALUE = "0" *) 
  (* C_M_AXI_GMEM1_WUSER_WIDTH = "1" *) 
  (* C_S_AXI_AXILITES_ADDR_WIDTH = "5" *) 
  (* C_S_AXI_AXILITES_DATA_WIDTH = "32" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter U0
       (.ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .interrupt(interrupt),
        .m_axi_gmem0_ARADDR(m_axi_gmem0_ARADDR),
        .m_axi_gmem0_ARBURST(m_axi_gmem0_ARBURST),
        .m_axi_gmem0_ARCACHE(m_axi_gmem0_ARCACHE),
        .m_axi_gmem0_ARID(NLW_U0_m_axi_gmem0_ARID_UNCONNECTED[0]),
        .m_axi_gmem0_ARLEN(m_axi_gmem0_ARLEN),
        .m_axi_gmem0_ARLOCK(m_axi_gmem0_ARLOCK),
        .m_axi_gmem0_ARPROT(m_axi_gmem0_ARPROT),
        .m_axi_gmem0_ARQOS(m_axi_gmem0_ARQOS),
        .m_axi_gmem0_ARREADY(m_axi_gmem0_ARREADY),
        .m_axi_gmem0_ARREGION(m_axi_gmem0_ARREGION),
        .m_axi_gmem0_ARSIZE(m_axi_gmem0_ARSIZE),
        .m_axi_gmem0_ARUSER(NLW_U0_m_axi_gmem0_ARUSER_UNCONNECTED[0]),
        .m_axi_gmem0_ARVALID(m_axi_gmem0_ARVALID),
        .m_axi_gmem0_AWADDR(m_axi_gmem0_AWADDR),
        .m_axi_gmem0_AWBURST(m_axi_gmem0_AWBURST),
        .m_axi_gmem0_AWCACHE(m_axi_gmem0_AWCACHE),
        .m_axi_gmem0_AWID(NLW_U0_m_axi_gmem0_AWID_UNCONNECTED[0]),
        .m_axi_gmem0_AWLEN(m_axi_gmem0_AWLEN),
        .m_axi_gmem0_AWLOCK(m_axi_gmem0_AWLOCK),
        .m_axi_gmem0_AWPROT(m_axi_gmem0_AWPROT),
        .m_axi_gmem0_AWQOS(m_axi_gmem0_AWQOS),
        .m_axi_gmem0_AWREADY(m_axi_gmem0_AWREADY),
        .m_axi_gmem0_AWREGION(m_axi_gmem0_AWREGION),
        .m_axi_gmem0_AWSIZE(m_axi_gmem0_AWSIZE),
        .m_axi_gmem0_AWUSER(NLW_U0_m_axi_gmem0_AWUSER_UNCONNECTED[0]),
        .m_axi_gmem0_AWVALID(m_axi_gmem0_AWVALID),
        .m_axi_gmem0_BID(1'b0),
        .m_axi_gmem0_BREADY(m_axi_gmem0_BREADY),
        .m_axi_gmem0_BRESP(m_axi_gmem0_BRESP),
        .m_axi_gmem0_BUSER(1'b0),
        .m_axi_gmem0_BVALID(m_axi_gmem0_BVALID),
        .m_axi_gmem0_RDATA(m_axi_gmem0_RDATA),
        .m_axi_gmem0_RID(1'b0),
        .m_axi_gmem0_RLAST(m_axi_gmem0_RLAST),
        .m_axi_gmem0_RREADY(m_axi_gmem0_RREADY),
        .m_axi_gmem0_RRESP(m_axi_gmem0_RRESP),
        .m_axi_gmem0_RUSER(1'b0),
        .m_axi_gmem0_RVALID(m_axi_gmem0_RVALID),
        .m_axi_gmem0_WDATA(m_axi_gmem0_WDATA),
        .m_axi_gmem0_WID(NLW_U0_m_axi_gmem0_WID_UNCONNECTED[0]),
        .m_axi_gmem0_WLAST(m_axi_gmem0_WLAST),
        .m_axi_gmem0_WREADY(m_axi_gmem0_WREADY),
        .m_axi_gmem0_WSTRB(m_axi_gmem0_WSTRB),
        .m_axi_gmem0_WUSER(NLW_U0_m_axi_gmem0_WUSER_UNCONNECTED[0]),
        .m_axi_gmem0_WVALID(m_axi_gmem0_WVALID),
        .m_axi_gmem1_ARADDR(m_axi_gmem1_ARADDR),
        .m_axi_gmem1_ARBURST(m_axi_gmem1_ARBURST),
        .m_axi_gmem1_ARCACHE(m_axi_gmem1_ARCACHE),
        .m_axi_gmem1_ARID(NLW_U0_m_axi_gmem1_ARID_UNCONNECTED[0]),
        .m_axi_gmem1_ARLEN(m_axi_gmem1_ARLEN),
        .m_axi_gmem1_ARLOCK(m_axi_gmem1_ARLOCK),
        .m_axi_gmem1_ARPROT(m_axi_gmem1_ARPROT),
        .m_axi_gmem1_ARQOS(m_axi_gmem1_ARQOS),
        .m_axi_gmem1_ARREADY(m_axi_gmem1_ARREADY),
        .m_axi_gmem1_ARREGION(m_axi_gmem1_ARREGION),
        .m_axi_gmem1_ARSIZE(m_axi_gmem1_ARSIZE),
        .m_axi_gmem1_ARUSER(NLW_U0_m_axi_gmem1_ARUSER_UNCONNECTED[0]),
        .m_axi_gmem1_ARVALID(m_axi_gmem1_ARVALID),
        .m_axi_gmem1_AWADDR(m_axi_gmem1_AWADDR),
        .m_axi_gmem1_AWBURST(m_axi_gmem1_AWBURST),
        .m_axi_gmem1_AWCACHE(m_axi_gmem1_AWCACHE),
        .m_axi_gmem1_AWID(NLW_U0_m_axi_gmem1_AWID_UNCONNECTED[0]),
        .m_axi_gmem1_AWLEN(m_axi_gmem1_AWLEN),
        .m_axi_gmem1_AWLOCK(m_axi_gmem1_AWLOCK),
        .m_axi_gmem1_AWPROT(m_axi_gmem1_AWPROT),
        .m_axi_gmem1_AWQOS(m_axi_gmem1_AWQOS),
        .m_axi_gmem1_AWREADY(m_axi_gmem1_AWREADY),
        .m_axi_gmem1_AWREGION(m_axi_gmem1_AWREGION),
        .m_axi_gmem1_AWSIZE(m_axi_gmem1_AWSIZE),
        .m_axi_gmem1_AWUSER(NLW_U0_m_axi_gmem1_AWUSER_UNCONNECTED[0]),
        .m_axi_gmem1_AWVALID(m_axi_gmem1_AWVALID),
        .m_axi_gmem1_BID(1'b0),
        .m_axi_gmem1_BREADY(m_axi_gmem1_BREADY),
        .m_axi_gmem1_BRESP(m_axi_gmem1_BRESP),
        .m_axi_gmem1_BUSER(1'b0),
        .m_axi_gmem1_BVALID(m_axi_gmem1_BVALID),
        .m_axi_gmem1_RDATA(m_axi_gmem1_RDATA),
        .m_axi_gmem1_RID(1'b0),
        .m_axi_gmem1_RLAST(m_axi_gmem1_RLAST),
        .m_axi_gmem1_RREADY(m_axi_gmem1_RREADY),
        .m_axi_gmem1_RRESP(m_axi_gmem1_RRESP),
        .m_axi_gmem1_RUSER(1'b0),
        .m_axi_gmem1_RVALID(m_axi_gmem1_RVALID),
        .m_axi_gmem1_WDATA(m_axi_gmem1_WDATA),
        .m_axi_gmem1_WID(NLW_U0_m_axi_gmem1_WID_UNCONNECTED[0]),
        .m_axi_gmem1_WLAST(m_axi_gmem1_WLAST),
        .m_axi_gmem1_WREADY(m_axi_gmem1_WREADY),
        .m_axi_gmem1_WSTRB(m_axi_gmem1_WSTRB),
        .m_axi_gmem1_WUSER(NLW_U0_m_axi_gmem1_WUSER_UNCONNECTED[0]),
        .m_axi_gmem1_WVALID(m_axi_gmem1_WVALID),
        .s_axi_AXILiteS_ARADDR(s_axi_AXILiteS_ARADDR),
        .s_axi_AXILiteS_ARREADY(s_axi_AXILiteS_ARREADY),
        .s_axi_AXILiteS_ARVALID(s_axi_AXILiteS_ARVALID),
        .s_axi_AXILiteS_AWADDR(s_axi_AXILiteS_AWADDR),
        .s_axi_AXILiteS_AWREADY(s_axi_AXILiteS_AWREADY),
        .s_axi_AXILiteS_AWVALID(s_axi_AXILiteS_AWVALID),
        .s_axi_AXILiteS_BREADY(s_axi_AXILiteS_BREADY),
        .s_axi_AXILiteS_BRESP(s_axi_AXILiteS_BRESP),
        .s_axi_AXILiteS_BVALID(s_axi_AXILiteS_BVALID),
        .s_axi_AXILiteS_RDATA(s_axi_AXILiteS_RDATA),
        .s_axi_AXILiteS_RREADY(s_axi_AXILiteS_RREADY),
        .s_axi_AXILiteS_RRESP(s_axi_AXILiteS_RRESP),
        .s_axi_AXILiteS_RVALID(s_axi_AXILiteS_RVALID),
        .s_axi_AXILiteS_WDATA(s_axi_AXILiteS_WDATA),
        .s_axi_AXILiteS_WREADY(s_axi_AXILiteS_WREADY),
        .s_axi_AXILiteS_WSTRB(s_axi_AXILiteS_WSTRB),
        .s_axi_AXILiteS_WVALID(s_axi_AXILiteS_WVALID));
endmodule

(* C_M_AXI_GMEM0_ADDR_WIDTH = "32" *) (* C_M_AXI_GMEM0_ARUSER_WIDTH = "1" *) (* C_M_AXI_GMEM0_AWUSER_WIDTH = "1" *) 
(* C_M_AXI_GMEM0_BUSER_WIDTH = "1" *) (* C_M_AXI_GMEM0_CACHE_VALUE = "3" *) (* C_M_AXI_GMEM0_DATA_WIDTH = "32" *) 
(* C_M_AXI_GMEM0_ID_WIDTH = "1" *) (* C_M_AXI_GMEM0_PROT_VALUE = "0" *) (* C_M_AXI_GMEM0_RUSER_WIDTH = "1" *) 
(* C_M_AXI_GMEM0_USER_VALUE = "0" *) (* C_M_AXI_GMEM0_WUSER_WIDTH = "1" *) (* C_M_AXI_GMEM1_ADDR_WIDTH = "32" *) 
(* C_M_AXI_GMEM1_ARUSER_WIDTH = "1" *) (* C_M_AXI_GMEM1_AWUSER_WIDTH = "1" *) (* C_M_AXI_GMEM1_BUSER_WIDTH = "1" *) 
(* C_M_AXI_GMEM1_CACHE_VALUE = "3" *) (* C_M_AXI_GMEM1_DATA_WIDTH = "32" *) (* C_M_AXI_GMEM1_ID_WIDTH = "1" *) 
(* C_M_AXI_GMEM1_PROT_VALUE = "0" *) (* C_M_AXI_GMEM1_RUSER_WIDTH = "1" *) (* C_M_AXI_GMEM1_USER_VALUE = "0" *) 
(* C_M_AXI_GMEM1_WUSER_WIDTH = "1" *) (* C_S_AXI_AXILITES_ADDR_WIDTH = "5" *) (* C_S_AXI_AXILITES_DATA_WIDTH = "32" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter
   (ap_clk,
    ap_rst_n,
    m_axi_gmem0_AWVALID,
    m_axi_gmem0_AWREADY,
    m_axi_gmem0_AWADDR,
    m_axi_gmem0_AWID,
    m_axi_gmem0_AWLEN,
    m_axi_gmem0_AWSIZE,
    m_axi_gmem0_AWBURST,
    m_axi_gmem0_AWLOCK,
    m_axi_gmem0_AWCACHE,
    m_axi_gmem0_AWPROT,
    m_axi_gmem0_AWQOS,
    m_axi_gmem0_AWREGION,
    m_axi_gmem0_AWUSER,
    m_axi_gmem0_WVALID,
    m_axi_gmem0_WREADY,
    m_axi_gmem0_WDATA,
    m_axi_gmem0_WSTRB,
    m_axi_gmem0_WLAST,
    m_axi_gmem0_WID,
    m_axi_gmem0_WUSER,
    m_axi_gmem0_ARVALID,
    m_axi_gmem0_ARREADY,
    m_axi_gmem0_ARADDR,
    m_axi_gmem0_ARID,
    m_axi_gmem0_ARLEN,
    m_axi_gmem0_ARSIZE,
    m_axi_gmem0_ARBURST,
    m_axi_gmem0_ARLOCK,
    m_axi_gmem0_ARCACHE,
    m_axi_gmem0_ARPROT,
    m_axi_gmem0_ARQOS,
    m_axi_gmem0_ARREGION,
    m_axi_gmem0_ARUSER,
    m_axi_gmem0_RVALID,
    m_axi_gmem0_RREADY,
    m_axi_gmem0_RDATA,
    m_axi_gmem0_RLAST,
    m_axi_gmem0_RID,
    m_axi_gmem0_RUSER,
    m_axi_gmem0_RRESP,
    m_axi_gmem0_BVALID,
    m_axi_gmem0_BREADY,
    m_axi_gmem0_BRESP,
    m_axi_gmem0_BID,
    m_axi_gmem0_BUSER,
    m_axi_gmem1_AWVALID,
    m_axi_gmem1_AWREADY,
    m_axi_gmem1_AWADDR,
    m_axi_gmem1_AWID,
    m_axi_gmem1_AWLEN,
    m_axi_gmem1_AWSIZE,
    m_axi_gmem1_AWBURST,
    m_axi_gmem1_AWLOCK,
    m_axi_gmem1_AWCACHE,
    m_axi_gmem1_AWPROT,
    m_axi_gmem1_AWQOS,
    m_axi_gmem1_AWREGION,
    m_axi_gmem1_AWUSER,
    m_axi_gmem1_WVALID,
    m_axi_gmem1_WREADY,
    m_axi_gmem1_WDATA,
    m_axi_gmem1_WSTRB,
    m_axi_gmem1_WLAST,
    m_axi_gmem1_WID,
    m_axi_gmem1_WUSER,
    m_axi_gmem1_ARVALID,
    m_axi_gmem1_ARREADY,
    m_axi_gmem1_ARADDR,
    m_axi_gmem1_ARID,
    m_axi_gmem1_ARLEN,
    m_axi_gmem1_ARSIZE,
    m_axi_gmem1_ARBURST,
    m_axi_gmem1_ARLOCK,
    m_axi_gmem1_ARCACHE,
    m_axi_gmem1_ARPROT,
    m_axi_gmem1_ARQOS,
    m_axi_gmem1_ARREGION,
    m_axi_gmem1_ARUSER,
    m_axi_gmem1_RVALID,
    m_axi_gmem1_RREADY,
    m_axi_gmem1_RDATA,
    m_axi_gmem1_RLAST,
    m_axi_gmem1_RID,
    m_axi_gmem1_RUSER,
    m_axi_gmem1_RRESP,
    m_axi_gmem1_BVALID,
    m_axi_gmem1_BREADY,
    m_axi_gmem1_BRESP,
    m_axi_gmem1_BID,
    m_axi_gmem1_BUSER,
    s_axi_AXILiteS_AWVALID,
    s_axi_AXILiteS_AWREADY,
    s_axi_AXILiteS_AWADDR,
    s_axi_AXILiteS_WVALID,
    s_axi_AXILiteS_WREADY,
    s_axi_AXILiteS_WDATA,
    s_axi_AXILiteS_WSTRB,
    s_axi_AXILiteS_ARVALID,
    s_axi_AXILiteS_ARREADY,
    s_axi_AXILiteS_ARADDR,
    s_axi_AXILiteS_RVALID,
    s_axi_AXILiteS_RREADY,
    s_axi_AXILiteS_RDATA,
    s_axi_AXILiteS_RRESP,
    s_axi_AXILiteS_BVALID,
    s_axi_AXILiteS_BREADY,
    s_axi_AXILiteS_BRESP,
    interrupt);
  input ap_clk;
  input ap_rst_n;
  output m_axi_gmem0_AWVALID;
  input m_axi_gmem0_AWREADY;
  output [31:0]m_axi_gmem0_AWADDR;
  output [0:0]m_axi_gmem0_AWID;
  output [7:0]m_axi_gmem0_AWLEN;
  output [2:0]m_axi_gmem0_AWSIZE;
  output [1:0]m_axi_gmem0_AWBURST;
  output [1:0]m_axi_gmem0_AWLOCK;
  output [3:0]m_axi_gmem0_AWCACHE;
  output [2:0]m_axi_gmem0_AWPROT;
  output [3:0]m_axi_gmem0_AWQOS;
  output [3:0]m_axi_gmem0_AWREGION;
  output [0:0]m_axi_gmem0_AWUSER;
  output m_axi_gmem0_WVALID;
  input m_axi_gmem0_WREADY;
  output [31:0]m_axi_gmem0_WDATA;
  output [3:0]m_axi_gmem0_WSTRB;
  output m_axi_gmem0_WLAST;
  output [0:0]m_axi_gmem0_WID;
  output [0:0]m_axi_gmem0_WUSER;
  output m_axi_gmem0_ARVALID;
  input m_axi_gmem0_ARREADY;
  output [31:0]m_axi_gmem0_ARADDR;
  output [0:0]m_axi_gmem0_ARID;
  output [7:0]m_axi_gmem0_ARLEN;
  output [2:0]m_axi_gmem0_ARSIZE;
  output [1:0]m_axi_gmem0_ARBURST;
  output [1:0]m_axi_gmem0_ARLOCK;
  output [3:0]m_axi_gmem0_ARCACHE;
  output [2:0]m_axi_gmem0_ARPROT;
  output [3:0]m_axi_gmem0_ARQOS;
  output [3:0]m_axi_gmem0_ARREGION;
  output [0:0]m_axi_gmem0_ARUSER;
  input m_axi_gmem0_RVALID;
  output m_axi_gmem0_RREADY;
  input [31:0]m_axi_gmem0_RDATA;
  input m_axi_gmem0_RLAST;
  input [0:0]m_axi_gmem0_RID;
  input [0:0]m_axi_gmem0_RUSER;
  input [1:0]m_axi_gmem0_RRESP;
  input m_axi_gmem0_BVALID;
  output m_axi_gmem0_BREADY;
  input [1:0]m_axi_gmem0_BRESP;
  input [0:0]m_axi_gmem0_BID;
  input [0:0]m_axi_gmem0_BUSER;
  output m_axi_gmem1_AWVALID;
  input m_axi_gmem1_AWREADY;
  output [31:0]m_axi_gmem1_AWADDR;
  output [0:0]m_axi_gmem1_AWID;
  output [7:0]m_axi_gmem1_AWLEN;
  output [2:0]m_axi_gmem1_AWSIZE;
  output [1:0]m_axi_gmem1_AWBURST;
  output [1:0]m_axi_gmem1_AWLOCK;
  output [3:0]m_axi_gmem1_AWCACHE;
  output [2:0]m_axi_gmem1_AWPROT;
  output [3:0]m_axi_gmem1_AWQOS;
  output [3:0]m_axi_gmem1_AWREGION;
  output [0:0]m_axi_gmem1_AWUSER;
  output m_axi_gmem1_WVALID;
  input m_axi_gmem1_WREADY;
  output [31:0]m_axi_gmem1_WDATA;
  output [3:0]m_axi_gmem1_WSTRB;
  output m_axi_gmem1_WLAST;
  output [0:0]m_axi_gmem1_WID;
  output [0:0]m_axi_gmem1_WUSER;
  output m_axi_gmem1_ARVALID;
  input m_axi_gmem1_ARREADY;
  output [31:0]m_axi_gmem1_ARADDR;
  output [0:0]m_axi_gmem1_ARID;
  output [7:0]m_axi_gmem1_ARLEN;
  output [2:0]m_axi_gmem1_ARSIZE;
  output [1:0]m_axi_gmem1_ARBURST;
  output [1:0]m_axi_gmem1_ARLOCK;
  output [3:0]m_axi_gmem1_ARCACHE;
  output [2:0]m_axi_gmem1_ARPROT;
  output [3:0]m_axi_gmem1_ARQOS;
  output [3:0]m_axi_gmem1_ARREGION;
  output [0:0]m_axi_gmem1_ARUSER;
  input m_axi_gmem1_RVALID;
  output m_axi_gmem1_RREADY;
  input [31:0]m_axi_gmem1_RDATA;
  input m_axi_gmem1_RLAST;
  input [0:0]m_axi_gmem1_RID;
  input [0:0]m_axi_gmem1_RUSER;
  input [1:0]m_axi_gmem1_RRESP;
  input m_axi_gmem1_BVALID;
  output m_axi_gmem1_BREADY;
  input [1:0]m_axi_gmem1_BRESP;
  input [0:0]m_axi_gmem1_BID;
  input [0:0]m_axi_gmem1_BUSER;
  input s_axi_AXILiteS_AWVALID;
  output s_axi_AXILiteS_AWREADY;
  input [4:0]s_axi_AXILiteS_AWADDR;
  input s_axi_AXILiteS_WVALID;
  output s_axi_AXILiteS_WREADY;
  input [31:0]s_axi_AXILiteS_WDATA;
  input [3:0]s_axi_AXILiteS_WSTRB;
  input s_axi_AXILiteS_ARVALID;
  output s_axi_AXILiteS_ARREADY;
  input [4:0]s_axi_AXILiteS_ARADDR;
  output s_axi_AXILiteS_RVALID;
  input s_axi_AXILiteS_RREADY;
  output [31:0]s_axi_AXILiteS_RDATA;
  output [1:0]s_axi_AXILiteS_RRESP;
  output s_axi_AXILiteS_BVALID;
  input s_axi_AXILiteS_BREADY;
  output [1:0]s_axi_AXILiteS_BRESP;
  output interrupt;

  wire \<const0> ;
  wire \<const1> ;
  wire ARESET;
  wire I_AWVALID;
  wire I_BREADY;
  wire I_BVALID;
  wire \ap_CS_fsm[0]_i_2_n_0 ;
  wire \ap_CS_fsm[0]_i_3_n_0 ;
  wire \ap_CS_fsm_reg_n_0_[0] ;
  wire \ap_CS_fsm_reg_n_0_[10] ;
  wire \ap_CS_fsm_reg_n_0_[11] ;
  wire \ap_CS_fsm_reg_n_0_[12] ;
  wire \ap_CS_fsm_reg_n_0_[13] ;
  wire \ap_CS_fsm_reg_n_0_[2] ;
  wire \ap_CS_fsm_reg_n_0_[3] ;
  wire \ap_CS_fsm_reg_n_0_[4] ;
  wire \ap_CS_fsm_reg_n_0_[5] ;
  wire \ap_CS_fsm_reg_n_0_[6] ;
  wire ap_CS_fsm_state10;
  wire ap_CS_fsm_state15;
  wire ap_CS_fsm_state2;
  wire ap_CS_fsm_state8;
  wire ap_CS_fsm_state9;
  wire [14:0]ap_NS_fsm;
  wire ap_NS_fsm19_out;
  wire ap_clk;
  wire ap_reg_ioackin_gmem1_AWREADY;
  wire ap_reg_ioackin_gmem1_AWREADY3_out;
  wire ap_rst_n;
  wire \bus_write/buff_wdata/push ;
  wire filter_gmem0_m_axi_U_n_5;
  wire gmem0_ARREADY;
  wire [7:0]gmem0_RDATA;
  wire gmem0_RVALID;
  wire [7:0]gmem0_addr_read_reg_195;
  wire [31:0]gmem0_addr_reg_178;
  wire gmem1_AWREADY;
  wire [29:0]gmem1_addr_reg_184;
  wire \gmem1_addr_reg_184[11]_i_2_n_0 ;
  wire \gmem1_addr_reg_184[11]_i_3_n_0 ;
  wire \gmem1_addr_reg_184[11]_i_4_n_0 ;
  wire \gmem1_addr_reg_184[11]_i_5_n_0 ;
  wire \gmem1_addr_reg_184[15]_i_2_n_0 ;
  wire \gmem1_addr_reg_184[15]_i_3_n_0 ;
  wire \gmem1_addr_reg_184[15]_i_4_n_0 ;
  wire \gmem1_addr_reg_184[15]_i_5_n_0 ;
  wire \gmem1_addr_reg_184[19]_i_2_n_0 ;
  wire \gmem1_addr_reg_184[19]_i_3_n_0 ;
  wire \gmem1_addr_reg_184[19]_i_4_n_0 ;
  wire \gmem1_addr_reg_184[19]_i_5_n_0 ;
  wire \gmem1_addr_reg_184[23]_i_2_n_0 ;
  wire \gmem1_addr_reg_184[23]_i_3_n_0 ;
  wire \gmem1_addr_reg_184[23]_i_4_n_0 ;
  wire \gmem1_addr_reg_184[23]_i_5_n_0 ;
  wire \gmem1_addr_reg_184[27]_i_2_n_0 ;
  wire \gmem1_addr_reg_184[27]_i_3_n_0 ;
  wire \gmem1_addr_reg_184[27]_i_4_n_0 ;
  wire \gmem1_addr_reg_184[27]_i_5_n_0 ;
  wire \gmem1_addr_reg_184[29]_i_2_n_0 ;
  wire \gmem1_addr_reg_184[29]_i_3_n_0 ;
  wire \gmem1_addr_reg_184[3]_i_2_n_0 ;
  wire \gmem1_addr_reg_184[3]_i_3_n_0 ;
  wire \gmem1_addr_reg_184[3]_i_4_n_0 ;
  wire \gmem1_addr_reg_184[3]_i_5_n_0 ;
  wire \gmem1_addr_reg_184[7]_i_2_n_0 ;
  wire \gmem1_addr_reg_184[7]_i_3_n_0 ;
  wire \gmem1_addr_reg_184[7]_i_4_n_0 ;
  wire \gmem1_addr_reg_184[7]_i_5_n_0 ;
  wire \gmem1_addr_reg_184_reg[11]_i_1_n_0 ;
  wire \gmem1_addr_reg_184_reg[11]_i_1_n_1 ;
  wire \gmem1_addr_reg_184_reg[11]_i_1_n_2 ;
  wire \gmem1_addr_reg_184_reg[11]_i_1_n_3 ;
  wire \gmem1_addr_reg_184_reg[15]_i_1_n_0 ;
  wire \gmem1_addr_reg_184_reg[15]_i_1_n_1 ;
  wire \gmem1_addr_reg_184_reg[15]_i_1_n_2 ;
  wire \gmem1_addr_reg_184_reg[15]_i_1_n_3 ;
  wire \gmem1_addr_reg_184_reg[19]_i_1_n_0 ;
  wire \gmem1_addr_reg_184_reg[19]_i_1_n_1 ;
  wire \gmem1_addr_reg_184_reg[19]_i_1_n_2 ;
  wire \gmem1_addr_reg_184_reg[19]_i_1_n_3 ;
  wire \gmem1_addr_reg_184_reg[23]_i_1_n_0 ;
  wire \gmem1_addr_reg_184_reg[23]_i_1_n_1 ;
  wire \gmem1_addr_reg_184_reg[23]_i_1_n_2 ;
  wire \gmem1_addr_reg_184_reg[23]_i_1_n_3 ;
  wire \gmem1_addr_reg_184_reg[27]_i_1_n_0 ;
  wire \gmem1_addr_reg_184_reg[27]_i_1_n_1 ;
  wire \gmem1_addr_reg_184_reg[27]_i_1_n_2 ;
  wire \gmem1_addr_reg_184_reg[27]_i_1_n_3 ;
  wire \gmem1_addr_reg_184_reg[29]_i_1_n_3 ;
  wire \gmem1_addr_reg_184_reg[3]_i_1_n_0 ;
  wire \gmem1_addr_reg_184_reg[3]_i_1_n_1 ;
  wire \gmem1_addr_reg_184_reg[3]_i_1_n_2 ;
  wire \gmem1_addr_reg_184_reg[3]_i_1_n_3 ;
  wire \gmem1_addr_reg_184_reg[7]_i_1_n_0 ;
  wire \gmem1_addr_reg_184_reg[7]_i_1_n_1 ;
  wire \gmem1_addr_reg_184_reg[7]_i_1_n_2 ;
  wire \gmem1_addr_reg_184_reg[7]_i_1_n_3 ;
  wire [31:0]inter_pix;
  wire interrupt;
  wire [29:0]j_1_fu_158_p2;
  wire [29:0]j_1_reg_190;
  wire \j_1_reg_190_reg[12]_i_1_n_0 ;
  wire \j_1_reg_190_reg[12]_i_1_n_1 ;
  wire \j_1_reg_190_reg[12]_i_1_n_2 ;
  wire \j_1_reg_190_reg[12]_i_1_n_3 ;
  wire \j_1_reg_190_reg[16]_i_1_n_0 ;
  wire \j_1_reg_190_reg[16]_i_1_n_1 ;
  wire \j_1_reg_190_reg[16]_i_1_n_2 ;
  wire \j_1_reg_190_reg[16]_i_1_n_3 ;
  wire \j_1_reg_190_reg[20]_i_1_n_0 ;
  wire \j_1_reg_190_reg[20]_i_1_n_1 ;
  wire \j_1_reg_190_reg[20]_i_1_n_2 ;
  wire \j_1_reg_190_reg[20]_i_1_n_3 ;
  wire \j_1_reg_190_reg[24]_i_1_n_0 ;
  wire \j_1_reg_190_reg[24]_i_1_n_1 ;
  wire \j_1_reg_190_reg[24]_i_1_n_2 ;
  wire \j_1_reg_190_reg[24]_i_1_n_3 ;
  wire \j_1_reg_190_reg[28]_i_1_n_0 ;
  wire \j_1_reg_190_reg[28]_i_1_n_1 ;
  wire \j_1_reg_190_reg[28]_i_1_n_2 ;
  wire \j_1_reg_190_reg[28]_i_1_n_3 ;
  wire \j_1_reg_190_reg[4]_i_1_n_0 ;
  wire \j_1_reg_190_reg[4]_i_1_n_1 ;
  wire \j_1_reg_190_reg[4]_i_1_n_2 ;
  wire \j_1_reg_190_reg[4]_i_1_n_3 ;
  wire \j_1_reg_190_reg[8]_i_1_n_0 ;
  wire \j_1_reg_190_reg[8]_i_1_n_1 ;
  wire \j_1_reg_190_reg[8]_i_1_n_2 ;
  wire \j_1_reg_190_reg[8]_i_1_n_3 ;
  wire [29:0]j_reg_104;
  wire [31:2]\^m_axi_gmem0_ARADDR ;
  wire [3:0]\^m_axi_gmem0_ARLEN ;
  wire m_axi_gmem0_ARREADY;
  wire m_axi_gmem0_ARVALID;
  wire [31:0]m_axi_gmem0_RDATA;
  wire m_axi_gmem0_RLAST;
  wire m_axi_gmem0_RREADY;
  wire [1:0]m_axi_gmem0_RRESP;
  wire m_axi_gmem0_RVALID;
  wire [31:2]\^m_axi_gmem1_AWADDR ;
  wire [3:0]\^m_axi_gmem1_AWLEN ;
  wire m_axi_gmem1_AWREADY;
  wire m_axi_gmem1_AWVALID;
  wire m_axi_gmem1_BREADY;
  wire m_axi_gmem1_BVALID;
  wire m_axi_gmem1_RREADY;
  wire m_axi_gmem1_RVALID;
  wire [31:0]m_axi_gmem1_WDATA;
  wire m_axi_gmem1_WLAST;
  wire m_axi_gmem1_WREADY;
  wire [3:0]m_axi_gmem1_WSTRB;
  wire m_axi_gmem1_WVALID;
  wire [31:2]out_pix;
  wire [29:0]out_pix4_sum_fu_143_p2;
  wire [4:0]s_axi_AXILiteS_ARADDR;
  wire s_axi_AXILiteS_ARREADY;
  wire s_axi_AXILiteS_ARVALID;
  wire [4:0]s_axi_AXILiteS_AWADDR;
  wire s_axi_AXILiteS_AWREADY;
  wire s_axi_AXILiteS_AWVALID;
  wire s_axi_AXILiteS_BREADY;
  wire s_axi_AXILiteS_BVALID;
  wire [31:0]s_axi_AXILiteS_RDATA;
  wire s_axi_AXILiteS_RREADY;
  wire s_axi_AXILiteS_RVALID;
  wire [31:0]s_axi_AXILiteS_WDATA;
  wire s_axi_AXILiteS_WREADY;
  wire [3:0]s_axi_AXILiteS_WSTRB;
  wire s_axi_AXILiteS_WVALID;
  wire [29:0]tmp_cast_reg_173;
  wire [3:1]\NLW_gmem1_addr_reg_184_reg[29]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_gmem1_addr_reg_184_reg[29]_i_1_O_UNCONNECTED ;
  wire [3:0]\NLW_j_1_reg_190_reg[29]_i_1_CO_UNCONNECTED ;
  wire [3:1]\NLW_j_1_reg_190_reg[29]_i_1_O_UNCONNECTED ;

  assign m_axi_gmem0_ARADDR[31:2] = \^m_axi_gmem0_ARADDR [31:2];
  assign m_axi_gmem0_ARADDR[1] = \<const0> ;
  assign m_axi_gmem0_ARADDR[0] = \<const0> ;
  assign m_axi_gmem0_ARBURST[1] = \<const0> ;
  assign m_axi_gmem0_ARBURST[0] = \<const1> ;
  assign m_axi_gmem0_ARCACHE[3] = \<const0> ;
  assign m_axi_gmem0_ARCACHE[2] = \<const0> ;
  assign m_axi_gmem0_ARCACHE[1] = \<const1> ;
  assign m_axi_gmem0_ARCACHE[0] = \<const1> ;
  assign m_axi_gmem0_ARID[0] = \<const0> ;
  assign m_axi_gmem0_ARLEN[7] = \<const0> ;
  assign m_axi_gmem0_ARLEN[6] = \<const0> ;
  assign m_axi_gmem0_ARLEN[5] = \<const0> ;
  assign m_axi_gmem0_ARLEN[4] = \<const0> ;
  assign m_axi_gmem0_ARLEN[3:0] = \^m_axi_gmem0_ARLEN [3:0];
  assign m_axi_gmem0_ARLOCK[1] = \<const0> ;
  assign m_axi_gmem0_ARLOCK[0] = \<const0> ;
  assign m_axi_gmem0_ARPROT[2] = \<const0> ;
  assign m_axi_gmem0_ARPROT[1] = \<const0> ;
  assign m_axi_gmem0_ARPROT[0] = \<const0> ;
  assign m_axi_gmem0_ARQOS[3] = \<const0> ;
  assign m_axi_gmem0_ARQOS[2] = \<const0> ;
  assign m_axi_gmem0_ARQOS[1] = \<const0> ;
  assign m_axi_gmem0_ARQOS[0] = \<const0> ;
  assign m_axi_gmem0_ARREGION[3] = \<const0> ;
  assign m_axi_gmem0_ARREGION[2] = \<const0> ;
  assign m_axi_gmem0_ARREGION[1] = \<const0> ;
  assign m_axi_gmem0_ARREGION[0] = \<const0> ;
  assign m_axi_gmem0_ARSIZE[2] = \<const0> ;
  assign m_axi_gmem0_ARSIZE[1] = \<const1> ;
  assign m_axi_gmem0_ARSIZE[0] = \<const0> ;
  assign m_axi_gmem0_ARUSER[0] = \<const0> ;
  assign m_axi_gmem0_AWADDR[31] = \<const0> ;
  assign m_axi_gmem0_AWADDR[30] = \<const0> ;
  assign m_axi_gmem0_AWADDR[29] = \<const0> ;
  assign m_axi_gmem0_AWADDR[28] = \<const0> ;
  assign m_axi_gmem0_AWADDR[27] = \<const0> ;
  assign m_axi_gmem0_AWADDR[26] = \<const0> ;
  assign m_axi_gmem0_AWADDR[25] = \<const0> ;
  assign m_axi_gmem0_AWADDR[24] = \<const0> ;
  assign m_axi_gmem0_AWADDR[23] = \<const0> ;
  assign m_axi_gmem0_AWADDR[22] = \<const0> ;
  assign m_axi_gmem0_AWADDR[21] = \<const0> ;
  assign m_axi_gmem0_AWADDR[20] = \<const0> ;
  assign m_axi_gmem0_AWADDR[19] = \<const0> ;
  assign m_axi_gmem0_AWADDR[18] = \<const0> ;
  assign m_axi_gmem0_AWADDR[17] = \<const0> ;
  assign m_axi_gmem0_AWADDR[16] = \<const0> ;
  assign m_axi_gmem0_AWADDR[15] = \<const0> ;
  assign m_axi_gmem0_AWADDR[14] = \<const0> ;
  assign m_axi_gmem0_AWADDR[13] = \<const0> ;
  assign m_axi_gmem0_AWADDR[12] = \<const0> ;
  assign m_axi_gmem0_AWADDR[11] = \<const0> ;
  assign m_axi_gmem0_AWADDR[10] = \<const0> ;
  assign m_axi_gmem0_AWADDR[9] = \<const0> ;
  assign m_axi_gmem0_AWADDR[8] = \<const0> ;
  assign m_axi_gmem0_AWADDR[7] = \<const0> ;
  assign m_axi_gmem0_AWADDR[6] = \<const0> ;
  assign m_axi_gmem0_AWADDR[5] = \<const0> ;
  assign m_axi_gmem0_AWADDR[4] = \<const0> ;
  assign m_axi_gmem0_AWADDR[3] = \<const0> ;
  assign m_axi_gmem0_AWADDR[2] = \<const0> ;
  assign m_axi_gmem0_AWADDR[1] = \<const0> ;
  assign m_axi_gmem0_AWADDR[0] = \<const0> ;
  assign m_axi_gmem0_AWBURST[1] = \<const0> ;
  assign m_axi_gmem0_AWBURST[0] = \<const1> ;
  assign m_axi_gmem0_AWCACHE[3] = \<const0> ;
  assign m_axi_gmem0_AWCACHE[2] = \<const0> ;
  assign m_axi_gmem0_AWCACHE[1] = \<const1> ;
  assign m_axi_gmem0_AWCACHE[0] = \<const1> ;
  assign m_axi_gmem0_AWID[0] = \<const0> ;
  assign m_axi_gmem0_AWLEN[7] = \<const0> ;
  assign m_axi_gmem0_AWLEN[6] = \<const0> ;
  assign m_axi_gmem0_AWLEN[5] = \<const0> ;
  assign m_axi_gmem0_AWLEN[4] = \<const0> ;
  assign m_axi_gmem0_AWLEN[3] = \<const0> ;
  assign m_axi_gmem0_AWLEN[2] = \<const0> ;
  assign m_axi_gmem0_AWLEN[1] = \<const0> ;
  assign m_axi_gmem0_AWLEN[0] = \<const0> ;
  assign m_axi_gmem0_AWLOCK[1] = \<const0> ;
  assign m_axi_gmem0_AWLOCK[0] = \<const0> ;
  assign m_axi_gmem0_AWPROT[2] = \<const0> ;
  assign m_axi_gmem0_AWPROT[1] = \<const0> ;
  assign m_axi_gmem0_AWPROT[0] = \<const0> ;
  assign m_axi_gmem0_AWQOS[3] = \<const0> ;
  assign m_axi_gmem0_AWQOS[2] = \<const0> ;
  assign m_axi_gmem0_AWQOS[1] = \<const0> ;
  assign m_axi_gmem0_AWQOS[0] = \<const0> ;
  assign m_axi_gmem0_AWREGION[3] = \<const0> ;
  assign m_axi_gmem0_AWREGION[2] = \<const0> ;
  assign m_axi_gmem0_AWREGION[1] = \<const0> ;
  assign m_axi_gmem0_AWREGION[0] = \<const0> ;
  assign m_axi_gmem0_AWSIZE[2] = \<const0> ;
  assign m_axi_gmem0_AWSIZE[1] = \<const1> ;
  assign m_axi_gmem0_AWSIZE[0] = \<const0> ;
  assign m_axi_gmem0_AWUSER[0] = \<const0> ;
  assign m_axi_gmem0_AWVALID = \<const0> ;
  assign m_axi_gmem0_BREADY = \<const1> ;
  assign m_axi_gmem0_WDATA[31] = \<const0> ;
  assign m_axi_gmem0_WDATA[30] = \<const0> ;
  assign m_axi_gmem0_WDATA[29] = \<const0> ;
  assign m_axi_gmem0_WDATA[28] = \<const0> ;
  assign m_axi_gmem0_WDATA[27] = \<const0> ;
  assign m_axi_gmem0_WDATA[26] = \<const0> ;
  assign m_axi_gmem0_WDATA[25] = \<const0> ;
  assign m_axi_gmem0_WDATA[24] = \<const0> ;
  assign m_axi_gmem0_WDATA[23] = \<const0> ;
  assign m_axi_gmem0_WDATA[22] = \<const0> ;
  assign m_axi_gmem0_WDATA[21] = \<const0> ;
  assign m_axi_gmem0_WDATA[20] = \<const0> ;
  assign m_axi_gmem0_WDATA[19] = \<const0> ;
  assign m_axi_gmem0_WDATA[18] = \<const0> ;
  assign m_axi_gmem0_WDATA[17] = \<const0> ;
  assign m_axi_gmem0_WDATA[16] = \<const0> ;
  assign m_axi_gmem0_WDATA[15] = \<const0> ;
  assign m_axi_gmem0_WDATA[14] = \<const0> ;
  assign m_axi_gmem0_WDATA[13] = \<const0> ;
  assign m_axi_gmem0_WDATA[12] = \<const0> ;
  assign m_axi_gmem0_WDATA[11] = \<const0> ;
  assign m_axi_gmem0_WDATA[10] = \<const0> ;
  assign m_axi_gmem0_WDATA[9] = \<const0> ;
  assign m_axi_gmem0_WDATA[8] = \<const0> ;
  assign m_axi_gmem0_WDATA[7] = \<const0> ;
  assign m_axi_gmem0_WDATA[6] = \<const0> ;
  assign m_axi_gmem0_WDATA[5] = \<const0> ;
  assign m_axi_gmem0_WDATA[4] = \<const0> ;
  assign m_axi_gmem0_WDATA[3] = \<const0> ;
  assign m_axi_gmem0_WDATA[2] = \<const0> ;
  assign m_axi_gmem0_WDATA[1] = \<const0> ;
  assign m_axi_gmem0_WDATA[0] = \<const0> ;
  assign m_axi_gmem0_WID[0] = \<const0> ;
  assign m_axi_gmem0_WLAST = \<const0> ;
  assign m_axi_gmem0_WSTRB[3] = \<const0> ;
  assign m_axi_gmem0_WSTRB[2] = \<const0> ;
  assign m_axi_gmem0_WSTRB[1] = \<const0> ;
  assign m_axi_gmem0_WSTRB[0] = \<const0> ;
  assign m_axi_gmem0_WUSER[0] = \<const0> ;
  assign m_axi_gmem0_WVALID = \<const0> ;
  assign m_axi_gmem1_ARADDR[31] = \<const0> ;
  assign m_axi_gmem1_ARADDR[30] = \<const0> ;
  assign m_axi_gmem1_ARADDR[29] = \<const0> ;
  assign m_axi_gmem1_ARADDR[28] = \<const0> ;
  assign m_axi_gmem1_ARADDR[27] = \<const0> ;
  assign m_axi_gmem1_ARADDR[26] = \<const0> ;
  assign m_axi_gmem1_ARADDR[25] = \<const0> ;
  assign m_axi_gmem1_ARADDR[24] = \<const0> ;
  assign m_axi_gmem1_ARADDR[23] = \<const0> ;
  assign m_axi_gmem1_ARADDR[22] = \<const0> ;
  assign m_axi_gmem1_ARADDR[21] = \<const0> ;
  assign m_axi_gmem1_ARADDR[20] = \<const0> ;
  assign m_axi_gmem1_ARADDR[19] = \<const0> ;
  assign m_axi_gmem1_ARADDR[18] = \<const0> ;
  assign m_axi_gmem1_ARADDR[17] = \<const0> ;
  assign m_axi_gmem1_ARADDR[16] = \<const0> ;
  assign m_axi_gmem1_ARADDR[15] = \<const0> ;
  assign m_axi_gmem1_ARADDR[14] = \<const0> ;
  assign m_axi_gmem1_ARADDR[13] = \<const0> ;
  assign m_axi_gmem1_ARADDR[12] = \<const0> ;
  assign m_axi_gmem1_ARADDR[11] = \<const0> ;
  assign m_axi_gmem1_ARADDR[10] = \<const0> ;
  assign m_axi_gmem1_ARADDR[9] = \<const0> ;
  assign m_axi_gmem1_ARADDR[8] = \<const0> ;
  assign m_axi_gmem1_ARADDR[7] = \<const0> ;
  assign m_axi_gmem1_ARADDR[6] = \<const0> ;
  assign m_axi_gmem1_ARADDR[5] = \<const0> ;
  assign m_axi_gmem1_ARADDR[4] = \<const0> ;
  assign m_axi_gmem1_ARADDR[3] = \<const0> ;
  assign m_axi_gmem1_ARADDR[2] = \<const0> ;
  assign m_axi_gmem1_ARADDR[1] = \<const0> ;
  assign m_axi_gmem1_ARADDR[0] = \<const0> ;
  assign m_axi_gmem1_ARBURST[1] = \<const0> ;
  assign m_axi_gmem1_ARBURST[0] = \<const1> ;
  assign m_axi_gmem1_ARCACHE[3] = \<const0> ;
  assign m_axi_gmem1_ARCACHE[2] = \<const0> ;
  assign m_axi_gmem1_ARCACHE[1] = \<const1> ;
  assign m_axi_gmem1_ARCACHE[0] = \<const1> ;
  assign m_axi_gmem1_ARID[0] = \<const0> ;
  assign m_axi_gmem1_ARLEN[7] = \<const0> ;
  assign m_axi_gmem1_ARLEN[6] = \<const0> ;
  assign m_axi_gmem1_ARLEN[5] = \<const0> ;
  assign m_axi_gmem1_ARLEN[4] = \<const0> ;
  assign m_axi_gmem1_ARLEN[3] = \<const0> ;
  assign m_axi_gmem1_ARLEN[2] = \<const0> ;
  assign m_axi_gmem1_ARLEN[1] = \<const0> ;
  assign m_axi_gmem1_ARLEN[0] = \<const0> ;
  assign m_axi_gmem1_ARLOCK[1] = \<const0> ;
  assign m_axi_gmem1_ARLOCK[0] = \<const0> ;
  assign m_axi_gmem1_ARPROT[2] = \<const0> ;
  assign m_axi_gmem1_ARPROT[1] = \<const0> ;
  assign m_axi_gmem1_ARPROT[0] = \<const0> ;
  assign m_axi_gmem1_ARQOS[3] = \<const0> ;
  assign m_axi_gmem1_ARQOS[2] = \<const0> ;
  assign m_axi_gmem1_ARQOS[1] = \<const0> ;
  assign m_axi_gmem1_ARQOS[0] = \<const0> ;
  assign m_axi_gmem1_ARREGION[3] = \<const0> ;
  assign m_axi_gmem1_ARREGION[2] = \<const0> ;
  assign m_axi_gmem1_ARREGION[1] = \<const0> ;
  assign m_axi_gmem1_ARREGION[0] = \<const0> ;
  assign m_axi_gmem1_ARSIZE[2] = \<const0> ;
  assign m_axi_gmem1_ARSIZE[1] = \<const1> ;
  assign m_axi_gmem1_ARSIZE[0] = \<const0> ;
  assign m_axi_gmem1_ARUSER[0] = \<const0> ;
  assign m_axi_gmem1_ARVALID = \<const0> ;
  assign m_axi_gmem1_AWADDR[31:2] = \^m_axi_gmem1_AWADDR [31:2];
  assign m_axi_gmem1_AWADDR[1] = \<const0> ;
  assign m_axi_gmem1_AWADDR[0] = \<const0> ;
  assign m_axi_gmem1_AWBURST[1] = \<const0> ;
  assign m_axi_gmem1_AWBURST[0] = \<const1> ;
  assign m_axi_gmem1_AWCACHE[3] = \<const0> ;
  assign m_axi_gmem1_AWCACHE[2] = \<const0> ;
  assign m_axi_gmem1_AWCACHE[1] = \<const1> ;
  assign m_axi_gmem1_AWCACHE[0] = \<const1> ;
  assign m_axi_gmem1_AWID[0] = \<const0> ;
  assign m_axi_gmem1_AWLEN[7] = \<const0> ;
  assign m_axi_gmem1_AWLEN[6] = \<const0> ;
  assign m_axi_gmem1_AWLEN[5] = \<const0> ;
  assign m_axi_gmem1_AWLEN[4] = \<const0> ;
  assign m_axi_gmem1_AWLEN[3:0] = \^m_axi_gmem1_AWLEN [3:0];
  assign m_axi_gmem1_AWLOCK[1] = \<const0> ;
  assign m_axi_gmem1_AWLOCK[0] = \<const0> ;
  assign m_axi_gmem1_AWPROT[2] = \<const0> ;
  assign m_axi_gmem1_AWPROT[1] = \<const0> ;
  assign m_axi_gmem1_AWPROT[0] = \<const0> ;
  assign m_axi_gmem1_AWQOS[3] = \<const0> ;
  assign m_axi_gmem1_AWQOS[2] = \<const0> ;
  assign m_axi_gmem1_AWQOS[1] = \<const0> ;
  assign m_axi_gmem1_AWQOS[0] = \<const0> ;
  assign m_axi_gmem1_AWREGION[3] = \<const0> ;
  assign m_axi_gmem1_AWREGION[2] = \<const0> ;
  assign m_axi_gmem1_AWREGION[1] = \<const0> ;
  assign m_axi_gmem1_AWREGION[0] = \<const0> ;
  assign m_axi_gmem1_AWSIZE[2] = \<const0> ;
  assign m_axi_gmem1_AWSIZE[1] = \<const1> ;
  assign m_axi_gmem1_AWSIZE[0] = \<const0> ;
  assign m_axi_gmem1_AWUSER[0] = \<const0> ;
  assign m_axi_gmem1_WID[0] = \<const0> ;
  assign m_axi_gmem1_WUSER[0] = \<const0> ;
  assign s_axi_AXILiteS_BRESP[1] = \<const0> ;
  assign s_axi_AXILiteS_BRESP[0] = \<const0> ;
  assign s_axi_AXILiteS_RRESP[1] = \<const0> ;
  assign s_axi_AXILiteS_RRESP[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \ap_CS_fsm[0]_i_2 
       (.I0(\ap_CS_fsm_reg_n_0_[11] ),
        .I1(\ap_CS_fsm_reg_n_0_[12] ),
        .I2(ap_CS_fsm_state10),
        .I3(\ap_CS_fsm_reg_n_0_[10] ),
        .I4(ap_CS_fsm_state15),
        .I5(\ap_CS_fsm_reg_n_0_[13] ),
        .O(\ap_CS_fsm[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \ap_CS_fsm[0]_i_3 
       (.I0(\ap_CS_fsm_reg_n_0_[5] ),
        .I1(\ap_CS_fsm_reg_n_0_[6] ),
        .I2(\ap_CS_fsm_reg_n_0_[3] ),
        .I3(\ap_CS_fsm_reg_n_0_[4] ),
        .I4(ap_CS_fsm_state9),
        .I5(ap_CS_fsm_state8),
        .O(\ap_CS_fsm[0]_i_3_n_0 ));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(\ap_CS_fsm_reg_n_0_[0] ),
        .S(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[10] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\bus_write/buff_wdata/push ),
        .Q(\ap_CS_fsm_reg_n_0_[10] ),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[11] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_CS_fsm_reg_n_0_[10] ),
        .Q(\ap_CS_fsm_reg_n_0_[11] ),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[12] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_CS_fsm_reg_n_0_[11] ),
        .Q(\ap_CS_fsm_reg_n_0_[12] ),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[13] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_CS_fsm_reg_n_0_[12] ),
        .Q(\ap_CS_fsm_reg_n_0_[13] ),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[14] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[14]),
        .Q(ap_CS_fsm_state15),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_state2),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[2]),
        .Q(\ap_CS_fsm_reg_n_0_[2] ),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_CS_fsm_reg_n_0_[2] ),
        .Q(\ap_CS_fsm_reg_n_0_[3] ),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[4] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_CS_fsm_reg_n_0_[3] ),
        .Q(\ap_CS_fsm_reg_n_0_[4] ),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[5] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_CS_fsm_reg_n_0_[4] ),
        .Q(\ap_CS_fsm_reg_n_0_[5] ),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[6] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_CS_fsm_reg_n_0_[5] ),
        .Q(\ap_CS_fsm_reg_n_0_[6] ),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[7] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_CS_fsm_reg_n_0_[6] ),
        .Q(ap_CS_fsm_state8),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[8] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[8]),
        .Q(ap_CS_fsm_state9),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[9] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[9]),
        .Q(ap_CS_fsm_state10),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    ap_reg_ioackin_gmem1_AWREADY_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(filter_gmem0_m_axi_U_n_5),
        .Q(ap_reg_ioackin_gmem1_AWREADY),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_AXILiteS_s_axi filter_AXILiteS_s_axi_U
       (.ARESET(ARESET),
        .D(ap_NS_fsm[1:0]),
        .\FSM_onehot_rstate_reg[1]_0 (s_axi_AXILiteS_ARREADY),
        .\FSM_onehot_wstate_reg[1]_0 (s_axi_AXILiteS_AWREADY),
        .\FSM_onehot_wstate_reg[2]_0 (s_axi_AXILiteS_WREADY),
        .I_BVALID(I_BVALID),
        .Q({ap_CS_fsm_state15,\ap_CS_fsm_reg_n_0_[2] ,ap_CS_fsm_state2,\ap_CS_fsm_reg_n_0_[0] }),
        .SR(ap_NS_fsm19_out),
        .\ap_CS_fsm_reg[0] (\ap_CS_fsm[0]_i_2_n_0 ),
        .\ap_CS_fsm_reg[0]_0 (\ap_CS_fsm[0]_i_3_n_0 ),
        .ap_clk(ap_clk),
        .gmem0_ARREADY(gmem0_ARREADY),
        .inter_pix(inter_pix),
        .interrupt(interrupt),
        .out_pix(out_pix),
        .s_axi_AXILiteS_ARADDR(s_axi_AXILiteS_ARADDR),
        .s_axi_AXILiteS_ARVALID(s_axi_AXILiteS_ARVALID),
        .s_axi_AXILiteS_AWADDR(s_axi_AXILiteS_AWADDR),
        .s_axi_AXILiteS_AWVALID(s_axi_AXILiteS_AWVALID),
        .s_axi_AXILiteS_BREADY(s_axi_AXILiteS_BREADY),
        .s_axi_AXILiteS_BVALID(s_axi_AXILiteS_BVALID),
        .s_axi_AXILiteS_RDATA(s_axi_AXILiteS_RDATA),
        .s_axi_AXILiteS_RREADY(s_axi_AXILiteS_RREADY),
        .s_axi_AXILiteS_RVALID(s_axi_AXILiteS_RVALID),
        .s_axi_AXILiteS_WDATA(s_axi_AXILiteS_WDATA),
        .s_axi_AXILiteS_WSTRB(s_axi_AXILiteS_WSTRB),
        .s_axi_AXILiteS_WVALID(s_axi_AXILiteS_WVALID));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem0_m_axi filter_gmem0_m_axi_U
       (.ARESET(ARESET),
        .D({ap_NS_fsm[8],ap_NS_fsm[2]}),
        .I_AWVALID(I_AWVALID),
        .Q({ap_CS_fsm_state9,ap_CS_fsm_state8,ap_CS_fsm_state2}),
        .RREADY(m_axi_gmem0_RREADY),
        .ap_clk(ap_clk),
        .ap_reg_ioackin_gmem1_AWREADY(ap_reg_ioackin_gmem1_AWREADY),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_0(filter_gmem0_m_axi_U_n_5),
        .\could_multi_bursts.arlen_buf_reg[3] (\^m_axi_gmem0_ARLEN ),
        .\data_p1_reg[7] (gmem0_RDATA),
        .\data_p2_reg[31] (gmem0_addr_reg_178),
        .gmem0_ARREADY(gmem0_ARREADY),
        .gmem1_AWREADY(gmem1_AWREADY),
        .m_axi_gmem0_ARADDR(\^m_axi_gmem0_ARADDR ),
        .m_axi_gmem0_ARREADY(m_axi_gmem0_ARREADY),
        .m_axi_gmem0_ARVALID(m_axi_gmem0_ARVALID),
        .m_axi_gmem0_RRESP(m_axi_gmem0_RRESP),
        .m_axi_gmem0_RVALID(m_axi_gmem0_RVALID),
        .mem_reg({m_axi_gmem0_RLAST,m_axi_gmem0_RDATA}),
        .\state_reg[0] (gmem0_RVALID));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi filter_gmem1_m_axi_U
       (.ARESET(ARESET),
        .AWLEN(\^m_axi_gmem1_AWLEN ),
        .D({ap_NS_fsm[14],\bus_write/buff_wdata/push ,ap_NS_fsm[9]}),
        .E(ap_reg_ioackin_gmem1_AWREADY3_out),
        .I_AWVALID(I_AWVALID),
        .I_BVALID(I_BVALID),
        .Q({ap_CS_fsm_state15,\ap_CS_fsm_reg_n_0_[13] ,ap_CS_fsm_state10,ap_CS_fsm_state9}),
        .RREADY(m_axi_gmem1_RREADY),
        .ap_clk(ap_clk),
        .ap_reg_ioackin_gmem1_AWREADY(ap_reg_ioackin_gmem1_AWREADY),
        .ap_rst_n(ap_rst_n),
        .\bus_equal_gen.WVALID_Dummy_reg (m_axi_gmem1_WVALID),
        .\data_p2_reg[0] (gmem0_RVALID),
        .\data_p2_reg[29] (gmem1_addr_reg_184),
        .empty_n_tmp_reg(I_BREADY),
        .full_n_tmp_reg(m_axi_gmem1_BREADY),
        .gmem1_AWREADY(gmem1_AWREADY),
        .m_axi_gmem1_AWADDR(\^m_axi_gmem1_AWADDR ),
        .m_axi_gmem1_AWREADY(m_axi_gmem1_AWREADY),
        .m_axi_gmem1_AWVALID(m_axi_gmem1_AWVALID),
        .m_axi_gmem1_BVALID(m_axi_gmem1_BVALID),
        .m_axi_gmem1_RVALID(m_axi_gmem1_RVALID),
        .m_axi_gmem1_WDATA(m_axi_gmem1_WDATA),
        .m_axi_gmem1_WLAST(m_axi_gmem1_WLAST),
        .m_axi_gmem1_WREADY(m_axi_gmem1_WREADY),
        .m_axi_gmem1_WSTRB(m_axi_gmem1_WSTRB),
        .\q_tmp_reg[31] (gmem0_addr_read_reg_195));
  FDRE \gmem0_addr_read_reg_195_reg[0] 
       (.C(ap_clk),
        .CE(ap_reg_ioackin_gmem1_AWREADY3_out),
        .D(gmem0_RDATA[0]),
        .Q(gmem0_addr_read_reg_195[0]),
        .R(1'b0));
  FDRE \gmem0_addr_read_reg_195_reg[1] 
       (.C(ap_clk),
        .CE(ap_reg_ioackin_gmem1_AWREADY3_out),
        .D(gmem0_RDATA[1]),
        .Q(gmem0_addr_read_reg_195[1]),
        .R(1'b0));
  FDRE \gmem0_addr_read_reg_195_reg[2] 
       (.C(ap_clk),
        .CE(ap_reg_ioackin_gmem1_AWREADY3_out),
        .D(gmem0_RDATA[2]),
        .Q(gmem0_addr_read_reg_195[2]),
        .R(1'b0));
  FDRE \gmem0_addr_read_reg_195_reg[3] 
       (.C(ap_clk),
        .CE(ap_reg_ioackin_gmem1_AWREADY3_out),
        .D(gmem0_RDATA[3]),
        .Q(gmem0_addr_read_reg_195[3]),
        .R(1'b0));
  FDRE \gmem0_addr_read_reg_195_reg[4] 
       (.C(ap_clk),
        .CE(ap_reg_ioackin_gmem1_AWREADY3_out),
        .D(gmem0_RDATA[4]),
        .Q(gmem0_addr_read_reg_195[4]),
        .R(1'b0));
  FDRE \gmem0_addr_read_reg_195_reg[5] 
       (.C(ap_clk),
        .CE(ap_reg_ioackin_gmem1_AWREADY3_out),
        .D(gmem0_RDATA[5]),
        .Q(gmem0_addr_read_reg_195[5]),
        .R(1'b0));
  FDRE \gmem0_addr_read_reg_195_reg[6] 
       (.C(ap_clk),
        .CE(ap_reg_ioackin_gmem1_AWREADY3_out),
        .D(gmem0_RDATA[6]),
        .Q(gmem0_addr_read_reg_195[6]),
        .R(1'b0));
  FDRE \gmem0_addr_read_reg_195_reg[7] 
       (.C(ap_clk),
        .CE(ap_reg_ioackin_gmem1_AWREADY3_out),
        .D(gmem0_RDATA[7]),
        .Q(gmem0_addr_read_reg_195[7]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[0]),
        .Q(gmem0_addr_reg_178[0]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[10] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[10]),
        .Q(gmem0_addr_reg_178[10]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[11] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[11]),
        .Q(gmem0_addr_reg_178[11]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[12] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[12]),
        .Q(gmem0_addr_reg_178[12]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[13] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[13]),
        .Q(gmem0_addr_reg_178[13]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[14] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[14]),
        .Q(gmem0_addr_reg_178[14]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[15] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[15]),
        .Q(gmem0_addr_reg_178[15]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[16] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[16]),
        .Q(gmem0_addr_reg_178[16]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[17] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[17]),
        .Q(gmem0_addr_reg_178[17]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[18] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[18]),
        .Q(gmem0_addr_reg_178[18]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[19] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[19]),
        .Q(gmem0_addr_reg_178[19]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[1]),
        .Q(gmem0_addr_reg_178[1]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[20] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[20]),
        .Q(gmem0_addr_reg_178[20]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[21] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[21]),
        .Q(gmem0_addr_reg_178[21]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[22] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[22]),
        .Q(gmem0_addr_reg_178[22]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[23] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[23]),
        .Q(gmem0_addr_reg_178[23]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[24] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[24]),
        .Q(gmem0_addr_reg_178[24]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[25] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[25]),
        .Q(gmem0_addr_reg_178[25]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[26] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[26]),
        .Q(gmem0_addr_reg_178[26]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[27] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[27]),
        .Q(gmem0_addr_reg_178[27]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[28] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[28]),
        .Q(gmem0_addr_reg_178[28]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[29] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[29]),
        .Q(gmem0_addr_reg_178[29]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[2]),
        .Q(gmem0_addr_reg_178[2]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[30] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[30]),
        .Q(gmem0_addr_reg_178[30]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[31] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[31]),
        .Q(gmem0_addr_reg_178[31]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[3]),
        .Q(gmem0_addr_reg_178[3]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[4]),
        .Q(gmem0_addr_reg_178[4]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[5]),
        .Q(gmem0_addr_reg_178[5]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[6] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[6]),
        .Q(gmem0_addr_reg_178[6]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[7] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[7]),
        .Q(gmem0_addr_reg_178[7]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[8] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[8]),
        .Q(gmem0_addr_reg_178[8]),
        .R(1'b0));
  FDRE \gmem0_addr_reg_178_reg[9] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(inter_pix[9]),
        .Q(gmem0_addr_reg_178[9]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[11]_i_2 
       (.I0(j_reg_104[11]),
        .I1(tmp_cast_reg_173[11]),
        .O(\gmem1_addr_reg_184[11]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[11]_i_3 
       (.I0(j_reg_104[10]),
        .I1(tmp_cast_reg_173[10]),
        .O(\gmem1_addr_reg_184[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[11]_i_4 
       (.I0(j_reg_104[9]),
        .I1(tmp_cast_reg_173[9]),
        .O(\gmem1_addr_reg_184[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[11]_i_5 
       (.I0(j_reg_104[8]),
        .I1(tmp_cast_reg_173[8]),
        .O(\gmem1_addr_reg_184[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[15]_i_2 
       (.I0(j_reg_104[15]),
        .I1(tmp_cast_reg_173[15]),
        .O(\gmem1_addr_reg_184[15]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[15]_i_3 
       (.I0(j_reg_104[14]),
        .I1(tmp_cast_reg_173[14]),
        .O(\gmem1_addr_reg_184[15]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[15]_i_4 
       (.I0(j_reg_104[13]),
        .I1(tmp_cast_reg_173[13]),
        .O(\gmem1_addr_reg_184[15]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[15]_i_5 
       (.I0(j_reg_104[12]),
        .I1(tmp_cast_reg_173[12]),
        .O(\gmem1_addr_reg_184[15]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[19]_i_2 
       (.I0(j_reg_104[19]),
        .I1(tmp_cast_reg_173[19]),
        .O(\gmem1_addr_reg_184[19]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[19]_i_3 
       (.I0(j_reg_104[18]),
        .I1(tmp_cast_reg_173[18]),
        .O(\gmem1_addr_reg_184[19]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[19]_i_4 
       (.I0(j_reg_104[17]),
        .I1(tmp_cast_reg_173[17]),
        .O(\gmem1_addr_reg_184[19]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[19]_i_5 
       (.I0(j_reg_104[16]),
        .I1(tmp_cast_reg_173[16]),
        .O(\gmem1_addr_reg_184[19]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[23]_i_2 
       (.I0(j_reg_104[23]),
        .I1(tmp_cast_reg_173[23]),
        .O(\gmem1_addr_reg_184[23]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[23]_i_3 
       (.I0(j_reg_104[22]),
        .I1(tmp_cast_reg_173[22]),
        .O(\gmem1_addr_reg_184[23]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[23]_i_4 
       (.I0(j_reg_104[21]),
        .I1(tmp_cast_reg_173[21]),
        .O(\gmem1_addr_reg_184[23]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[23]_i_5 
       (.I0(j_reg_104[20]),
        .I1(tmp_cast_reg_173[20]),
        .O(\gmem1_addr_reg_184[23]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[27]_i_2 
       (.I0(j_reg_104[27]),
        .I1(tmp_cast_reg_173[27]),
        .O(\gmem1_addr_reg_184[27]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[27]_i_3 
       (.I0(j_reg_104[26]),
        .I1(tmp_cast_reg_173[26]),
        .O(\gmem1_addr_reg_184[27]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[27]_i_4 
       (.I0(j_reg_104[25]),
        .I1(tmp_cast_reg_173[25]),
        .O(\gmem1_addr_reg_184[27]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[27]_i_5 
       (.I0(j_reg_104[24]),
        .I1(tmp_cast_reg_173[24]),
        .O(\gmem1_addr_reg_184[27]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[29]_i_2 
       (.I0(j_reg_104[29]),
        .I1(tmp_cast_reg_173[29]),
        .O(\gmem1_addr_reg_184[29]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[29]_i_3 
       (.I0(j_reg_104[28]),
        .I1(tmp_cast_reg_173[28]),
        .O(\gmem1_addr_reg_184[29]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[3]_i_2 
       (.I0(j_reg_104[3]),
        .I1(tmp_cast_reg_173[3]),
        .O(\gmem1_addr_reg_184[3]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[3]_i_3 
       (.I0(j_reg_104[2]),
        .I1(tmp_cast_reg_173[2]),
        .O(\gmem1_addr_reg_184[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[3]_i_4 
       (.I0(j_reg_104[1]),
        .I1(tmp_cast_reg_173[1]),
        .O(\gmem1_addr_reg_184[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[3]_i_5 
       (.I0(j_reg_104[0]),
        .I1(tmp_cast_reg_173[0]),
        .O(\gmem1_addr_reg_184[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[7]_i_2 
       (.I0(j_reg_104[7]),
        .I1(tmp_cast_reg_173[7]),
        .O(\gmem1_addr_reg_184[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[7]_i_3 
       (.I0(j_reg_104[6]),
        .I1(tmp_cast_reg_173[6]),
        .O(\gmem1_addr_reg_184[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[7]_i_4 
       (.I0(j_reg_104[5]),
        .I1(tmp_cast_reg_173[5]),
        .O(\gmem1_addr_reg_184[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \gmem1_addr_reg_184[7]_i_5 
       (.I0(j_reg_104[4]),
        .I1(tmp_cast_reg_173[4]),
        .O(\gmem1_addr_reg_184[7]_i_5_n_0 ));
  FDRE \gmem1_addr_reg_184_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[0]),
        .Q(gmem1_addr_reg_184[0]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[10] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[10]),
        .Q(gmem1_addr_reg_184[10]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[11] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[11]),
        .Q(gmem1_addr_reg_184[11]),
        .R(1'b0));
  CARRY4 \gmem1_addr_reg_184_reg[11]_i_1 
       (.CI(\gmem1_addr_reg_184_reg[7]_i_1_n_0 ),
        .CO({\gmem1_addr_reg_184_reg[11]_i_1_n_0 ,\gmem1_addr_reg_184_reg[11]_i_1_n_1 ,\gmem1_addr_reg_184_reg[11]_i_1_n_2 ,\gmem1_addr_reg_184_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(j_reg_104[11:8]),
        .O(out_pix4_sum_fu_143_p2[11:8]),
        .S({\gmem1_addr_reg_184[11]_i_2_n_0 ,\gmem1_addr_reg_184[11]_i_3_n_0 ,\gmem1_addr_reg_184[11]_i_4_n_0 ,\gmem1_addr_reg_184[11]_i_5_n_0 }));
  FDRE \gmem1_addr_reg_184_reg[12] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[12]),
        .Q(gmem1_addr_reg_184[12]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[13] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[13]),
        .Q(gmem1_addr_reg_184[13]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[14] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[14]),
        .Q(gmem1_addr_reg_184[14]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[15] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[15]),
        .Q(gmem1_addr_reg_184[15]),
        .R(1'b0));
  CARRY4 \gmem1_addr_reg_184_reg[15]_i_1 
       (.CI(\gmem1_addr_reg_184_reg[11]_i_1_n_0 ),
        .CO({\gmem1_addr_reg_184_reg[15]_i_1_n_0 ,\gmem1_addr_reg_184_reg[15]_i_1_n_1 ,\gmem1_addr_reg_184_reg[15]_i_1_n_2 ,\gmem1_addr_reg_184_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(j_reg_104[15:12]),
        .O(out_pix4_sum_fu_143_p2[15:12]),
        .S({\gmem1_addr_reg_184[15]_i_2_n_0 ,\gmem1_addr_reg_184[15]_i_3_n_0 ,\gmem1_addr_reg_184[15]_i_4_n_0 ,\gmem1_addr_reg_184[15]_i_5_n_0 }));
  FDRE \gmem1_addr_reg_184_reg[16] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[16]),
        .Q(gmem1_addr_reg_184[16]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[17] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[17]),
        .Q(gmem1_addr_reg_184[17]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[18] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[18]),
        .Q(gmem1_addr_reg_184[18]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[19] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[19]),
        .Q(gmem1_addr_reg_184[19]),
        .R(1'b0));
  CARRY4 \gmem1_addr_reg_184_reg[19]_i_1 
       (.CI(\gmem1_addr_reg_184_reg[15]_i_1_n_0 ),
        .CO({\gmem1_addr_reg_184_reg[19]_i_1_n_0 ,\gmem1_addr_reg_184_reg[19]_i_1_n_1 ,\gmem1_addr_reg_184_reg[19]_i_1_n_2 ,\gmem1_addr_reg_184_reg[19]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(j_reg_104[19:16]),
        .O(out_pix4_sum_fu_143_p2[19:16]),
        .S({\gmem1_addr_reg_184[19]_i_2_n_0 ,\gmem1_addr_reg_184[19]_i_3_n_0 ,\gmem1_addr_reg_184[19]_i_4_n_0 ,\gmem1_addr_reg_184[19]_i_5_n_0 }));
  FDRE \gmem1_addr_reg_184_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[1]),
        .Q(gmem1_addr_reg_184[1]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[20] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[20]),
        .Q(gmem1_addr_reg_184[20]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[21] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[21]),
        .Q(gmem1_addr_reg_184[21]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[22] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[22]),
        .Q(gmem1_addr_reg_184[22]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[23] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[23]),
        .Q(gmem1_addr_reg_184[23]),
        .R(1'b0));
  CARRY4 \gmem1_addr_reg_184_reg[23]_i_1 
       (.CI(\gmem1_addr_reg_184_reg[19]_i_1_n_0 ),
        .CO({\gmem1_addr_reg_184_reg[23]_i_1_n_0 ,\gmem1_addr_reg_184_reg[23]_i_1_n_1 ,\gmem1_addr_reg_184_reg[23]_i_1_n_2 ,\gmem1_addr_reg_184_reg[23]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(j_reg_104[23:20]),
        .O(out_pix4_sum_fu_143_p2[23:20]),
        .S({\gmem1_addr_reg_184[23]_i_2_n_0 ,\gmem1_addr_reg_184[23]_i_3_n_0 ,\gmem1_addr_reg_184[23]_i_4_n_0 ,\gmem1_addr_reg_184[23]_i_5_n_0 }));
  FDRE \gmem1_addr_reg_184_reg[24] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[24]),
        .Q(gmem1_addr_reg_184[24]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[25] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[25]),
        .Q(gmem1_addr_reg_184[25]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[26] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[26]),
        .Q(gmem1_addr_reg_184[26]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[27] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[27]),
        .Q(gmem1_addr_reg_184[27]),
        .R(1'b0));
  CARRY4 \gmem1_addr_reg_184_reg[27]_i_1 
       (.CI(\gmem1_addr_reg_184_reg[23]_i_1_n_0 ),
        .CO({\gmem1_addr_reg_184_reg[27]_i_1_n_0 ,\gmem1_addr_reg_184_reg[27]_i_1_n_1 ,\gmem1_addr_reg_184_reg[27]_i_1_n_2 ,\gmem1_addr_reg_184_reg[27]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(j_reg_104[27:24]),
        .O(out_pix4_sum_fu_143_p2[27:24]),
        .S({\gmem1_addr_reg_184[27]_i_2_n_0 ,\gmem1_addr_reg_184[27]_i_3_n_0 ,\gmem1_addr_reg_184[27]_i_4_n_0 ,\gmem1_addr_reg_184[27]_i_5_n_0 }));
  FDRE \gmem1_addr_reg_184_reg[28] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[28]),
        .Q(gmem1_addr_reg_184[28]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[29] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[29]),
        .Q(gmem1_addr_reg_184[29]),
        .R(1'b0));
  CARRY4 \gmem1_addr_reg_184_reg[29]_i_1 
       (.CI(\gmem1_addr_reg_184_reg[27]_i_1_n_0 ),
        .CO({\NLW_gmem1_addr_reg_184_reg[29]_i_1_CO_UNCONNECTED [3:1],\gmem1_addr_reg_184_reg[29]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,j_reg_104[28]}),
        .O({\NLW_gmem1_addr_reg_184_reg[29]_i_1_O_UNCONNECTED [3:2],out_pix4_sum_fu_143_p2[29:28]}),
        .S({1'b0,1'b0,\gmem1_addr_reg_184[29]_i_2_n_0 ,\gmem1_addr_reg_184[29]_i_3_n_0 }));
  FDRE \gmem1_addr_reg_184_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[2]),
        .Q(gmem1_addr_reg_184[2]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[3]),
        .Q(gmem1_addr_reg_184[3]),
        .R(1'b0));
  CARRY4 \gmem1_addr_reg_184_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\gmem1_addr_reg_184_reg[3]_i_1_n_0 ,\gmem1_addr_reg_184_reg[3]_i_1_n_1 ,\gmem1_addr_reg_184_reg[3]_i_1_n_2 ,\gmem1_addr_reg_184_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(j_reg_104[3:0]),
        .O(out_pix4_sum_fu_143_p2[3:0]),
        .S({\gmem1_addr_reg_184[3]_i_2_n_0 ,\gmem1_addr_reg_184[3]_i_3_n_0 ,\gmem1_addr_reg_184[3]_i_4_n_0 ,\gmem1_addr_reg_184[3]_i_5_n_0 }));
  FDRE \gmem1_addr_reg_184_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[4]),
        .Q(gmem1_addr_reg_184[4]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[5]),
        .Q(gmem1_addr_reg_184[5]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[6]),
        .Q(gmem1_addr_reg_184[6]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[7]),
        .Q(gmem1_addr_reg_184[7]),
        .R(1'b0));
  CARRY4 \gmem1_addr_reg_184_reg[7]_i_1 
       (.CI(\gmem1_addr_reg_184_reg[3]_i_1_n_0 ),
        .CO({\gmem1_addr_reg_184_reg[7]_i_1_n_0 ,\gmem1_addr_reg_184_reg[7]_i_1_n_1 ,\gmem1_addr_reg_184_reg[7]_i_1_n_2 ,\gmem1_addr_reg_184_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(j_reg_104[7:4]),
        .O(out_pix4_sum_fu_143_p2[7:4]),
        .S({\gmem1_addr_reg_184[7]_i_2_n_0 ,\gmem1_addr_reg_184[7]_i_3_n_0 ,\gmem1_addr_reg_184[7]_i_4_n_0 ,\gmem1_addr_reg_184[7]_i_5_n_0 }));
  FDRE \gmem1_addr_reg_184_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[8]),
        .Q(gmem1_addr_reg_184[8]),
        .R(1'b0));
  FDRE \gmem1_addr_reg_184_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(out_pix4_sum_fu_143_p2[9]),
        .Q(gmem1_addr_reg_184[9]),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \j_1_reg_190[0]_i_1 
       (.I0(j_reg_104[0]),
        .O(j_1_fu_158_p2[0]));
  FDRE \j_1_reg_190_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[0]),
        .Q(j_1_reg_190[0]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[10] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[10]),
        .Q(j_1_reg_190[10]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[11] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[11]),
        .Q(j_1_reg_190[11]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[12] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[12]),
        .Q(j_1_reg_190[12]),
        .R(1'b0));
  CARRY4 \j_1_reg_190_reg[12]_i_1 
       (.CI(\j_1_reg_190_reg[8]_i_1_n_0 ),
        .CO({\j_1_reg_190_reg[12]_i_1_n_0 ,\j_1_reg_190_reg[12]_i_1_n_1 ,\j_1_reg_190_reg[12]_i_1_n_2 ,\j_1_reg_190_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(j_1_fu_158_p2[12:9]),
        .S(j_reg_104[12:9]));
  FDRE \j_1_reg_190_reg[13] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[13]),
        .Q(j_1_reg_190[13]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[14] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[14]),
        .Q(j_1_reg_190[14]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[15] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[15]),
        .Q(j_1_reg_190[15]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[16] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[16]),
        .Q(j_1_reg_190[16]),
        .R(1'b0));
  CARRY4 \j_1_reg_190_reg[16]_i_1 
       (.CI(\j_1_reg_190_reg[12]_i_1_n_0 ),
        .CO({\j_1_reg_190_reg[16]_i_1_n_0 ,\j_1_reg_190_reg[16]_i_1_n_1 ,\j_1_reg_190_reg[16]_i_1_n_2 ,\j_1_reg_190_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(j_1_fu_158_p2[16:13]),
        .S(j_reg_104[16:13]));
  FDRE \j_1_reg_190_reg[17] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[17]),
        .Q(j_1_reg_190[17]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[18] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[18]),
        .Q(j_1_reg_190[18]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[19] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[19]),
        .Q(j_1_reg_190[19]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[1]),
        .Q(j_1_reg_190[1]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[20] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[20]),
        .Q(j_1_reg_190[20]),
        .R(1'b0));
  CARRY4 \j_1_reg_190_reg[20]_i_1 
       (.CI(\j_1_reg_190_reg[16]_i_1_n_0 ),
        .CO({\j_1_reg_190_reg[20]_i_1_n_0 ,\j_1_reg_190_reg[20]_i_1_n_1 ,\j_1_reg_190_reg[20]_i_1_n_2 ,\j_1_reg_190_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(j_1_fu_158_p2[20:17]),
        .S(j_reg_104[20:17]));
  FDRE \j_1_reg_190_reg[21] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[21]),
        .Q(j_1_reg_190[21]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[22] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[22]),
        .Q(j_1_reg_190[22]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[23] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[23]),
        .Q(j_1_reg_190[23]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[24] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[24]),
        .Q(j_1_reg_190[24]),
        .R(1'b0));
  CARRY4 \j_1_reg_190_reg[24]_i_1 
       (.CI(\j_1_reg_190_reg[20]_i_1_n_0 ),
        .CO({\j_1_reg_190_reg[24]_i_1_n_0 ,\j_1_reg_190_reg[24]_i_1_n_1 ,\j_1_reg_190_reg[24]_i_1_n_2 ,\j_1_reg_190_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(j_1_fu_158_p2[24:21]),
        .S(j_reg_104[24:21]));
  FDRE \j_1_reg_190_reg[25] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[25]),
        .Q(j_1_reg_190[25]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[26] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[26]),
        .Q(j_1_reg_190[26]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[27] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[27]),
        .Q(j_1_reg_190[27]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[28] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[28]),
        .Q(j_1_reg_190[28]),
        .R(1'b0));
  CARRY4 \j_1_reg_190_reg[28]_i_1 
       (.CI(\j_1_reg_190_reg[24]_i_1_n_0 ),
        .CO({\j_1_reg_190_reg[28]_i_1_n_0 ,\j_1_reg_190_reg[28]_i_1_n_1 ,\j_1_reg_190_reg[28]_i_1_n_2 ,\j_1_reg_190_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(j_1_fu_158_p2[28:25]),
        .S(j_reg_104[28:25]));
  FDRE \j_1_reg_190_reg[29] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[29]),
        .Q(j_1_reg_190[29]),
        .R(1'b0));
  CARRY4 \j_1_reg_190_reg[29]_i_1 
       (.CI(\j_1_reg_190_reg[28]_i_1_n_0 ),
        .CO(\NLW_j_1_reg_190_reg[29]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_j_1_reg_190_reg[29]_i_1_O_UNCONNECTED [3:1],j_1_fu_158_p2[29]}),
        .S({1'b0,1'b0,1'b0,j_reg_104[29]}));
  FDRE \j_1_reg_190_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[2]),
        .Q(j_1_reg_190[2]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[3]),
        .Q(j_1_reg_190[3]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[4]),
        .Q(j_1_reg_190[4]),
        .R(1'b0));
  CARRY4 \j_1_reg_190_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\j_1_reg_190_reg[4]_i_1_n_0 ,\j_1_reg_190_reg[4]_i_1_n_1 ,\j_1_reg_190_reg[4]_i_1_n_2 ,\j_1_reg_190_reg[4]_i_1_n_3 }),
        .CYINIT(j_reg_104[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(j_1_fu_158_p2[4:1]),
        .S(j_reg_104[4:1]));
  FDRE \j_1_reg_190_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[5]),
        .Q(j_1_reg_190[5]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[6]),
        .Q(j_1_reg_190[6]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[7]),
        .Q(j_1_reg_190[7]),
        .R(1'b0));
  FDRE \j_1_reg_190_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[8]),
        .Q(j_1_reg_190[8]),
        .R(1'b0));
  CARRY4 \j_1_reg_190_reg[8]_i_1 
       (.CI(\j_1_reg_190_reg[4]_i_1_n_0 ),
        .CO({\j_1_reg_190_reg[8]_i_1_n_0 ,\j_1_reg_190_reg[8]_i_1_n_1 ,\j_1_reg_190_reg[8]_i_1_n_2 ,\j_1_reg_190_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(j_1_fu_158_p2[8:5]),
        .S(j_reg_104[8:5]));
  FDRE \j_1_reg_190_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(j_1_fu_158_p2[9]),
        .Q(j_1_reg_190[9]),
        .R(1'b0));
  FDRE \j_reg_104_reg[0] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[0]),
        .Q(j_reg_104[0]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[10] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[10]),
        .Q(j_reg_104[10]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[11] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[11]),
        .Q(j_reg_104[11]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[12] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[12]),
        .Q(j_reg_104[12]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[13] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[13]),
        .Q(j_reg_104[13]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[14] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[14]),
        .Q(j_reg_104[14]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[15] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[15]),
        .Q(j_reg_104[15]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[16] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[16]),
        .Q(j_reg_104[16]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[17] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[17]),
        .Q(j_reg_104[17]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[18] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[18]),
        .Q(j_reg_104[18]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[19] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[19]),
        .Q(j_reg_104[19]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[1] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[1]),
        .Q(j_reg_104[1]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[20] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[20]),
        .Q(j_reg_104[20]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[21] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[21]),
        .Q(j_reg_104[21]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[22] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[22]),
        .Q(j_reg_104[22]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[23] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[23]),
        .Q(j_reg_104[23]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[24] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[24]),
        .Q(j_reg_104[24]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[25] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[25]),
        .Q(j_reg_104[25]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[26] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[26]),
        .Q(j_reg_104[26]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[27] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[27]),
        .Q(j_reg_104[27]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[28] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[28]),
        .Q(j_reg_104[28]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[29] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[29]),
        .Q(j_reg_104[29]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[2] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[2]),
        .Q(j_reg_104[2]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[3] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[3]),
        .Q(j_reg_104[3]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[4] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[4]),
        .Q(j_reg_104[4]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[5] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[5]),
        .Q(j_reg_104[5]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[6] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[6]),
        .Q(j_reg_104[6]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[7] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[7]),
        .Q(j_reg_104[7]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[8] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[8]),
        .Q(j_reg_104[8]),
        .R(ap_NS_fsm19_out));
  FDRE \j_reg_104_reg[9] 
       (.C(ap_clk),
        .CE(I_BREADY),
        .D(j_1_reg_190[9]),
        .Q(j_reg_104[9]),
        .R(ap_NS_fsm19_out));
  FDRE \tmp_cast_reg_173_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[2]),
        .Q(tmp_cast_reg_173[0]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[10] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[12]),
        .Q(tmp_cast_reg_173[10]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[11] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[13]),
        .Q(tmp_cast_reg_173[11]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[12] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[14]),
        .Q(tmp_cast_reg_173[12]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[13] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[15]),
        .Q(tmp_cast_reg_173[13]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[14] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[16]),
        .Q(tmp_cast_reg_173[14]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[15] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[17]),
        .Q(tmp_cast_reg_173[15]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[16] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[18]),
        .Q(tmp_cast_reg_173[16]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[17] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[19]),
        .Q(tmp_cast_reg_173[17]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[18] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[20]),
        .Q(tmp_cast_reg_173[18]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[19] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[21]),
        .Q(tmp_cast_reg_173[19]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[3]),
        .Q(tmp_cast_reg_173[1]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[20] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[22]),
        .Q(tmp_cast_reg_173[20]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[21] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[23]),
        .Q(tmp_cast_reg_173[21]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[22] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[24]),
        .Q(tmp_cast_reg_173[22]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[23] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[25]),
        .Q(tmp_cast_reg_173[23]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[24] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[26]),
        .Q(tmp_cast_reg_173[24]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[25] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[27]),
        .Q(tmp_cast_reg_173[25]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[26] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[28]),
        .Q(tmp_cast_reg_173[26]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[27] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[29]),
        .Q(tmp_cast_reg_173[27]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[28] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[30]),
        .Q(tmp_cast_reg_173[28]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[29] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[31]),
        .Q(tmp_cast_reg_173[29]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[4]),
        .Q(tmp_cast_reg_173[2]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[5]),
        .Q(tmp_cast_reg_173[3]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[6]),
        .Q(tmp_cast_reg_173[4]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[7]),
        .Q(tmp_cast_reg_173[5]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[6] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[8]),
        .Q(tmp_cast_reg_173[6]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[7] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[9]),
        .Q(tmp_cast_reg_173[7]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[8] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[10]),
        .Q(tmp_cast_reg_173[8]),
        .R(1'b0));
  FDRE \tmp_cast_reg_173_reg[9] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(out_pix[11]),
        .Q(tmp_cast_reg_173[9]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_AXILiteS_s_axi
   (D,
    SR,
    s_axi_AXILiteS_BVALID,
    \FSM_onehot_wstate_reg[2]_0 ,
    \FSM_onehot_wstate_reg[1]_0 ,
    s_axi_AXILiteS_RVALID,
    \FSM_onehot_rstate_reg[1]_0 ,
    inter_pix,
    out_pix,
    s_axi_AXILiteS_RDATA,
    interrupt,
    \ap_CS_fsm_reg[0] ,
    Q,
    \ap_CS_fsm_reg[0]_0 ,
    I_BVALID,
    gmem0_ARREADY,
    ARESET,
    ap_clk,
    s_axi_AXILiteS_AWADDR,
    s_axi_AXILiteS_WDATA,
    s_axi_AXILiteS_WSTRB,
    s_axi_AXILiteS_AWVALID,
    s_axi_AXILiteS_BREADY,
    s_axi_AXILiteS_WVALID,
    s_axi_AXILiteS_ARADDR,
    s_axi_AXILiteS_ARVALID,
    s_axi_AXILiteS_RREADY);
  output [1:0]D;
  output [0:0]SR;
  output s_axi_AXILiteS_BVALID;
  output \FSM_onehot_wstate_reg[2]_0 ;
  output \FSM_onehot_wstate_reg[1]_0 ;
  output s_axi_AXILiteS_RVALID;
  output \FSM_onehot_rstate_reg[1]_0 ;
  output [31:0]inter_pix;
  output [29:0]out_pix;
  output [31:0]s_axi_AXILiteS_RDATA;
  output interrupt;
  input \ap_CS_fsm_reg[0] ;
  input [3:0]Q;
  input \ap_CS_fsm_reg[0]_0 ;
  input I_BVALID;
  input gmem0_ARREADY;
  input ARESET;
  input ap_clk;
  input [4:0]s_axi_AXILiteS_AWADDR;
  input [31:0]s_axi_AXILiteS_WDATA;
  input [3:0]s_axi_AXILiteS_WSTRB;
  input s_axi_AXILiteS_AWVALID;
  input s_axi_AXILiteS_BREADY;
  input s_axi_AXILiteS_WVALID;
  input [4:0]s_axi_AXILiteS_ARADDR;
  input s_axi_AXILiteS_ARVALID;
  input s_axi_AXILiteS_RREADY;

  wire ARESET;
  wire [1:0]D;
  wire \FSM_onehot_rstate_reg[1]_0 ;
  wire \FSM_onehot_wstate[1]_i_1_n_0 ;
  wire \FSM_onehot_wstate[2]_i_1_n_0 ;
  wire \FSM_onehot_wstate[3]_i_1_n_0 ;
  wire \FSM_onehot_wstate_reg[1]_0 ;
  wire \FSM_onehot_wstate_reg[2]_0 ;
  wire I_BVALID;
  wire [3:0]Q;
  wire [0:0]SR;
  wire \ap_CS_fsm_reg[0] ;
  wire \ap_CS_fsm_reg[0]_0 ;
  wire ap_clk;
  wire ap_idle;
  wire ap_start;
  wire ar_hs;
  wire [7:2]data0;
  wire gmem0_ARREADY;
  wire int_ap_start_i_1_n_0;
  wire int_ap_start_i_2_n_0;
  wire int_ap_start_i_3_n_0;
  wire int_auto_restart_i_1_n_0;
  wire int_gie_i_1_n_0;
  wire int_gie_i_2_n_0;
  wire int_gie_reg_n_0;
  wire \int_ier[0]_i_1_n_0 ;
  wire \int_ier[1]_i_1_n_0 ;
  wire \int_ier_reg_n_0_[0] ;
  wire \int_isr[0]_i_1_n_0 ;
  wire \int_isr[1]_i_1_n_0 ;
  wire \int_isr_reg_n_0_[0] ;
  wire \int_out_pix_reg_n_0_[0] ;
  wire \int_out_pix_reg_n_0_[1] ;
  wire [31:0]inter_pix;
  wire interrupt;
  wire [31:0]\or ;
  wire [31:0]or0_out;
  wire [29:0]out_pix;
  wire p_0_in;
  wire p_0_in11_out;
  wire p_0_in13_out;
  wire p_1_in;
  wire [31:0]rdata_data;
  wire \rdata_data[0]_i_2_n_0 ;
  wire \rdata_data[1]_i_2_n_0 ;
  wire \rdata_data[1]_i_3_n_0 ;
  wire \rdata_data[2]_i_2_n_0 ;
  wire \rdata_data[31]_i_3_n_0 ;
  wire \rdata_data[31]_i_4_n_0 ;
  wire \rdata_data[7]_i_2_n_0 ;
  wire [2:1]rnext;
  wire [4:0]s_axi_AXILiteS_ARADDR;
  wire s_axi_AXILiteS_ARVALID;
  wire [4:0]s_axi_AXILiteS_AWADDR;
  wire s_axi_AXILiteS_AWVALID;
  wire s_axi_AXILiteS_BREADY;
  wire s_axi_AXILiteS_BVALID;
  wire [31:0]s_axi_AXILiteS_RDATA;
  wire s_axi_AXILiteS_RREADY;
  wire s_axi_AXILiteS_RVALID;
  wire [31:0]s_axi_AXILiteS_WDATA;
  wire [3:0]s_axi_AXILiteS_WSTRB;
  wire s_axi_AXILiteS_WVALID;
  wire waddr;
  wire \waddr_reg_n_0_[0] ;
  wire \waddr_reg_n_0_[1] ;
  wire \waddr_reg_n_0_[2] ;
  wire \waddr_reg_n_0_[3] ;
  wire \waddr_reg_n_0_[4] ;

  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hF747)) 
    \FSM_onehot_rstate[1]_i_1 
       (.I0(s_axi_AXILiteS_ARVALID),
        .I1(\FSM_onehot_rstate_reg[1]_0 ),
        .I2(s_axi_AXILiteS_RVALID),
        .I3(s_axi_AXILiteS_RREADY),
        .O(rnext[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h8F88)) 
    \FSM_onehot_rstate[2]_i_1 
       (.I0(s_axi_AXILiteS_ARVALID),
        .I1(\FSM_onehot_rstate_reg[1]_0 ),
        .I2(s_axi_AXILiteS_RREADY),
        .I3(s_axi_AXILiteS_RVALID),
        .O(rnext[2]));
  (* FSM_ENCODED_STATES = "rddata:100,rdidle:010,iSTATE:001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_rstate_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(rnext[1]),
        .Q(\FSM_onehot_rstate_reg[1]_0 ),
        .R(ARESET));
  (* FSM_ENCODED_STATES = "rddata:100,rdidle:010,iSTATE:001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_rstate_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(rnext[2]),
        .Q(s_axi_AXILiteS_RVALID),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hFF0C1D1D)) 
    \FSM_onehot_wstate[1]_i_1 
       (.I0(\FSM_onehot_wstate_reg[2]_0 ),
        .I1(\FSM_onehot_wstate_reg[1]_0 ),
        .I2(s_axi_AXILiteS_AWVALID),
        .I3(s_axi_AXILiteS_BREADY),
        .I4(s_axi_AXILiteS_BVALID),
        .O(\FSM_onehot_wstate[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF444)) 
    \FSM_onehot_wstate[2]_i_1 
       (.I0(s_axi_AXILiteS_WVALID),
        .I1(\FSM_onehot_wstate_reg[2]_0 ),
        .I2(s_axi_AXILiteS_AWVALID),
        .I3(\FSM_onehot_wstate_reg[1]_0 ),
        .O(\FSM_onehot_wstate[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF444)) 
    \FSM_onehot_wstate[3]_i_1 
       (.I0(s_axi_AXILiteS_BREADY),
        .I1(s_axi_AXILiteS_BVALID),
        .I2(\FSM_onehot_wstate_reg[2]_0 ),
        .I3(s_axi_AXILiteS_WVALID),
        .O(\FSM_onehot_wstate[3]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "wrdata:0100,wrresp:1000,wridle:0010,iSTATE:0001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\FSM_onehot_wstate[1]_i_1_n_0 ),
        .Q(\FSM_onehot_wstate_reg[1]_0 ),
        .R(ARESET));
  (* FSM_ENCODED_STATES = "wrdata:0100,wrresp:1000,wridle:0010,iSTATE:0001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\FSM_onehot_wstate[2]_i_1_n_0 ),
        .Q(\FSM_onehot_wstate_reg[2]_0 ),
        .R(ARESET));
  (* FSM_ENCODED_STATES = "wrdata:0100,wrresp:1000,wridle:0010,iSTATE:0001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\FSM_onehot_wstate[3]_i_1_n_0 ),
        .Q(s_axi_AXILiteS_BVALID),
        .R(ARESET));
  LUT6 #(
    .INIT(64'h0002020200000000)) 
    \ap_CS_fsm[0]_i_1 
       (.I0(\ap_CS_fsm_reg[0] ),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(ap_start),
        .I5(\ap_CS_fsm_reg[0]_0 ),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hF888FFFFF888F888)) 
    \ap_CS_fsm[1]_i_1 
       (.I0(ap_start),
        .I1(Q[0]),
        .I2(I_BVALID),
        .I3(Q[3]),
        .I4(gmem0_ARREADY),
        .I5(Q[1]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h2)) 
    int_ap_idle_i_1
       (.I0(Q[0]),
        .I1(ap_start),
        .O(ap_idle));
  FDRE int_ap_idle_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_idle),
        .Q(data0[2]),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hFFFF0800)) 
    int_ap_start_i_1
       (.I0(int_ap_start_i_2_n_0),
        .I1(s_axi_AXILiteS_WDATA[0]),
        .I2(\waddr_reg_n_0_[3] ),
        .I3(int_ap_start_i_3_n_0),
        .I4(ap_start),
        .O(int_ap_start_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h2)) 
    int_ap_start_i_2
       (.I0(s_axi_AXILiteS_WSTRB[0]),
        .I1(\waddr_reg_n_0_[4] ),
        .O(int_ap_start_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00000008)) 
    int_ap_start_i_3
       (.I0(s_axi_AXILiteS_WVALID),
        .I1(\FSM_onehot_wstate_reg[2]_0 ),
        .I2(\waddr_reg_n_0_[1] ),
        .I3(\waddr_reg_n_0_[0] ),
        .I4(\waddr_reg_n_0_[2] ),
        .O(int_ap_start_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    int_ap_start_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_ap_start_i_1_n_0),
        .Q(ap_start),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFFEFFFFF00200000)) 
    int_auto_restart_i_1
       (.I0(s_axi_AXILiteS_WDATA[7]),
        .I1(\waddr_reg_n_0_[3] ),
        .I2(s_axi_AXILiteS_WSTRB[0]),
        .I3(\waddr_reg_n_0_[4] ),
        .I4(int_ap_start_i_3_n_0),
        .I5(data0[7]),
        .O(int_auto_restart_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    int_auto_restart_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_auto_restart_i_1_n_0),
        .Q(data0[7]),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFFBFFFFF00800000)) 
    int_gie_i_1
       (.I0(s_axi_AXILiteS_WDATA[0]),
        .I1(int_ap_start_i_2_n_0),
        .I2(\waddr_reg_n_0_[2] ),
        .I3(\waddr_reg_n_0_[3] ),
        .I4(int_gie_i_2_n_0),
        .I5(int_gie_reg_n_0),
        .O(int_gie_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h1000)) 
    int_gie_i_2
       (.I0(\waddr_reg_n_0_[0] ),
        .I1(\waddr_reg_n_0_[1] ),
        .I2(\FSM_onehot_wstate_reg[2]_0 ),
        .I3(s_axi_AXILiteS_WVALID),
        .O(int_gie_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    int_gie_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_gie_i_1_n_0),
        .Q(int_gie_reg_n_0),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFFBFFFFF00800000)) 
    \int_ier[0]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[0]),
        .I1(\waddr_reg_n_0_[3] ),
        .I2(s_axi_AXILiteS_WSTRB[0]),
        .I3(\waddr_reg_n_0_[4] ),
        .I4(int_ap_start_i_3_n_0),
        .I5(\int_ier_reg_n_0_[0] ),
        .O(\int_ier[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFBFFFFF00800000)) 
    \int_ier[1]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[1]),
        .I1(\waddr_reg_n_0_[3] ),
        .I2(s_axi_AXILiteS_WSTRB[0]),
        .I3(\waddr_reg_n_0_[4] ),
        .I4(int_ap_start_i_3_n_0),
        .I5(p_0_in),
        .O(\int_ier[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \int_ier_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ier[0]_i_1_n_0 ),
        .Q(\int_ier_reg_n_0_[0] ),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_ier_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ier[1]_i_1_n_0 ),
        .Q(p_0_in),
        .R(ARESET));
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[0]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[0]),
        .I1(s_axi_AXILiteS_WSTRB[0]),
        .I2(inter_pix[0]),
        .O(or0_out[0]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[10]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[10]),
        .I1(s_axi_AXILiteS_WSTRB[1]),
        .I2(inter_pix[10]),
        .O(or0_out[10]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[11]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[11]),
        .I1(s_axi_AXILiteS_WSTRB[1]),
        .I2(inter_pix[11]),
        .O(or0_out[11]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[12]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[12]),
        .I1(s_axi_AXILiteS_WSTRB[1]),
        .I2(inter_pix[12]),
        .O(or0_out[12]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[13]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[13]),
        .I1(s_axi_AXILiteS_WSTRB[1]),
        .I2(inter_pix[13]),
        .O(or0_out[13]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[14]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[14]),
        .I1(s_axi_AXILiteS_WSTRB[1]),
        .I2(inter_pix[14]),
        .O(or0_out[14]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[15]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[15]),
        .I1(s_axi_AXILiteS_WSTRB[1]),
        .I2(inter_pix[15]),
        .O(or0_out[15]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[16]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[16]),
        .I1(s_axi_AXILiteS_WSTRB[2]),
        .I2(inter_pix[16]),
        .O(or0_out[16]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[17]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[17]),
        .I1(s_axi_AXILiteS_WSTRB[2]),
        .I2(inter_pix[17]),
        .O(or0_out[17]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[18]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[18]),
        .I1(s_axi_AXILiteS_WSTRB[2]),
        .I2(inter_pix[18]),
        .O(or0_out[18]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[19]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[19]),
        .I1(s_axi_AXILiteS_WSTRB[2]),
        .I2(inter_pix[19]),
        .O(or0_out[19]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[1]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[1]),
        .I1(s_axi_AXILiteS_WSTRB[0]),
        .I2(inter_pix[1]),
        .O(or0_out[1]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[20]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[20]),
        .I1(s_axi_AXILiteS_WSTRB[2]),
        .I2(inter_pix[20]),
        .O(or0_out[20]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[21]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[21]),
        .I1(s_axi_AXILiteS_WSTRB[2]),
        .I2(inter_pix[21]),
        .O(or0_out[21]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[22]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[22]),
        .I1(s_axi_AXILiteS_WSTRB[2]),
        .I2(inter_pix[22]),
        .O(or0_out[22]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[23]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[23]),
        .I1(s_axi_AXILiteS_WSTRB[2]),
        .I2(inter_pix[23]),
        .O(or0_out[23]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[24]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[24]),
        .I1(s_axi_AXILiteS_WSTRB[3]),
        .I2(inter_pix[24]),
        .O(or0_out[24]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[25]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[25]),
        .I1(s_axi_AXILiteS_WSTRB[3]),
        .I2(inter_pix[25]),
        .O(or0_out[25]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[26]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[26]),
        .I1(s_axi_AXILiteS_WSTRB[3]),
        .I2(inter_pix[26]),
        .O(or0_out[26]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[27]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[27]),
        .I1(s_axi_AXILiteS_WSTRB[3]),
        .I2(inter_pix[27]),
        .O(or0_out[27]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[28]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[28]),
        .I1(s_axi_AXILiteS_WSTRB[3]),
        .I2(inter_pix[28]),
        .O(or0_out[28]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[29]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[29]),
        .I1(s_axi_AXILiteS_WSTRB[3]),
        .I2(inter_pix[29]),
        .O(or0_out[29]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[2]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[2]),
        .I1(s_axi_AXILiteS_WSTRB[0]),
        .I2(inter_pix[2]),
        .O(or0_out[2]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[30]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[30]),
        .I1(s_axi_AXILiteS_WSTRB[3]),
        .I2(inter_pix[30]),
        .O(or0_out[30]));
  LUT3 #(
    .INIT(8'h40)) 
    \int_inter_pix[31]_i_1 
       (.I0(\waddr_reg_n_0_[3] ),
        .I1(\waddr_reg_n_0_[4] ),
        .I2(int_ap_start_i_3_n_0),
        .O(p_0_in13_out));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[31]_i_2 
       (.I0(s_axi_AXILiteS_WDATA[31]),
        .I1(s_axi_AXILiteS_WSTRB[3]),
        .I2(inter_pix[31]),
        .O(or0_out[31]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[3]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[3]),
        .I1(s_axi_AXILiteS_WSTRB[0]),
        .I2(inter_pix[3]),
        .O(or0_out[3]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[4]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[4]),
        .I1(s_axi_AXILiteS_WSTRB[0]),
        .I2(inter_pix[4]),
        .O(or0_out[4]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[5]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[5]),
        .I1(s_axi_AXILiteS_WSTRB[0]),
        .I2(inter_pix[5]),
        .O(or0_out[5]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[6]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[6]),
        .I1(s_axi_AXILiteS_WSTRB[0]),
        .I2(inter_pix[6]),
        .O(or0_out[6]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[7]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[7]),
        .I1(s_axi_AXILiteS_WSTRB[0]),
        .I2(inter_pix[7]),
        .O(or0_out[7]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[8]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[8]),
        .I1(s_axi_AXILiteS_WSTRB[1]),
        .I2(inter_pix[8]),
        .O(or0_out[8]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_inter_pix[9]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[9]),
        .I1(s_axi_AXILiteS_WSTRB[1]),
        .I2(inter_pix[9]),
        .O(or0_out[9]));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[0] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[0]),
        .Q(inter_pix[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[10] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[10]),
        .Q(inter_pix[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[11] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[11]),
        .Q(inter_pix[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[12] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[12]),
        .Q(inter_pix[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[13] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[13]),
        .Q(inter_pix[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[14] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[14]),
        .Q(inter_pix[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[15] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[15]),
        .Q(inter_pix[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[16] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[16]),
        .Q(inter_pix[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[17] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[17]),
        .Q(inter_pix[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[18] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[18]),
        .Q(inter_pix[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[19] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[19]),
        .Q(inter_pix[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[1] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[1]),
        .Q(inter_pix[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[20] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[20]),
        .Q(inter_pix[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[21] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[21]),
        .Q(inter_pix[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[22] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[22]),
        .Q(inter_pix[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[23] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[23]),
        .Q(inter_pix[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[24] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[24]),
        .Q(inter_pix[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[25] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[25]),
        .Q(inter_pix[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[26] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[26]),
        .Q(inter_pix[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[27] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[27]),
        .Q(inter_pix[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[28] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[28]),
        .Q(inter_pix[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[29] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[29]),
        .Q(inter_pix[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[2] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[2]),
        .Q(inter_pix[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[30] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[30]),
        .Q(inter_pix[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[31] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[31]),
        .Q(inter_pix[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[3] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[3]),
        .Q(inter_pix[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[4] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[4]),
        .Q(inter_pix[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[5] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[5]),
        .Q(inter_pix[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[6] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[6]),
        .Q(inter_pix[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[7] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[7]),
        .Q(inter_pix[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[8] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[8]),
        .Q(inter_pix[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_inter_pix_reg[9] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(or0_out[9]),
        .Q(inter_pix[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \int_isr[0]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[0]),
        .I1(int_ap_start_i_2_n_0),
        .I2(\waddr_reg_n_0_[2] ),
        .I3(\waddr_reg_n_0_[3] ),
        .I4(int_gie_i_2_n_0),
        .I5(\int_isr_reg_n_0_[0] ),
        .O(\int_isr[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \int_isr[1]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[1]),
        .I1(int_ap_start_i_2_n_0),
        .I2(\waddr_reg_n_0_[2] ),
        .I3(\waddr_reg_n_0_[3] ),
        .I4(int_gie_i_2_n_0),
        .I5(p_1_in),
        .O(\int_isr[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \int_isr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_isr[0]_i_1_n_0 ),
        .Q(\int_isr_reg_n_0_[0] ),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_isr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_isr[1]_i_1_n_0 ),
        .Q(p_1_in),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[0]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[0]),
        .I1(s_axi_AXILiteS_WSTRB[0]),
        .I2(\int_out_pix_reg_n_0_[0] ),
        .O(\or [0]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[10]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[10]),
        .I1(s_axi_AXILiteS_WSTRB[1]),
        .I2(out_pix[8]),
        .O(\or [10]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[11]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[11]),
        .I1(s_axi_AXILiteS_WSTRB[1]),
        .I2(out_pix[9]),
        .O(\or [11]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[12]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[12]),
        .I1(s_axi_AXILiteS_WSTRB[1]),
        .I2(out_pix[10]),
        .O(\or [12]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[13]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[13]),
        .I1(s_axi_AXILiteS_WSTRB[1]),
        .I2(out_pix[11]),
        .O(\or [13]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[14]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[14]),
        .I1(s_axi_AXILiteS_WSTRB[1]),
        .I2(out_pix[12]),
        .O(\or [14]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[15]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[15]),
        .I1(s_axi_AXILiteS_WSTRB[1]),
        .I2(out_pix[13]),
        .O(\or [15]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[16]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[16]),
        .I1(s_axi_AXILiteS_WSTRB[2]),
        .I2(out_pix[14]),
        .O(\or [16]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[17]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[17]),
        .I1(s_axi_AXILiteS_WSTRB[2]),
        .I2(out_pix[15]),
        .O(\or [17]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[18]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[18]),
        .I1(s_axi_AXILiteS_WSTRB[2]),
        .I2(out_pix[16]),
        .O(\or [18]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[19]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[19]),
        .I1(s_axi_AXILiteS_WSTRB[2]),
        .I2(out_pix[17]),
        .O(\or [19]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[1]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[1]),
        .I1(s_axi_AXILiteS_WSTRB[0]),
        .I2(\int_out_pix_reg_n_0_[1] ),
        .O(\or [1]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[20]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[20]),
        .I1(s_axi_AXILiteS_WSTRB[2]),
        .I2(out_pix[18]),
        .O(\or [20]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[21]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[21]),
        .I1(s_axi_AXILiteS_WSTRB[2]),
        .I2(out_pix[19]),
        .O(\or [21]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[22]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[22]),
        .I1(s_axi_AXILiteS_WSTRB[2]),
        .I2(out_pix[20]),
        .O(\or [22]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[23]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[23]),
        .I1(s_axi_AXILiteS_WSTRB[2]),
        .I2(out_pix[21]),
        .O(\or [23]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[24]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[24]),
        .I1(s_axi_AXILiteS_WSTRB[3]),
        .I2(out_pix[22]),
        .O(\or [24]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[25]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[25]),
        .I1(s_axi_AXILiteS_WSTRB[3]),
        .I2(out_pix[23]),
        .O(\or [25]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[26]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[26]),
        .I1(s_axi_AXILiteS_WSTRB[3]),
        .I2(out_pix[24]),
        .O(\or [26]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[27]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[27]),
        .I1(s_axi_AXILiteS_WSTRB[3]),
        .I2(out_pix[25]),
        .O(\or [27]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[28]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[28]),
        .I1(s_axi_AXILiteS_WSTRB[3]),
        .I2(out_pix[26]),
        .O(\or [28]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[29]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[29]),
        .I1(s_axi_AXILiteS_WSTRB[3]),
        .I2(out_pix[27]),
        .O(\or [29]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[2]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[2]),
        .I1(s_axi_AXILiteS_WSTRB[0]),
        .I2(out_pix[0]),
        .O(\or [2]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[30]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[30]),
        .I1(s_axi_AXILiteS_WSTRB[3]),
        .I2(out_pix[28]),
        .O(\or [30]));
  LUT3 #(
    .INIT(8'h80)) 
    \int_out_pix[31]_i_1 
       (.I0(\waddr_reg_n_0_[4] ),
        .I1(\waddr_reg_n_0_[3] ),
        .I2(int_ap_start_i_3_n_0),
        .O(p_0_in11_out));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[31]_i_2 
       (.I0(s_axi_AXILiteS_WDATA[31]),
        .I1(s_axi_AXILiteS_WSTRB[3]),
        .I2(out_pix[29]),
        .O(\or [31]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[3]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[3]),
        .I1(s_axi_AXILiteS_WSTRB[0]),
        .I2(out_pix[1]),
        .O(\or [3]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[4]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[4]),
        .I1(s_axi_AXILiteS_WSTRB[0]),
        .I2(out_pix[2]),
        .O(\or [4]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[5]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[5]),
        .I1(s_axi_AXILiteS_WSTRB[0]),
        .I2(out_pix[3]),
        .O(\or [5]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[6]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[6]),
        .I1(s_axi_AXILiteS_WSTRB[0]),
        .I2(out_pix[4]),
        .O(\or [6]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[7]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[7]),
        .I1(s_axi_AXILiteS_WSTRB[0]),
        .I2(out_pix[5]),
        .O(\or [7]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[8]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[8]),
        .I1(s_axi_AXILiteS_WSTRB[1]),
        .I2(out_pix[6]),
        .O(\or [8]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_out_pix[9]_i_1 
       (.I0(s_axi_AXILiteS_WDATA[9]),
        .I1(s_axi_AXILiteS_WSTRB[1]),
        .I2(out_pix[7]),
        .O(\or [9]));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[0] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [0]),
        .Q(\int_out_pix_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[10] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [10]),
        .Q(out_pix[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[11] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [11]),
        .Q(out_pix[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[12] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [12]),
        .Q(out_pix[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[13] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [13]),
        .Q(out_pix[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[14] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [14]),
        .Q(out_pix[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[15] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [15]),
        .Q(out_pix[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[16] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [16]),
        .Q(out_pix[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[17] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [17]),
        .Q(out_pix[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[18] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [18]),
        .Q(out_pix[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[19] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [19]),
        .Q(out_pix[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[1] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [1]),
        .Q(\int_out_pix_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[20] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [20]),
        .Q(out_pix[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[21] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [21]),
        .Q(out_pix[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[22] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [22]),
        .Q(out_pix[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[23] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [23]),
        .Q(out_pix[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[24] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [24]),
        .Q(out_pix[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[25] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [25]),
        .Q(out_pix[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[26] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [26]),
        .Q(out_pix[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[27] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [27]),
        .Q(out_pix[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[28] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [28]),
        .Q(out_pix[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[29] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [29]),
        .Q(out_pix[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[2] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [2]),
        .Q(out_pix[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[30] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [30]),
        .Q(out_pix[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[31] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [31]),
        .Q(out_pix[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[3] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [3]),
        .Q(out_pix[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[4] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [4]),
        .Q(out_pix[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[5] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [5]),
        .Q(out_pix[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[6] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [6]),
        .Q(out_pix[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[7] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [7]),
        .Q(out_pix[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[8] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [8]),
        .Q(out_pix[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_out_pix_reg[9] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [9]),
        .Q(out_pix[7]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hE0)) 
    interrupt_INST_0
       (.I0(\int_isr_reg_n_0_[0] ),
        .I1(p_1_in),
        .I2(int_gie_reg_n_0),
        .O(interrupt));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[0]_i_1 
       (.I0(\rdata_data[31]_i_4_n_0 ),
        .I1(inter_pix[0]),
        .I2(\rdata_data[31]_i_3_n_0 ),
        .I3(\int_out_pix_reg_n_0_[0] ),
        .I4(\rdata_data[0]_i_2_n_0 ),
        .I5(\rdata_data[1]_i_2_n_0 ),
        .O(rdata_data[0]));
  LUT6 #(
    .INIT(64'hCFCFAFA0C0C0AFA0)) 
    \rdata_data[0]_i_2 
       (.I0(\int_ier_reg_n_0_[0] ),
        .I1(\int_isr_reg_n_0_[0] ),
        .I2(s_axi_AXILiteS_ARADDR[3]),
        .I3(ap_start),
        .I4(s_axi_AXILiteS_ARADDR[2]),
        .I5(int_gie_reg_n_0),
        .O(\rdata_data[0]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[10]_i_1 
       (.I0(out_pix[8]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[10]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[10]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[11]_i_1 
       (.I0(out_pix[9]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[11]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[11]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[12]_i_1 
       (.I0(out_pix[10]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[12]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[12]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[13]_i_1 
       (.I0(out_pix[11]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[13]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[13]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[14]_i_1 
       (.I0(out_pix[12]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[14]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[14]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[15]_i_1 
       (.I0(out_pix[13]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[15]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[15]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[16]_i_1 
       (.I0(out_pix[14]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[16]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[16]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[17]_i_1 
       (.I0(out_pix[15]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[17]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[17]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[18]_i_1 
       (.I0(out_pix[16]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[18]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[18]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[19]_i_1 
       (.I0(out_pix[17]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[19]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[19]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[1]_i_1 
       (.I0(\rdata_data[31]_i_4_n_0 ),
        .I1(inter_pix[1]),
        .I2(\rdata_data[31]_i_3_n_0 ),
        .I3(\int_out_pix_reg_n_0_[1] ),
        .I4(\rdata_data[1]_i_2_n_0 ),
        .I5(\rdata_data[1]_i_3_n_0 ),
        .O(rdata_data[1]));
  LUT3 #(
    .INIT(8'h01)) 
    \rdata_data[1]_i_2 
       (.I0(s_axi_AXILiteS_ARADDR[4]),
        .I1(s_axi_AXILiteS_ARADDR[1]),
        .I2(s_axi_AXILiteS_ARADDR[0]),
        .O(\rdata_data[1]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hE200)) 
    \rdata_data[1]_i_3 
       (.I0(p_0_in),
        .I1(s_axi_AXILiteS_ARADDR[2]),
        .I2(p_1_in),
        .I3(s_axi_AXILiteS_ARADDR[3]),
        .O(\rdata_data[1]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[20]_i_1 
       (.I0(out_pix[18]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[20]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[20]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[21]_i_1 
       (.I0(out_pix[19]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[21]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[21]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[22]_i_1 
       (.I0(out_pix[20]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[22]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[22]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[23]_i_1 
       (.I0(out_pix[21]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[23]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[23]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[24]_i_1 
       (.I0(out_pix[22]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[24]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[24]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[25]_i_1 
       (.I0(out_pix[23]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[25]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[25]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[26]_i_1 
       (.I0(out_pix[24]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[26]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[26]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[27]_i_1 
       (.I0(out_pix[25]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[27]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[27]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[28]_i_1 
       (.I0(out_pix[26]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[28]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[28]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[29]_i_1 
       (.I0(out_pix[27]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[29]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[29]));
  LUT5 #(
    .INIT(32'hFFFFF888)) 
    \rdata_data[2]_i_1 
       (.I0(\rdata_data[31]_i_4_n_0 ),
        .I1(inter_pix[2]),
        .I2(\rdata_data[31]_i_3_n_0 ),
        .I3(out_pix[0]),
        .I4(\rdata_data[2]_i_2_n_0 ),
        .O(rdata_data[2]));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \rdata_data[2]_i_2 
       (.I0(data0[2]),
        .I1(s_axi_AXILiteS_ARADDR[0]),
        .I2(s_axi_AXILiteS_ARADDR[1]),
        .I3(s_axi_AXILiteS_ARADDR[3]),
        .I4(s_axi_AXILiteS_ARADDR[4]),
        .I5(s_axi_AXILiteS_ARADDR[2]),
        .O(\rdata_data[2]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[30]_i_1 
       (.I0(out_pix[28]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[30]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[30]));
  LUT2 #(
    .INIT(4'h8)) 
    \rdata_data[31]_i_1 
       (.I0(s_axi_AXILiteS_ARVALID),
        .I1(\FSM_onehot_rstate_reg[1]_0 ),
        .O(ar_hs));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[31]_i_2 
       (.I0(out_pix[29]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[31]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[31]));
  LUT5 #(
    .INIT(32'h00001000)) 
    \rdata_data[31]_i_3 
       (.I0(s_axi_AXILiteS_ARADDR[0]),
        .I1(s_axi_AXILiteS_ARADDR[1]),
        .I2(s_axi_AXILiteS_ARADDR[4]),
        .I3(s_axi_AXILiteS_ARADDR[3]),
        .I4(s_axi_AXILiteS_ARADDR[2]),
        .O(\rdata_data[31]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h00000010)) 
    \rdata_data[31]_i_4 
       (.I0(s_axi_AXILiteS_ARADDR[0]),
        .I1(s_axi_AXILiteS_ARADDR[1]),
        .I2(s_axi_AXILiteS_ARADDR[4]),
        .I3(s_axi_AXILiteS_ARADDR[3]),
        .I4(s_axi_AXILiteS_ARADDR[2]),
        .O(\rdata_data[31]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[3]_i_1 
       (.I0(out_pix[1]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[3]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[3]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[4]_i_1 
       (.I0(out_pix[2]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[4]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[4]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[5]_i_1 
       (.I0(out_pix[3]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[5]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[5]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[6]_i_1 
       (.I0(out_pix[4]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[6]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[6]));
  LUT5 #(
    .INIT(32'hFFFFF888)) 
    \rdata_data[7]_i_1 
       (.I0(\rdata_data[31]_i_4_n_0 ),
        .I1(inter_pix[7]),
        .I2(\rdata_data[31]_i_3_n_0 ),
        .I3(out_pix[5]),
        .I4(\rdata_data[7]_i_2_n_0 ),
        .O(rdata_data[7]));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \rdata_data[7]_i_2 
       (.I0(data0[7]),
        .I1(s_axi_AXILiteS_ARADDR[0]),
        .I2(s_axi_AXILiteS_ARADDR[1]),
        .I3(s_axi_AXILiteS_ARADDR[3]),
        .I4(s_axi_AXILiteS_ARADDR[4]),
        .I5(s_axi_AXILiteS_ARADDR[2]),
        .O(\rdata_data[7]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[8]_i_1 
       (.I0(out_pix[6]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[8]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[8]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[9]_i_1 
       (.I0(out_pix[7]),
        .I1(\rdata_data[31]_i_3_n_0 ),
        .I2(inter_pix[9]),
        .I3(\rdata_data[31]_i_4_n_0 ),
        .O(rdata_data[9]));
  FDRE \rdata_data_reg[0] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[0]),
        .Q(s_axi_AXILiteS_RDATA[0]),
        .R(1'b0));
  FDRE \rdata_data_reg[10] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[10]),
        .Q(s_axi_AXILiteS_RDATA[10]),
        .R(1'b0));
  FDRE \rdata_data_reg[11] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[11]),
        .Q(s_axi_AXILiteS_RDATA[11]),
        .R(1'b0));
  FDRE \rdata_data_reg[12] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[12]),
        .Q(s_axi_AXILiteS_RDATA[12]),
        .R(1'b0));
  FDRE \rdata_data_reg[13] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[13]),
        .Q(s_axi_AXILiteS_RDATA[13]),
        .R(1'b0));
  FDRE \rdata_data_reg[14] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[14]),
        .Q(s_axi_AXILiteS_RDATA[14]),
        .R(1'b0));
  FDRE \rdata_data_reg[15] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[15]),
        .Q(s_axi_AXILiteS_RDATA[15]),
        .R(1'b0));
  FDRE \rdata_data_reg[16] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[16]),
        .Q(s_axi_AXILiteS_RDATA[16]),
        .R(1'b0));
  FDRE \rdata_data_reg[17] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[17]),
        .Q(s_axi_AXILiteS_RDATA[17]),
        .R(1'b0));
  FDRE \rdata_data_reg[18] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[18]),
        .Q(s_axi_AXILiteS_RDATA[18]),
        .R(1'b0));
  FDRE \rdata_data_reg[19] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[19]),
        .Q(s_axi_AXILiteS_RDATA[19]),
        .R(1'b0));
  FDRE \rdata_data_reg[1] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[1]),
        .Q(s_axi_AXILiteS_RDATA[1]),
        .R(1'b0));
  FDRE \rdata_data_reg[20] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[20]),
        .Q(s_axi_AXILiteS_RDATA[20]),
        .R(1'b0));
  FDRE \rdata_data_reg[21] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[21]),
        .Q(s_axi_AXILiteS_RDATA[21]),
        .R(1'b0));
  FDRE \rdata_data_reg[22] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[22]),
        .Q(s_axi_AXILiteS_RDATA[22]),
        .R(1'b0));
  FDRE \rdata_data_reg[23] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[23]),
        .Q(s_axi_AXILiteS_RDATA[23]),
        .R(1'b0));
  FDRE \rdata_data_reg[24] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[24]),
        .Q(s_axi_AXILiteS_RDATA[24]),
        .R(1'b0));
  FDRE \rdata_data_reg[25] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[25]),
        .Q(s_axi_AXILiteS_RDATA[25]),
        .R(1'b0));
  FDRE \rdata_data_reg[26] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[26]),
        .Q(s_axi_AXILiteS_RDATA[26]),
        .R(1'b0));
  FDRE \rdata_data_reg[27] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[27]),
        .Q(s_axi_AXILiteS_RDATA[27]),
        .R(1'b0));
  FDRE \rdata_data_reg[28] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[28]),
        .Q(s_axi_AXILiteS_RDATA[28]),
        .R(1'b0));
  FDRE \rdata_data_reg[29] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[29]),
        .Q(s_axi_AXILiteS_RDATA[29]),
        .R(1'b0));
  FDRE \rdata_data_reg[2] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[2]),
        .Q(s_axi_AXILiteS_RDATA[2]),
        .R(1'b0));
  FDRE \rdata_data_reg[30] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[30]),
        .Q(s_axi_AXILiteS_RDATA[30]),
        .R(1'b0));
  FDRE \rdata_data_reg[31] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[31]),
        .Q(s_axi_AXILiteS_RDATA[31]),
        .R(1'b0));
  FDRE \rdata_data_reg[3] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[3]),
        .Q(s_axi_AXILiteS_RDATA[3]),
        .R(1'b0));
  FDRE \rdata_data_reg[4] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[4]),
        .Q(s_axi_AXILiteS_RDATA[4]),
        .R(1'b0));
  FDRE \rdata_data_reg[5] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[5]),
        .Q(s_axi_AXILiteS_RDATA[5]),
        .R(1'b0));
  FDRE \rdata_data_reg[6] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[6]),
        .Q(s_axi_AXILiteS_RDATA[6]),
        .R(1'b0));
  FDRE \rdata_data_reg[7] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[7]),
        .Q(s_axi_AXILiteS_RDATA[7]),
        .R(1'b0));
  FDRE \rdata_data_reg[8] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[8]),
        .Q(s_axi_AXILiteS_RDATA[8]),
        .R(1'b0));
  FDRE \rdata_data_reg[9] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[9]),
        .Q(s_axi_AXILiteS_RDATA[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \tmp_cast_reg_173[29]_i_1 
       (.I0(Q[0]),
        .I1(ap_start),
        .O(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \waddr[4]_i_1 
       (.I0(s_axi_AXILiteS_AWVALID),
        .I1(\FSM_onehot_wstate_reg[1]_0 ),
        .O(waddr));
  FDRE \waddr_reg[0] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_AXILiteS_AWADDR[0]),
        .Q(\waddr_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \waddr_reg[1] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_AXILiteS_AWADDR[1]),
        .Q(\waddr_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \waddr_reg[2] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_AXILiteS_AWADDR[2]),
        .Q(\waddr_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \waddr_reg[3] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_AXILiteS_AWADDR[3]),
        .Q(\waddr_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \waddr_reg[4] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_AXILiteS_AWADDR[4]),
        .Q(\waddr_reg_n_0_[4] ),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem0_m_axi
   (I_AWVALID,
    \state_reg[0] ,
    D,
    ARESET,
    ap_rst_n_0,
    RREADY,
    m_axi_gmem0_ARADDR,
    \could_multi_bursts.arlen_buf_reg[3] ,
    gmem0_ARREADY,
    \data_p1_reg[7] ,
    m_axi_gmem0_ARVALID,
    Q,
    ap_reg_ioackin_gmem1_AWREADY,
    gmem1_AWREADY,
    ap_rst_n,
    m_axi_gmem0_RVALID,
    ap_clk,
    mem_reg,
    m_axi_gmem0_RRESP,
    \data_p2_reg[31] ,
    m_axi_gmem0_ARREADY);
  output I_AWVALID;
  output [0:0]\state_reg[0] ;
  output [1:0]D;
  output ARESET;
  output ap_rst_n_0;
  output RREADY;
  output [29:0]m_axi_gmem0_ARADDR;
  output [3:0]\could_multi_bursts.arlen_buf_reg[3] ;
  output gmem0_ARREADY;
  output [7:0]\data_p1_reg[7] ;
  output m_axi_gmem0_ARVALID;
  input [2:0]Q;
  input ap_reg_ioackin_gmem1_AWREADY;
  input gmem1_AWREADY;
  input ap_rst_n;
  input m_axi_gmem0_RVALID;
  input ap_clk;
  input [32:0]mem_reg;
  input [1:0]m_axi_gmem0_RRESP;
  input [31:0]\data_p2_reg[31] ;
  input m_axi_gmem0_ARREADY;

  wire ARESET;
  wire [1:0]D;
  wire I_AWVALID;
  wire [2:0]Q;
  wire RREADY;
  wire ap_clk;
  wire ap_reg_ioackin_gmem1_AWREADY;
  wire ap_rst_n;
  wire ap_rst_n_0;
  wire [3:0]\could_multi_bursts.arlen_buf_reg[3] ;
  wire [7:0]\data_p1_reg[7] ;
  wire [31:0]\data_p2_reg[31] ;
  wire gmem0_ARREADY;
  wire gmem1_AWREADY;
  wire [29:0]m_axi_gmem0_ARADDR;
  wire m_axi_gmem0_ARREADY;
  wire m_axi_gmem0_ARVALID;
  wire [1:0]m_axi_gmem0_RRESP;
  wire m_axi_gmem0_RVALID;
  wire [32:0]mem_reg;
  wire [0:0]\state_reg[0] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem0_m_axi_read bus_read
       (.ARLEN(\could_multi_bursts.arlen_buf_reg[3] ),
        .D(D),
        .I_AWVALID(I_AWVALID),
        .Q(Q),
        .RREADY(RREADY),
        .SR(ARESET),
        .ap_clk(ap_clk),
        .ap_reg_ioackin_gmem1_AWREADY(ap_reg_ioackin_gmem1_AWREADY),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_0(ap_rst_n_0),
        .\data_p1_reg[7] (\data_p1_reg[7] ),
        .\data_p2_reg[31] (\data_p2_reg[31] ),
        .gmem1_AWREADY(gmem1_AWREADY),
        .m_axi_gmem0_ARADDR(m_axi_gmem0_ARADDR),
        .m_axi_gmem0_ARREADY(m_axi_gmem0_ARREADY),
        .m_axi_gmem0_ARVALID(m_axi_gmem0_ARVALID),
        .m_axi_gmem0_RRESP(m_axi_gmem0_RRESP),
        .m_axi_gmem0_RVALID(m_axi_gmem0_RVALID),
        .mem_reg(mem_reg),
        .rdata_valid(\state_reg[0] ),
        .s_ready_t_reg(gmem0_ARREADY));
endmodule

(* ORIG_REF_NAME = "filter_gmem0_m_axi_buffer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem0_m_axi_buffer__parameterized1
   (full_n_reg_0,
    SR,
    beat_valid,
    S,
    Q,
    \usedw_reg[6]_0 ,
    D,
    \dout_buf_reg[16]_0 ,
    \dout_buf_reg[34]_0 ,
    \dout_buf_reg[17]_0 ,
    \dout_buf_reg[18]_0 ,
    \dout_buf_reg[19]_0 ,
    \dout_buf_reg[20]_0 ,
    \dout_buf_reg[21]_0 ,
    \dout_buf_reg[22]_0 ,
    \dout_buf_reg[23]_0 ,
    dout_valid_reg_0,
    DI,
    dout_valid_reg_1,
    \dout_buf_reg[31]_0 ,
    \dout_buf_reg[30]_0 ,
    \dout_buf_reg[29]_0 ,
    \dout_buf_reg[28]_0 ,
    \dout_buf_reg[27]_0 ,
    \dout_buf_reg[26]_0 ,
    \dout_buf_reg[25]_0 ,
    \dout_buf_reg[24]_0 ,
    ap_clk,
    mem_reg_0,
    m_axi_gmem0_RRESP,
    m_axi_gmem0_RVALID,
    ap_rst_n,
    \bus_wide_gen.data_buf_reg[22] ,
    \bus_wide_gen.data_buf_reg[23] ,
    \bus_wide_gen.data_buf_reg[22]_0 ,
    \bus_wide_gen.data_buf_reg[8] ,
    \bus_wide_gen.data_buf_reg[8]_0 ,
    \bus_wide_gen.data_buf_reg[25] ,
    s_ready,
    \bus_wide_gen.data_buf_reg[25]_0 ,
    last_split,
    \pout_reg[0] ,
    \usedw_reg[7]_0 );
  output full_n_reg_0;
  output [0:0]SR;
  output beat_valid;
  output [3:0]S;
  output [5:0]Q;
  output [2:0]\usedw_reg[6]_0 ;
  output [1:0]D;
  output \dout_buf_reg[16]_0 ;
  output [22:0]\dout_buf_reg[34]_0 ;
  output \dout_buf_reg[17]_0 ;
  output \dout_buf_reg[18]_0 ;
  output \dout_buf_reg[19]_0 ;
  output \dout_buf_reg[20]_0 ;
  output \dout_buf_reg[21]_0 ;
  output \dout_buf_reg[22]_0 ;
  output \dout_buf_reg[23]_0 ;
  output dout_valid_reg_0;
  output [0:0]DI;
  output dout_valid_reg_1;
  output \dout_buf_reg[31]_0 ;
  output \dout_buf_reg[30]_0 ;
  output \dout_buf_reg[29]_0 ;
  output \dout_buf_reg[28]_0 ;
  output \dout_buf_reg[27]_0 ;
  output \dout_buf_reg[26]_0 ;
  output \dout_buf_reg[25]_0 ;
  output \dout_buf_reg[24]_0 ;
  input ap_clk;
  input [32:0]mem_reg_0;
  input [1:0]m_axi_gmem0_RRESP;
  input m_axi_gmem0_RVALID;
  input ap_rst_n;
  input \bus_wide_gen.data_buf_reg[22] ;
  input [1:0]\bus_wide_gen.data_buf_reg[23] ;
  input \bus_wide_gen.data_buf_reg[22]_0 ;
  input [1:0]\bus_wide_gen.data_buf_reg[8] ;
  input \bus_wide_gen.data_buf_reg[8]_0 ;
  input \bus_wide_gen.data_buf_reg[25] ;
  input s_ready;
  input \bus_wide_gen.data_buf_reg[25]_0 ;
  input last_split;
  input \pout_reg[0] ;
  input [6:0]\usedw_reg[7]_0 ;

  wire [1:0]D;
  wire [0:0]DI;
  wire [5:0]Q;
  wire [3:0]S;
  wire [0:0]SR;
  wire ap_clk;
  wire ap_rst_n;
  wire beat_valid;
  wire \bus_wide_gen.data_buf[22]_i_2_n_0 ;
  wire \bus_wide_gen.data_buf[23]_i_2_n_0 ;
  wire \bus_wide_gen.data_buf_reg[22] ;
  wire \bus_wide_gen.data_buf_reg[22]_0 ;
  wire [1:0]\bus_wide_gen.data_buf_reg[23] ;
  wire \bus_wide_gen.data_buf_reg[25] ;
  wire \bus_wide_gen.data_buf_reg[25]_0 ;
  wire [1:0]\bus_wide_gen.data_buf_reg[8] ;
  wire \bus_wide_gen.data_buf_reg[8]_0 ;
  wire \dout_buf[0]_i_1_n_0 ;
  wire \dout_buf[10]_i_1_n_0 ;
  wire \dout_buf[11]_i_1_n_0 ;
  wire \dout_buf[12]_i_1_n_0 ;
  wire \dout_buf[13]_i_1_n_0 ;
  wire \dout_buf[14]_i_1_n_0 ;
  wire \dout_buf[15]_i_1_n_0 ;
  wire \dout_buf[16]_i_1_n_0 ;
  wire \dout_buf[17]_i_1_n_0 ;
  wire \dout_buf[18]_i_1_n_0 ;
  wire \dout_buf[19]_i_1_n_0 ;
  wire \dout_buf[1]_i_1_n_0 ;
  wire \dout_buf[20]_i_1_n_0 ;
  wire \dout_buf[21]_i_1_n_0 ;
  wire \dout_buf[22]_i_1_n_0 ;
  wire \dout_buf[23]_i_1_n_0 ;
  wire \dout_buf[24]_i_1_n_0 ;
  wire \dout_buf[25]_i_1_n_0 ;
  wire \dout_buf[26]_i_1_n_0 ;
  wire \dout_buf[27]_i_1_n_0 ;
  wire \dout_buf[28]_i_1_n_0 ;
  wire \dout_buf[29]_i_1_n_0 ;
  wire \dout_buf[2]_i_1_n_0 ;
  wire \dout_buf[30]_i_1_n_0 ;
  wire \dout_buf[31]_i_1_n_0 ;
  wire \dout_buf[34]_i_2_n_0 ;
  wire \dout_buf[3]_i_1_n_0 ;
  wire \dout_buf[4]_i_1_n_0 ;
  wire \dout_buf[5]_i_1_n_0 ;
  wire \dout_buf[6]_i_1_n_0 ;
  wire \dout_buf[7]_i_1_n_0 ;
  wire \dout_buf[8]_i_1_n_0 ;
  wire \dout_buf[9]_i_1_n_0 ;
  wire \dout_buf_reg[16]_0 ;
  wire \dout_buf_reg[17]_0 ;
  wire \dout_buf_reg[18]_0 ;
  wire \dout_buf_reg[19]_0 ;
  wire \dout_buf_reg[20]_0 ;
  wire \dout_buf_reg[21]_0 ;
  wire \dout_buf_reg[22]_0 ;
  wire \dout_buf_reg[23]_0 ;
  wire \dout_buf_reg[24]_0 ;
  wire \dout_buf_reg[25]_0 ;
  wire \dout_buf_reg[26]_0 ;
  wire \dout_buf_reg[27]_0 ;
  wire \dout_buf_reg[28]_0 ;
  wire \dout_buf_reg[29]_0 ;
  wire \dout_buf_reg[30]_0 ;
  wire \dout_buf_reg[31]_0 ;
  wire [22:0]\dout_buf_reg[34]_0 ;
  wire \dout_buf_reg_n_0_[10] ;
  wire \dout_buf_reg_n_0_[11] ;
  wire \dout_buf_reg_n_0_[12] ;
  wire \dout_buf_reg_n_0_[13] ;
  wire \dout_buf_reg_n_0_[14] ;
  wire \dout_buf_reg_n_0_[15] ;
  wire \dout_buf_reg_n_0_[22] ;
  wire \dout_buf_reg_n_0_[23] ;
  wire \dout_buf_reg_n_0_[8] ;
  wire \dout_buf_reg_n_0_[9] ;
  wire dout_valid_i_1_n_0;
  wire dout_valid_reg_0;
  wire dout_valid_reg_1;
  wire empty_n;
  wire empty_n0;
  wire empty_n_i_2_n_0;
  wire empty_n_i_3_n_0;
  wire empty_n_reg_n_0;
  wire full_n0;
  wire full_n_i_3_n_0;
  wire full_n_i_4_n_0;
  wire full_n_reg_0;
  wire last_split;
  wire [1:0]m_axi_gmem0_RRESP;
  wire m_axi_gmem0_RVALID;
  wire [32:0]mem_reg_0;
  wire mem_reg_i_10_n_0;
  wire mem_reg_i_1_n_0;
  wire mem_reg_i_2_n_0;
  wire mem_reg_i_3_n_0;
  wire mem_reg_i_4_n_0;
  wire mem_reg_i_5_n_0;
  wire mem_reg_i_6_n_0;
  wire mem_reg_i_7_n_0;
  wire mem_reg_i_8_n_0;
  wire mem_reg_i_9_n_0;
  wire mem_reg_n_32;
  wire mem_reg_n_33;
  wire pop;
  wire \pout_reg[0] ;
  wire push;
  wire [34:0]q_buf;
  wire [34:0]q_tmp;
  wire [7:0]raddr;
  wire s_ready;
  wire show_ahead;
  wire show_ahead0;
  wire \usedw[0]_i_1_n_0 ;
  wire [2:0]\usedw_reg[6]_0 ;
  wire [6:0]\usedw_reg[7]_0 ;
  wire [7:6]usedw_reg__0;
  wire [7:0]waddr;
  wire \waddr[6]_i_2_n_0 ;
  wire \waddr[7]_i_3_n_0 ;
  wire \waddr[7]_i_4_n_0 ;
  wire [7:0]wnext;
  wire [1:1]NLW_mem_reg_DOPBDOP_UNCONNECTED;

  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bus_wide_gen.data_buf[0]_i_2 
       (.I0(\dout_buf_reg[34]_0 [14]),
        .I1(\dout_buf_reg_n_0_[8] ),
        .I2(\bus_wide_gen.data_buf_reg[8] [0]),
        .I3(\dout_buf_reg[34]_0 [8]),
        .I4(\bus_wide_gen.data_buf_reg[8] [1]),
        .I5(\dout_buf_reg[34]_0 [0]),
        .O(\dout_buf_reg[24]_0 ));
  LUT6 #(
    .INIT(64'h30BBFFFF30880000)) 
    \bus_wide_gen.data_buf[10]_i_2 
       (.I0(\dout_buf_reg[34]_0 [10]),
        .I1(\bus_wide_gen.data_buf_reg[8] [0]),
        .I2(\dout_buf_reg[34]_0 [16]),
        .I3(\bus_wide_gen.data_buf_reg[8] [1]),
        .I4(\bus_wide_gen.data_buf_reg[8]_0 ),
        .I5(\dout_buf_reg_n_0_[10] ),
        .O(\dout_buf_reg[18]_0 ));
  LUT6 #(
    .INIT(64'h30BBFFFF30880000)) 
    \bus_wide_gen.data_buf[11]_i_2 
       (.I0(\dout_buf_reg[34]_0 [11]),
        .I1(\bus_wide_gen.data_buf_reg[8] [0]),
        .I2(\dout_buf_reg[34]_0 [17]),
        .I3(\bus_wide_gen.data_buf_reg[8] [1]),
        .I4(\bus_wide_gen.data_buf_reg[8]_0 ),
        .I5(\dout_buf_reg_n_0_[11] ),
        .O(\dout_buf_reg[19]_0 ));
  LUT6 #(
    .INIT(64'h30BBFFFF30880000)) 
    \bus_wide_gen.data_buf[12]_i_2 
       (.I0(\dout_buf_reg[34]_0 [12]),
        .I1(\bus_wide_gen.data_buf_reg[8] [0]),
        .I2(\dout_buf_reg[34]_0 [18]),
        .I3(\bus_wide_gen.data_buf_reg[8] [1]),
        .I4(\bus_wide_gen.data_buf_reg[8]_0 ),
        .I5(\dout_buf_reg_n_0_[12] ),
        .O(\dout_buf_reg[20]_0 ));
  LUT6 #(
    .INIT(64'h30BBFFFF30880000)) 
    \bus_wide_gen.data_buf[13]_i_2 
       (.I0(\dout_buf_reg[34]_0 [13]),
        .I1(\bus_wide_gen.data_buf_reg[8] [0]),
        .I2(\dout_buf_reg[34]_0 [19]),
        .I3(\bus_wide_gen.data_buf_reg[8] [1]),
        .I4(\bus_wide_gen.data_buf_reg[8]_0 ),
        .I5(\dout_buf_reg_n_0_[13] ),
        .O(\dout_buf_reg[21]_0 ));
  LUT6 #(
    .INIT(64'h30BBFFFF30880000)) 
    \bus_wide_gen.data_buf[14]_i_2 
       (.I0(\dout_buf_reg_n_0_[22] ),
        .I1(\bus_wide_gen.data_buf_reg[8] [0]),
        .I2(\dout_buf_reg[34]_0 [20]),
        .I3(\bus_wide_gen.data_buf_reg[8] [1]),
        .I4(\bus_wide_gen.data_buf_reg[8]_0 ),
        .I5(\dout_buf_reg_n_0_[14] ),
        .O(\dout_buf_reg[22]_0 ));
  LUT6 #(
    .INIT(64'h30BBFFFF30880000)) 
    \bus_wide_gen.data_buf[15]_i_2 
       (.I0(\dout_buf_reg_n_0_[23] ),
        .I1(\bus_wide_gen.data_buf_reg[8] [0]),
        .I2(\dout_buf_reg[34]_0 [21]),
        .I3(\bus_wide_gen.data_buf_reg[8] [1]),
        .I4(\bus_wide_gen.data_buf_reg[8]_0 ),
        .I5(\dout_buf_reg_n_0_[15] ),
        .O(\dout_buf_reg[23]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bus_wide_gen.data_buf[1]_i_2 
       (.I0(\dout_buf_reg[34]_0 [15]),
        .I1(\dout_buf_reg_n_0_[9] ),
        .I2(\bus_wide_gen.data_buf_reg[8] [0]),
        .I3(\dout_buf_reg[34]_0 [9]),
        .I4(\bus_wide_gen.data_buf_reg[8] [1]),
        .I5(\dout_buf_reg[34]_0 [1]),
        .O(\dout_buf_reg[25]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \bus_wide_gen.data_buf[22]_i_1 
       (.I0(\bus_wide_gen.data_buf[22]_i_2_n_0 ),
        .I1(\bus_wide_gen.data_buf_reg[22] ),
        .I2(\bus_wide_gen.data_buf_reg[23] [0]),
        .I3(\bus_wide_gen.data_buf_reg[22]_0 ),
        .I4(\dout_buf_reg_n_0_[22] ),
        .O(D[0]));
  LUT4 #(
    .INIT(16'h00B8)) 
    \bus_wide_gen.data_buf[22]_i_2 
       (.I0(\dout_buf_reg[34]_0 [20]),
        .I1(\bus_wide_gen.data_buf_reg[8] [0]),
        .I2(\dout_buf_reg_n_0_[22] ),
        .I3(\bus_wide_gen.data_buf_reg[8] [1]),
        .O(\bus_wide_gen.data_buf[22]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \bus_wide_gen.data_buf[23]_i_1 
       (.I0(\bus_wide_gen.data_buf[23]_i_2_n_0 ),
        .I1(\bus_wide_gen.data_buf_reg[22] ),
        .I2(\bus_wide_gen.data_buf_reg[23] [1]),
        .I3(\bus_wide_gen.data_buf_reg[22]_0 ),
        .I4(\dout_buf_reg_n_0_[23] ),
        .O(D[1]));
  LUT4 #(
    .INIT(16'h00B8)) 
    \bus_wide_gen.data_buf[23]_i_2 
       (.I0(\dout_buf_reg[34]_0 [21]),
        .I1(\bus_wide_gen.data_buf_reg[8] [0]),
        .I2(\dout_buf_reg_n_0_[23] ),
        .I3(\bus_wide_gen.data_buf_reg[8] [1]),
        .O(\bus_wide_gen.data_buf[23]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bus_wide_gen.data_buf[2]_i_2 
       (.I0(\dout_buf_reg[34]_0 [16]),
        .I1(\dout_buf_reg_n_0_[10] ),
        .I2(\bus_wide_gen.data_buf_reg[8] [0]),
        .I3(\dout_buf_reg[34]_0 [10]),
        .I4(\bus_wide_gen.data_buf_reg[8] [1]),
        .I5(\dout_buf_reg[34]_0 [2]),
        .O(\dout_buf_reg[26]_0 ));
  LUT4 #(
    .INIT(16'h5DFF)) 
    \bus_wide_gen.data_buf[31]_i_5 
       (.I0(beat_valid),
        .I1(\bus_wide_gen.data_buf_reg[25] ),
        .I2(s_ready),
        .I3(\bus_wide_gen.data_buf_reg[25]_0 ),
        .O(dout_valid_reg_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bus_wide_gen.data_buf[3]_i_2 
       (.I0(\dout_buf_reg[34]_0 [17]),
        .I1(\dout_buf_reg_n_0_[11] ),
        .I2(\bus_wide_gen.data_buf_reg[8] [0]),
        .I3(\dout_buf_reg[34]_0 [11]),
        .I4(\bus_wide_gen.data_buf_reg[8] [1]),
        .I5(\dout_buf_reg[34]_0 [3]),
        .O(\dout_buf_reg[27]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bus_wide_gen.data_buf[4]_i_2 
       (.I0(\dout_buf_reg[34]_0 [18]),
        .I1(\dout_buf_reg_n_0_[12] ),
        .I2(\bus_wide_gen.data_buf_reg[8] [0]),
        .I3(\dout_buf_reg[34]_0 [12]),
        .I4(\bus_wide_gen.data_buf_reg[8] [1]),
        .I5(\dout_buf_reg[34]_0 [4]),
        .O(\dout_buf_reg[28]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bus_wide_gen.data_buf[5]_i_2 
       (.I0(\dout_buf_reg[34]_0 [19]),
        .I1(\dout_buf_reg_n_0_[13] ),
        .I2(\bus_wide_gen.data_buf_reg[8] [0]),
        .I3(\dout_buf_reg[34]_0 [13]),
        .I4(\bus_wide_gen.data_buf_reg[8] [1]),
        .I5(\dout_buf_reg[34]_0 [5]),
        .O(\dout_buf_reg[29]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bus_wide_gen.data_buf[6]_i_2 
       (.I0(\dout_buf_reg[34]_0 [20]),
        .I1(\dout_buf_reg_n_0_[14] ),
        .I2(\bus_wide_gen.data_buf_reg[8] [0]),
        .I3(\dout_buf_reg_n_0_[22] ),
        .I4(\bus_wide_gen.data_buf_reg[8] [1]),
        .I5(\dout_buf_reg[34]_0 [6]),
        .O(\dout_buf_reg[30]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bus_wide_gen.data_buf[7]_i_2 
       (.I0(\dout_buf_reg[34]_0 [21]),
        .I1(\dout_buf_reg_n_0_[15] ),
        .I2(\bus_wide_gen.data_buf_reg[8] [0]),
        .I3(\dout_buf_reg_n_0_[23] ),
        .I4(\bus_wide_gen.data_buf_reg[8] [1]),
        .I5(\dout_buf_reg[34]_0 [7]),
        .O(\dout_buf_reg[31]_0 ));
  LUT6 #(
    .INIT(64'h30BBFFFF30880000)) 
    \bus_wide_gen.data_buf[8]_i_2 
       (.I0(\dout_buf_reg[34]_0 [8]),
        .I1(\bus_wide_gen.data_buf_reg[8] [0]),
        .I2(\dout_buf_reg[34]_0 [14]),
        .I3(\bus_wide_gen.data_buf_reg[8] [1]),
        .I4(\bus_wide_gen.data_buf_reg[8]_0 ),
        .I5(\dout_buf_reg_n_0_[8] ),
        .O(\dout_buf_reg[16]_0 ));
  LUT6 #(
    .INIT(64'h30BBFFFF30880000)) 
    \bus_wide_gen.data_buf[9]_i_2 
       (.I0(\dout_buf_reg[34]_0 [9]),
        .I1(\bus_wide_gen.data_buf_reg[8] [0]),
        .I2(\dout_buf_reg[34]_0 [15]),
        .I3(\bus_wide_gen.data_buf_reg[8] [1]),
        .I4(\bus_wide_gen.data_buf_reg[8]_0 ),
        .I5(\dout_buf_reg_n_0_[9] ),
        .O(\dout_buf_reg[17]_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \could_multi_bursts.ARVALID_Dummy_i_1 
       (.I0(ap_rst_n),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[0]_i_1 
       (.I0(q_tmp[0]),
        .I1(q_buf[0]),
        .I2(show_ahead),
        .O(\dout_buf[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[10]_i_1 
       (.I0(q_tmp[10]),
        .I1(q_buf[10]),
        .I2(show_ahead),
        .O(\dout_buf[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[11]_i_1 
       (.I0(q_tmp[11]),
        .I1(q_buf[11]),
        .I2(show_ahead),
        .O(\dout_buf[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[12]_i_1 
       (.I0(q_tmp[12]),
        .I1(q_buf[12]),
        .I2(show_ahead),
        .O(\dout_buf[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[13]_i_1 
       (.I0(q_tmp[13]),
        .I1(q_buf[13]),
        .I2(show_ahead),
        .O(\dout_buf[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[14]_i_1 
       (.I0(q_tmp[14]),
        .I1(q_buf[14]),
        .I2(show_ahead),
        .O(\dout_buf[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[15]_i_1 
       (.I0(q_tmp[15]),
        .I1(q_buf[15]),
        .I2(show_ahead),
        .O(\dout_buf[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[16]_i_1 
       (.I0(q_tmp[16]),
        .I1(q_buf[16]),
        .I2(show_ahead),
        .O(\dout_buf[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[17]_i_1 
       (.I0(q_tmp[17]),
        .I1(q_buf[17]),
        .I2(show_ahead),
        .O(\dout_buf[17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[18]_i_1 
       (.I0(q_tmp[18]),
        .I1(q_buf[18]),
        .I2(show_ahead),
        .O(\dout_buf[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[19]_i_1 
       (.I0(q_tmp[19]),
        .I1(q_buf[19]),
        .I2(show_ahead),
        .O(\dout_buf[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[1]_i_1 
       (.I0(q_tmp[1]),
        .I1(q_buf[1]),
        .I2(show_ahead),
        .O(\dout_buf[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[20]_i_1 
       (.I0(q_tmp[20]),
        .I1(q_buf[20]),
        .I2(show_ahead),
        .O(\dout_buf[20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[21]_i_1 
       (.I0(q_tmp[21]),
        .I1(q_buf[21]),
        .I2(show_ahead),
        .O(\dout_buf[21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[22]_i_1 
       (.I0(q_tmp[22]),
        .I1(q_buf[22]),
        .I2(show_ahead),
        .O(\dout_buf[22]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[23]_i_1 
       (.I0(q_tmp[23]),
        .I1(q_buf[23]),
        .I2(show_ahead),
        .O(\dout_buf[23]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[24]_i_1 
       (.I0(q_tmp[24]),
        .I1(q_buf[24]),
        .I2(show_ahead),
        .O(\dout_buf[24]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[25]_i_1 
       (.I0(q_tmp[25]),
        .I1(q_buf[25]),
        .I2(show_ahead),
        .O(\dout_buf[25]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[26]_i_1 
       (.I0(q_tmp[26]),
        .I1(q_buf[26]),
        .I2(show_ahead),
        .O(\dout_buf[26]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[27]_i_1 
       (.I0(q_tmp[27]),
        .I1(q_buf[27]),
        .I2(show_ahead),
        .O(\dout_buf[27]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[28]_i_1 
       (.I0(q_tmp[28]),
        .I1(q_buf[28]),
        .I2(show_ahead),
        .O(\dout_buf[28]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[29]_i_1 
       (.I0(q_tmp[29]),
        .I1(q_buf[29]),
        .I2(show_ahead),
        .O(\dout_buf[29]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[2]_i_1 
       (.I0(q_tmp[2]),
        .I1(q_buf[2]),
        .I2(show_ahead),
        .O(\dout_buf[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[30]_i_1 
       (.I0(q_tmp[30]),
        .I1(q_buf[30]),
        .I2(show_ahead),
        .O(\dout_buf[30]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[31]_i_1 
       (.I0(q_tmp[31]),
        .I1(q_buf[31]),
        .I2(show_ahead),
        .O(\dout_buf[31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h8A)) 
    \dout_buf[34]_i_1 
       (.I0(empty_n_reg_n_0),
        .I1(last_split),
        .I2(beat_valid),
        .O(pop));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[34]_i_2 
       (.I0(q_tmp[34]),
        .I1(q_buf[34]),
        .I2(show_ahead),
        .O(\dout_buf[34]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[3]_i_1 
       (.I0(q_tmp[3]),
        .I1(q_buf[3]),
        .I2(show_ahead),
        .O(\dout_buf[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[4]_i_1 
       (.I0(q_tmp[4]),
        .I1(q_buf[4]),
        .I2(show_ahead),
        .O(\dout_buf[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[5]_i_1 
       (.I0(q_tmp[5]),
        .I1(q_buf[5]),
        .I2(show_ahead),
        .O(\dout_buf[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[6]_i_1 
       (.I0(q_tmp[6]),
        .I1(q_buf[6]),
        .I2(show_ahead),
        .O(\dout_buf[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[7]_i_1 
       (.I0(q_tmp[7]),
        .I1(q_buf[7]),
        .I2(show_ahead),
        .O(\dout_buf[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[8]_i_1 
       (.I0(q_tmp[8]),
        .I1(q_buf[8]),
        .I2(show_ahead),
        .O(\dout_buf[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[9]_i_1 
       (.I0(q_tmp[9]),
        .I1(q_buf[9]),
        .I2(show_ahead),
        .O(\dout_buf[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[0] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[0]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[10] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[10]_i_1_n_0 ),
        .Q(\dout_buf_reg_n_0_[10] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[11] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[11]_i_1_n_0 ),
        .Q(\dout_buf_reg_n_0_[11] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[12] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[12]_i_1_n_0 ),
        .Q(\dout_buf_reg_n_0_[12] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[13] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[13]_i_1_n_0 ),
        .Q(\dout_buf_reg_n_0_[13] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[14] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[14]_i_1_n_0 ),
        .Q(\dout_buf_reg_n_0_[14] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[15] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[15]_i_1_n_0 ),
        .Q(\dout_buf_reg_n_0_[15] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[16] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[16]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[17] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[17]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [9]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[18] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[18]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[19] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[19]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[1] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[1]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[20] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[20]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[21] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[21]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[22] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[22]_i_1_n_0 ),
        .Q(\dout_buf_reg_n_0_[22] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[23] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[23]_i_1_n_0 ),
        .Q(\dout_buf_reg_n_0_[23] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[24] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[24]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [14]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[25] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[25]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[26] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[26]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [16]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[27] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[27]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [17]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[28] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[28]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [18]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[29] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[29]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [19]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[2] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[2]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[30] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[30]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [20]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[31] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[31]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [21]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[34] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[34]_i_2_n_0 ),
        .Q(\dout_buf_reg[34]_0 [22]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[3] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[3]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[4] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[4]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[5] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[5]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[6] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[6]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[7] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[7]_i_1_n_0 ),
        .Q(\dout_buf_reg[34]_0 [7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[8] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[8]_i_1_n_0 ),
        .Q(\dout_buf_reg_n_0_[8] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[9] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[9]_i_1_n_0 ),
        .Q(\dout_buf_reg_n_0_[9] ),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'hF2)) 
    dout_valid_i_1
       (.I0(beat_valid),
        .I1(last_split),
        .I2(empty_n_reg_n_0),
        .O(dout_valid_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    dout_valid_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(dout_valid_i_1_n_0),
        .Q(beat_valid),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'hFFFF40FF)) 
    empty_n_i_1
       (.I0(pop),
        .I1(full_n_reg_0),
        .I2(m_axi_gmem0_RVALID),
        .I3(Q[0]),
        .I4(empty_n_i_2_n_0),
        .O(empty_n0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    empty_n_i_2
       (.I0(usedw_reg__0[6]),
        .I1(usedw_reg__0[7]),
        .I2(Q[4]),
        .I3(empty_n_i_3_n_0),
        .O(empty_n_i_2_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    empty_n_i_3
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(Q[5]),
        .I3(Q[2]),
        .O(empty_n_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    empty_n_reg
       (.C(ap_clk),
        .CE(empty_n),
        .D(empty_n0),
        .Q(empty_n_reg_n_0),
        .R(SR));
  LUT3 #(
    .INIT(8'h6A)) 
    full_n_i_1
       (.I0(pop),
        .I1(m_axi_gmem0_RVALID),
        .I2(full_n_reg_0),
        .O(empty_n));
  LUT5 #(
    .INIT(32'h70787878)) 
    full_n_i_2
       (.I0(full_n_reg_0),
        .I1(m_axi_gmem0_RVALID),
        .I2(pop),
        .I3(Q[0]),
        .I4(full_n_i_3_n_0),
        .O(full_n0));
  LUT4 #(
    .INIT(16'h0080)) 
    full_n_i_3
       (.I0(Q[1]),
        .I1(Q[5]),
        .I2(usedw_reg__0[7]),
        .I3(full_n_i_4_n_0),
        .O(full_n_i_3_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    full_n_i_4
       (.I0(Q[4]),
        .I1(Q[3]),
        .I2(usedw_reg__0[6]),
        .I3(Q[2]),
        .O(full_n_i_4_n_0));
  FDSE #(
    .INIT(1'b1)) 
    full_n_reg
       (.C(ap_clk),
        .CE(empty_n),
        .D(full_n0),
        .Q(full_n_reg_0),
        .S(SR));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p3_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p3_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "8960" *) 
  (* RTL_RAM_NAME = "mem" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "34" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "511" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "34" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    mem_reg
       (.ADDRARDADDR({1'b1,mem_reg_i_1_n_0,mem_reg_i_2_n_0,mem_reg_i_3_n_0,mem_reg_i_4_n_0,mem_reg_i_5_n_0,mem_reg_i_6_n_0,mem_reg_i_7_n_0,mem_reg_i_8_n_0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,waddr,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(ap_clk),
        .CLKBWRCLK(ap_clk),
        .DIADI(mem_reg_0[15:0]),
        .DIBDI(mem_reg_0[31:16]),
        .DIPADIP(m_axi_gmem0_RRESP),
        .DIPBDIP({1'b1,mem_reg_0[32]}),
        .DOADO(q_buf[15:0]),
        .DOBDO(q_buf[31:16]),
        .DOPADOP({mem_reg_n_32,mem_reg_n_33}),
        .DOPBDOP({NLW_mem_reg_DOPBDOP_UNCONNECTED[1],q_buf[34]}),
        .ENARDEN(1'b1),
        .ENBWREN(full_n_reg_0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({m_axi_gmem0_RVALID,m_axi_gmem0_RVALID,m_axi_gmem0_RVALID,m_axi_gmem0_RVALID}));
  LUT6 #(
    .INIT(64'hAAAA6AAAAAAAAAAA)) 
    mem_reg_i_1
       (.I0(raddr[7]),
        .I1(raddr[6]),
        .I2(raddr[5]),
        .I3(raddr[4]),
        .I4(mem_reg_i_9_n_0),
        .I5(pop),
        .O(mem_reg_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    mem_reg_i_10
       (.I0(raddr[0]),
        .I1(raddr[1]),
        .I2(raddr[2]),
        .O(mem_reg_i_10_n_0));
  LUT6 #(
    .INIT(64'hAAAA6AAAAAAAAAAA)) 
    mem_reg_i_2
       (.I0(raddr[6]),
        .I1(raddr[5]),
        .I2(pop),
        .I3(raddr[3]),
        .I4(mem_reg_i_10_n_0),
        .I5(raddr[4]),
        .O(mem_reg_i_2_n_0));
  LUT5 #(
    .INIT(32'hAA6AAAAA)) 
    mem_reg_i_3
       (.I0(raddr[5]),
        .I1(pop),
        .I2(raddr[3]),
        .I3(mem_reg_i_10_n_0),
        .I4(raddr[4]),
        .O(mem_reg_i_3_n_0));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    mem_reg_i_4
       (.I0(raddr[4]),
        .I1(raddr[2]),
        .I2(raddr[1]),
        .I3(raddr[0]),
        .I4(raddr[3]),
        .I5(pop),
        .O(mem_reg_i_4_n_0));
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    mem_reg_i_5
       (.I0(raddr[3]),
        .I1(raddr[0]),
        .I2(raddr[1]),
        .I3(raddr[2]),
        .I4(pop),
        .O(mem_reg_i_5_n_0));
  LUT4 #(
    .INIT(16'h6AAA)) 
    mem_reg_i_6
       (.I0(raddr[2]),
        .I1(raddr[1]),
        .I2(raddr[0]),
        .I3(pop),
        .O(mem_reg_i_6_n_0));
  LUT3 #(
    .INIT(8'h78)) 
    mem_reg_i_7
       (.I0(raddr[0]),
        .I1(pop),
        .I2(raddr[1]),
        .O(mem_reg_i_7_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    mem_reg_i_8
       (.I0(raddr[0]),
        .I1(pop),
        .O(mem_reg_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    mem_reg_i_9
       (.I0(raddr[2]),
        .I1(raddr[1]),
        .I2(raddr[0]),
        .I3(raddr[3]),
        .O(mem_reg_i_9_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out_carry__0_i_1
       (.I0(usedw_reg__0[6]),
        .I1(usedw_reg__0[7]),
        .O(\usedw_reg[6]_0 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out_carry__0_i_2
       (.I0(Q[5]),
        .I1(usedw_reg__0[6]),
        .O(\usedw_reg[6]_0 [1]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out_carry__0_i_3
       (.I0(Q[4]),
        .I1(Q[5]),
        .O(\usedw_reg[6]_0 [0]));
  LUT3 #(
    .INIT(8'h08)) 
    p_0_out_carry_i_1
       (.I0(m_axi_gmem0_RVALID),
        .I1(full_n_reg_0),
        .I2(pop),
        .O(DI));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out_carry_i_2
       (.I0(Q[3]),
        .I1(Q[4]),
        .O(S[3]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out_carry_i_3
       (.I0(Q[2]),
        .I1(Q[3]),
        .O(S[2]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out_carry_i_4
       (.I0(Q[1]),
        .I1(Q[2]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h6555)) 
    p_0_out_carry_i_5
       (.I0(Q[1]),
        .I1(pop),
        .I2(full_n_reg_0),
        .I3(m_axi_gmem0_RVALID),
        .O(S[0]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT4 #(
    .INIT(16'h80FF)) 
    \pout[3]_i_4 
       (.I0(beat_valid),
        .I1(\dout_buf_reg[34]_0 [22]),
        .I2(last_split),
        .I3(\pout_reg[0] ),
        .O(dout_valid_reg_1));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[0] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[0]),
        .Q(q_tmp[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[10] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[10]),
        .Q(q_tmp[10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[11] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[11]),
        .Q(q_tmp[11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[12] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[12]),
        .Q(q_tmp[12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[13] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[13]),
        .Q(q_tmp[13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[14] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[14]),
        .Q(q_tmp[14]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[15] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[15]),
        .Q(q_tmp[15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[16] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[16]),
        .Q(q_tmp[16]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[17] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[17]),
        .Q(q_tmp[17]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[18] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[18]),
        .Q(q_tmp[18]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[19] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[19]),
        .Q(q_tmp[19]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[1] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[1]),
        .Q(q_tmp[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[20] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[20]),
        .Q(q_tmp[20]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[21] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[21]),
        .Q(q_tmp[21]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[22] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[22]),
        .Q(q_tmp[22]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[23] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[23]),
        .Q(q_tmp[23]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[24] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[24]),
        .Q(q_tmp[24]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[25] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[25]),
        .Q(q_tmp[25]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[26] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[26]),
        .Q(q_tmp[26]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[27] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[27]),
        .Q(q_tmp[27]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[28] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[28]),
        .Q(q_tmp[28]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[29] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[29]),
        .Q(q_tmp[29]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[2] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[2]),
        .Q(q_tmp[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[30] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[30]),
        .Q(q_tmp[30]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[31] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[31]),
        .Q(q_tmp[31]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[34] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[32]),
        .Q(q_tmp[34]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[3] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[3]),
        .Q(q_tmp[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[4] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[4]),
        .Q(q_tmp[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[5] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[5]),
        .Q(q_tmp[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[6] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[6]),
        .Q(q_tmp[6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[7] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[7]),
        .Q(q_tmp[7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[8] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[8]),
        .Q(q_tmp[8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[9] 
       (.C(ap_clk),
        .CE(push),
        .D(mem_reg_0[9]),
        .Q(q_tmp[9]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \raddr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(mem_reg_i_8_n_0),
        .Q(raddr[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \raddr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(mem_reg_i_7_n_0),
        .Q(raddr[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \raddr_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(mem_reg_i_6_n_0),
        .Q(raddr[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \raddr_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(mem_reg_i_5_n_0),
        .Q(raddr[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \raddr_reg[4] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(mem_reg_i_4_n_0),
        .Q(raddr[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \raddr_reg[5] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(mem_reg_i_3_n_0),
        .Q(raddr[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \raddr_reg[6] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(mem_reg_i_2_n_0),
        .Q(raddr[6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \raddr_reg[7] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(mem_reg_i_1_n_0),
        .Q(raddr[7]),
        .R(SR));
  LUT5 #(
    .INIT(32'h0000D000)) 
    show_ahead_i_1
       (.I0(Q[0]),
        .I1(pop),
        .I2(full_n_reg_0),
        .I3(m_axi_gmem0_RVALID),
        .I4(empty_n_i_2_n_0),
        .O(show_ahead0));
  FDRE #(
    .INIT(1'b0)) 
    show_ahead_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(show_ahead0),
        .Q(show_ahead),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \usedw[0]_i_1 
       (.I0(Q[0]),
        .O(\usedw[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[0] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw[0]_i_1_n_0 ),
        .Q(Q[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[1] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[7]_0 [0]),
        .Q(Q[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[2] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[7]_0 [1]),
        .Q(Q[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[3] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[7]_0 [2]),
        .Q(Q[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[4] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[7]_0 [3]),
        .Q(Q[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[5] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[7]_0 [4]),
        .Q(Q[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[6] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[7]_0 [5]),
        .Q(usedw_reg__0[6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[7] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[7]_0 [6]),
        .Q(usedw_reg__0[7]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \waddr[0]_i_1 
       (.I0(waddr[0]),
        .O(wnext[0]));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \waddr[1]_i_1 
       (.I0(waddr[0]),
        .I1(waddr[1]),
        .O(wnext[1]));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \waddr[2]_i_1 
       (.I0(waddr[2]),
        .I1(waddr[0]),
        .I2(waddr[1]),
        .O(wnext[2]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \waddr[3]_i_1 
       (.I0(waddr[3]),
        .I1(waddr[0]),
        .I2(waddr[1]),
        .I3(waddr[2]),
        .O(wnext[3]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \waddr[4]_i_1__0 
       (.I0(waddr[4]),
        .I1(waddr[2]),
        .I2(waddr[1]),
        .I3(waddr[0]),
        .I4(waddr[3]),
        .O(wnext[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \waddr[5]_i_1 
       (.I0(waddr[5]),
        .I1(waddr[3]),
        .I2(waddr[0]),
        .I3(waddr[1]),
        .I4(waddr[2]),
        .I5(waddr[4]),
        .O(wnext[5]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \waddr[6]_i_1 
       (.I0(waddr[6]),
        .I1(waddr[4]),
        .I2(waddr[2]),
        .I3(\waddr[6]_i_2_n_0 ),
        .I4(waddr[3]),
        .I5(waddr[5]),
        .O(wnext[6]));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \waddr[6]_i_2 
       (.I0(waddr[1]),
        .I1(waddr[0]),
        .O(\waddr[6]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \waddr[7]_i_1 
       (.I0(full_n_reg_0),
        .I1(m_axi_gmem0_RVALID),
        .O(push));
  LUT4 #(
    .INIT(16'hB8CC)) 
    \waddr[7]_i_2 
       (.I0(\waddr[7]_i_3_n_0 ),
        .I1(waddr[7]),
        .I2(\waddr[7]_i_4_n_0 ),
        .I3(waddr[6]),
        .O(wnext[7]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \waddr[7]_i_3 
       (.I0(waddr[4]),
        .I1(waddr[2]),
        .I2(waddr[0]),
        .I3(waddr[1]),
        .I4(waddr[3]),
        .I5(waddr[5]),
        .O(\waddr[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \waddr[7]_i_4 
       (.I0(waddr[4]),
        .I1(waddr[2]),
        .I2(waddr[1]),
        .I3(waddr[0]),
        .I4(waddr[3]),
        .I5(waddr[5]),
        .O(\waddr[7]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \waddr_reg[0] 
       (.C(ap_clk),
        .CE(push),
        .D(wnext[0]),
        .Q(waddr[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \waddr_reg[1] 
       (.C(ap_clk),
        .CE(push),
        .D(wnext[1]),
        .Q(waddr[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \waddr_reg[2] 
       (.C(ap_clk),
        .CE(push),
        .D(wnext[2]),
        .Q(waddr[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \waddr_reg[3] 
       (.C(ap_clk),
        .CE(push),
        .D(wnext[3]),
        .Q(waddr[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \waddr_reg[4] 
       (.C(ap_clk),
        .CE(push),
        .D(wnext[4]),
        .Q(waddr[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \waddr_reg[5] 
       (.C(ap_clk),
        .CE(push),
        .D(wnext[5]),
        .Q(waddr[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \waddr_reg[6] 
       (.C(ap_clk),
        .CE(push),
        .D(wnext[6]),
        .Q(waddr[6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \waddr_reg[7] 
       (.C(ap_clk),
        .CE(push),
        .D(wnext[7]),
        .Q(waddr[7]),
        .R(SR));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem0_m_axi_fifo
   (fifo_rreq_valid,
    rs2f_rreq_ack,
    E,
    empty_n_tmp_reg_0,
    D,
    next_rreq,
    S,
    \end_addr_buf_reg[31] ,
    minusOp,
    \q_reg[32]_0 ,
    empty_n_tmp_reg_1,
    \q_reg[31]_0 ,
    SR,
    ap_clk,
    \align_len_reg[31] ,
    invalid_len_event_reg,
    invalid_len_event,
    \align_len_reg[31]_0 ,
    \align_len_reg[31]_1 ,
    Q,
    plusOp_0,
    last_sect_carry__0,
    ap_rst_n,
    \pout_reg[2]_0 ,
    full_n_tmp_reg_0,
    last_sect_carry__0_0,
    push,
    invalid_len_event_reg_0,
    \q_reg[31]_1 );
  output fifo_rreq_valid;
  output rs2f_rreq_ack;
  output [0:0]E;
  output [0:0]empty_n_tmp_reg_0;
  output [19:0]D;
  output next_rreq;
  output [3:0]S;
  output [2:0]\end_addr_buf_reg[31] ;
  output [0:0]minusOp;
  output \q_reg[32]_0 ;
  output empty_n_tmp_reg_1;
  output [31:0]\q_reg[31]_0 ;
  input [0:0]SR;
  input ap_clk;
  input \align_len_reg[31] ;
  input invalid_len_event_reg;
  input invalid_len_event;
  input \align_len_reg[31]_0 ;
  input [0:0]\align_len_reg[31]_1 ;
  input [19:0]Q;
  input [18:0]plusOp_0;
  input [19:0]last_sect_carry__0;
  input ap_rst_n;
  input \pout_reg[2]_0 ;
  input [0:0]full_n_tmp_reg_0;
  input [19:0]last_sect_carry__0_0;
  input push;
  input invalid_len_event_reg_0;
  input [31:0]\q_reg[31]_1 ;

  wire [19:0]D;
  wire [0:0]E;
  wire [19:0]Q;
  wire [3:0]S;
  wire [0:0]SR;
  wire \align_len_reg[31] ;
  wire \align_len_reg[31]_0 ;
  wire [0:0]\align_len_reg[31]_1 ;
  wire ap_clk;
  wire ap_rst_n;
  wire data_vld_i_1_n_0;
  wire data_vld_i_2_n_0;
  wire data_vld_reg_n_0;
  wire empty_n_tmp_i_1_n_0;
  wire [0:0]empty_n_tmp_reg_0;
  wire empty_n_tmp_reg_1;
  wire [2:0]\end_addr_buf_reg[31] ;
  wire [32:32]fifo_rreq_data;
  wire fifo_rreq_valid;
  wire full_n_tmp_i_1_n_0;
  wire full_n_tmp_i_2__0_n_0;
  wire [0:0]full_n_tmp_reg_0;
  wire invalid_len_event;
  wire invalid_len_event_reg;
  wire invalid_len_event_reg_0;
  wire [19:0]last_sect_carry__0;
  wire [19:0]last_sect_carry__0_0;
  wire \mem_reg[4][0]_srl5_n_0 ;
  wire \mem_reg[4][10]_srl5_n_0 ;
  wire \mem_reg[4][11]_srl5_n_0 ;
  wire \mem_reg[4][12]_srl5_n_0 ;
  wire \mem_reg[4][13]_srl5_n_0 ;
  wire \mem_reg[4][14]_srl5_n_0 ;
  wire \mem_reg[4][15]_srl5_n_0 ;
  wire \mem_reg[4][16]_srl5_n_0 ;
  wire \mem_reg[4][17]_srl5_n_0 ;
  wire \mem_reg[4][18]_srl5_n_0 ;
  wire \mem_reg[4][19]_srl5_n_0 ;
  wire \mem_reg[4][1]_srl5_n_0 ;
  wire \mem_reg[4][20]_srl5_n_0 ;
  wire \mem_reg[4][21]_srl5_n_0 ;
  wire \mem_reg[4][22]_srl5_n_0 ;
  wire \mem_reg[4][23]_srl5_n_0 ;
  wire \mem_reg[4][24]_srl5_n_0 ;
  wire \mem_reg[4][25]_srl5_n_0 ;
  wire \mem_reg[4][26]_srl5_n_0 ;
  wire \mem_reg[4][27]_srl5_n_0 ;
  wire \mem_reg[4][28]_srl5_n_0 ;
  wire \mem_reg[4][29]_srl5_n_0 ;
  wire \mem_reg[4][2]_srl5_n_0 ;
  wire \mem_reg[4][30]_srl5_n_0 ;
  wire \mem_reg[4][31]_srl5_n_0 ;
  wire \mem_reg[4][32]_srl5_n_0 ;
  wire \mem_reg[4][3]_srl5_n_0 ;
  wire \mem_reg[4][4]_srl5_n_0 ;
  wire \mem_reg[4][5]_srl5_n_0 ;
  wire \mem_reg[4][6]_srl5_n_0 ;
  wire \mem_reg[4][7]_srl5_n_0 ;
  wire \mem_reg[4][8]_srl5_n_0 ;
  wire \mem_reg[4][9]_srl5_n_0 ;
  wire [0:0]minusOp;
  wire next_rreq;
  wire [18:0]plusOp_0;
  wire \pout[0]_i_1_n_0 ;
  wire \pout[1]_i_1_n_0 ;
  wire \pout[2]_i_1_n_0 ;
  wire \pout_reg[2]_0 ;
  wire \pout_reg_n_0_[0] ;
  wire \pout_reg_n_0_[1] ;
  wire \pout_reg_n_0_[2] ;
  wire push;
  wire [31:0]\q_reg[31]_0 ;
  wire [31:0]\q_reg[31]_1 ;
  wire \q_reg[32]_0 ;
  wire rs2f_rreq_ack;

  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \align_len[31]_i_1 
       (.I0(fifo_rreq_data),
        .O(minusOp));
  LUT6 #(
    .INIT(64'hFFFEFFFFAAAAAAAA)) 
    data_vld_i_1
       (.I0(push),
        .I1(\pout_reg_n_0_[2] ),
        .I2(\pout_reg_n_0_[0] ),
        .I3(\pout_reg_n_0_[1] ),
        .I4(data_vld_i_2_n_0),
        .I5(data_vld_reg_n_0),
        .O(data_vld_i_1_n_0));
  LUT6 #(
    .INIT(64'h8AAAAAAA8AAA8AAA)) 
    data_vld_i_2
       (.I0(data_vld_reg_n_0),
        .I1(invalid_len_event),
        .I2(fifo_rreq_valid),
        .I3(\align_len_reg[31] ),
        .I4(\align_len_reg[31]_0 ),
        .I5(\align_len_reg[31]_1 ),
        .O(data_vld_i_2_n_0));
  FDRE data_vld_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(data_vld_i_1_n_0),
        .Q(data_vld_reg_n_0),
        .R(SR));
  LUT5 #(
    .INIT(32'hBFFFBFBF)) 
    empty_n_tmp_i_1
       (.I0(invalid_len_event),
        .I1(fifo_rreq_valid),
        .I2(\align_len_reg[31] ),
        .I3(\align_len_reg[31]_0 ),
        .I4(\align_len_reg[31]_1 ),
        .O(empty_n_tmp_i_1_n_0));
  FDRE empty_n_tmp_reg
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(data_vld_reg_n_0),
        .Q(fifo_rreq_valid),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT5 #(
    .INIT(32'hCAAACACA)) 
    fifo_rreq_valid_buf_i_1
       (.I0(fifo_rreq_valid),
        .I1(invalid_len_event_reg),
        .I2(\align_len_reg[31] ),
        .I3(\align_len_reg[31]_0 ),
        .I4(\align_len_reg[31]_1 ),
        .O(empty_n_tmp_reg_1));
  LUT6 #(
    .INIT(64'hDFFF5F5FFFFF5555)) 
    full_n_tmp_i_1
       (.I0(ap_rst_n),
        .I1(full_n_tmp_i_2__0_n_0),
        .I2(\pout_reg[2]_0 ),
        .I3(full_n_tmp_reg_0),
        .I4(rs2f_rreq_ack),
        .I5(data_vld_reg_n_0),
        .O(full_n_tmp_i_1_n_0));
  LUT3 #(
    .INIT(8'hBF)) 
    full_n_tmp_i_2__0
       (.I0(\pout_reg_n_0_[2] ),
        .I1(\pout_reg_n_0_[0] ),
        .I2(\pout_reg_n_0_[1] ),
        .O(full_n_tmp_i_2__0_n_0));
  FDRE full_n_tmp_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(full_n_tmp_i_1_n_0),
        .Q(rs2f_rreq_ack),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT5 #(
    .INIT(32'hFF470044)) 
    invalid_len_event_i_1
       (.I0(fifo_rreq_data),
        .I1(fifo_rreq_valid),
        .I2(invalid_len_event_reg),
        .I3(invalid_len_event_reg_0),
        .I4(invalid_len_event),
        .O(\q_reg[32]_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    last_sect_carry__0_i_1
       (.I0(last_sect_carry__0_0[19]),
        .I1(last_sect_carry__0[19]),
        .I2(last_sect_carry__0_0[18]),
        .I3(last_sect_carry__0[18]),
        .O(\end_addr_buf_reg[31] [2]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    last_sect_carry__0_i_2
       (.I0(last_sect_carry__0_0[17]),
        .I1(last_sect_carry__0[17]),
        .I2(last_sect_carry__0[15]),
        .I3(last_sect_carry__0_0[15]),
        .I4(last_sect_carry__0[16]),
        .I5(last_sect_carry__0_0[16]),
        .O(\end_addr_buf_reg[31] [1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    last_sect_carry__0_i_3
       (.I0(last_sect_carry__0[12]),
        .I1(last_sect_carry__0_0[12]),
        .I2(last_sect_carry__0[13]),
        .I3(last_sect_carry__0_0[13]),
        .I4(last_sect_carry__0_0[14]),
        .I5(last_sect_carry__0[14]),
        .O(\end_addr_buf_reg[31] [0]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    last_sect_carry_i_1
       (.I0(last_sect_carry__0[9]),
        .I1(last_sect_carry__0_0[9]),
        .I2(last_sect_carry__0[10]),
        .I3(last_sect_carry__0_0[10]),
        .I4(last_sect_carry__0_0[11]),
        .I5(last_sect_carry__0[11]),
        .O(S[3]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    last_sect_carry_i_2
       (.I0(last_sect_carry__0[6]),
        .I1(last_sect_carry__0_0[6]),
        .I2(last_sect_carry__0[7]),
        .I3(last_sect_carry__0_0[7]),
        .I4(last_sect_carry__0_0[8]),
        .I5(last_sect_carry__0[8]),
        .O(S[2]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    last_sect_carry_i_3
       (.I0(last_sect_carry__0_0[5]),
        .I1(last_sect_carry__0[5]),
        .I2(last_sect_carry__0[3]),
        .I3(last_sect_carry__0_0[3]),
        .I4(last_sect_carry__0[4]),
        .I5(last_sect_carry__0_0[4]),
        .O(S[1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    last_sect_carry_i_4
       (.I0(last_sect_carry__0_0[0]),
        .I1(last_sect_carry__0[0]),
        .I2(last_sect_carry__0[1]),
        .I3(last_sect_carry__0_0[1]),
        .I4(last_sect_carry__0[2]),
        .I5(last_sect_carry__0_0[2]),
        .O(S[0]));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][0]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][0]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [0]),
        .Q(\mem_reg[4][0]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][10]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][10]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [10]),
        .Q(\mem_reg[4][10]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][11]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][11]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [11]),
        .Q(\mem_reg[4][11]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][12]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][12]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [12]),
        .Q(\mem_reg[4][12]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][13]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][13]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [13]),
        .Q(\mem_reg[4][13]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][14]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][14]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [14]),
        .Q(\mem_reg[4][14]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][15]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][15]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [15]),
        .Q(\mem_reg[4][15]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][16]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][16]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [16]),
        .Q(\mem_reg[4][16]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][17]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][17]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [17]),
        .Q(\mem_reg[4][17]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][18]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][18]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [18]),
        .Q(\mem_reg[4][18]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][19]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][19]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [19]),
        .Q(\mem_reg[4][19]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][1]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][1]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [1]),
        .Q(\mem_reg[4][1]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][20]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][20]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [20]),
        .Q(\mem_reg[4][20]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][21]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][21]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [21]),
        .Q(\mem_reg[4][21]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][22]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][22]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [22]),
        .Q(\mem_reg[4][22]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][23]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][23]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [23]),
        .Q(\mem_reg[4][23]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][24]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][24]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [24]),
        .Q(\mem_reg[4][24]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][25]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][25]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [25]),
        .Q(\mem_reg[4][25]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][26]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][26]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [26]),
        .Q(\mem_reg[4][26]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][27]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][27]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [27]),
        .Q(\mem_reg[4][27]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][28]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][28]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [28]),
        .Q(\mem_reg[4][28]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][29]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][29]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [29]),
        .Q(\mem_reg[4][29]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][2]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][2]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [2]),
        .Q(\mem_reg[4][2]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][30]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][30]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [30]),
        .Q(\mem_reg[4][30]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][31]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][31]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [31]),
        .Q(\mem_reg[4][31]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][32]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][32]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(1'b1),
        .Q(\mem_reg[4][32]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][3]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][3]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [3]),
        .Q(\mem_reg[4][3]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][4]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][4]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [4]),
        .Q(\mem_reg[4][4]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][5]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][5]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [5]),
        .Q(\mem_reg[4][5]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][6]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][6]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [6]),
        .Q(\mem_reg[4][6]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][7]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][7]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [7]),
        .Q(\mem_reg[4][7]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][8]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][8]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [8]),
        .Q(\mem_reg[4][8]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/fifo_rreq/mem_reg[4][9]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][9]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[31]_1 [9]),
        .Q(\mem_reg[4][9]_srl5_n_0 ));
  LUT6 #(
    .INIT(64'h7777BBBB88884440)) 
    \pout[0]_i_1 
       (.I0(\pout_reg[2]_0 ),
        .I1(data_vld_reg_n_0),
        .I2(\pout_reg_n_0_[2] ),
        .I3(\pout_reg_n_0_[1] ),
        .I4(push),
        .I5(\pout_reg_n_0_[0] ),
        .O(\pout[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6C6CCCCCC9C8CCCC)) 
    \pout[1]_i_1 
       (.I0(push),
        .I1(\pout_reg_n_0_[1] ),
        .I2(\pout_reg_n_0_[0] ),
        .I3(\pout_reg_n_0_[2] ),
        .I4(data_vld_reg_n_0),
        .I5(\pout_reg[2]_0 ),
        .O(\pout[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7F80FF00FE00FF00)) 
    \pout[2]_i_1 
       (.I0(push),
        .I1(\pout_reg_n_0_[1] ),
        .I2(\pout_reg_n_0_[0] ),
        .I3(\pout_reg_n_0_[2] ),
        .I4(data_vld_reg_n_0),
        .I5(\pout_reg[2]_0 ),
        .O(\pout[2]_i_1_n_0 ));
  FDRE \pout_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\pout[0]_i_1_n_0 ),
        .Q(\pout_reg_n_0_[0] ),
        .R(SR));
  FDRE \pout_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\pout[1]_i_1_n_0 ),
        .Q(\pout_reg_n_0_[1] ),
        .R(SR));
  FDRE \pout_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\pout[2]_i_1_n_0 ),
        .Q(\pout_reg_n_0_[2] ),
        .R(SR));
  FDRE \q_reg[0] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][0]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [0]),
        .R(SR));
  FDRE \q_reg[10] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][10]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [10]),
        .R(SR));
  FDRE \q_reg[11] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][11]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [11]),
        .R(SR));
  FDRE \q_reg[12] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][12]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [12]),
        .R(SR));
  FDRE \q_reg[13] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][13]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [13]),
        .R(SR));
  FDRE \q_reg[14] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][14]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [14]),
        .R(SR));
  FDRE \q_reg[15] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][15]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [15]),
        .R(SR));
  FDRE \q_reg[16] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][16]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [16]),
        .R(SR));
  FDRE \q_reg[17] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][17]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [17]),
        .R(SR));
  FDRE \q_reg[18] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][18]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [18]),
        .R(SR));
  FDRE \q_reg[19] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][19]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [19]),
        .R(SR));
  FDRE \q_reg[1] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][1]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [1]),
        .R(SR));
  FDRE \q_reg[20] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][20]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [20]),
        .R(SR));
  FDRE \q_reg[21] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][21]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [21]),
        .R(SR));
  FDRE \q_reg[22] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][22]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [22]),
        .R(SR));
  FDRE \q_reg[23] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][23]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [23]),
        .R(SR));
  FDRE \q_reg[24] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][24]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [24]),
        .R(SR));
  FDRE \q_reg[25] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][25]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [25]),
        .R(SR));
  FDRE \q_reg[26] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][26]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [26]),
        .R(SR));
  FDRE \q_reg[27] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][27]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [27]),
        .R(SR));
  FDRE \q_reg[28] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][28]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [28]),
        .R(SR));
  FDRE \q_reg[29] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][29]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [29]),
        .R(SR));
  FDRE \q_reg[2] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][2]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [2]),
        .R(SR));
  FDRE \q_reg[30] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][30]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [30]),
        .R(SR));
  FDRE \q_reg[31] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][31]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [31]),
        .R(SR));
  FDRE \q_reg[32] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][32]_srl5_n_0 ),
        .Q(fifo_rreq_data),
        .R(SR));
  FDRE \q_reg[3] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][3]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [3]),
        .R(SR));
  FDRE \q_reg[4] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][4]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [4]),
        .R(SR));
  FDRE \q_reg[5] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][5]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [5]),
        .R(SR));
  FDRE \q_reg[6] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][6]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [6]),
        .R(SR));
  FDRE \q_reg[7] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][7]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [7]),
        .R(SR));
  FDRE \q_reg[8] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][8]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [8]),
        .R(SR));
  FDRE \q_reg[9] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1_n_0),
        .D(\mem_reg[4][9]_srl5_n_0 ),
        .Q(\q_reg[31]_0 [9]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT3 #(
    .INIT(8'h8B)) 
    \sect_cnt[0]_i_1 
       (.I0(Q[0]),
        .I1(next_rreq),
        .I2(last_sect_carry__0[0]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[10]_i_1 
       (.I0(Q[10]),
        .I1(next_rreq),
        .I2(plusOp_0[9]),
        .O(D[10]));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[11]_i_1 
       (.I0(Q[11]),
        .I1(next_rreq),
        .I2(plusOp_0[10]),
        .O(D[11]));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[12]_i_1 
       (.I0(Q[12]),
        .I1(next_rreq),
        .I2(plusOp_0[11]),
        .O(D[12]));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[13]_i_1 
       (.I0(Q[13]),
        .I1(next_rreq),
        .I2(plusOp_0[12]),
        .O(D[13]));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[14]_i_1 
       (.I0(Q[14]),
        .I1(next_rreq),
        .I2(plusOp_0[13]),
        .O(D[14]));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[15]_i_1 
       (.I0(Q[15]),
        .I1(next_rreq),
        .I2(plusOp_0[14]),
        .O(D[15]));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[16]_i_1 
       (.I0(Q[16]),
        .I1(next_rreq),
        .I2(plusOp_0[15]),
        .O(D[16]));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[17]_i_1 
       (.I0(Q[17]),
        .I1(next_rreq),
        .I2(plusOp_0[16]),
        .O(D[17]));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[18]_i_1 
       (.I0(Q[18]),
        .I1(next_rreq),
        .I2(plusOp_0[17]),
        .O(D[18]));
  LUT5 #(
    .INIT(32'h0054FFFF)) 
    \sect_cnt[19]_i_1__0 
       (.I0(\align_len_reg[31] ),
        .I1(invalid_len_event_reg),
        .I2(fifo_rreq_valid),
        .I3(invalid_len_event),
        .I4(\align_len_reg[31]_0 ),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[19]_i_2 
       (.I0(Q[19]),
        .I1(next_rreq),
        .I2(plusOp_0[18]),
        .O(D[19]));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[1]_i_1 
       (.I0(Q[1]),
        .I1(next_rreq),
        .I2(plusOp_0[0]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[2]_i_1 
       (.I0(Q[2]),
        .I1(next_rreq),
        .I2(plusOp_0[1]),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[3]_i_1 
       (.I0(Q[3]),
        .I1(next_rreq),
        .I2(plusOp_0[2]),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[4]_i_1 
       (.I0(Q[4]),
        .I1(next_rreq),
        .I2(plusOp_0[3]),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[5]_i_1 
       (.I0(Q[5]),
        .I1(next_rreq),
        .I2(plusOp_0[4]),
        .O(D[5]));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[6]_i_1 
       (.I0(Q[6]),
        .I1(next_rreq),
        .I2(plusOp_0[5]),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[7]_i_1 
       (.I0(Q[7]),
        .I1(next_rreq),
        .I2(plusOp_0[6]),
        .O(D[7]));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[8]_i_1 
       (.I0(Q[8]),
        .I1(next_rreq),
        .I2(plusOp_0[7]),
        .O(D[8]));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[9]_i_1 
       (.I0(Q[9]),
        .I1(next_rreq),
        .I2(plusOp_0[8]),
        .O(D[9]));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT4 #(
    .INIT(16'h08AA)) 
    \start_addr[31]_i_1 
       (.I0(fifo_rreq_valid),
        .I1(\align_len_reg[31]_1 ),
        .I2(\align_len_reg[31]_0 ),
        .I3(\align_len_reg[31] ),
        .O(empty_n_tmp_reg_0));
  LUT6 #(
    .INIT(64'h0054545400540054)) 
    \start_addr_buf[31]_i_1 
       (.I0(invalid_len_event),
        .I1(fifo_rreq_valid),
        .I2(invalid_len_event_reg),
        .I3(\align_len_reg[31] ),
        .I4(\align_len_reg[31]_0 ),
        .I5(\align_len_reg[31]_1 ),
        .O(next_rreq));
endmodule

(* ORIG_REF_NAME = "filter_gmem0_m_axi_fifo" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem0_m_axi_fifo__parameterized1
   (full_n0_in,
    ap_rst_n_0,
    last_split,
    \bus_wide_gen.split_cnt_buf_reg[1] ,
    E,
    \bus_wide_gen.split_cnt_buf_reg[0] ,
    \sect_len_buf_reg[9] ,
    in,
    D,
    \q_reg[11]_0 ,
    \q_reg[10]_0 ,
    s_ready_t_reg,
    \bus_wide_gen.split_cnt_buf_reg[0]_0 ,
    \bus_wide_gen.split_cnt_buf_reg[0]_1 ,
    \could_multi_bursts.loop_cnt_reg[0] ,
    full_n_tmp_reg_0,
    s_ready_t_reg_0,
    SR,
    ap_clk,
    ap_rst_n,
    \bus_wide_gen.split_cnt_buf_reg[1]_0 ,
    \bus_wide_gen.split_cnt_buf_reg[0]_2 ,
    push,
    \q_reg[8]_0 ,
    \q_reg[9]_0 ,
    Q,
    \could_multi_bursts.arlen_buf_reg[0] ,
    \bus_wide_gen.data_buf_reg[31] ,
    \bus_wide_gen.len_cnt_reg[0] ,
    s_ready,
    \bus_wide_gen.rdata_valid_t_reg ,
    beat_valid,
    \bus_wide_gen.data_buf_reg[21] ,
    \bus_wide_gen.data_buf_reg[0] ,
    \bus_wide_gen.data_buf_reg[1] ,
    \bus_wide_gen.data_buf_reg[2] ,
    \bus_wide_gen.data_buf_reg[3] ,
    \bus_wide_gen.data_buf_reg[4] ,
    \bus_wide_gen.data_buf_reg[5] ,
    \bus_wide_gen.data_buf_reg[6] ,
    \bus_wide_gen.data_buf_reg[7] ,
    \bus_wide_gen.data_buf_reg[8] ,
    \bus_wide_gen.data_buf_reg[9] ,
    \bus_wide_gen.data_buf_reg[10] ,
    \bus_wide_gen.data_buf_reg[11] ,
    \bus_wide_gen.data_buf_reg[12] ,
    \bus_wide_gen.data_buf_reg[13] ,
    \bus_wide_gen.data_buf_reg[14] ,
    \bus_wide_gen.data_buf_reg[15] ,
    \bus_wide_gen.data_buf_reg[25] ,
    \bus_wide_gen.len_cnt_reg[0]_0 ,
    \bus_wide_gen.split_cnt_buf[1]_i_3_0 ,
    \q_reg[11]_1 ,
    fifo_rctl_ready,
    \could_multi_bursts.ARVALID_Dummy_reg ,
    m_axi_gmem0_ARVALID,
    m_axi_gmem0_ARREADY);
  output full_n0_in;
  output [0:0]ap_rst_n_0;
  output last_split;
  output \bus_wide_gen.split_cnt_buf_reg[1] ;
  output [0:0]E;
  output \bus_wide_gen.split_cnt_buf_reg[0] ;
  output \sect_len_buf_reg[9] ;
  output [3:0]in;
  output [29:0]D;
  output [1:0]\q_reg[11]_0 ;
  output \q_reg[10]_0 ;
  output s_ready_t_reg;
  output \bus_wide_gen.split_cnt_buf_reg[0]_0 ;
  output \bus_wide_gen.split_cnt_buf_reg[0]_1 ;
  output \could_multi_bursts.loop_cnt_reg[0] ;
  output full_n_tmp_reg_0;
  output s_ready_t_reg_0;
  input [0:0]SR;
  input ap_clk;
  input ap_rst_n;
  input \bus_wide_gen.split_cnt_buf_reg[1]_0 ;
  input \bus_wide_gen.split_cnt_buf_reg[0]_2 ;
  input push;
  input \q_reg[8]_0 ;
  input \q_reg[9]_0 ;
  input [9:0]Q;
  input [5:0]\could_multi_bursts.arlen_buf_reg[0] ;
  input [21:0]\bus_wide_gen.data_buf_reg[31] ;
  input \bus_wide_gen.len_cnt_reg[0] ;
  input s_ready;
  input \bus_wide_gen.rdata_valid_t_reg ;
  input beat_valid;
  input [21:0]\bus_wide_gen.data_buf_reg[21] ;
  input \bus_wide_gen.data_buf_reg[0] ;
  input \bus_wide_gen.data_buf_reg[1] ;
  input \bus_wide_gen.data_buf_reg[2] ;
  input \bus_wide_gen.data_buf_reg[3] ;
  input \bus_wide_gen.data_buf_reg[4] ;
  input \bus_wide_gen.data_buf_reg[5] ;
  input \bus_wide_gen.data_buf_reg[6] ;
  input \bus_wide_gen.data_buf_reg[7] ;
  input \bus_wide_gen.data_buf_reg[8] ;
  input \bus_wide_gen.data_buf_reg[9] ;
  input \bus_wide_gen.data_buf_reg[10] ;
  input \bus_wide_gen.data_buf_reg[11] ;
  input \bus_wide_gen.data_buf_reg[12] ;
  input \bus_wide_gen.data_buf_reg[13] ;
  input \bus_wide_gen.data_buf_reg[14] ;
  input \bus_wide_gen.data_buf_reg[15] ;
  input \bus_wide_gen.data_buf_reg[25] ;
  input \bus_wide_gen.len_cnt_reg[0]_0 ;
  input [7:0]\bus_wide_gen.split_cnt_buf[1]_i_3_0 ;
  input [1:0]\q_reg[11]_1 ;
  input fifo_rctl_ready;
  input \could_multi_bursts.ARVALID_Dummy_reg ;
  input m_axi_gmem0_ARVALID;
  input m_axi_gmem0_ARREADY;

  wire [29:0]D;
  wire [0:0]E;
  wire [9:0]Q;
  wire [0:0]SR;
  wire ap_clk;
  wire ap_rst_n;
  wire [0:0]ap_rst_n_0;
  wire beat_valid;
  wire burst_valid;
  wire \bus_wide_gen.data_buf[16]_i_2_n_0 ;
  wire \bus_wide_gen.data_buf[17]_i_2_n_0 ;
  wire \bus_wide_gen.data_buf[18]_i_2_n_0 ;
  wire \bus_wide_gen.data_buf[19]_i_2_n_0 ;
  wire \bus_wide_gen.data_buf[20]_i_2_n_0 ;
  wire \bus_wide_gen.data_buf[21]_i_2_n_0 ;
  wire \bus_wide_gen.data_buf[31]_i_3_n_0 ;
  wire \bus_wide_gen.data_buf[31]_i_7_n_0 ;
  wire \bus_wide_gen.data_buf_reg[0] ;
  wire \bus_wide_gen.data_buf_reg[10] ;
  wire \bus_wide_gen.data_buf_reg[11] ;
  wire \bus_wide_gen.data_buf_reg[12] ;
  wire \bus_wide_gen.data_buf_reg[13] ;
  wire \bus_wide_gen.data_buf_reg[14] ;
  wire \bus_wide_gen.data_buf_reg[15] ;
  wire \bus_wide_gen.data_buf_reg[1] ;
  wire [21:0]\bus_wide_gen.data_buf_reg[21] ;
  wire \bus_wide_gen.data_buf_reg[25] ;
  wire \bus_wide_gen.data_buf_reg[2] ;
  wire [21:0]\bus_wide_gen.data_buf_reg[31] ;
  wire \bus_wide_gen.data_buf_reg[3] ;
  wire \bus_wide_gen.data_buf_reg[4] ;
  wire \bus_wide_gen.data_buf_reg[5] ;
  wire \bus_wide_gen.data_buf_reg[6] ;
  wire \bus_wide_gen.data_buf_reg[7] ;
  wire \bus_wide_gen.data_buf_reg[8] ;
  wire \bus_wide_gen.data_buf_reg[9] ;
  wire \bus_wide_gen.len_cnt[7]_i_4_n_0 ;
  wire \bus_wide_gen.len_cnt[7]_i_7_n_0 ;
  wire \bus_wide_gen.len_cnt_reg[0] ;
  wire \bus_wide_gen.len_cnt_reg[0]_0 ;
  wire \bus_wide_gen.rdata_valid_t_i_2_n_0 ;
  wire \bus_wide_gen.rdata_valid_t_reg ;
  wire \bus_wide_gen.split_cnt_buf[1]_i_2_n_0 ;
  wire [7:0]\bus_wide_gen.split_cnt_buf[1]_i_3_0 ;
  wire \bus_wide_gen.split_cnt_buf[1]_i_3_n_0 ;
  wire \bus_wide_gen.split_cnt_buf[1]_i_4_n_0 ;
  wire \bus_wide_gen.split_cnt_buf_reg[0] ;
  wire \bus_wide_gen.split_cnt_buf_reg[0]_0 ;
  wire \bus_wide_gen.split_cnt_buf_reg[0]_1 ;
  wire \bus_wide_gen.split_cnt_buf_reg[0]_2 ;
  wire \bus_wide_gen.split_cnt_buf_reg[1] ;
  wire \bus_wide_gen.split_cnt_buf_reg[1]_0 ;
  wire \could_multi_bursts.ARVALID_Dummy_reg ;
  wire \could_multi_bursts.arlen_buf[3]_i_3_n_0 ;
  wire \could_multi_bursts.arlen_buf[3]_i_4_n_0 ;
  wire [5:0]\could_multi_bursts.arlen_buf_reg[0] ;
  wire \could_multi_bursts.loop_cnt_reg[0] ;
  wire data_vld_i_1__1_n_0;
  wire data_vld_reg_n_0;
  wire fifo_rctl_ready;
  wire full_n0_in;
  wire full_n_tmp_i_1__1_n_0;
  wire full_n_tmp_i_2_n_0;
  wire full_n_tmp_i_3_n_0;
  wire full_n_tmp_i_4_n_0;
  wire full_n_tmp_reg_0;
  wire [3:0]in;
  wire last_split;
  wire m_axi_gmem0_ARREADY;
  wire m_axi_gmem0_ARVALID;
  wire \mem_reg[4][0]_srl5_n_0 ;
  wire \mem_reg[4][10]_srl5_i_1_n_0 ;
  wire \mem_reg[4][10]_srl5_n_0 ;
  wire \mem_reg[4][11]_srl5_i_1_n_0 ;
  wire \mem_reg[4][11]_srl5_n_0 ;
  wire \mem_reg[4][1]_srl5_n_0 ;
  wire \mem_reg[4][2]_srl5_n_0 ;
  wire \mem_reg[4][3]_srl5_n_0 ;
  wire \mem_reg[4][8]_srl5_i_1_n_0 ;
  wire \mem_reg[4][8]_srl5_n_0 ;
  wire \mem_reg[4][9]_srl5_i_1_n_0 ;
  wire \mem_reg[4][9]_srl5_n_0 ;
  wire \pout[0]_i_1_n_0 ;
  wire \pout[1]_i_1_n_0 ;
  wire \pout[2]_i_1_n_0 ;
  wire \pout_reg_n_0_[0] ;
  wire \pout_reg_n_0_[1] ;
  wire \pout_reg_n_0_[2] ;
  wire push;
  wire \q[11]_i_1_n_0 ;
  wire \q[11]_i_2_n_0 ;
  wire \q_reg[10]_0 ;
  wire [1:0]\q_reg[11]_0 ;
  wire [1:0]\q_reg[11]_1 ;
  wire \q_reg[8]_0 ;
  wire \q_reg[9]_0 ;
  wire \q_reg_n_0_[0] ;
  wire \q_reg_n_0_[1] ;
  wire \q_reg_n_0_[2] ;
  wire \q_reg_n_0_[3] ;
  wire s_ready;
  wire s_ready_t_reg;
  wire s_ready_t_reg_0;
  wire \sect_len_buf_reg[9] ;
  wire [1:0]tail_split;

  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \bus_wide_gen.data_buf[0]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [0]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf_reg[0] ),
        .I3(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .I4(\bus_wide_gen.data_buf_reg[31] [0]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bus_wide_gen.data_buf[10]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [10]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf_reg[10] ),
        .O(D[10]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bus_wide_gen.data_buf[11]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [11]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf_reg[11] ),
        .O(D[11]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bus_wide_gen.data_buf[12]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [12]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf_reg[12] ),
        .O(D[12]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bus_wide_gen.data_buf[13]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [13]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf_reg[13] ),
        .O(D[13]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bus_wide_gen.data_buf[14]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [14]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf_reg[14] ),
        .O(D[14]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bus_wide_gen.data_buf[15]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [15]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf_reg[15] ),
        .O(D[15]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bus_wide_gen.data_buf[16]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [16]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf[16]_i_2_n_0 ),
        .O(D[16]));
  LUT5 #(
    .INIT(32'h0BFF0800)) 
    \bus_wide_gen.data_buf[16]_i_2 
       (.I0(\bus_wide_gen.data_buf_reg[31] [14]),
        .I1(\q_reg[11]_0 [0]),
        .I2(\q_reg[11]_0 [1]),
        .I3(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .I4(\bus_wide_gen.data_buf_reg[31] [8]),
        .O(\bus_wide_gen.data_buf[16]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bus_wide_gen.data_buf[17]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [17]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf[17]_i_2_n_0 ),
        .O(D[17]));
  LUT5 #(
    .INIT(32'h0BFF0800)) 
    \bus_wide_gen.data_buf[17]_i_2 
       (.I0(\bus_wide_gen.data_buf_reg[31] [15]),
        .I1(\q_reg[11]_0 [0]),
        .I2(\q_reg[11]_0 [1]),
        .I3(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .I4(\bus_wide_gen.data_buf_reg[31] [9]),
        .O(\bus_wide_gen.data_buf[17]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bus_wide_gen.data_buf[18]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [18]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf[18]_i_2_n_0 ),
        .O(D[18]));
  LUT5 #(
    .INIT(32'h0BFF0800)) 
    \bus_wide_gen.data_buf[18]_i_2 
       (.I0(\bus_wide_gen.data_buf_reg[31] [16]),
        .I1(\q_reg[11]_0 [0]),
        .I2(\q_reg[11]_0 [1]),
        .I3(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .I4(\bus_wide_gen.data_buf_reg[31] [10]),
        .O(\bus_wide_gen.data_buf[18]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bus_wide_gen.data_buf[19]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [19]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf[19]_i_2_n_0 ),
        .O(D[19]));
  LUT5 #(
    .INIT(32'h0BFF0800)) 
    \bus_wide_gen.data_buf[19]_i_2 
       (.I0(\bus_wide_gen.data_buf_reg[31] [17]),
        .I1(\q_reg[11]_0 [0]),
        .I2(\q_reg[11]_0 [1]),
        .I3(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .I4(\bus_wide_gen.data_buf_reg[31] [11]),
        .O(\bus_wide_gen.data_buf[19]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \bus_wide_gen.data_buf[1]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [1]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf_reg[1] ),
        .I3(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .I4(\bus_wide_gen.data_buf_reg[31] [1]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bus_wide_gen.data_buf[20]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [20]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf[20]_i_2_n_0 ),
        .O(D[20]));
  LUT5 #(
    .INIT(32'h0BFF0800)) 
    \bus_wide_gen.data_buf[20]_i_2 
       (.I0(\bus_wide_gen.data_buf_reg[31] [18]),
        .I1(\q_reg[11]_0 [0]),
        .I2(\q_reg[11]_0 [1]),
        .I3(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .I4(\bus_wide_gen.data_buf_reg[31] [12]),
        .O(\bus_wide_gen.data_buf[20]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bus_wide_gen.data_buf[21]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [21]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf[21]_i_2_n_0 ),
        .O(D[21]));
  LUT5 #(
    .INIT(32'h0BFF0800)) 
    \bus_wide_gen.data_buf[21]_i_2 
       (.I0(\bus_wide_gen.data_buf_reg[31] [19]),
        .I1(\q_reg[11]_0 [0]),
        .I2(\q_reg[11]_0 [1]),
        .I3(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .I4(\bus_wide_gen.data_buf_reg[31] [13]),
        .O(\bus_wide_gen.data_buf[21]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h10001F00)) 
    \bus_wide_gen.data_buf[24]_i_1 
       (.I0(\q_reg[11]_0 [1]),
        .I1(\q_reg[11]_0 [0]),
        .I2(\q_reg[10]_0 ),
        .I3(\bus_wide_gen.data_buf_reg[31] [14]),
        .I4(s_ready_t_reg),
        .O(D[22]));
  LUT5 #(
    .INIT(32'h57005500)) 
    \bus_wide_gen.data_buf[25]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[25] ),
        .I1(\q_reg[11]_0 [0]),
        .I2(\q_reg[11]_0 [1]),
        .I3(\bus_wide_gen.data_buf_reg[31] [15]),
        .I4(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .O(D[23]));
  LUT5 #(
    .INIT(32'h57005500)) 
    \bus_wide_gen.data_buf[26]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[25] ),
        .I1(\q_reg[11]_0 [0]),
        .I2(\q_reg[11]_0 [1]),
        .I3(\bus_wide_gen.data_buf_reg[31] [16]),
        .I4(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .O(D[24]));
  LUT5 #(
    .INIT(32'h10001F00)) 
    \bus_wide_gen.data_buf[27]_i_1 
       (.I0(\q_reg[11]_0 [1]),
        .I1(\q_reg[11]_0 [0]),
        .I2(\q_reg[10]_0 ),
        .I3(\bus_wide_gen.data_buf_reg[31] [17]),
        .I4(s_ready_t_reg),
        .O(D[25]));
  LUT6 #(
    .INIT(64'h0000000020130203)) 
    \bus_wide_gen.data_buf[27]_i_2 
       (.I0(\q_reg[11]_0 [0]),
        .I1(\bus_wide_gen.data_buf[31]_i_7_n_0 ),
        .I2(\bus_wide_gen.split_cnt_buf_reg[1]_0 ),
        .I3(\bus_wide_gen.split_cnt_buf_reg[0]_2 ),
        .I4(\q_reg[11]_0 [1]),
        .I5(\bus_wide_gen.len_cnt_reg[0] ),
        .O(\q_reg[10]_0 ));
  LUT5 #(
    .INIT(32'h000075FF)) 
    \bus_wide_gen.data_buf[27]_i_3 
       (.I0(\bus_wide_gen.split_cnt_buf_reg[0]_0 ),
        .I1(s_ready),
        .I2(\bus_wide_gen.rdata_valid_t_reg ),
        .I3(beat_valid),
        .I4(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .O(s_ready_t_reg));
  LUT5 #(
    .INIT(32'h57005500)) 
    \bus_wide_gen.data_buf[28]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[25] ),
        .I1(\q_reg[11]_0 [0]),
        .I2(\q_reg[11]_0 [1]),
        .I3(\bus_wide_gen.data_buf_reg[31] [18]),
        .I4(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .O(D[26]));
  LUT5 #(
    .INIT(32'h57005500)) 
    \bus_wide_gen.data_buf[29]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[25] ),
        .I1(\q_reg[11]_0 [0]),
        .I2(\q_reg[11]_0 [1]),
        .I3(\bus_wide_gen.data_buf_reg[31] [19]),
        .I4(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .O(D[27]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \bus_wide_gen.data_buf[2]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [2]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf_reg[2] ),
        .I3(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .I4(\bus_wide_gen.data_buf_reg[31] [2]),
        .O(D[2]));
  LUT5 #(
    .INIT(32'h57005500)) 
    \bus_wide_gen.data_buf[30]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[25] ),
        .I1(\q_reg[11]_0 [0]),
        .I2(\q_reg[11]_0 [1]),
        .I3(\bus_wide_gen.data_buf_reg[31] [20]),
        .I4(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .O(D[28]));
  LUT5 #(
    .INIT(32'hBB0BBBBB)) 
    \bus_wide_gen.data_buf[31]_i_1 
       (.I0(s_ready),
        .I1(\bus_wide_gen.rdata_valid_t_reg ),
        .I2(\bus_wide_gen.data_buf[31]_i_3_n_0 ),
        .I3(beat_valid),
        .I4(\bus_wide_gen.split_cnt_buf_reg[0]_0 ),
        .O(E));
  LUT5 #(
    .INIT(32'h57005500)) 
    \bus_wide_gen.data_buf[31]_i_2 
       (.I0(\bus_wide_gen.data_buf_reg[25] ),
        .I1(\q_reg[11]_0 [0]),
        .I2(\q_reg[11]_0 [1]),
        .I3(\bus_wide_gen.data_buf_reg[31] [21]),
        .I4(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .O(D[29]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT5 #(
    .INIT(32'hBEFFFABA)) 
    \bus_wide_gen.data_buf[31]_i_3 
       (.I0(\bus_wide_gen.data_buf[31]_i_7_n_0 ),
        .I1(\q_reg[11]_0 [1]),
        .I2(\bus_wide_gen.split_cnt_buf_reg[1]_0 ),
        .I3(\q_reg[11]_0 [0]),
        .I4(\bus_wide_gen.split_cnt_buf_reg[0]_2 ),
        .O(\bus_wide_gen.data_buf[31]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \bus_wide_gen.data_buf[31]_i_4 
       (.I0(\bus_wide_gen.split_cnt_buf_reg[0]_2 ),
        .I1(\bus_wide_gen.data_buf[31]_i_7_n_0 ),
        .I2(\bus_wide_gen.split_cnt_buf_reg[1]_0 ),
        .O(\bus_wide_gen.split_cnt_buf_reg[0]_0 ));
  LUT6 #(
    .INIT(64'h0000000041110051)) 
    \bus_wide_gen.data_buf[31]_i_6 
       (.I0(\bus_wide_gen.len_cnt_reg[0] ),
        .I1(\bus_wide_gen.split_cnt_buf_reg[0]_2 ),
        .I2(\q_reg[11]_0 [0]),
        .I3(\bus_wide_gen.split_cnt_buf_reg[1]_0 ),
        .I4(\q_reg[11]_0 [1]),
        .I5(\bus_wide_gen.data_buf[31]_i_7_n_0 ),
        .O(\bus_wide_gen.split_cnt_buf_reg[0]_1 ));
  LUT5 #(
    .INIT(32'hFFFEFFFF)) 
    \bus_wide_gen.data_buf[31]_i_7 
       (.I0(\bus_wide_gen.split_cnt_buf[1]_i_3_0 [3]),
        .I1(\bus_wide_gen.split_cnt_buf[1]_i_3_0 [2]),
        .I2(\bus_wide_gen.split_cnt_buf[1]_i_3_0 [1]),
        .I3(\bus_wide_gen.split_cnt_buf[1]_i_3_0 [0]),
        .I4(\bus_wide_gen.split_cnt_buf[1]_i_4_n_0 ),
        .O(\bus_wide_gen.data_buf[31]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \bus_wide_gen.data_buf[3]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [3]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf_reg[3] ),
        .I3(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .I4(\bus_wide_gen.data_buf_reg[31] [3]),
        .O(D[3]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \bus_wide_gen.data_buf[4]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [4]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf_reg[4] ),
        .I3(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .I4(\bus_wide_gen.data_buf_reg[31] [4]),
        .O(D[4]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \bus_wide_gen.data_buf[5]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [5]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf_reg[5] ),
        .I3(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .I4(\bus_wide_gen.data_buf_reg[31] [5]),
        .O(D[5]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \bus_wide_gen.data_buf[6]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [6]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf_reg[6] ),
        .I3(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .I4(\bus_wide_gen.data_buf_reg[31] [6]),
        .O(D[6]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \bus_wide_gen.data_buf[7]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [7]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf_reg[7] ),
        .I3(\bus_wide_gen.split_cnt_buf_reg[0]_1 ),
        .I4(\bus_wide_gen.data_buf_reg[31] [7]),
        .O(D[7]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bus_wide_gen.data_buf[8]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [8]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf_reg[8] ),
        .O(D[8]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bus_wide_gen.data_buf[9]_i_1 
       (.I0(\bus_wide_gen.data_buf_reg[21] [9]),
        .I1(s_ready_t_reg),
        .I2(\bus_wide_gen.data_buf_reg[9] ),
        .O(D[9]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \bus_wide_gen.len_cnt[7]_i_1 
       (.I0(\bus_wide_gen.len_cnt[7]_i_4_n_0 ),
        .I1(last_split),
        .I2(ap_rst_n),
        .O(ap_rst_n_0));
  LUT6 #(
    .INIT(64'h1400001411001100)) 
    \bus_wide_gen.len_cnt[7]_i_2 
       (.I0(\bus_wide_gen.len_cnt_reg[0] ),
        .I1(\bus_wide_gen.split_cnt_buf[1]_i_3_n_0 ),
        .I2(tail_split[0]),
        .I3(\bus_wide_gen.split_cnt_buf[1]_i_2_n_0 ),
        .I4(tail_split[1]),
        .I5(\bus_wide_gen.len_cnt[7]_i_4_n_0 ),
        .O(last_split));
  LUT6 #(
    .INIT(64'h2002000000002002)) 
    \bus_wide_gen.len_cnt[7]_i_4 
       (.I0(\bus_wide_gen.split_cnt_buf[1]_i_4_n_0 ),
        .I1(\bus_wide_gen.len_cnt[7]_i_7_n_0 ),
        .I2(\q_reg_n_0_[3] ),
        .I3(\bus_wide_gen.split_cnt_buf[1]_i_3_0 [3]),
        .I4(\q_reg_n_0_[1] ),
        .I5(\bus_wide_gen.split_cnt_buf[1]_i_3_0 [1]),
        .O(\bus_wide_gen.len_cnt[7]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h6FF6)) 
    \bus_wide_gen.len_cnt[7]_i_7 
       (.I0(\q_reg_n_0_[0] ),
        .I1(\bus_wide_gen.split_cnt_buf[1]_i_3_0 [0]),
        .I2(\q_reg_n_0_[2] ),
        .I3(\bus_wide_gen.split_cnt_buf[1]_i_3_0 [2]),
        .O(\bus_wide_gen.len_cnt[7]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hF075)) 
    \bus_wide_gen.rdata_valid_t_i_1 
       (.I0(s_ready_t_reg),
        .I1(s_ready),
        .I2(\bus_wide_gen.rdata_valid_t_reg ),
        .I3(\bus_wide_gen.rdata_valid_t_i_2_n_0 ),
        .O(s_ready_t_reg_0));
  LUT6 #(
    .INIT(64'h00000000F6CCFFC4)) 
    \bus_wide_gen.rdata_valid_t_i_2 
       (.I0(\q_reg[11]_0 [0]),
        .I1(\bus_wide_gen.split_cnt_buf_reg[0]_2 ),
        .I2(\bus_wide_gen.data_buf[31]_i_7_n_0 ),
        .I3(\bus_wide_gen.split_cnt_buf_reg[1]_0 ),
        .I4(\q_reg[11]_0 [1]),
        .I5(\bus_wide_gen.len_cnt_reg[0] ),
        .O(\bus_wide_gen.rdata_valid_t_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h0000E200)) 
    \bus_wide_gen.split_cnt_buf[0]_i_1 
       (.I0(\bus_wide_gen.split_cnt_buf_reg[0]_2 ),
        .I1(E),
        .I2(\bus_wide_gen.split_cnt_buf[1]_i_3_n_0 ),
        .I3(ap_rst_n),
        .I4(last_split),
        .O(\bus_wide_gen.split_cnt_buf_reg[0] ));
  LUT6 #(
    .INIT(64'h00000000E22E0000)) 
    \bus_wide_gen.split_cnt_buf[1]_i_1 
       (.I0(\bus_wide_gen.split_cnt_buf_reg[1]_0 ),
        .I1(E),
        .I2(\bus_wide_gen.split_cnt_buf[1]_i_2_n_0 ),
        .I3(\bus_wide_gen.split_cnt_buf[1]_i_3_n_0 ),
        .I4(ap_rst_n),
        .I5(last_split),
        .O(\bus_wide_gen.split_cnt_buf_reg[1] ));
  LUT5 #(
    .INIT(32'hF0F2F0F0)) 
    \bus_wide_gen.split_cnt_buf[1]_i_2 
       (.I0(\bus_wide_gen.split_cnt_buf[1]_i_4_n_0 ),
        .I1(\bus_wide_gen.len_cnt_reg[0]_0 ),
        .I2(\bus_wide_gen.split_cnt_buf_reg[1]_0 ),
        .I3(\bus_wide_gen.split_cnt_buf_reg[0]_2 ),
        .I4(\q_reg[11]_0 [1]),
        .O(\bus_wide_gen.split_cnt_buf[1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h33333313)) 
    \bus_wide_gen.split_cnt_buf[1]_i_3 
       (.I0(\q_reg[11]_0 [0]),
        .I1(\bus_wide_gen.split_cnt_buf_reg[0]_2 ),
        .I2(\bus_wide_gen.split_cnt_buf[1]_i_4_n_0 ),
        .I3(\bus_wide_gen.len_cnt_reg[0]_0 ),
        .I4(\bus_wide_gen.split_cnt_buf_reg[1]_0 ),
        .O(\bus_wide_gen.split_cnt_buf[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000010000000000)) 
    \bus_wide_gen.split_cnt_buf[1]_i_4 
       (.I0(\bus_wide_gen.split_cnt_buf[1]_i_3_0 [7]),
        .I1(\bus_wide_gen.split_cnt_buf[1]_i_3_0 [5]),
        .I2(\bus_wide_gen.split_cnt_buf[1]_i_3_0 [6]),
        .I3(burst_valid),
        .I4(\bus_wide_gen.split_cnt_buf[1]_i_3_0 [4]),
        .I5(beat_valid),
        .O(\bus_wide_gen.split_cnt_buf[1]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h8080FF80)) 
    \could_multi_bursts.ARVALID_Dummy_i_2 
       (.I0(full_n0_in),
        .I1(fifo_rctl_ready),
        .I2(\could_multi_bursts.ARVALID_Dummy_reg ),
        .I3(m_axi_gmem0_ARVALID),
        .I4(m_axi_gmem0_ARREADY),
        .O(full_n_tmp_reg_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \could_multi_bursts.araddr_buf[31]_i_4 
       (.I0(\could_multi_bursts.arlen_buf_reg[0] [0]),
        .I1(\could_multi_bursts.arlen_buf_reg[0] [1]),
        .I2(\could_multi_bursts.arlen_buf_reg[0] [2]),
        .I3(\could_multi_bursts.arlen_buf_reg[0] [5]),
        .I4(\could_multi_bursts.arlen_buf_reg[0] [4]),
        .I5(\could_multi_bursts.arlen_buf_reg[0] [3]),
        .O(\could_multi_bursts.loop_cnt_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \could_multi_bursts.arlen_buf[0]_i_1 
       (.I0(Q[0]),
        .I1(\sect_len_buf_reg[9] ),
        .O(in[0]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \could_multi_bursts.arlen_buf[1]_i_1 
       (.I0(Q[1]),
        .I1(\sect_len_buf_reg[9] ),
        .O(in[1]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \could_multi_bursts.arlen_buf[2]_i_1 
       (.I0(Q[2]),
        .I1(\sect_len_buf_reg[9] ),
        .O(in[2]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \could_multi_bursts.arlen_buf[3]_i_1 
       (.I0(Q[3]),
        .I1(\sect_len_buf_reg[9] ),
        .O(in[3]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF6FF6)) 
    \could_multi_bursts.arlen_buf[3]_i_2 
       (.I0(Q[9]),
        .I1(\could_multi_bursts.arlen_buf_reg[0] [5]),
        .I2(Q[4]),
        .I3(\could_multi_bursts.arlen_buf_reg[0] [0]),
        .I4(\could_multi_bursts.arlen_buf[3]_i_3_n_0 ),
        .I5(\could_multi_bursts.arlen_buf[3]_i_4_n_0 ),
        .O(\sect_len_buf_reg[9] ));
  LUT4 #(
    .INIT(16'h6FF6)) 
    \could_multi_bursts.arlen_buf[3]_i_3 
       (.I0(\could_multi_bursts.arlen_buf_reg[0] [4]),
        .I1(Q[8]),
        .I2(\could_multi_bursts.arlen_buf_reg[0] [3]),
        .I3(Q[7]),
        .O(\could_multi_bursts.arlen_buf[3]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h6FF6)) 
    \could_multi_bursts.arlen_buf[3]_i_4 
       (.I0(\could_multi_bursts.arlen_buf_reg[0] [1]),
        .I1(Q[5]),
        .I2(\could_multi_bursts.arlen_buf_reg[0] [2]),
        .I3(Q[6]),
        .O(\could_multi_bursts.arlen_buf[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFEFFFFAAAAAAAA)) 
    data_vld_i_1__1
       (.I0(push),
        .I1(\pout_reg_n_0_[0] ),
        .I2(\pout_reg_n_0_[1] ),
        .I3(\pout_reg_n_0_[2] ),
        .I4(full_n_tmp_i_2_n_0),
        .I5(data_vld_reg_n_0),
        .O(data_vld_i_1__1_n_0));
  FDRE data_vld_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(data_vld_i_1__1_n_0),
        .Q(data_vld_reg_n_0),
        .R(SR));
  FDRE empty_n_tmp_reg
       (.C(ap_clk),
        .CE(\q[11]_i_1_n_0 ),
        .D(data_vld_reg_n_0),
        .Q(burst_valid),
        .R(SR));
  LUT6 #(
    .INIT(64'hFBFBFBFBBBFBFBFB)) 
    full_n_tmp_i_1__1
       (.I0(full_n_tmp_i_2_n_0),
        .I1(ap_rst_n),
        .I2(full_n0_in),
        .I3(full_n_tmp_i_3_n_0),
        .I4(full_n_tmp_i_4_n_0),
        .I5(\pout_reg_n_0_[2] ),
        .O(full_n_tmp_i_1__1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT4 #(
    .INIT(16'h80AA)) 
    full_n_tmp_i_2
       (.I0(data_vld_reg_n_0),
        .I1(\bus_wide_gen.len_cnt[7]_i_4_n_0 ),
        .I2(last_split),
        .I3(burst_valid),
        .O(full_n_tmp_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT5 #(
    .INIT(32'h70000000)) 
    full_n_tmp_i_3
       (.I0(\bus_wide_gen.len_cnt[7]_i_4_n_0 ),
        .I1(last_split),
        .I2(burst_valid),
        .I3(push),
        .I4(data_vld_reg_n_0),
        .O(full_n_tmp_i_3_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    full_n_tmp_i_4
       (.I0(\pout_reg_n_0_[1] ),
        .I1(\pout_reg_n_0_[0] ),
        .O(full_n_tmp_i_4_n_0));
  FDRE full_n_tmp_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(full_n_tmp_i_1__1_n_0),
        .Q(full_n0_in),
        .R(1'b0));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/bus_wide_gen.fifo_burst/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/bus_wide_gen.fifo_burst/mem_reg[4][0]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][0]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(in[0]),
        .Q(\mem_reg[4][0]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/bus_wide_gen.fifo_burst/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/bus_wide_gen.fifo_burst/mem_reg[4][10]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][10]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\mem_reg[4][10]_srl5_i_1_n_0 ),
        .Q(\mem_reg[4][10]_srl5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \mem_reg[4][10]_srl5_i_1 
       (.I0(\q_reg[11]_1 [0]),
        .I1(\could_multi_bursts.loop_cnt_reg[0] ),
        .O(\mem_reg[4][10]_srl5_i_1_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/bus_wide_gen.fifo_burst/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/bus_wide_gen.fifo_burst/mem_reg[4][11]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][11]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\mem_reg[4][11]_srl5_i_1_n_0 ),
        .Q(\mem_reg[4][11]_srl5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \mem_reg[4][11]_srl5_i_1 
       (.I0(\q_reg[11]_1 [1]),
        .I1(\could_multi_bursts.loop_cnt_reg[0] ),
        .O(\mem_reg[4][11]_srl5_i_1_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/bus_wide_gen.fifo_burst/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/bus_wide_gen.fifo_burst/mem_reg[4][1]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][1]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(in[1]),
        .Q(\mem_reg[4][1]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/bus_wide_gen.fifo_burst/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/bus_wide_gen.fifo_burst/mem_reg[4][2]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][2]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(in[2]),
        .Q(\mem_reg[4][2]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/bus_wide_gen.fifo_burst/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/bus_wide_gen.fifo_burst/mem_reg[4][3]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][3]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(in[3]),
        .Q(\mem_reg[4][3]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/bus_wide_gen.fifo_burst/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/bus_wide_gen.fifo_burst/mem_reg[4][8]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][8]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\mem_reg[4][8]_srl5_i_1_n_0 ),
        .Q(\mem_reg[4][8]_srl5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \mem_reg[4][8]_srl5_i_1 
       (.I0(\q_reg[8]_0 ),
        .I1(\sect_len_buf_reg[9] ),
        .O(\mem_reg[4][8]_srl5_i_1_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem0_m_axi_U/bus_read/bus_wide_gen.fifo_burst/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem0_m_axi_U/bus_read/bus_wide_gen.fifo_burst/mem_reg[4][9]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][9]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\mem_reg[4][9]_srl5_i_1_n_0 ),
        .Q(\mem_reg[4][9]_srl5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \mem_reg[4][9]_srl5_i_1 
       (.I0(\q_reg[9]_0 ),
        .I1(\sect_len_buf_reg[9] ),
        .O(\mem_reg[4][9]_srl5_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7777BBBB88884440)) 
    \pout[0]_i_1 
       (.I0(\q[11]_i_2_n_0 ),
        .I1(data_vld_reg_n_0),
        .I2(\pout_reg_n_0_[1] ),
        .I3(\pout_reg_n_0_[2] ),
        .I4(push),
        .I5(\pout_reg_n_0_[0] ),
        .O(\pout[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h5AF0F0F0F0A4F0F0)) 
    \pout[1]_i_1 
       (.I0(push),
        .I1(\pout_reg_n_0_[2] ),
        .I2(\pout_reg_n_0_[1] ),
        .I3(\pout_reg_n_0_[0] ),
        .I4(data_vld_reg_n_0),
        .I5(\q[11]_i_2_n_0 ),
        .O(\pout[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6CCCCCCCCCC8CCCC)) 
    \pout[2]_i_1 
       (.I0(push),
        .I1(\pout_reg_n_0_[2] ),
        .I2(\pout_reg_n_0_[1] ),
        .I3(\pout_reg_n_0_[0] ),
        .I4(data_vld_reg_n_0),
        .I5(\q[11]_i_2_n_0 ),
        .O(\pout[2]_i_1_n_0 ));
  FDRE \pout_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\pout[0]_i_1_n_0 ),
        .Q(\pout_reg_n_0_[0] ),
        .R(SR));
  FDRE \pout_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\pout[1]_i_1_n_0 ),
        .Q(\pout_reg_n_0_[1] ),
        .R(SR));
  FDRE \pout_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\pout[2]_i_1_n_0 ),
        .Q(\pout_reg_n_0_[2] ),
        .R(SR));
  LUT1 #(
    .INIT(2'h1)) 
    \q[11]_i_1 
       (.I0(\q[11]_i_2_n_0 ),
        .O(\q[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'h2A)) 
    \q[11]_i_2 
       (.I0(burst_valid),
        .I1(last_split),
        .I2(\bus_wide_gen.len_cnt[7]_i_4_n_0 ),
        .O(\q[11]_i_2_n_0 ));
  FDRE \q_reg[0] 
       (.C(ap_clk),
        .CE(\q[11]_i_1_n_0 ),
        .D(\mem_reg[4][0]_srl5_n_0 ),
        .Q(\q_reg_n_0_[0] ),
        .R(SR));
  FDRE \q_reg[10] 
       (.C(ap_clk),
        .CE(\q[11]_i_1_n_0 ),
        .D(\mem_reg[4][10]_srl5_n_0 ),
        .Q(\q_reg[11]_0 [0]),
        .R(SR));
  FDRE \q_reg[11] 
       (.C(ap_clk),
        .CE(\q[11]_i_1_n_0 ),
        .D(\mem_reg[4][11]_srl5_n_0 ),
        .Q(\q_reg[11]_0 [1]),
        .R(SR));
  FDRE \q_reg[1] 
       (.C(ap_clk),
        .CE(\q[11]_i_1_n_0 ),
        .D(\mem_reg[4][1]_srl5_n_0 ),
        .Q(\q_reg_n_0_[1] ),
        .R(SR));
  FDRE \q_reg[2] 
       (.C(ap_clk),
        .CE(\q[11]_i_1_n_0 ),
        .D(\mem_reg[4][2]_srl5_n_0 ),
        .Q(\q_reg_n_0_[2] ),
        .R(SR));
  FDRE \q_reg[3] 
       (.C(ap_clk),
        .CE(\q[11]_i_1_n_0 ),
        .D(\mem_reg[4][3]_srl5_n_0 ),
        .Q(\q_reg_n_0_[3] ),
        .R(SR));
  FDRE \q_reg[8] 
       (.C(ap_clk),
        .CE(\q[11]_i_1_n_0 ),
        .D(\mem_reg[4][8]_srl5_n_0 ),
        .Q(tail_split[0]),
        .R(SR));
  FDRE \q_reg[9] 
       (.C(ap_clk),
        .CE(\q[11]_i_1_n_0 ),
        .D(\mem_reg[4][9]_srl5_n_0 ),
        .Q(tail_split[1]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "filter_gmem0_m_axi_fifo" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem0_m_axi_fifo__parameterized3
   (fifo_rctl_ready,
    empty_n_tmp_reg_0,
    ap_rst_n_0,
    \could_multi_bursts.sect_handling_reg ,
    ap_rst_n_1,
    push,
    \could_multi_bursts.sect_handling_reg_0 ,
    \start_addr_buf_reg[2] ,
    \start_addr_buf_reg[3] ,
    \beat_len_buf_reg[2] ,
    \end_addr_buf_reg[5] ,
    \start_addr_buf_reg[6] ,
    \beat_len_buf_reg[5] ,
    \beat_len_buf_reg[6] ,
    \beat_len_buf_reg[7] ,
    \beat_len_buf_reg[8] ,
    \end_addr_buf_reg[11] ,
    \end_addr_buf_reg[1] ,
    \end_addr_buf_reg[0] ,
    rreq_handling_reg,
    rreq_handling_reg_0,
    E,
    \bus_wide_gen.len_cnt_reg[0] ,
    rreq_handling_reg_1,
    \could_multi_bursts.sect_handling_reg_1 ,
    ap_clk,
    SR,
    ap_rst_n,
    CO,
    \could_multi_bursts.sect_handling_reg_2 ,
    \could_multi_bursts.sect_handling_reg_3 ,
    rreq_handling_reg_2,
    \pout_reg[0]_0 ,
    \sect_end_buf_reg[1] ,
    Q,
    \sect_len_buf_reg[9] ,
    \sect_len_buf_reg[9]_0 ,
    \sect_end_buf_reg[1]_0 ,
    \sect_end_buf_reg[0] ,
    fifo_rreq_valid,
    invalid_len_event,
    last_split,
    empty_n_tmp_reg_1,
    beat_valid,
    m_axi_gmem0_ARREADY,
    m_axi_gmem0_ARVALID,
    full_n0_in,
    \bus_wide_gen.split_cnt_buf[1]_i_2 ,
    rreq_handling_reg_3);
  output fifo_rctl_ready;
  output empty_n_tmp_reg_0;
  output [0:0]ap_rst_n_0;
  output \could_multi_bursts.sect_handling_reg ;
  output [0:0]ap_rst_n_1;
  output push;
  output \could_multi_bursts.sect_handling_reg_0 ;
  output \start_addr_buf_reg[2] ;
  output \start_addr_buf_reg[3] ;
  output \beat_len_buf_reg[2] ;
  output \end_addr_buf_reg[5] ;
  output \start_addr_buf_reg[6] ;
  output \beat_len_buf_reg[5] ;
  output \beat_len_buf_reg[6] ;
  output \beat_len_buf_reg[7] ;
  output \beat_len_buf_reg[8] ;
  output \end_addr_buf_reg[11] ;
  output \end_addr_buf_reg[1] ;
  output \end_addr_buf_reg[0] ;
  output rreq_handling_reg;
  output rreq_handling_reg_0;
  output [0:0]E;
  output \bus_wide_gen.len_cnt_reg[0] ;
  output rreq_handling_reg_1;
  output \could_multi_bursts.sect_handling_reg_1 ;
  input ap_clk;
  input [0:0]SR;
  input ap_rst_n;
  input [0:0]CO;
  input \could_multi_bursts.sect_handling_reg_2 ;
  input \could_multi_bursts.sect_handling_reg_3 ;
  input rreq_handling_reg_2;
  input \pout_reg[0]_0 ;
  input [0:0]\sect_end_buf_reg[1] ;
  input [9:0]Q;
  input [11:0]\sect_len_buf_reg[9] ;
  input [9:0]\sect_len_buf_reg[9]_0 ;
  input \sect_end_buf_reg[1]_0 ;
  input \sect_end_buf_reg[0] ;
  input fifo_rreq_valid;
  input invalid_len_event;
  input last_split;
  input [0:0]empty_n_tmp_reg_1;
  input beat_valid;
  input m_axi_gmem0_ARREADY;
  input m_axi_gmem0_ARVALID;
  input full_n0_in;
  input [3:0]\bus_wide_gen.split_cnt_buf[1]_i_2 ;
  input rreq_handling_reg_3;

  wire [0:0]CO;
  wire [0:0]E;
  wire [9:0]Q;
  wire [0:0]SR;
  wire ap_clk;
  wire ap_rst_n;
  wire [0:0]ap_rst_n_0;
  wire [0:0]ap_rst_n_1;
  wire \beat_len_buf_reg[2] ;
  wire \beat_len_buf_reg[5] ;
  wire \beat_len_buf_reg[6] ;
  wire \beat_len_buf_reg[7] ;
  wire \beat_len_buf_reg[8] ;
  wire beat_valid;
  wire \bus_wide_gen.len_cnt_reg[0] ;
  wire [3:0]\bus_wide_gen.split_cnt_buf[1]_i_2 ;
  wire \could_multi_bursts.sect_handling_reg ;
  wire \could_multi_bursts.sect_handling_reg_0 ;
  wire \could_multi_bursts.sect_handling_reg_1 ;
  wire \could_multi_bursts.sect_handling_reg_2 ;
  wire \could_multi_bursts.sect_handling_reg_3 ;
  wire data_vld_i_1__0_n_0;
  wire data_vld_i_2__0_n_0;
  wire data_vld_reg_n_0;
  wire empty_n_tmp_i_1__0_n_0;
  wire empty_n_tmp_reg_0;
  wire [0:0]empty_n_tmp_reg_1;
  wire \end_addr_buf_reg[0] ;
  wire \end_addr_buf_reg[11] ;
  wire \end_addr_buf_reg[1] ;
  wire \end_addr_buf_reg[5] ;
  wire fifo_rctl_ready;
  wire fifo_rreq_valid;
  wire full_n0_in;
  wire full_n_tmp_i_1__0_n_0;
  wire full_n_tmp_i_2__1_n_0;
  wire invalid_len_event;
  wire last_split;
  wire m_axi_gmem0_ARREADY;
  wire m_axi_gmem0_ARVALID;
  wire \pout[0]_i_1_n_0 ;
  wire \pout[1]_i_1_n_0 ;
  wire \pout[2]_i_1_n_0 ;
  wire \pout[3]_i_1__0_n_0 ;
  wire \pout[3]_i_2_n_0 ;
  wire \pout[3]_i_3_n_0 ;
  wire \pout[3]_i_5_n_0 ;
  wire \pout_reg[0]_0 ;
  wire [3:0]pout_reg__0;
  wire push;
  wire rreq_handling_reg;
  wire rreq_handling_reg_0;
  wire rreq_handling_reg_1;
  wire rreq_handling_reg_2;
  wire rreq_handling_reg_3;
  wire \sect_end_buf_reg[0] ;
  wire [0:0]\sect_end_buf_reg[1] ;
  wire \sect_end_buf_reg[1]_0 ;
  wire [11:0]\sect_len_buf_reg[9] ;
  wire [9:0]\sect_len_buf_reg[9]_0 ;
  wire \start_addr_buf_reg[2] ;
  wire \start_addr_buf_reg[3] ;
  wire \start_addr_buf_reg[6] ;

  LUT4 #(
    .INIT(16'hFFFE)) 
    \bus_wide_gen.split_cnt_buf[1]_i_5 
       (.I0(\bus_wide_gen.split_cnt_buf[1]_i_2 [0]),
        .I1(\bus_wide_gen.split_cnt_buf[1]_i_2 [1]),
        .I2(\bus_wide_gen.split_cnt_buf[1]_i_2 [2]),
        .I3(\bus_wide_gen.split_cnt_buf[1]_i_2 [3]),
        .O(\bus_wide_gen.len_cnt_reg[0] ));
  LUT5 #(
    .INIT(32'hB0000000)) 
    \could_multi_bursts.araddr_buf[31]_i_1 
       (.I0(m_axi_gmem0_ARREADY),
        .I1(m_axi_gmem0_ARVALID),
        .I2(\could_multi_bursts.sect_handling_reg_3 ),
        .I3(fifo_rctl_ready),
        .I4(full_n0_in),
        .O(push));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \could_multi_bursts.loop_cnt[5]_i_1 
       (.I0(ap_rst_n),
        .I1(\could_multi_bursts.sect_handling_reg ),
        .O(ap_rst_n_0));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT4 #(
    .INIT(16'hEECE)) 
    \could_multi_bursts.sect_handling_i_1 
       (.I0(\could_multi_bursts.sect_handling_reg_3 ),
        .I1(rreq_handling_reg_2),
        .I2(push),
        .I3(\could_multi_bursts.sect_handling_reg_2 ),
        .O(\could_multi_bursts.sect_handling_reg_1 ));
  LUT6 #(
    .INIT(64'hFFFEFFFFAAAAAAAA)) 
    data_vld_i_1__0
       (.I0(push),
        .I1(pout_reg__0[2]),
        .I2(pout_reg__0[3]),
        .I3(\pout[3]_i_3_n_0 ),
        .I4(data_vld_i_2__0_n_0),
        .I5(data_vld_reg_n_0),
        .O(data_vld_i_1__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT5 #(
    .INIT(32'hA2222222)) 
    data_vld_i_2__0
       (.I0(data_vld_reg_n_0),
        .I1(empty_n_tmp_reg_0),
        .I2(last_split),
        .I3(empty_n_tmp_reg_1),
        .I4(beat_valid),
        .O(data_vld_i_2__0_n_0));
  FDRE data_vld_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(data_vld_i_1__0_n_0),
        .Q(data_vld_reg_n_0),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT5 #(
    .INIT(32'hBFFFAAAA)) 
    empty_n_tmp_i_1__0
       (.I0(data_vld_reg_n_0),
        .I1(beat_valid),
        .I2(empty_n_tmp_reg_1),
        .I3(last_split),
        .I4(empty_n_tmp_reg_0),
        .O(empty_n_tmp_i_1__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT4 #(
    .INIT(16'hD0FF)) 
    empty_n_tmp_i_2
       (.I0(push),
        .I1(\could_multi_bursts.sect_handling_reg_2 ),
        .I2(\could_multi_bursts.sect_handling_reg_3 ),
        .I3(rreq_handling_reg_2),
        .O(\could_multi_bursts.sect_handling_reg ));
  FDRE empty_n_tmp_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(empty_n_tmp_i_1__0_n_0),
        .Q(empty_n_tmp_reg_0),
        .R(SR));
  LUT6 #(
    .INIT(64'hF5FDDDDDFDFDDDDD)) 
    full_n_tmp_i_1__0
       (.I0(ap_rst_n),
        .I1(fifo_rctl_ready),
        .I2(\pout_reg[0]_0 ),
        .I3(push),
        .I4(data_vld_reg_n_0),
        .I5(full_n_tmp_i_2__1_n_0),
        .O(full_n_tmp_i_1__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    full_n_tmp_i_2__1
       (.I0(pout_reg__0[2]),
        .I1(pout_reg__0[3]),
        .I2(pout_reg__0[0]),
        .I3(pout_reg__0[1]),
        .O(full_n_tmp_i_2__1_n_0));
  FDRE full_n_tmp_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(full_n_tmp_i_1__0_n_0),
        .Q(fifo_rctl_ready),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    invalid_len_event_i_2
       (.I0(rreq_handling_reg_2),
        .I1(\could_multi_bursts.sect_handling_reg ),
        .I2(\sect_end_buf_reg[1] ),
        .O(rreq_handling_reg_0));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \pout[0]_i_1 
       (.I0(pout_reg__0[0]),
        .O(\pout[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \pout[1]_i_1 
       (.I0(\pout[3]_i_5_n_0 ),
        .I1(pout_reg__0[1]),
        .I2(pout_reg__0[0]),
        .O(\pout[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'hA96A)) 
    \pout[2]_i_1 
       (.I0(pout_reg__0[2]),
        .I1(pout_reg__0[1]),
        .I2(pout_reg__0[0]),
        .I3(\pout[3]_i_5_n_0 ),
        .O(\pout[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT5 #(
    .INIT(32'h0000D000)) 
    \pout[2]_i_2 
       (.I0(\sect_end_buf_reg[1] ),
        .I1(\could_multi_bursts.sect_handling_reg ),
        .I2(rreq_handling_reg_2),
        .I3(fifo_rreq_valid),
        .I4(invalid_len_event),
        .O(rreq_handling_reg));
  LUT6 #(
    .INIT(64'h0000FE00FF000000)) 
    \pout[3]_i_1__0 
       (.I0(\pout[3]_i_3_n_0 ),
        .I1(pout_reg__0[3]),
        .I2(pout_reg__0[2]),
        .I3(data_vld_reg_n_0),
        .I4(push),
        .I5(\pout_reg[0]_0 ),
        .O(\pout[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT5 #(
    .INIT(32'h9AAAAAA6)) 
    \pout[3]_i_2 
       (.I0(pout_reg__0[3]),
        .I1(\pout[3]_i_5_n_0 ),
        .I2(pout_reg__0[2]),
        .I3(pout_reg__0[0]),
        .I4(pout_reg__0[1]),
        .O(\pout[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \pout[3]_i_3 
       (.I0(pout_reg__0[0]),
        .I1(pout_reg__0[1]),
        .O(\pout[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hD555FFFFFFFFFFFF)) 
    \pout[3]_i_5 
       (.I0(empty_n_tmp_reg_0),
        .I1(last_split),
        .I2(empty_n_tmp_reg_1),
        .I3(beat_valid),
        .I4(push),
        .I5(data_vld_reg_n_0),
        .O(\pout[3]_i_5_n_0 ));
  FDRE \pout_reg[0] 
       (.C(ap_clk),
        .CE(\pout[3]_i_1__0_n_0 ),
        .D(\pout[0]_i_1_n_0 ),
        .Q(pout_reg__0[0]),
        .R(SR));
  FDRE \pout_reg[1] 
       (.C(ap_clk),
        .CE(\pout[3]_i_1__0_n_0 ),
        .D(\pout[1]_i_1_n_0 ),
        .Q(pout_reg__0[1]),
        .R(SR));
  FDRE \pout_reg[2] 
       (.C(ap_clk),
        .CE(\pout[3]_i_1__0_n_0 ),
        .D(\pout[2]_i_1_n_0 ),
        .Q(pout_reg__0[2]),
        .R(SR));
  FDRE \pout_reg[3] 
       (.C(ap_clk),
        .CE(\pout[3]_i_1__0_n_0 ),
        .D(\pout[3]_i_2_n_0 ),
        .Q(pout_reg__0[3]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT5 #(
    .INIT(32'hAEAE0CAE)) 
    rreq_handling_i_1
       (.I0(rreq_handling_reg_2),
        .I1(rreq_handling_reg_3),
        .I2(invalid_len_event),
        .I3(\sect_end_buf_reg[1] ),
        .I4(\could_multi_bursts.sect_handling_reg ),
        .O(rreq_handling_reg_1));
  LUT3 #(
    .INIT(8'h1F)) 
    \sect_addr_buf[11]_i_1 
       (.I0(\could_multi_bursts.sect_handling_reg ),
        .I1(CO),
        .I2(ap_rst_n),
        .O(ap_rst_n_1));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \sect_addr_buf[31]_i_1 
       (.I0(\could_multi_bursts.sect_handling_reg ),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT4 #(
    .INIT(16'hFB0B)) 
    \sect_end_buf[0]_i_1 
       (.I0(\sect_len_buf_reg[9] [0]),
        .I1(\sect_end_buf_reg[1] ),
        .I2(\could_multi_bursts.sect_handling_reg ),
        .I3(\sect_end_buf_reg[0] ),
        .O(\end_addr_buf_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT4 #(
    .INIT(16'hFB0B)) 
    \sect_end_buf[1]_i_1 
       (.I0(\sect_len_buf_reg[9] [1]),
        .I1(\sect_end_buf_reg[1] ),
        .I2(\could_multi_bursts.sect_handling_reg ),
        .I3(\sect_end_buf_reg[1]_0 ),
        .O(\end_addr_buf_reg[1] ));
  LUT6 #(
    .INIT(64'hCFFFC1F10F3F0131)) 
    \sect_len_buf[0]_i_1 
       (.I0(\could_multi_bursts.sect_handling_reg ),
        .I1(\sect_end_buf_reg[1] ),
        .I2(CO),
        .I3(Q[0]),
        .I4(\sect_len_buf_reg[9] [2]),
        .I5(\sect_len_buf_reg[9]_0 [0]),
        .O(\start_addr_buf_reg[2] ));
  LUT6 #(
    .INIT(64'hCFFFC1F10F3F0131)) 
    \sect_len_buf[1]_i_1 
       (.I0(\could_multi_bursts.sect_handling_reg ),
        .I1(\sect_end_buf_reg[1] ),
        .I2(CO),
        .I3(Q[1]),
        .I4(\sect_len_buf_reg[9] [3]),
        .I5(\sect_len_buf_reg[9]_0 [1]),
        .O(\start_addr_buf_reg[3] ));
  LUT6 #(
    .INIT(64'hCF0FC101FF3FF131)) 
    \sect_len_buf[2]_i_1 
       (.I0(\could_multi_bursts.sect_handling_reg ),
        .I1(\sect_end_buf_reg[1] ),
        .I2(CO),
        .I3(\sect_len_buf_reg[9]_0 [2]),
        .I4(\sect_len_buf_reg[9] [4]),
        .I5(Q[2]),
        .O(\beat_len_buf_reg[2] ));
  LUT6 #(
    .INIT(64'hCFC10F01FFF13F31)) 
    \sect_len_buf[3]_i_1 
       (.I0(\could_multi_bursts.sect_handling_reg ),
        .I1(\sect_end_buf_reg[1] ),
        .I2(CO),
        .I3(\sect_len_buf_reg[9] [5]),
        .I4(\sect_len_buf_reg[9]_0 [3]),
        .I5(Q[3]),
        .O(\end_addr_buf_reg[5] ));
  LUT6 #(
    .INIT(64'hCFFFC1F10F3F0131)) 
    \sect_len_buf[4]_i_1 
       (.I0(\could_multi_bursts.sect_handling_reg ),
        .I1(\sect_end_buf_reg[1] ),
        .I2(CO),
        .I3(Q[4]),
        .I4(\sect_len_buf_reg[9] [6]),
        .I5(\sect_len_buf_reg[9]_0 [4]),
        .O(\start_addr_buf_reg[6] ));
  LUT6 #(
    .INIT(64'hCF0FC101FF3FF131)) 
    \sect_len_buf[5]_i_1 
       (.I0(\could_multi_bursts.sect_handling_reg ),
        .I1(\sect_end_buf_reg[1] ),
        .I2(CO),
        .I3(\sect_len_buf_reg[9]_0 [5]),
        .I4(\sect_len_buf_reg[9] [7]),
        .I5(Q[5]),
        .O(\beat_len_buf_reg[5] ));
  LUT6 #(
    .INIT(64'hCF0FC101FF3FF131)) 
    \sect_len_buf[6]_i_1 
       (.I0(\could_multi_bursts.sect_handling_reg ),
        .I1(\sect_end_buf_reg[1] ),
        .I2(CO),
        .I3(\sect_len_buf_reg[9]_0 [6]),
        .I4(\sect_len_buf_reg[9] [8]),
        .I5(Q[6]),
        .O(\beat_len_buf_reg[6] ));
  LUT6 #(
    .INIT(64'hCF0FC101FF3FF131)) 
    \sect_len_buf[7]_i_1 
       (.I0(\could_multi_bursts.sect_handling_reg ),
        .I1(\sect_end_buf_reg[1] ),
        .I2(CO),
        .I3(\sect_len_buf_reg[9]_0 [7]),
        .I4(\sect_len_buf_reg[9] [9]),
        .I5(Q[7]),
        .O(\beat_len_buf_reg[7] ));
  LUT6 #(
    .INIT(64'hCF0FC101FF3FF131)) 
    \sect_len_buf[8]_i_1 
       (.I0(\could_multi_bursts.sect_handling_reg ),
        .I1(\sect_end_buf_reg[1] ),
        .I2(CO),
        .I3(\sect_len_buf_reg[9]_0 [8]),
        .I4(\sect_len_buf_reg[9] [10]),
        .I5(Q[8]),
        .O(\beat_len_buf_reg[8] ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \sect_len_buf[9]_i_1__0 
       (.I0(\could_multi_bursts.sect_handling_reg ),
        .O(\could_multi_bursts.sect_handling_reg_0 ));
  LUT6 #(
    .INIT(64'hCFC1FFF10F013F31)) 
    \sect_len_buf[9]_i_2 
       (.I0(\could_multi_bursts.sect_handling_reg ),
        .I1(\sect_end_buf_reg[1] ),
        .I2(CO),
        .I3(\sect_len_buf_reg[9] [11]),
        .I4(Q[9]),
        .I5(\sect_len_buf_reg[9]_0 [9]),
        .O(\end_addr_buf_reg[11] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem0_m_axi_read
   (I_AWVALID,
    rdata_valid,
    D,
    SR,
    ap_rst_n_0,
    RREADY,
    m_axi_gmem0_ARADDR,
    ARLEN,
    s_ready_t_reg,
    \data_p1_reg[7] ,
    m_axi_gmem0_ARVALID,
    Q,
    ap_reg_ioackin_gmem1_AWREADY,
    gmem1_AWREADY,
    ap_rst_n,
    m_axi_gmem0_RVALID,
    ap_clk,
    mem_reg,
    m_axi_gmem0_RRESP,
    \data_p2_reg[31] ,
    m_axi_gmem0_ARREADY);
  output I_AWVALID;
  output rdata_valid;
  output [1:0]D;
  output [0:0]SR;
  output ap_rst_n_0;
  output RREADY;
  output [29:0]m_axi_gmem0_ARADDR;
  output [3:0]ARLEN;
  output s_ready_t_reg;
  output [7:0]\data_p1_reg[7] ;
  output m_axi_gmem0_ARVALID;
  input [2:0]Q;
  input ap_reg_ioackin_gmem1_AWREADY;
  input gmem1_AWREADY;
  input ap_rst_n;
  input m_axi_gmem0_RVALID;
  input ap_clk;
  input [32:0]mem_reg;
  input [1:0]m_axi_gmem0_RRESP;
  input [31:0]\data_p2_reg[31] ;
  input m_axi_gmem0_ARREADY;

  wire [3:0]ARLEN;
  wire [4:3]COUNT;
  wire [1:0]D;
  wire I_AWVALID;
  wire [2:0]Q;
  wire RREADY;
  wire [9:0]SHIFT_RIGHT;
  wire [0:0]SR;
  wire align_len;
  wire \align_len_reg_n_0_[31] ;
  wire ap_clk;
  wire ap_reg_ioackin_gmem1_AWREADY;
  wire ap_rst_n;
  wire ap_rst_n_0;
  wire [31:2]araddr_tmp;
  wire [3:0]arlen_tmp;
  wire [9:0]beat_len_buf;
  wire \beat_len_buf[1]_i_2_n_0 ;
  wire \beat_len_buf[1]_i_3_n_0 ;
  wire \beat_len_buf_reg[1]_i_1_n_0 ;
  wire \beat_len_buf_reg[1]_i_1_n_1 ;
  wire \beat_len_buf_reg[1]_i_1_n_2 ;
  wire \beat_len_buf_reg[1]_i_1_n_3 ;
  wire \beat_len_buf_reg[5]_i_1_n_0 ;
  wire \beat_len_buf_reg[5]_i_1_n_1 ;
  wire \beat_len_buf_reg[5]_i_1_n_2 ;
  wire \beat_len_buf_reg[5]_i_1_n_3 ;
  wire \beat_len_buf_reg[9]_i_1_n_1 ;
  wire \beat_len_buf_reg[9]_i_1_n_2 ;
  wire \beat_len_buf_reg[9]_i_1_n_3 ;
  wire beat_valid;
  wire \bus_wide_gen.data_buf_reg_n_0_[10] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[11] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[12] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[13] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[14] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[15] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[16] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[17] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[18] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[19] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[20] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[21] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[22] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[23] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[24] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[25] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[26] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[27] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[28] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[29] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[30] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[31] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[8] ;
  wire \bus_wide_gen.data_buf_reg_n_0_[9] ;
  wire \bus_wide_gen.fifo_burst_n_1 ;
  wire \bus_wide_gen.fifo_burst_n_11 ;
  wire \bus_wide_gen.fifo_burst_n_12 ;
  wire \bus_wide_gen.fifo_burst_n_13 ;
  wire \bus_wide_gen.fifo_burst_n_14 ;
  wire \bus_wide_gen.fifo_burst_n_15 ;
  wire \bus_wide_gen.fifo_burst_n_16 ;
  wire \bus_wide_gen.fifo_burst_n_17 ;
  wire \bus_wide_gen.fifo_burst_n_18 ;
  wire \bus_wide_gen.fifo_burst_n_19 ;
  wire \bus_wide_gen.fifo_burst_n_20 ;
  wire \bus_wide_gen.fifo_burst_n_21 ;
  wire \bus_wide_gen.fifo_burst_n_22 ;
  wire \bus_wide_gen.fifo_burst_n_23 ;
  wire \bus_wide_gen.fifo_burst_n_24 ;
  wire \bus_wide_gen.fifo_burst_n_25 ;
  wire \bus_wide_gen.fifo_burst_n_26 ;
  wire \bus_wide_gen.fifo_burst_n_27 ;
  wire \bus_wide_gen.fifo_burst_n_28 ;
  wire \bus_wide_gen.fifo_burst_n_29 ;
  wire \bus_wide_gen.fifo_burst_n_3 ;
  wire \bus_wide_gen.fifo_burst_n_30 ;
  wire \bus_wide_gen.fifo_burst_n_31 ;
  wire \bus_wide_gen.fifo_burst_n_32 ;
  wire \bus_wide_gen.fifo_burst_n_33 ;
  wire \bus_wide_gen.fifo_burst_n_34 ;
  wire \bus_wide_gen.fifo_burst_n_35 ;
  wire \bus_wide_gen.fifo_burst_n_36 ;
  wire \bus_wide_gen.fifo_burst_n_37 ;
  wire \bus_wide_gen.fifo_burst_n_38 ;
  wire \bus_wide_gen.fifo_burst_n_39 ;
  wire \bus_wide_gen.fifo_burst_n_4 ;
  wire \bus_wide_gen.fifo_burst_n_40 ;
  wire \bus_wide_gen.fifo_burst_n_43 ;
  wire \bus_wide_gen.fifo_burst_n_44 ;
  wire \bus_wide_gen.fifo_burst_n_45 ;
  wire \bus_wide_gen.fifo_burst_n_46 ;
  wire \bus_wide_gen.fifo_burst_n_47 ;
  wire \bus_wide_gen.fifo_burst_n_48 ;
  wire \bus_wide_gen.fifo_burst_n_49 ;
  wire \bus_wide_gen.fifo_burst_n_5 ;
  wire \bus_wide_gen.fifo_burst_n_6 ;
  wire \bus_wide_gen.len_cnt[7]_i_6_n_0 ;
  wire [7:0]\bus_wide_gen.len_cnt_reg__0 ;
  wire \bus_wide_gen.rdata_valid_t_reg_n_0 ;
  wire \bus_wide_gen.split_cnt_buf_reg_n_0_[0] ;
  wire \bus_wide_gen.split_cnt_buf_reg_n_0_[1] ;
  wire \could_multi_bursts.araddr_buf[4]_i_3_n_0 ;
  wire \could_multi_bursts.araddr_buf[4]_i_4_n_0 ;
  wire \could_multi_bursts.araddr_buf[4]_i_5_n_0 ;
  wire \could_multi_bursts.araddr_buf[8]_i_3_n_0 ;
  wire \could_multi_bursts.araddr_buf[8]_i_4_n_0 ;
  wire \could_multi_bursts.araddr_buf_reg[12]_i_2_n_0 ;
  wire \could_multi_bursts.araddr_buf_reg[12]_i_2_n_1 ;
  wire \could_multi_bursts.araddr_buf_reg[12]_i_2_n_2 ;
  wire \could_multi_bursts.araddr_buf_reg[12]_i_2_n_3 ;
  wire \could_multi_bursts.araddr_buf_reg[16]_i_2_n_0 ;
  wire \could_multi_bursts.araddr_buf_reg[16]_i_2_n_1 ;
  wire \could_multi_bursts.araddr_buf_reg[16]_i_2_n_2 ;
  wire \could_multi_bursts.araddr_buf_reg[16]_i_2_n_3 ;
  wire \could_multi_bursts.araddr_buf_reg[20]_i_2_n_0 ;
  wire \could_multi_bursts.araddr_buf_reg[20]_i_2_n_1 ;
  wire \could_multi_bursts.araddr_buf_reg[20]_i_2_n_2 ;
  wire \could_multi_bursts.araddr_buf_reg[20]_i_2_n_3 ;
  wire \could_multi_bursts.araddr_buf_reg[24]_i_2_n_0 ;
  wire \could_multi_bursts.araddr_buf_reg[24]_i_2_n_1 ;
  wire \could_multi_bursts.araddr_buf_reg[24]_i_2_n_2 ;
  wire \could_multi_bursts.araddr_buf_reg[24]_i_2_n_3 ;
  wire \could_multi_bursts.araddr_buf_reg[28]_i_2_n_0 ;
  wire \could_multi_bursts.araddr_buf_reg[28]_i_2_n_1 ;
  wire \could_multi_bursts.araddr_buf_reg[28]_i_2_n_2 ;
  wire \could_multi_bursts.araddr_buf_reg[28]_i_2_n_3 ;
  wire \could_multi_bursts.araddr_buf_reg[31]_i_3_n_2 ;
  wire \could_multi_bursts.araddr_buf_reg[31]_i_3_n_3 ;
  wire \could_multi_bursts.araddr_buf_reg[4]_i_2_n_0 ;
  wire \could_multi_bursts.araddr_buf_reg[4]_i_2_n_1 ;
  wire \could_multi_bursts.araddr_buf_reg[4]_i_2_n_2 ;
  wire \could_multi_bursts.araddr_buf_reg[4]_i_2_n_3 ;
  wire \could_multi_bursts.araddr_buf_reg[8]_i_2_n_0 ;
  wire \could_multi_bursts.araddr_buf_reg[8]_i_2_n_1 ;
  wire \could_multi_bursts.araddr_buf_reg[8]_i_2_n_2 ;
  wire \could_multi_bursts.araddr_buf_reg[8]_i_2_n_3 ;
  wire [5:0]\could_multi_bursts.loop_cnt_reg__0 ;
  wire \could_multi_bursts.sect_handling_reg_n_0 ;
  wire [31:2]data1;
  wire [7:0]\data_p1_reg[7] ;
  wire [31:0]\data_p2_reg[31] ;
  wire [34:34]data_pack;
  wire [31:0]end_addr;
  wire \end_addr_buf_reg_n_0_[0] ;
  wire \end_addr_buf_reg_n_0_[10] ;
  wire \end_addr_buf_reg_n_0_[11] ;
  wire \end_addr_buf_reg_n_0_[1] ;
  wire \end_addr_buf_reg_n_0_[2] ;
  wire \end_addr_buf_reg_n_0_[3] ;
  wire \end_addr_buf_reg_n_0_[4] ;
  wire \end_addr_buf_reg_n_0_[5] ;
  wire \end_addr_buf_reg_n_0_[6] ;
  wire \end_addr_buf_reg_n_0_[7] ;
  wire \end_addr_buf_reg_n_0_[8] ;
  wire \end_addr_buf_reg_n_0_[9] ;
  wire end_addr_carry__0_i_1_n_0;
  wire end_addr_carry__0_i_2_n_0;
  wire end_addr_carry__0_i_3_n_0;
  wire end_addr_carry__0_i_4_n_0;
  wire end_addr_carry__0_n_0;
  wire end_addr_carry__0_n_1;
  wire end_addr_carry__0_n_2;
  wire end_addr_carry__0_n_3;
  wire end_addr_carry__1_i_1_n_0;
  wire end_addr_carry__1_i_2_n_0;
  wire end_addr_carry__1_i_3_n_0;
  wire end_addr_carry__1_i_4_n_0;
  wire end_addr_carry__1_n_0;
  wire end_addr_carry__1_n_1;
  wire end_addr_carry__1_n_2;
  wire end_addr_carry__1_n_3;
  wire end_addr_carry__2_i_1_n_0;
  wire end_addr_carry__2_i_2_n_0;
  wire end_addr_carry__2_i_3_n_0;
  wire end_addr_carry__2_i_4_n_0;
  wire end_addr_carry__2_n_0;
  wire end_addr_carry__2_n_1;
  wire end_addr_carry__2_n_2;
  wire end_addr_carry__2_n_3;
  wire end_addr_carry__3_i_1_n_0;
  wire end_addr_carry__3_i_2_n_0;
  wire end_addr_carry__3_i_3_n_0;
  wire end_addr_carry__3_i_4_n_0;
  wire end_addr_carry__3_n_0;
  wire end_addr_carry__3_n_1;
  wire end_addr_carry__3_n_2;
  wire end_addr_carry__3_n_3;
  wire end_addr_carry__4_i_1_n_0;
  wire end_addr_carry__4_i_2_n_0;
  wire end_addr_carry__4_i_3_n_0;
  wire end_addr_carry__4_i_4_n_0;
  wire end_addr_carry__4_n_0;
  wire end_addr_carry__4_n_1;
  wire end_addr_carry__4_n_2;
  wire end_addr_carry__4_n_3;
  wire end_addr_carry__5_i_1_n_0;
  wire end_addr_carry__5_i_2_n_0;
  wire end_addr_carry__5_i_3_n_0;
  wire end_addr_carry__5_i_4_n_0;
  wire end_addr_carry__5_n_0;
  wire end_addr_carry__5_n_1;
  wire end_addr_carry__5_n_2;
  wire end_addr_carry__5_n_3;
  wire end_addr_carry__6_i_1_n_0;
  wire end_addr_carry__6_i_2_n_0;
  wire end_addr_carry__6_i_3_n_0;
  wire end_addr_carry__6_i_4_n_0;
  wire end_addr_carry__6_n_1;
  wire end_addr_carry__6_n_2;
  wire end_addr_carry__6_n_3;
  wire end_addr_carry_i_1_n_0;
  wire end_addr_carry_i_2_n_0;
  wire end_addr_carry_i_3_n_0;
  wire end_addr_carry_i_4_n_0;
  wire end_addr_carry_n_0;
  wire end_addr_carry_n_1;
  wire end_addr_carry_n_2;
  wire end_addr_carry_n_3;
  wire fifo_rctl_n_1;
  wire fifo_rctl_n_10;
  wire fifo_rctl_n_11;
  wire fifo_rctl_n_12;
  wire fifo_rctl_n_13;
  wire fifo_rctl_n_14;
  wire fifo_rctl_n_15;
  wire fifo_rctl_n_16;
  wire fifo_rctl_n_17;
  wire fifo_rctl_n_18;
  wire fifo_rctl_n_19;
  wire fifo_rctl_n_2;
  wire fifo_rctl_n_20;
  wire fifo_rctl_n_22;
  wire fifo_rctl_n_23;
  wire fifo_rctl_n_24;
  wire fifo_rctl_n_3;
  wire fifo_rctl_n_4;
  wire fifo_rctl_n_6;
  wire fifo_rctl_n_7;
  wire fifo_rctl_n_8;
  wire fifo_rctl_n_9;
  wire fifo_rctl_ready;
  wire fifo_rdata_n_13;
  wire fifo_rdata_n_14;
  wire fifo_rdata_n_15;
  wire fifo_rdata_n_16;
  wire fifo_rdata_n_17;
  wire fifo_rdata_n_18;
  wire fifo_rdata_n_20;
  wire fifo_rdata_n_21;
  wire fifo_rdata_n_22;
  wire fifo_rdata_n_23;
  wire fifo_rdata_n_24;
  wire fifo_rdata_n_25;
  wire fifo_rdata_n_26;
  wire fifo_rdata_n_27;
  wire fifo_rdata_n_28;
  wire fifo_rdata_n_29;
  wire fifo_rdata_n_3;
  wire fifo_rdata_n_30;
  wire fifo_rdata_n_31;
  wire fifo_rdata_n_32;
  wire fifo_rdata_n_33;
  wire fifo_rdata_n_34;
  wire fifo_rdata_n_35;
  wire fifo_rdata_n_36;
  wire fifo_rdata_n_37;
  wire fifo_rdata_n_38;
  wire fifo_rdata_n_39;
  wire fifo_rdata_n_4;
  wire fifo_rdata_n_40;
  wire fifo_rdata_n_41;
  wire fifo_rdata_n_42;
  wire fifo_rdata_n_43;
  wire fifo_rdata_n_44;
  wire fifo_rdata_n_45;
  wire fifo_rdata_n_46;
  wire fifo_rdata_n_47;
  wire fifo_rdata_n_48;
  wire fifo_rdata_n_49;
  wire fifo_rdata_n_5;
  wire fifo_rdata_n_51;
  wire fifo_rdata_n_52;
  wire fifo_rdata_n_53;
  wire fifo_rdata_n_54;
  wire fifo_rdata_n_55;
  wire fifo_rdata_n_56;
  wire fifo_rdata_n_57;
  wire fifo_rdata_n_58;
  wire fifo_rdata_n_59;
  wire fifo_rdata_n_6;
  wire fifo_rreq_n_10;
  wire fifo_rreq_n_11;
  wire fifo_rreq_n_12;
  wire fifo_rreq_n_13;
  wire fifo_rreq_n_14;
  wire fifo_rreq_n_15;
  wire fifo_rreq_n_16;
  wire fifo_rreq_n_17;
  wire fifo_rreq_n_18;
  wire fifo_rreq_n_19;
  wire fifo_rreq_n_2;
  wire fifo_rreq_n_20;
  wire fifo_rreq_n_21;
  wire fifo_rreq_n_22;
  wire fifo_rreq_n_23;
  wire fifo_rreq_n_25;
  wire fifo_rreq_n_26;
  wire fifo_rreq_n_27;
  wire fifo_rreq_n_28;
  wire fifo_rreq_n_29;
  wire fifo_rreq_n_30;
  wire fifo_rreq_n_31;
  wire fifo_rreq_n_33;
  wire fifo_rreq_n_34;
  wire fifo_rreq_n_4;
  wire fifo_rreq_n_5;
  wire fifo_rreq_n_6;
  wire fifo_rreq_n_7;
  wire fifo_rreq_n_8;
  wire fifo_rreq_n_9;
  wire fifo_rreq_valid;
  wire fifo_rreq_valid_buf_reg_n_0;
  wire first_sect;
  wire first_sect_carry__0_i_1_n_0;
  wire first_sect_carry__0_i_2_n_0;
  wire first_sect_carry__0_i_3_n_0;
  wire first_sect_carry__0_n_2;
  wire first_sect_carry__0_n_3;
  wire first_sect_carry_i_1_n_0;
  wire first_sect_carry_i_2_n_0;
  wire first_sect_carry_i_3_n_0;
  wire first_sect_carry_i_4_n_0;
  wire first_sect_carry_n_0;
  wire first_sect_carry_n_1;
  wire first_sect_carry_n_2;
  wire first_sect_carry_n_3;
  wire full_n0_in;
  wire gmem1_AWREADY;
  wire invalid_len_event;
  wire last_sect;
  wire last_sect_carry__0_n_2;
  wire last_sect_carry__0_n_3;
  wire last_sect_carry_n_0;
  wire last_sect_carry_n_1;
  wire last_sect_carry_n_2;
  wire last_sect_carry_n_3;
  wire last_split;
  wire [29:0]m_axi_gmem0_ARADDR;
  wire m_axi_gmem0_ARREADY;
  wire m_axi_gmem0_ARVALID;
  wire [1:0]m_axi_gmem0_RRESP;
  wire m_axi_gmem0_RVALID;
  wire [32:0]mem_reg;
  wire [31:31]minusOp;
  wire next_rreq;
  wire [19:0]p_0_in;
  wire [19:0]p_0_in0_in;
  wire p_0_out_carry__0_n_2;
  wire p_0_out_carry__0_n_3;
  wire p_0_out_carry__0_n_5;
  wire p_0_out_carry__0_n_6;
  wire p_0_out_carry__0_n_7;
  wire p_0_out_carry_n_0;
  wire p_0_out_carry_n_1;
  wire p_0_out_carry_n_2;
  wire p_0_out_carry_n_3;
  wire p_0_out_carry_n_4;
  wire p_0_out_carry_n_5;
  wire p_0_out_carry_n_6;
  wire p_0_out_carry_n_7;
  wire p_27_in;
  wire [5:0]plusOp;
  wire [19:1]plusOp_0;
  wire [7:0]plusOp__0;
  wire plusOp_carry__0_n_0;
  wire plusOp_carry__0_n_1;
  wire plusOp_carry__0_n_2;
  wire plusOp_carry__0_n_3;
  wire plusOp_carry__1_n_0;
  wire plusOp_carry__1_n_1;
  wire plusOp_carry__1_n_2;
  wire plusOp_carry__1_n_3;
  wire plusOp_carry__2_n_0;
  wire plusOp_carry__2_n_1;
  wire plusOp_carry__2_n_2;
  wire plusOp_carry__2_n_3;
  wire plusOp_carry__3_n_2;
  wire plusOp_carry__3_n_3;
  wire plusOp_carry_n_0;
  wire plusOp_carry_n_1;
  wire plusOp_carry_n_2;
  wire plusOp_carry_n_3;
  wire push;
  wire push_0;
  wire [31:0]q;
  wire rdata_valid;
  wire rreq_handling_reg_n_0;
  wire rs2f_rreq_ack;
  wire [31:0]rs2f_rreq_data;
  wire rs2f_rreq_valid;
  wire rs_rdata_n_5;
  wire [7:0]s_data;
  wire s_ready;
  wire s_ready_t_reg;
  wire [31:0]sect_addr;
  wire \sect_addr_buf_reg_n_0_[0] ;
  wire \sect_addr_buf_reg_n_0_[10] ;
  wire \sect_addr_buf_reg_n_0_[11] ;
  wire \sect_addr_buf_reg_n_0_[12] ;
  wire \sect_addr_buf_reg_n_0_[13] ;
  wire \sect_addr_buf_reg_n_0_[14] ;
  wire \sect_addr_buf_reg_n_0_[15] ;
  wire \sect_addr_buf_reg_n_0_[16] ;
  wire \sect_addr_buf_reg_n_0_[17] ;
  wire \sect_addr_buf_reg_n_0_[18] ;
  wire \sect_addr_buf_reg_n_0_[19] ;
  wire \sect_addr_buf_reg_n_0_[1] ;
  wire \sect_addr_buf_reg_n_0_[20] ;
  wire \sect_addr_buf_reg_n_0_[21] ;
  wire \sect_addr_buf_reg_n_0_[22] ;
  wire \sect_addr_buf_reg_n_0_[23] ;
  wire \sect_addr_buf_reg_n_0_[24] ;
  wire \sect_addr_buf_reg_n_0_[25] ;
  wire \sect_addr_buf_reg_n_0_[26] ;
  wire \sect_addr_buf_reg_n_0_[27] ;
  wire \sect_addr_buf_reg_n_0_[28] ;
  wire \sect_addr_buf_reg_n_0_[29] ;
  wire \sect_addr_buf_reg_n_0_[2] ;
  wire \sect_addr_buf_reg_n_0_[30] ;
  wire \sect_addr_buf_reg_n_0_[31] ;
  wire \sect_addr_buf_reg_n_0_[3] ;
  wire \sect_addr_buf_reg_n_0_[4] ;
  wire \sect_addr_buf_reg_n_0_[5] ;
  wire \sect_addr_buf_reg_n_0_[6] ;
  wire \sect_addr_buf_reg_n_0_[7] ;
  wire \sect_addr_buf_reg_n_0_[8] ;
  wire \sect_addr_buf_reg_n_0_[9] ;
  wire \sect_cnt_reg_n_0_[0] ;
  wire \sect_cnt_reg_n_0_[10] ;
  wire \sect_cnt_reg_n_0_[11] ;
  wire \sect_cnt_reg_n_0_[12] ;
  wire \sect_cnt_reg_n_0_[13] ;
  wire \sect_cnt_reg_n_0_[14] ;
  wire \sect_cnt_reg_n_0_[15] ;
  wire \sect_cnt_reg_n_0_[16] ;
  wire \sect_cnt_reg_n_0_[17] ;
  wire \sect_cnt_reg_n_0_[18] ;
  wire \sect_cnt_reg_n_0_[19] ;
  wire \sect_cnt_reg_n_0_[1] ;
  wire \sect_cnt_reg_n_0_[2] ;
  wire \sect_cnt_reg_n_0_[3] ;
  wire \sect_cnt_reg_n_0_[4] ;
  wire \sect_cnt_reg_n_0_[5] ;
  wire \sect_cnt_reg_n_0_[6] ;
  wire \sect_cnt_reg_n_0_[7] ;
  wire \sect_cnt_reg_n_0_[8] ;
  wire \sect_cnt_reg_n_0_[9] ;
  wire \sect_end_buf_reg_n_0_[0] ;
  wire \sect_end_buf_reg_n_0_[1] ;
  wire \sect_len_buf_reg_n_0_[0] ;
  wire \sect_len_buf_reg_n_0_[1] ;
  wire \sect_len_buf_reg_n_0_[2] ;
  wire \sect_len_buf_reg_n_0_[3] ;
  wire \sect_len_buf_reg_n_0_[4] ;
  wire \sect_len_buf_reg_n_0_[5] ;
  wire \sect_len_buf_reg_n_0_[6] ;
  wire \sect_len_buf_reg_n_0_[7] ;
  wire \sect_len_buf_reg_n_0_[8] ;
  wire \sect_len_buf_reg_n_0_[9] ;
  wire \start_addr_buf_reg_n_0_[0] ;
  wire \start_addr_buf_reg_n_0_[10] ;
  wire \start_addr_buf_reg_n_0_[11] ;
  wire \start_addr_buf_reg_n_0_[1] ;
  wire \start_addr_buf_reg_n_0_[2] ;
  wire \start_addr_buf_reg_n_0_[3] ;
  wire \start_addr_buf_reg_n_0_[4] ;
  wire \start_addr_buf_reg_n_0_[5] ;
  wire \start_addr_buf_reg_n_0_[6] ;
  wire \start_addr_buf_reg_n_0_[7] ;
  wire \start_addr_buf_reg_n_0_[8] ;
  wire \start_addr_buf_reg_n_0_[9] ;
  wire \start_addr_reg_n_0_[0] ;
  wire \start_addr_reg_n_0_[10] ;
  wire \start_addr_reg_n_0_[11] ;
  wire \start_addr_reg_n_0_[12] ;
  wire \start_addr_reg_n_0_[13] ;
  wire \start_addr_reg_n_0_[14] ;
  wire \start_addr_reg_n_0_[15] ;
  wire \start_addr_reg_n_0_[16] ;
  wire \start_addr_reg_n_0_[17] ;
  wire \start_addr_reg_n_0_[18] ;
  wire \start_addr_reg_n_0_[19] ;
  wire \start_addr_reg_n_0_[1] ;
  wire \start_addr_reg_n_0_[20] ;
  wire \start_addr_reg_n_0_[21] ;
  wire \start_addr_reg_n_0_[22] ;
  wire \start_addr_reg_n_0_[23] ;
  wire \start_addr_reg_n_0_[24] ;
  wire \start_addr_reg_n_0_[25] ;
  wire \start_addr_reg_n_0_[26] ;
  wire \start_addr_reg_n_0_[27] ;
  wire \start_addr_reg_n_0_[28] ;
  wire \start_addr_reg_n_0_[29] ;
  wire \start_addr_reg_n_0_[2] ;
  wire \start_addr_reg_n_0_[30] ;
  wire \start_addr_reg_n_0_[31] ;
  wire \start_addr_reg_n_0_[3] ;
  wire \start_addr_reg_n_0_[4] ;
  wire \start_addr_reg_n_0_[5] ;
  wire \start_addr_reg_n_0_[6] ;
  wire \start_addr_reg_n_0_[7] ;
  wire \start_addr_reg_n_0_[8] ;
  wire \start_addr_reg_n_0_[9] ;
  wire usedw15_out;
  wire [5:0]usedw_reg;
  wire [1:0]\NLW_beat_len_buf_reg[1]_i_1_O_UNCONNECTED ;
  wire [3:3]\NLW_beat_len_buf_reg[9]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_could_multi_bursts.araddr_buf_reg[31]_i_3_CO_UNCONNECTED ;
  wire [3:3]\NLW_could_multi_bursts.araddr_buf_reg[31]_i_3_O_UNCONNECTED ;
  wire [0:0]\NLW_could_multi_bursts.araddr_buf_reg[4]_i_2_O_UNCONNECTED ;
  wire [3:3]NLW_end_addr_carry__6_CO_UNCONNECTED;
  wire [3:0]NLW_first_sect_carry_O_UNCONNECTED;
  wire [3:3]NLW_first_sect_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_first_sect_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_last_sect_carry_O_UNCONNECTED;
  wire [3:3]NLW_last_sect_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_last_sect_carry__0_O_UNCONNECTED;
  wire [3:2]NLW_p_0_out_carry__0_CO_UNCONNECTED;
  wire [3:3]NLW_p_0_out_carry__0_O_UNCONNECTED;
  wire [3:2]NLW_plusOp_carry__3_CO_UNCONNECTED;
  wire [3:3]NLW_plusOp_carry__3_O_UNCONNECTED;

  FDRE \align_len_reg[31] 
       (.C(ap_clk),
        .CE(align_len),
        .D(minusOp),
        .Q(\align_len_reg_n_0_[31] ),
        .R(SR));
  LUT2 #(
    .INIT(4'h6)) 
    \beat_len_buf[1]_i_2 
       (.I0(\align_len_reg_n_0_[31] ),
        .I1(\start_addr_reg_n_0_[1] ),
        .O(\beat_len_buf[1]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \beat_len_buf[1]_i_3 
       (.I0(\align_len_reg_n_0_[31] ),
        .I1(\start_addr_reg_n_0_[0] ),
        .O(\beat_len_buf[1]_i_3_n_0 ));
  FDRE \beat_len_buf_reg[0] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(SHIFT_RIGHT[0]),
        .Q(beat_len_buf[0]),
        .R(SR));
  FDRE \beat_len_buf_reg[1] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(SHIFT_RIGHT[1]),
        .Q(beat_len_buf[1]),
        .R(SR));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \beat_len_buf_reg[1]_i_1 
       (.CI(1'b0),
        .CO({\beat_len_buf_reg[1]_i_1_n_0 ,\beat_len_buf_reg[1]_i_1_n_1 ,\beat_len_buf_reg[1]_i_1_n_2 ,\beat_len_buf_reg[1]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,\align_len_reg_n_0_[31] ,\align_len_reg_n_0_[31] }),
        .O({SHIFT_RIGHT[1:0],\NLW_beat_len_buf_reg[1]_i_1_O_UNCONNECTED [1:0]}),
        .S({\align_len_reg_n_0_[31] ,\align_len_reg_n_0_[31] ,\beat_len_buf[1]_i_2_n_0 ,\beat_len_buf[1]_i_3_n_0 }));
  FDRE \beat_len_buf_reg[2] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(SHIFT_RIGHT[2]),
        .Q(beat_len_buf[2]),
        .R(SR));
  FDRE \beat_len_buf_reg[3] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(SHIFT_RIGHT[3]),
        .Q(beat_len_buf[3]),
        .R(SR));
  FDRE \beat_len_buf_reg[4] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(SHIFT_RIGHT[4]),
        .Q(beat_len_buf[4]),
        .R(SR));
  FDRE \beat_len_buf_reg[5] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(SHIFT_RIGHT[5]),
        .Q(beat_len_buf[5]),
        .R(SR));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \beat_len_buf_reg[5]_i_1 
       (.CI(\beat_len_buf_reg[1]_i_1_n_0 ),
        .CO({\beat_len_buf_reg[5]_i_1_n_0 ,\beat_len_buf_reg[5]_i_1_n_1 ,\beat_len_buf_reg[5]_i_1_n_2 ,\beat_len_buf_reg[5]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(SHIFT_RIGHT[5:2]),
        .S({\align_len_reg_n_0_[31] ,\align_len_reg_n_0_[31] ,\align_len_reg_n_0_[31] ,\align_len_reg_n_0_[31] }));
  FDRE \beat_len_buf_reg[6] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(SHIFT_RIGHT[6]),
        .Q(beat_len_buf[6]),
        .R(SR));
  FDRE \beat_len_buf_reg[7] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(SHIFT_RIGHT[7]),
        .Q(beat_len_buf[7]),
        .R(SR));
  FDRE \beat_len_buf_reg[8] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(SHIFT_RIGHT[8]),
        .Q(beat_len_buf[8]),
        .R(SR));
  FDRE \beat_len_buf_reg[9] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(SHIFT_RIGHT[9]),
        .Q(beat_len_buf[9]),
        .R(SR));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \beat_len_buf_reg[9]_i_1 
       (.CI(\beat_len_buf_reg[5]_i_1_n_0 ),
        .CO({\NLW_beat_len_buf_reg[9]_i_1_CO_UNCONNECTED [3],\beat_len_buf_reg[9]_i_1_n_1 ,\beat_len_buf_reg[9]_i_1_n_2 ,\beat_len_buf_reg[9]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(SHIFT_RIGHT[9:6]),
        .S({\align_len_reg_n_0_[31] ,\align_len_reg_n_0_[31] ,\align_len_reg_n_0_[31] ,\align_len_reg_n_0_[31] }));
  FDRE \bus_wide_gen.data_buf_reg[0] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_40 ),
        .Q(s_data[0]),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[10] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_30 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[10] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[11] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_29 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[11] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[12] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_28 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[12] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[13] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_27 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[13] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[14] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_26 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[14] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[15] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_25 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[15] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[16] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_24 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[16] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[17] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_23 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[17] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[18] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_22 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[18] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[19] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_21 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[19] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[1] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_39 ),
        .Q(s_data[1]),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[20] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_20 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[20] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[21] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_19 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[21] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[22] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(fifo_rdata_n_17),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[22] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[23] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(fifo_rdata_n_16),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[23] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[24] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_18 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[24] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[25] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_17 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[25] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[26] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_16 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[26] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[27] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_15 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[27] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[28] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_14 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[28] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[29] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_13 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[29] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[2] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_38 ),
        .Q(s_data[2]),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[30] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_12 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[30] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[31] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_11 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[31] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[3] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_37 ),
        .Q(s_data[3]),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[4] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_36 ),
        .Q(s_data[4]),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[5] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_35 ),
        .Q(s_data[5]),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[6] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_34 ),
        .Q(s_data[6]),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[7] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_33 ),
        .Q(s_data[7]),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[8] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_32 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \bus_wide_gen.data_buf_reg[9] 
       (.C(ap_clk),
        .CE(\bus_wide_gen.fifo_burst_n_4 ),
        .D(\bus_wide_gen.fifo_burst_n_31 ),
        .Q(\bus_wide_gen.data_buf_reg_n_0_[9] ),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem0_m_axi_fifo__parameterized1 \bus_wide_gen.fifo_burst 
       (.D({\bus_wide_gen.fifo_burst_n_11 ,\bus_wide_gen.fifo_burst_n_12 ,\bus_wide_gen.fifo_burst_n_13 ,\bus_wide_gen.fifo_burst_n_14 ,\bus_wide_gen.fifo_burst_n_15 ,\bus_wide_gen.fifo_burst_n_16 ,\bus_wide_gen.fifo_burst_n_17 ,\bus_wide_gen.fifo_burst_n_18 ,\bus_wide_gen.fifo_burst_n_19 ,\bus_wide_gen.fifo_burst_n_20 ,\bus_wide_gen.fifo_burst_n_21 ,\bus_wide_gen.fifo_burst_n_22 ,\bus_wide_gen.fifo_burst_n_23 ,\bus_wide_gen.fifo_burst_n_24 ,\bus_wide_gen.fifo_burst_n_25 ,\bus_wide_gen.fifo_burst_n_26 ,\bus_wide_gen.fifo_burst_n_27 ,\bus_wide_gen.fifo_burst_n_28 ,\bus_wide_gen.fifo_burst_n_29 ,\bus_wide_gen.fifo_burst_n_30 ,\bus_wide_gen.fifo_burst_n_31 ,\bus_wide_gen.fifo_burst_n_32 ,\bus_wide_gen.fifo_burst_n_33 ,\bus_wide_gen.fifo_burst_n_34 ,\bus_wide_gen.fifo_burst_n_35 ,\bus_wide_gen.fifo_burst_n_36 ,\bus_wide_gen.fifo_burst_n_37 ,\bus_wide_gen.fifo_burst_n_38 ,\bus_wide_gen.fifo_burst_n_39 ,\bus_wide_gen.fifo_burst_n_40 }),
        .E(\bus_wide_gen.fifo_burst_n_4 ),
        .Q({\sect_len_buf_reg_n_0_[9] ,\sect_len_buf_reg_n_0_[8] ,\sect_len_buf_reg_n_0_[7] ,\sect_len_buf_reg_n_0_[6] ,\sect_len_buf_reg_n_0_[5] ,\sect_len_buf_reg_n_0_[4] ,\sect_len_buf_reg_n_0_[3] ,\sect_len_buf_reg_n_0_[2] ,\sect_len_buf_reg_n_0_[1] ,\sect_len_buf_reg_n_0_[0] }),
        .SR(SR),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_0(\bus_wide_gen.fifo_burst_n_1 ),
        .beat_valid(beat_valid),
        .\bus_wide_gen.data_buf_reg[0] (fifo_rdata_n_59),
        .\bus_wide_gen.data_buf_reg[10] (fifo_rdata_n_43),
        .\bus_wide_gen.data_buf_reg[11] (fifo_rdata_n_44),
        .\bus_wide_gen.data_buf_reg[12] (fifo_rdata_n_45),
        .\bus_wide_gen.data_buf_reg[13] (fifo_rdata_n_46),
        .\bus_wide_gen.data_buf_reg[14] (fifo_rdata_n_47),
        .\bus_wide_gen.data_buf_reg[15] (fifo_rdata_n_48),
        .\bus_wide_gen.data_buf_reg[1] (fifo_rdata_n_58),
        .\bus_wide_gen.data_buf_reg[21] ({\bus_wide_gen.data_buf_reg_n_0_[29] ,\bus_wide_gen.data_buf_reg_n_0_[28] ,\bus_wide_gen.data_buf_reg_n_0_[27] ,\bus_wide_gen.data_buf_reg_n_0_[26] ,\bus_wide_gen.data_buf_reg_n_0_[25] ,\bus_wide_gen.data_buf_reg_n_0_[24] ,\bus_wide_gen.data_buf_reg_n_0_[23] ,\bus_wide_gen.data_buf_reg_n_0_[22] ,\bus_wide_gen.data_buf_reg_n_0_[21] ,\bus_wide_gen.data_buf_reg_n_0_[20] ,\bus_wide_gen.data_buf_reg_n_0_[19] ,\bus_wide_gen.data_buf_reg_n_0_[18] ,\bus_wide_gen.data_buf_reg_n_0_[17] ,\bus_wide_gen.data_buf_reg_n_0_[16] ,\bus_wide_gen.data_buf_reg_n_0_[15] ,\bus_wide_gen.data_buf_reg_n_0_[14] ,\bus_wide_gen.data_buf_reg_n_0_[13] ,\bus_wide_gen.data_buf_reg_n_0_[12] ,\bus_wide_gen.data_buf_reg_n_0_[11] ,\bus_wide_gen.data_buf_reg_n_0_[10] ,\bus_wide_gen.data_buf_reg_n_0_[9] ,\bus_wide_gen.data_buf_reg_n_0_[8] }),
        .\bus_wide_gen.data_buf_reg[25] (fifo_rdata_n_49),
        .\bus_wide_gen.data_buf_reg[2] (fifo_rdata_n_57),
        .\bus_wide_gen.data_buf_reg[31] ({fifo_rdata_n_20,fifo_rdata_n_21,fifo_rdata_n_22,fifo_rdata_n_23,fifo_rdata_n_24,fifo_rdata_n_25,fifo_rdata_n_26,fifo_rdata_n_27,fifo_rdata_n_28,fifo_rdata_n_29,fifo_rdata_n_30,fifo_rdata_n_31,fifo_rdata_n_32,fifo_rdata_n_33,fifo_rdata_n_34,fifo_rdata_n_35,fifo_rdata_n_36,fifo_rdata_n_37,fifo_rdata_n_38,fifo_rdata_n_39,fifo_rdata_n_40,fifo_rdata_n_41}),
        .\bus_wide_gen.data_buf_reg[3] (fifo_rdata_n_56),
        .\bus_wide_gen.data_buf_reg[4] (fifo_rdata_n_55),
        .\bus_wide_gen.data_buf_reg[5] (fifo_rdata_n_54),
        .\bus_wide_gen.data_buf_reg[6] (fifo_rdata_n_53),
        .\bus_wide_gen.data_buf_reg[7] (fifo_rdata_n_52),
        .\bus_wide_gen.data_buf_reg[8] (fifo_rdata_n_18),
        .\bus_wide_gen.data_buf_reg[9] (fifo_rdata_n_42),
        .\bus_wide_gen.len_cnt_reg[0] (rs_rdata_n_5),
        .\bus_wide_gen.len_cnt_reg[0]_0 (fifo_rctl_n_22),
        .\bus_wide_gen.rdata_valid_t_reg (\bus_wide_gen.rdata_valid_t_reg_n_0 ),
        .\bus_wide_gen.split_cnt_buf[1]_i_3_0 (\bus_wide_gen.len_cnt_reg__0 ),
        .\bus_wide_gen.split_cnt_buf_reg[0] (\bus_wide_gen.fifo_burst_n_5 ),
        .\bus_wide_gen.split_cnt_buf_reg[0]_0 (\bus_wide_gen.fifo_burst_n_45 ),
        .\bus_wide_gen.split_cnt_buf_reg[0]_1 (\bus_wide_gen.fifo_burst_n_46 ),
        .\bus_wide_gen.split_cnt_buf_reg[0]_2 (\bus_wide_gen.split_cnt_buf_reg_n_0_[0] ),
        .\bus_wide_gen.split_cnt_buf_reg[1] (\bus_wide_gen.fifo_burst_n_3 ),
        .\bus_wide_gen.split_cnt_buf_reg[1]_0 (\bus_wide_gen.split_cnt_buf_reg_n_0_[1] ),
        .\could_multi_bursts.ARVALID_Dummy_reg (\could_multi_bursts.sect_handling_reg_n_0 ),
        .\could_multi_bursts.arlen_buf_reg[0] (\could_multi_bursts.loop_cnt_reg__0 ),
        .\could_multi_bursts.loop_cnt_reg[0] (\bus_wide_gen.fifo_burst_n_47 ),
        .fifo_rctl_ready(fifo_rctl_ready),
        .full_n0_in(full_n0_in),
        .full_n_tmp_reg_0(\bus_wide_gen.fifo_burst_n_48 ),
        .in(arlen_tmp),
        .last_split(last_split),
        .m_axi_gmem0_ARREADY(m_axi_gmem0_ARREADY),
        .m_axi_gmem0_ARVALID(m_axi_gmem0_ARVALID),
        .push(push),
        .\q_reg[10]_0 (\bus_wide_gen.fifo_burst_n_43 ),
        .\q_reg[11]_0 (COUNT),
        .\q_reg[11]_1 ({\sect_addr_buf_reg_n_0_[1] ,\sect_addr_buf_reg_n_0_[0] }),
        .\q_reg[8]_0 (\sect_end_buf_reg_n_0_[0] ),
        .\q_reg[9]_0 (\sect_end_buf_reg_n_0_[1] ),
        .s_ready(s_ready),
        .s_ready_t_reg(\bus_wide_gen.fifo_burst_n_44 ),
        .s_ready_t_reg_0(\bus_wide_gen.fifo_burst_n_49 ),
        .\sect_len_buf_reg[9] (\bus_wide_gen.fifo_burst_n_6 ));
  LUT1 #(
    .INIT(2'h1)) 
    \bus_wide_gen.len_cnt[0]_i_1 
       (.I0(\bus_wide_gen.len_cnt_reg__0 [0]),
        .O(plusOp__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \bus_wide_gen.len_cnt[1]_i_1 
       (.I0(\bus_wide_gen.len_cnt_reg__0 [1]),
        .I1(\bus_wide_gen.len_cnt_reg__0 [0]),
        .O(plusOp__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \bus_wide_gen.len_cnt[2]_i_1 
       (.I0(\bus_wide_gen.len_cnt_reg__0 [2]),
        .I1(\bus_wide_gen.len_cnt_reg__0 [0]),
        .I2(\bus_wide_gen.len_cnt_reg__0 [1]),
        .O(plusOp__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \bus_wide_gen.len_cnt[3]_i_1 
       (.I0(\bus_wide_gen.len_cnt_reg__0 [3]),
        .I1(\bus_wide_gen.len_cnt_reg__0 [1]),
        .I2(\bus_wide_gen.len_cnt_reg__0 [0]),
        .I3(\bus_wide_gen.len_cnt_reg__0 [2]),
        .O(plusOp__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \bus_wide_gen.len_cnt[4]_i_1 
       (.I0(\bus_wide_gen.len_cnt_reg__0 [4]),
        .I1(\bus_wide_gen.len_cnt_reg__0 [2]),
        .I2(\bus_wide_gen.len_cnt_reg__0 [0]),
        .I3(\bus_wide_gen.len_cnt_reg__0 [1]),
        .I4(\bus_wide_gen.len_cnt_reg__0 [3]),
        .O(plusOp__0[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \bus_wide_gen.len_cnt[5]_i_1 
       (.I0(\bus_wide_gen.len_cnt_reg__0 [5]),
        .I1(\bus_wide_gen.len_cnt_reg__0 [3]),
        .I2(\bus_wide_gen.len_cnt_reg__0 [1]),
        .I3(\bus_wide_gen.len_cnt_reg__0 [0]),
        .I4(\bus_wide_gen.len_cnt_reg__0 [2]),
        .I5(\bus_wide_gen.len_cnt_reg__0 [4]),
        .O(plusOp__0[5]));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \bus_wide_gen.len_cnt[6]_i_1 
       (.I0(\bus_wide_gen.len_cnt_reg__0 [6]),
        .I1(\bus_wide_gen.len_cnt[7]_i_6_n_0 ),
        .O(plusOp__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \bus_wide_gen.len_cnt[7]_i_3 
       (.I0(\bus_wide_gen.len_cnt_reg__0 [7]),
        .I1(\bus_wide_gen.len_cnt[7]_i_6_n_0 ),
        .I2(\bus_wide_gen.len_cnt_reg__0 [6]),
        .O(plusOp__0[7]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \bus_wide_gen.len_cnt[7]_i_6 
       (.I0(\bus_wide_gen.len_cnt_reg__0 [5]),
        .I1(\bus_wide_gen.len_cnt_reg__0 [3]),
        .I2(\bus_wide_gen.len_cnt_reg__0 [1]),
        .I3(\bus_wide_gen.len_cnt_reg__0 [0]),
        .I4(\bus_wide_gen.len_cnt_reg__0 [2]),
        .I5(\bus_wide_gen.len_cnt_reg__0 [4]),
        .O(\bus_wide_gen.len_cnt[7]_i_6_n_0 ));
  FDRE \bus_wide_gen.len_cnt_reg[0] 
       (.C(ap_clk),
        .CE(last_split),
        .D(plusOp__0[0]),
        .Q(\bus_wide_gen.len_cnt_reg__0 [0]),
        .R(\bus_wide_gen.fifo_burst_n_1 ));
  FDRE \bus_wide_gen.len_cnt_reg[1] 
       (.C(ap_clk),
        .CE(last_split),
        .D(plusOp__0[1]),
        .Q(\bus_wide_gen.len_cnt_reg__0 [1]),
        .R(\bus_wide_gen.fifo_burst_n_1 ));
  FDRE \bus_wide_gen.len_cnt_reg[2] 
       (.C(ap_clk),
        .CE(last_split),
        .D(plusOp__0[2]),
        .Q(\bus_wide_gen.len_cnt_reg__0 [2]),
        .R(\bus_wide_gen.fifo_burst_n_1 ));
  FDRE \bus_wide_gen.len_cnt_reg[3] 
       (.C(ap_clk),
        .CE(last_split),
        .D(plusOp__0[3]),
        .Q(\bus_wide_gen.len_cnt_reg__0 [3]),
        .R(\bus_wide_gen.fifo_burst_n_1 ));
  FDRE \bus_wide_gen.len_cnt_reg[4] 
       (.C(ap_clk),
        .CE(last_split),
        .D(plusOp__0[4]),
        .Q(\bus_wide_gen.len_cnt_reg__0 [4]),
        .R(\bus_wide_gen.fifo_burst_n_1 ));
  FDRE \bus_wide_gen.len_cnt_reg[5] 
       (.C(ap_clk),
        .CE(last_split),
        .D(plusOp__0[5]),
        .Q(\bus_wide_gen.len_cnt_reg__0 [5]),
        .R(\bus_wide_gen.fifo_burst_n_1 ));
  FDRE \bus_wide_gen.len_cnt_reg[6] 
       (.C(ap_clk),
        .CE(last_split),
        .D(plusOp__0[6]),
        .Q(\bus_wide_gen.len_cnt_reg__0 [6]),
        .R(\bus_wide_gen.fifo_burst_n_1 ));
  FDRE \bus_wide_gen.len_cnt_reg[7] 
       (.C(ap_clk),
        .CE(last_split),
        .D(plusOp__0[7]),
        .Q(\bus_wide_gen.len_cnt_reg__0 [7]),
        .R(\bus_wide_gen.fifo_burst_n_1 ));
  FDRE \bus_wide_gen.rdata_valid_t_reg 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\bus_wide_gen.fifo_burst_n_49 ),
        .Q(\bus_wide_gen.rdata_valid_t_reg_n_0 ),
        .R(SR));
  FDRE \bus_wide_gen.split_cnt_buf_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\bus_wide_gen.fifo_burst_n_5 ),
        .Q(\bus_wide_gen.split_cnt_buf_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \bus_wide_gen.split_cnt_buf_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\bus_wide_gen.fifo_burst_n_3 ),
        .Q(\bus_wide_gen.split_cnt_buf_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \could_multi_bursts.ARVALID_Dummy_reg 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\bus_wide_gen.fifo_burst_n_48 ),
        .Q(m_axi_gmem0_ARVALID),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[10]_i_1 
       (.I0(data1[10]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[10] ),
        .O(araddr_tmp[10]));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[11]_i_1 
       (.I0(data1[11]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[11] ),
        .O(araddr_tmp[11]));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[12]_i_1 
       (.I0(data1[12]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[12] ),
        .O(araddr_tmp[12]));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[13]_i_1 
       (.I0(data1[13]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[13] ),
        .O(araddr_tmp[13]));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[14]_i_1 
       (.I0(data1[14]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[14] ),
        .O(araddr_tmp[14]));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[15]_i_1 
       (.I0(data1[15]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[15] ),
        .O(araddr_tmp[15]));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[16]_i_1 
       (.I0(data1[16]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[16] ),
        .O(araddr_tmp[16]));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[17]_i_1 
       (.I0(data1[17]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[17] ),
        .O(araddr_tmp[17]));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[18]_i_1 
       (.I0(data1[18]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[18] ),
        .O(araddr_tmp[18]));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[19]_i_1 
       (.I0(data1[19]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[19] ),
        .O(araddr_tmp[19]));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[20]_i_1 
       (.I0(data1[20]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[20] ),
        .O(araddr_tmp[20]));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[21]_i_1 
       (.I0(data1[21]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[21] ),
        .O(araddr_tmp[21]));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[22]_i_1 
       (.I0(data1[22]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[22] ),
        .O(araddr_tmp[22]));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[23]_i_1 
       (.I0(data1[23]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[23] ),
        .O(araddr_tmp[23]));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[24]_i_1 
       (.I0(data1[24]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[24] ),
        .O(araddr_tmp[24]));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[25]_i_1 
       (.I0(data1[25]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[25] ),
        .O(araddr_tmp[25]));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[26]_i_1 
       (.I0(data1[26]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[26] ),
        .O(araddr_tmp[26]));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[27]_i_1 
       (.I0(data1[27]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[27] ),
        .O(araddr_tmp[27]));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[28]_i_1 
       (.I0(data1[28]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[28] ),
        .O(araddr_tmp[28]));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[29]_i_1 
       (.I0(data1[29]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[29] ),
        .O(araddr_tmp[29]));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[2]_i_1 
       (.I0(data1[2]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[2] ),
        .O(araddr_tmp[2]));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[30]_i_1 
       (.I0(data1[30]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[30] ),
        .O(araddr_tmp[30]));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[31]_i_2 
       (.I0(data1[31]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[31] ),
        .O(araddr_tmp[31]));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[3]_i_1 
       (.I0(data1[3]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[3] ),
        .O(araddr_tmp[3]));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[4]_i_1 
       (.I0(data1[4]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[4] ),
        .O(araddr_tmp[4]));
  LUT4 #(
    .INIT(16'h956A)) 
    \could_multi_bursts.araddr_buf[4]_i_3 
       (.I0(m_axi_gmem0_ARADDR[2]),
        .I1(ARLEN[1]),
        .I2(ARLEN[0]),
        .I3(ARLEN[2]),
        .O(\could_multi_bursts.araddr_buf[4]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h96)) 
    \could_multi_bursts.araddr_buf[4]_i_4 
       (.I0(m_axi_gmem0_ARADDR[1]),
        .I1(ARLEN[0]),
        .I2(ARLEN[1]),
        .O(\could_multi_bursts.araddr_buf[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \could_multi_bursts.araddr_buf[4]_i_5 
       (.I0(m_axi_gmem0_ARADDR[0]),
        .I1(ARLEN[0]),
        .O(\could_multi_bursts.araddr_buf[4]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[5]_i_1 
       (.I0(data1[5]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[5] ),
        .O(araddr_tmp[5]));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[6]_i_1 
       (.I0(data1[6]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[6] ),
        .O(araddr_tmp[6]));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[7]_i_1 
       (.I0(data1[7]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[7] ),
        .O(araddr_tmp[7]));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[8]_i_1 
       (.I0(data1[8]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[8] ),
        .O(araddr_tmp[8]));
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \could_multi_bursts.araddr_buf[8]_i_3 
       (.I0(m_axi_gmem0_ARADDR[4]),
        .I1(ARLEN[2]),
        .I2(ARLEN[0]),
        .I3(ARLEN[1]),
        .I4(ARLEN[3]),
        .O(\could_multi_bursts.araddr_buf[8]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h95556AAA)) 
    \could_multi_bursts.araddr_buf[8]_i_4 
       (.I0(m_axi_gmem0_ARADDR[3]),
        .I1(ARLEN[2]),
        .I2(ARLEN[0]),
        .I3(ARLEN[1]),
        .I4(ARLEN[3]),
        .O(\could_multi_bursts.araddr_buf[8]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.araddr_buf[9]_i_1 
       (.I0(data1[9]),
        .I1(\bus_wide_gen.fifo_burst_n_47 ),
        .I2(\sect_addr_buf_reg_n_0_[9] ),
        .O(araddr_tmp[9]));
  FDRE \could_multi_bursts.araddr_buf_reg[10] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[10]),
        .Q(m_axi_gmem0_ARADDR[8]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[11] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[11]),
        .Q(m_axi_gmem0_ARADDR[9]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[12] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[12]),
        .Q(m_axi_gmem0_ARADDR[10]),
        .R(SR));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \could_multi_bursts.araddr_buf_reg[12]_i_2 
       (.CI(\could_multi_bursts.araddr_buf_reg[8]_i_2_n_0 ),
        .CO({\could_multi_bursts.araddr_buf_reg[12]_i_2_n_0 ,\could_multi_bursts.araddr_buf_reg[12]_i_2_n_1 ,\could_multi_bursts.araddr_buf_reg[12]_i_2_n_2 ,\could_multi_bursts.araddr_buf_reg[12]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data1[12:9]),
        .S(m_axi_gmem0_ARADDR[10:7]));
  FDRE \could_multi_bursts.araddr_buf_reg[13] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[13]),
        .Q(m_axi_gmem0_ARADDR[11]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[14] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[14]),
        .Q(m_axi_gmem0_ARADDR[12]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[15] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[15]),
        .Q(m_axi_gmem0_ARADDR[13]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[16] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[16]),
        .Q(m_axi_gmem0_ARADDR[14]),
        .R(SR));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \could_multi_bursts.araddr_buf_reg[16]_i_2 
       (.CI(\could_multi_bursts.araddr_buf_reg[12]_i_2_n_0 ),
        .CO({\could_multi_bursts.araddr_buf_reg[16]_i_2_n_0 ,\could_multi_bursts.araddr_buf_reg[16]_i_2_n_1 ,\could_multi_bursts.araddr_buf_reg[16]_i_2_n_2 ,\could_multi_bursts.araddr_buf_reg[16]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data1[16:13]),
        .S(m_axi_gmem0_ARADDR[14:11]));
  FDRE \could_multi_bursts.araddr_buf_reg[17] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[17]),
        .Q(m_axi_gmem0_ARADDR[15]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[18] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[18]),
        .Q(m_axi_gmem0_ARADDR[16]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[19] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[19]),
        .Q(m_axi_gmem0_ARADDR[17]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[20] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[20]),
        .Q(m_axi_gmem0_ARADDR[18]),
        .R(SR));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \could_multi_bursts.araddr_buf_reg[20]_i_2 
       (.CI(\could_multi_bursts.araddr_buf_reg[16]_i_2_n_0 ),
        .CO({\could_multi_bursts.araddr_buf_reg[20]_i_2_n_0 ,\could_multi_bursts.araddr_buf_reg[20]_i_2_n_1 ,\could_multi_bursts.araddr_buf_reg[20]_i_2_n_2 ,\could_multi_bursts.araddr_buf_reg[20]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data1[20:17]),
        .S(m_axi_gmem0_ARADDR[18:15]));
  FDRE \could_multi_bursts.araddr_buf_reg[21] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[21]),
        .Q(m_axi_gmem0_ARADDR[19]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[22] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[22]),
        .Q(m_axi_gmem0_ARADDR[20]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[23] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[23]),
        .Q(m_axi_gmem0_ARADDR[21]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[24] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[24]),
        .Q(m_axi_gmem0_ARADDR[22]),
        .R(SR));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \could_multi_bursts.araddr_buf_reg[24]_i_2 
       (.CI(\could_multi_bursts.araddr_buf_reg[20]_i_2_n_0 ),
        .CO({\could_multi_bursts.araddr_buf_reg[24]_i_2_n_0 ,\could_multi_bursts.araddr_buf_reg[24]_i_2_n_1 ,\could_multi_bursts.araddr_buf_reg[24]_i_2_n_2 ,\could_multi_bursts.araddr_buf_reg[24]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data1[24:21]),
        .S(m_axi_gmem0_ARADDR[22:19]));
  FDRE \could_multi_bursts.araddr_buf_reg[25] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[25]),
        .Q(m_axi_gmem0_ARADDR[23]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[26] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[26]),
        .Q(m_axi_gmem0_ARADDR[24]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[27] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[27]),
        .Q(m_axi_gmem0_ARADDR[25]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[28] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[28]),
        .Q(m_axi_gmem0_ARADDR[26]),
        .R(SR));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \could_multi_bursts.araddr_buf_reg[28]_i_2 
       (.CI(\could_multi_bursts.araddr_buf_reg[24]_i_2_n_0 ),
        .CO({\could_multi_bursts.araddr_buf_reg[28]_i_2_n_0 ,\could_multi_bursts.araddr_buf_reg[28]_i_2_n_1 ,\could_multi_bursts.araddr_buf_reg[28]_i_2_n_2 ,\could_multi_bursts.araddr_buf_reg[28]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data1[28:25]),
        .S(m_axi_gmem0_ARADDR[26:23]));
  FDRE \could_multi_bursts.araddr_buf_reg[29] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[29]),
        .Q(m_axi_gmem0_ARADDR[27]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[2] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[2]),
        .Q(m_axi_gmem0_ARADDR[0]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[30] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[30]),
        .Q(m_axi_gmem0_ARADDR[28]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[31] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[31]),
        .Q(m_axi_gmem0_ARADDR[29]),
        .R(SR));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \could_multi_bursts.araddr_buf_reg[31]_i_3 
       (.CI(\could_multi_bursts.araddr_buf_reg[28]_i_2_n_0 ),
        .CO({\NLW_could_multi_bursts.araddr_buf_reg[31]_i_3_CO_UNCONNECTED [3:2],\could_multi_bursts.araddr_buf_reg[31]_i_3_n_2 ,\could_multi_bursts.araddr_buf_reg[31]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_could_multi_bursts.araddr_buf_reg[31]_i_3_O_UNCONNECTED [3],data1[31:29]}),
        .S({1'b0,m_axi_gmem0_ARADDR[29:27]}));
  FDRE \could_multi_bursts.araddr_buf_reg[3] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[3]),
        .Q(m_axi_gmem0_ARADDR[1]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[4] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[4]),
        .Q(m_axi_gmem0_ARADDR[2]),
        .R(SR));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \could_multi_bursts.araddr_buf_reg[4]_i_2 
       (.CI(1'b0),
        .CO({\could_multi_bursts.araddr_buf_reg[4]_i_2_n_0 ,\could_multi_bursts.araddr_buf_reg[4]_i_2_n_1 ,\could_multi_bursts.araddr_buf_reg[4]_i_2_n_2 ,\could_multi_bursts.araddr_buf_reg[4]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({m_axi_gmem0_ARADDR[2:0],1'b0}),
        .O({data1[4:2],\NLW_could_multi_bursts.araddr_buf_reg[4]_i_2_O_UNCONNECTED [0]}),
        .S({\could_multi_bursts.araddr_buf[4]_i_3_n_0 ,\could_multi_bursts.araddr_buf[4]_i_4_n_0 ,\could_multi_bursts.araddr_buf[4]_i_5_n_0 ,1'b0}));
  FDRE \could_multi_bursts.araddr_buf_reg[5] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[5]),
        .Q(m_axi_gmem0_ARADDR[3]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[6] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[6]),
        .Q(m_axi_gmem0_ARADDR[4]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[7] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[7]),
        .Q(m_axi_gmem0_ARADDR[5]),
        .R(SR));
  FDRE \could_multi_bursts.araddr_buf_reg[8] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[8]),
        .Q(m_axi_gmem0_ARADDR[6]),
        .R(SR));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \could_multi_bursts.araddr_buf_reg[8]_i_2 
       (.CI(\could_multi_bursts.araddr_buf_reg[4]_i_2_n_0 ),
        .CO({\could_multi_bursts.araddr_buf_reg[8]_i_2_n_0 ,\could_multi_bursts.araddr_buf_reg[8]_i_2_n_1 ,\could_multi_bursts.araddr_buf_reg[8]_i_2_n_2 ,\could_multi_bursts.araddr_buf_reg[8]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,m_axi_gmem0_ARADDR[4:3]}),
        .O(data1[8:5]),
        .S({m_axi_gmem0_ARADDR[6:5],\could_multi_bursts.araddr_buf[8]_i_3_n_0 ,\could_multi_bursts.araddr_buf[8]_i_4_n_0 }));
  FDRE \could_multi_bursts.araddr_buf_reg[9] 
       (.C(ap_clk),
        .CE(push),
        .D(araddr_tmp[9]),
        .Q(m_axi_gmem0_ARADDR[7]),
        .R(SR));
  FDRE \could_multi_bursts.arlen_buf_reg[0] 
       (.C(ap_clk),
        .CE(push),
        .D(arlen_tmp[0]),
        .Q(ARLEN[0]),
        .R(SR));
  FDRE \could_multi_bursts.arlen_buf_reg[1] 
       (.C(ap_clk),
        .CE(push),
        .D(arlen_tmp[1]),
        .Q(ARLEN[1]),
        .R(SR));
  FDRE \could_multi_bursts.arlen_buf_reg[2] 
       (.C(ap_clk),
        .CE(push),
        .D(arlen_tmp[2]),
        .Q(ARLEN[2]),
        .R(SR));
  FDRE \could_multi_bursts.arlen_buf_reg[3] 
       (.C(ap_clk),
        .CE(push),
        .D(arlen_tmp[3]),
        .Q(ARLEN[3]),
        .R(SR));
  LUT1 #(
    .INIT(2'h1)) 
    \could_multi_bursts.loop_cnt[0]_i_1 
       (.I0(\could_multi_bursts.loop_cnt_reg__0 [0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \could_multi_bursts.loop_cnt[1]_i_1 
       (.I0(\could_multi_bursts.loop_cnt_reg__0 [1]),
        .I1(\could_multi_bursts.loop_cnt_reg__0 [0]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \could_multi_bursts.loop_cnt[2]_i_1 
       (.I0(\could_multi_bursts.loop_cnt_reg__0 [2]),
        .I1(\could_multi_bursts.loop_cnt_reg__0 [0]),
        .I2(\could_multi_bursts.loop_cnt_reg__0 [1]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \could_multi_bursts.loop_cnt[3]_i_1 
       (.I0(\could_multi_bursts.loop_cnt_reg__0 [3]),
        .I1(\could_multi_bursts.loop_cnt_reg__0 [1]),
        .I2(\could_multi_bursts.loop_cnt_reg__0 [0]),
        .I3(\could_multi_bursts.loop_cnt_reg__0 [2]),
        .O(plusOp[3]));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \could_multi_bursts.loop_cnt[4]_i_1 
       (.I0(\could_multi_bursts.loop_cnt_reg__0 [4]),
        .I1(\could_multi_bursts.loop_cnt_reg__0 [2]),
        .I2(\could_multi_bursts.loop_cnt_reg__0 [0]),
        .I3(\could_multi_bursts.loop_cnt_reg__0 [1]),
        .I4(\could_multi_bursts.loop_cnt_reg__0 [3]),
        .O(plusOp[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \could_multi_bursts.loop_cnt[5]_i_2 
       (.I0(\could_multi_bursts.loop_cnt_reg__0 [5]),
        .I1(\could_multi_bursts.loop_cnt_reg__0 [3]),
        .I2(\could_multi_bursts.loop_cnt_reg__0 [1]),
        .I3(\could_multi_bursts.loop_cnt_reg__0 [0]),
        .I4(\could_multi_bursts.loop_cnt_reg__0 [2]),
        .I5(\could_multi_bursts.loop_cnt_reg__0 [4]),
        .O(plusOp[5]));
  FDRE \could_multi_bursts.loop_cnt_reg[0] 
       (.C(ap_clk),
        .CE(push),
        .D(plusOp[0]),
        .Q(\could_multi_bursts.loop_cnt_reg__0 [0]),
        .R(fifo_rctl_n_2));
  FDRE \could_multi_bursts.loop_cnt_reg[1] 
       (.C(ap_clk),
        .CE(push),
        .D(plusOp[1]),
        .Q(\could_multi_bursts.loop_cnt_reg__0 [1]),
        .R(fifo_rctl_n_2));
  FDRE \could_multi_bursts.loop_cnt_reg[2] 
       (.C(ap_clk),
        .CE(push),
        .D(plusOp[2]),
        .Q(\could_multi_bursts.loop_cnt_reg__0 [2]),
        .R(fifo_rctl_n_2));
  FDRE \could_multi_bursts.loop_cnt_reg[3] 
       (.C(ap_clk),
        .CE(push),
        .D(plusOp[3]),
        .Q(\could_multi_bursts.loop_cnt_reg__0 [3]),
        .R(fifo_rctl_n_2));
  FDRE \could_multi_bursts.loop_cnt_reg[4] 
       (.C(ap_clk),
        .CE(push),
        .D(plusOp[4]),
        .Q(\could_multi_bursts.loop_cnt_reg__0 [4]),
        .R(fifo_rctl_n_2));
  FDRE \could_multi_bursts.loop_cnt_reg[5] 
       (.C(ap_clk),
        .CE(push),
        .D(plusOp[5]),
        .Q(\could_multi_bursts.loop_cnt_reg__0 [5]),
        .R(fifo_rctl_n_2));
  FDRE \could_multi_bursts.sect_handling_reg 
       (.C(ap_clk),
        .CE(1'b1),
        .D(fifo_rctl_n_24),
        .Q(\could_multi_bursts.sect_handling_reg_n_0 ),
        .R(SR));
  FDRE \end_addr_buf_reg[0] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[0]),
        .Q(\end_addr_buf_reg_n_0_[0] ),
        .R(SR));
  FDRE \end_addr_buf_reg[10] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[10]),
        .Q(\end_addr_buf_reg_n_0_[10] ),
        .R(SR));
  FDRE \end_addr_buf_reg[11] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[11]),
        .Q(\end_addr_buf_reg_n_0_[11] ),
        .R(SR));
  FDRE \end_addr_buf_reg[12] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[12]),
        .Q(p_0_in0_in[0]),
        .R(SR));
  FDRE \end_addr_buf_reg[13] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[13]),
        .Q(p_0_in0_in[1]),
        .R(SR));
  FDRE \end_addr_buf_reg[14] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[14]),
        .Q(p_0_in0_in[2]),
        .R(SR));
  FDRE \end_addr_buf_reg[15] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[15]),
        .Q(p_0_in0_in[3]),
        .R(SR));
  FDRE \end_addr_buf_reg[16] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[16]),
        .Q(p_0_in0_in[4]),
        .R(SR));
  FDRE \end_addr_buf_reg[17] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[17]),
        .Q(p_0_in0_in[5]),
        .R(SR));
  FDRE \end_addr_buf_reg[18] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[18]),
        .Q(p_0_in0_in[6]),
        .R(SR));
  FDRE \end_addr_buf_reg[19] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[19]),
        .Q(p_0_in0_in[7]),
        .R(SR));
  FDRE \end_addr_buf_reg[1] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[1]),
        .Q(\end_addr_buf_reg_n_0_[1] ),
        .R(SR));
  FDRE \end_addr_buf_reg[20] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[20]),
        .Q(p_0_in0_in[8]),
        .R(SR));
  FDRE \end_addr_buf_reg[21] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[21]),
        .Q(p_0_in0_in[9]),
        .R(SR));
  FDRE \end_addr_buf_reg[22] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[22]),
        .Q(p_0_in0_in[10]),
        .R(SR));
  FDRE \end_addr_buf_reg[23] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[23]),
        .Q(p_0_in0_in[11]),
        .R(SR));
  FDRE \end_addr_buf_reg[24] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[24]),
        .Q(p_0_in0_in[12]),
        .R(SR));
  FDRE \end_addr_buf_reg[25] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[25]),
        .Q(p_0_in0_in[13]),
        .R(SR));
  FDRE \end_addr_buf_reg[26] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[26]),
        .Q(p_0_in0_in[14]),
        .R(SR));
  FDRE \end_addr_buf_reg[27] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[27]),
        .Q(p_0_in0_in[15]),
        .R(SR));
  FDRE \end_addr_buf_reg[28] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[28]),
        .Q(p_0_in0_in[16]),
        .R(SR));
  FDRE \end_addr_buf_reg[29] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[29]),
        .Q(p_0_in0_in[17]),
        .R(SR));
  FDRE \end_addr_buf_reg[2] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[2]),
        .Q(\end_addr_buf_reg_n_0_[2] ),
        .R(SR));
  FDRE \end_addr_buf_reg[30] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[30]),
        .Q(p_0_in0_in[18]),
        .R(SR));
  FDRE \end_addr_buf_reg[31] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[31]),
        .Q(p_0_in0_in[19]),
        .R(SR));
  FDRE \end_addr_buf_reg[3] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[3]),
        .Q(\end_addr_buf_reg_n_0_[3] ),
        .R(SR));
  FDRE \end_addr_buf_reg[4] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[4]),
        .Q(\end_addr_buf_reg_n_0_[4] ),
        .R(SR));
  FDRE \end_addr_buf_reg[5] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[5]),
        .Q(\end_addr_buf_reg_n_0_[5] ),
        .R(SR));
  FDRE \end_addr_buf_reg[6] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[6]),
        .Q(\end_addr_buf_reg_n_0_[6] ),
        .R(SR));
  FDRE \end_addr_buf_reg[7] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[7]),
        .Q(\end_addr_buf_reg_n_0_[7] ),
        .R(SR));
  FDRE \end_addr_buf_reg[8] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[8]),
        .Q(\end_addr_buf_reg_n_0_[8] ),
        .R(SR));
  FDRE \end_addr_buf_reg[9] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(end_addr[9]),
        .Q(\end_addr_buf_reg_n_0_[9] ),
        .R(SR));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 end_addr_carry
       (.CI(1'b0),
        .CO({end_addr_carry_n_0,end_addr_carry_n_1,end_addr_carry_n_2,end_addr_carry_n_3}),
        .CYINIT(1'b0),
        .DI({\start_addr_reg_n_0_[3] ,\start_addr_reg_n_0_[2] ,\start_addr_reg_n_0_[1] ,\start_addr_reg_n_0_[0] }),
        .O(end_addr[3:0]),
        .S({end_addr_carry_i_1_n_0,end_addr_carry_i_2_n_0,end_addr_carry_i_3_n_0,end_addr_carry_i_4_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 end_addr_carry__0
       (.CI(end_addr_carry_n_0),
        .CO({end_addr_carry__0_n_0,end_addr_carry__0_n_1,end_addr_carry__0_n_2,end_addr_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({\start_addr_reg_n_0_[7] ,\start_addr_reg_n_0_[6] ,\start_addr_reg_n_0_[5] ,\start_addr_reg_n_0_[4] }),
        .O(end_addr[7:4]),
        .S({end_addr_carry__0_i_1_n_0,end_addr_carry__0_i_2_n_0,end_addr_carry__0_i_3_n_0,end_addr_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__0_i_1
       (.I0(\start_addr_reg_n_0_[7] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__0_i_2
       (.I0(\start_addr_reg_n_0_[6] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__0_i_3
       (.I0(\start_addr_reg_n_0_[5] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__0_i_4
       (.I0(\start_addr_reg_n_0_[4] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__0_i_4_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 end_addr_carry__1
       (.CI(end_addr_carry__0_n_0),
        .CO({end_addr_carry__1_n_0,end_addr_carry__1_n_1,end_addr_carry__1_n_2,end_addr_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({\start_addr_reg_n_0_[11] ,\start_addr_reg_n_0_[10] ,\start_addr_reg_n_0_[9] ,\start_addr_reg_n_0_[8] }),
        .O(end_addr[11:8]),
        .S({end_addr_carry__1_i_1_n_0,end_addr_carry__1_i_2_n_0,end_addr_carry__1_i_3_n_0,end_addr_carry__1_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__1_i_1
       (.I0(\start_addr_reg_n_0_[11] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__1_i_2
       (.I0(\start_addr_reg_n_0_[10] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__1_i_3
       (.I0(\start_addr_reg_n_0_[9] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__1_i_4
       (.I0(\start_addr_reg_n_0_[8] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__1_i_4_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 end_addr_carry__2
       (.CI(end_addr_carry__1_n_0),
        .CO({end_addr_carry__2_n_0,end_addr_carry__2_n_1,end_addr_carry__2_n_2,end_addr_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({\start_addr_reg_n_0_[15] ,\start_addr_reg_n_0_[14] ,\start_addr_reg_n_0_[13] ,\start_addr_reg_n_0_[12] }),
        .O(end_addr[15:12]),
        .S({end_addr_carry__2_i_1_n_0,end_addr_carry__2_i_2_n_0,end_addr_carry__2_i_3_n_0,end_addr_carry__2_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__2_i_1
       (.I0(\start_addr_reg_n_0_[15] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__2_i_2
       (.I0(\start_addr_reg_n_0_[14] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__2_i_3
       (.I0(\start_addr_reg_n_0_[13] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__2_i_4
       (.I0(\start_addr_reg_n_0_[12] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__2_i_4_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 end_addr_carry__3
       (.CI(end_addr_carry__2_n_0),
        .CO({end_addr_carry__3_n_0,end_addr_carry__3_n_1,end_addr_carry__3_n_2,end_addr_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({\start_addr_reg_n_0_[19] ,\start_addr_reg_n_0_[18] ,\start_addr_reg_n_0_[17] ,\start_addr_reg_n_0_[16] }),
        .O(end_addr[19:16]),
        .S({end_addr_carry__3_i_1_n_0,end_addr_carry__3_i_2_n_0,end_addr_carry__3_i_3_n_0,end_addr_carry__3_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__3_i_1
       (.I0(\start_addr_reg_n_0_[19] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__3_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__3_i_2
       (.I0(\start_addr_reg_n_0_[18] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__3_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__3_i_3
       (.I0(\start_addr_reg_n_0_[17] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__3_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__3_i_4
       (.I0(\start_addr_reg_n_0_[16] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__3_i_4_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 end_addr_carry__4
       (.CI(end_addr_carry__3_n_0),
        .CO({end_addr_carry__4_n_0,end_addr_carry__4_n_1,end_addr_carry__4_n_2,end_addr_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({\start_addr_reg_n_0_[23] ,\start_addr_reg_n_0_[22] ,\start_addr_reg_n_0_[21] ,\start_addr_reg_n_0_[20] }),
        .O(end_addr[23:20]),
        .S({end_addr_carry__4_i_1_n_0,end_addr_carry__4_i_2_n_0,end_addr_carry__4_i_3_n_0,end_addr_carry__4_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__4_i_1
       (.I0(\start_addr_reg_n_0_[23] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__4_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__4_i_2
       (.I0(\start_addr_reg_n_0_[22] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__4_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__4_i_3
       (.I0(\start_addr_reg_n_0_[21] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__4_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__4_i_4
       (.I0(\start_addr_reg_n_0_[20] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__4_i_4_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 end_addr_carry__5
       (.CI(end_addr_carry__4_n_0),
        .CO({end_addr_carry__5_n_0,end_addr_carry__5_n_1,end_addr_carry__5_n_2,end_addr_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({\start_addr_reg_n_0_[27] ,\start_addr_reg_n_0_[26] ,\start_addr_reg_n_0_[25] ,\start_addr_reg_n_0_[24] }),
        .O(end_addr[27:24]),
        .S({end_addr_carry__5_i_1_n_0,end_addr_carry__5_i_2_n_0,end_addr_carry__5_i_3_n_0,end_addr_carry__5_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__5_i_1
       (.I0(\start_addr_reg_n_0_[27] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__5_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__5_i_2
       (.I0(\start_addr_reg_n_0_[26] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__5_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__5_i_3
       (.I0(\start_addr_reg_n_0_[25] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__5_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__5_i_4
       (.I0(\start_addr_reg_n_0_[24] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__5_i_4_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 end_addr_carry__6
       (.CI(end_addr_carry__5_n_0),
        .CO({NLW_end_addr_carry__6_CO_UNCONNECTED[3],end_addr_carry__6_n_1,end_addr_carry__6_n_2,end_addr_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,\start_addr_reg_n_0_[30] ,\start_addr_reg_n_0_[29] ,\start_addr_reg_n_0_[28] }),
        .O(end_addr[31:28]),
        .S({end_addr_carry__6_i_1_n_0,end_addr_carry__6_i_2_n_0,end_addr_carry__6_i_3_n_0,end_addr_carry__6_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__6_i_1
       (.I0(\align_len_reg_n_0_[31] ),
        .I1(\start_addr_reg_n_0_[31] ),
        .O(end_addr_carry__6_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__6_i_2
       (.I0(\start_addr_reg_n_0_[30] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__6_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__6_i_3
       (.I0(\start_addr_reg_n_0_[29] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__6_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__6_i_4
       (.I0(\start_addr_reg_n_0_[28] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__6_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry_i_1
       (.I0(\start_addr_reg_n_0_[3] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry_i_2
       (.I0(\start_addr_reg_n_0_[2] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry_i_3
       (.I0(\start_addr_reg_n_0_[1] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry_i_4
       (.I0(\start_addr_reg_n_0_[0] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry_i_4_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem0_m_axi_fifo__parameterized3 fifo_rctl
       (.CO(first_sect),
        .E(p_27_in),
        .Q({\start_addr_buf_reg_n_0_[11] ,\start_addr_buf_reg_n_0_[10] ,\start_addr_buf_reg_n_0_[9] ,\start_addr_buf_reg_n_0_[8] ,\start_addr_buf_reg_n_0_[7] ,\start_addr_buf_reg_n_0_[6] ,\start_addr_buf_reg_n_0_[5] ,\start_addr_buf_reg_n_0_[4] ,\start_addr_buf_reg_n_0_[3] ,\start_addr_buf_reg_n_0_[2] }),
        .SR(SR),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_0(fifo_rctl_n_2),
        .ap_rst_n_1(fifo_rctl_n_4),
        .\beat_len_buf_reg[2] (fifo_rctl_n_9),
        .\beat_len_buf_reg[5] (fifo_rctl_n_12),
        .\beat_len_buf_reg[6] (fifo_rctl_n_13),
        .\beat_len_buf_reg[7] (fifo_rctl_n_14),
        .\beat_len_buf_reg[8] (fifo_rctl_n_15),
        .beat_valid(beat_valid),
        .\bus_wide_gen.len_cnt_reg[0] (fifo_rctl_n_22),
        .\bus_wide_gen.split_cnt_buf[1]_i_2 (\bus_wide_gen.len_cnt_reg__0 [3:0]),
        .\could_multi_bursts.sect_handling_reg (fifo_rctl_n_3),
        .\could_multi_bursts.sect_handling_reg_0 (fifo_rctl_n_6),
        .\could_multi_bursts.sect_handling_reg_1 (fifo_rctl_n_24),
        .\could_multi_bursts.sect_handling_reg_2 (\bus_wide_gen.fifo_burst_n_6 ),
        .\could_multi_bursts.sect_handling_reg_3 (\could_multi_bursts.sect_handling_reg_n_0 ),
        .empty_n_tmp_reg_0(fifo_rctl_n_1),
        .empty_n_tmp_reg_1(data_pack),
        .\end_addr_buf_reg[0] (fifo_rctl_n_18),
        .\end_addr_buf_reg[11] (fifo_rctl_n_16),
        .\end_addr_buf_reg[1] (fifo_rctl_n_17),
        .\end_addr_buf_reg[5] (fifo_rctl_n_10),
        .fifo_rctl_ready(fifo_rctl_ready),
        .fifo_rreq_valid(fifo_rreq_valid),
        .full_n0_in(full_n0_in),
        .invalid_len_event(invalid_len_event),
        .last_split(last_split),
        .m_axi_gmem0_ARREADY(m_axi_gmem0_ARREADY),
        .m_axi_gmem0_ARVALID(m_axi_gmem0_ARVALID),
        .\pout_reg[0]_0 (fifo_rdata_n_51),
        .push(push),
        .rreq_handling_reg(fifo_rctl_n_19),
        .rreq_handling_reg_0(fifo_rctl_n_20),
        .rreq_handling_reg_1(fifo_rctl_n_23),
        .rreq_handling_reg_2(rreq_handling_reg_n_0),
        .rreq_handling_reg_3(fifo_rreq_valid_buf_reg_n_0),
        .\sect_end_buf_reg[0] (\sect_end_buf_reg_n_0_[0] ),
        .\sect_end_buf_reg[1] (last_sect),
        .\sect_end_buf_reg[1]_0 (\sect_end_buf_reg_n_0_[1] ),
        .\sect_len_buf_reg[9] ({\end_addr_buf_reg_n_0_[11] ,\end_addr_buf_reg_n_0_[10] ,\end_addr_buf_reg_n_0_[9] ,\end_addr_buf_reg_n_0_[8] ,\end_addr_buf_reg_n_0_[7] ,\end_addr_buf_reg_n_0_[6] ,\end_addr_buf_reg_n_0_[5] ,\end_addr_buf_reg_n_0_[4] ,\end_addr_buf_reg_n_0_[3] ,\end_addr_buf_reg_n_0_[2] ,\end_addr_buf_reg_n_0_[1] ,\end_addr_buf_reg_n_0_[0] }),
        .\sect_len_buf_reg[9]_0 (beat_len_buf),
        .\start_addr_buf_reg[2] (fifo_rctl_n_7),
        .\start_addr_buf_reg[3] (fifo_rctl_n_8),
        .\start_addr_buf_reg[6] (fifo_rctl_n_11));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem0_m_axi_buffer__parameterized1 fifo_rdata
       (.D({fifo_rdata_n_16,fifo_rdata_n_17}),
        .DI(usedw15_out),
        .Q(usedw_reg),
        .S({fifo_rdata_n_3,fifo_rdata_n_4,fifo_rdata_n_5,fifo_rdata_n_6}),
        .SR(SR),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .beat_valid(beat_valid),
        .\bus_wide_gen.data_buf_reg[22] (\bus_wide_gen.fifo_burst_n_43 ),
        .\bus_wide_gen.data_buf_reg[22]_0 (\bus_wide_gen.fifo_burst_n_44 ),
        .\bus_wide_gen.data_buf_reg[23] ({\bus_wide_gen.data_buf_reg_n_0_[31] ,\bus_wide_gen.data_buf_reg_n_0_[30] }),
        .\bus_wide_gen.data_buf_reg[25] (\bus_wide_gen.rdata_valid_t_reg_n_0 ),
        .\bus_wide_gen.data_buf_reg[25]_0 (\bus_wide_gen.fifo_burst_n_45 ),
        .\bus_wide_gen.data_buf_reg[8] (COUNT),
        .\bus_wide_gen.data_buf_reg[8]_0 (\bus_wide_gen.fifo_burst_n_46 ),
        .\dout_buf_reg[16]_0 (fifo_rdata_n_18),
        .\dout_buf_reg[17]_0 (fifo_rdata_n_42),
        .\dout_buf_reg[18]_0 (fifo_rdata_n_43),
        .\dout_buf_reg[19]_0 (fifo_rdata_n_44),
        .\dout_buf_reg[20]_0 (fifo_rdata_n_45),
        .\dout_buf_reg[21]_0 (fifo_rdata_n_46),
        .\dout_buf_reg[22]_0 (fifo_rdata_n_47),
        .\dout_buf_reg[23]_0 (fifo_rdata_n_48),
        .\dout_buf_reg[24]_0 (fifo_rdata_n_59),
        .\dout_buf_reg[25]_0 (fifo_rdata_n_58),
        .\dout_buf_reg[26]_0 (fifo_rdata_n_57),
        .\dout_buf_reg[27]_0 (fifo_rdata_n_56),
        .\dout_buf_reg[28]_0 (fifo_rdata_n_55),
        .\dout_buf_reg[29]_0 (fifo_rdata_n_54),
        .\dout_buf_reg[30]_0 (fifo_rdata_n_53),
        .\dout_buf_reg[31]_0 (fifo_rdata_n_52),
        .\dout_buf_reg[34]_0 ({data_pack,fifo_rdata_n_20,fifo_rdata_n_21,fifo_rdata_n_22,fifo_rdata_n_23,fifo_rdata_n_24,fifo_rdata_n_25,fifo_rdata_n_26,fifo_rdata_n_27,fifo_rdata_n_28,fifo_rdata_n_29,fifo_rdata_n_30,fifo_rdata_n_31,fifo_rdata_n_32,fifo_rdata_n_33,fifo_rdata_n_34,fifo_rdata_n_35,fifo_rdata_n_36,fifo_rdata_n_37,fifo_rdata_n_38,fifo_rdata_n_39,fifo_rdata_n_40,fifo_rdata_n_41}),
        .dout_valid_reg_0(fifo_rdata_n_49),
        .dout_valid_reg_1(fifo_rdata_n_51),
        .full_n_reg_0(RREADY),
        .last_split(last_split),
        .m_axi_gmem0_RRESP(m_axi_gmem0_RRESP),
        .m_axi_gmem0_RVALID(m_axi_gmem0_RVALID),
        .mem_reg_0(mem_reg),
        .\pout_reg[0] (fifo_rctl_n_1),
        .s_ready(s_ready),
        .\usedw_reg[6]_0 ({fifo_rdata_n_13,fifo_rdata_n_14,fifo_rdata_n_15}),
        .\usedw_reg[7]_0 ({p_0_out_carry__0_n_5,p_0_out_carry__0_n_6,p_0_out_carry__0_n_7,p_0_out_carry_n_4,p_0_out_carry_n_5,p_0_out_carry_n_6,p_0_out_carry_n_7}));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem0_m_axi_fifo fifo_rreq
       (.D({fifo_rreq_n_4,fifo_rreq_n_5,fifo_rreq_n_6,fifo_rreq_n_7,fifo_rreq_n_8,fifo_rreq_n_9,fifo_rreq_n_10,fifo_rreq_n_11,fifo_rreq_n_12,fifo_rreq_n_13,fifo_rreq_n_14,fifo_rreq_n_15,fifo_rreq_n_16,fifo_rreq_n_17,fifo_rreq_n_18,fifo_rreq_n_19,fifo_rreq_n_20,fifo_rreq_n_21,fifo_rreq_n_22,fifo_rreq_n_23}),
        .E(fifo_rreq_n_2),
        .Q({\start_addr_reg_n_0_[31] ,\start_addr_reg_n_0_[30] ,\start_addr_reg_n_0_[29] ,\start_addr_reg_n_0_[28] ,\start_addr_reg_n_0_[27] ,\start_addr_reg_n_0_[26] ,\start_addr_reg_n_0_[25] ,\start_addr_reg_n_0_[24] ,\start_addr_reg_n_0_[23] ,\start_addr_reg_n_0_[22] ,\start_addr_reg_n_0_[21] ,\start_addr_reg_n_0_[20] ,\start_addr_reg_n_0_[19] ,\start_addr_reg_n_0_[18] ,\start_addr_reg_n_0_[17] ,\start_addr_reg_n_0_[16] ,\start_addr_reg_n_0_[15] ,\start_addr_reg_n_0_[14] ,\start_addr_reg_n_0_[13] ,\start_addr_reg_n_0_[12] }),
        .S({fifo_rreq_n_25,fifo_rreq_n_26,fifo_rreq_n_27,fifo_rreq_n_28}),
        .SR(SR),
        .\align_len_reg[31] (rreq_handling_reg_n_0),
        .\align_len_reg[31]_0 (fifo_rctl_n_3),
        .\align_len_reg[31]_1 (last_sect),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .empty_n_tmp_reg_0(align_len),
        .empty_n_tmp_reg_1(fifo_rreq_n_34),
        .\end_addr_buf_reg[31] ({fifo_rreq_n_29,fifo_rreq_n_30,fifo_rreq_n_31}),
        .fifo_rreq_valid(fifo_rreq_valid),
        .full_n_tmp_reg_0(rs2f_rreq_valid),
        .invalid_len_event(invalid_len_event),
        .invalid_len_event_reg(fifo_rreq_valid_buf_reg_n_0),
        .invalid_len_event_reg_0(fifo_rctl_n_20),
        .last_sect_carry__0({\sect_cnt_reg_n_0_[19] ,\sect_cnt_reg_n_0_[18] ,\sect_cnt_reg_n_0_[17] ,\sect_cnt_reg_n_0_[16] ,\sect_cnt_reg_n_0_[15] ,\sect_cnt_reg_n_0_[14] ,\sect_cnt_reg_n_0_[13] ,\sect_cnt_reg_n_0_[12] ,\sect_cnt_reg_n_0_[11] ,\sect_cnt_reg_n_0_[10] ,\sect_cnt_reg_n_0_[9] ,\sect_cnt_reg_n_0_[8] ,\sect_cnt_reg_n_0_[7] ,\sect_cnt_reg_n_0_[6] ,\sect_cnt_reg_n_0_[5] ,\sect_cnt_reg_n_0_[4] ,\sect_cnt_reg_n_0_[3] ,\sect_cnt_reg_n_0_[2] ,\sect_cnt_reg_n_0_[1] ,\sect_cnt_reg_n_0_[0] }),
        .last_sect_carry__0_0(p_0_in0_in),
        .minusOp(minusOp),
        .next_rreq(next_rreq),
        .plusOp_0(plusOp_0),
        .\pout_reg[2]_0 (fifo_rctl_n_19),
        .push(push_0),
        .\q_reg[31]_0 (q),
        .\q_reg[31]_1 (rs2f_rreq_data),
        .\q_reg[32]_0 (fifo_rreq_n_33),
        .rs2f_rreq_ack(rs2f_rreq_ack));
  FDRE fifo_rreq_valid_buf_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(fifo_rreq_n_34),
        .Q(fifo_rreq_valid_buf_reg_n_0),
        .R(SR));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 first_sect_carry
       (.CI(1'b0),
        .CO({first_sect_carry_n_0,first_sect_carry_n_1,first_sect_carry_n_2,first_sect_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_first_sect_carry_O_UNCONNECTED[3:0]),
        .S({first_sect_carry_i_1_n_0,first_sect_carry_i_2_n_0,first_sect_carry_i_3_n_0,first_sect_carry_i_4_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 first_sect_carry__0
       (.CI(first_sect_carry_n_0),
        .CO({NLW_first_sect_carry__0_CO_UNCONNECTED[3],first_sect,first_sect_carry__0_n_2,first_sect_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_first_sect_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,first_sect_carry__0_i_1_n_0,first_sect_carry__0_i_2_n_0,first_sect_carry__0_i_3_n_0}));
  LUT4 #(
    .INIT(16'h9009)) 
    first_sect_carry__0_i_1
       (.I0(\sect_cnt_reg_n_0_[19] ),
        .I1(p_0_in[19]),
        .I2(\sect_cnt_reg_n_0_[18] ),
        .I3(p_0_in[18]),
        .O(first_sect_carry__0_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    first_sect_carry__0_i_2
       (.I0(p_0_in[17]),
        .I1(\sect_cnt_reg_n_0_[17] ),
        .I2(p_0_in[15]),
        .I3(\sect_cnt_reg_n_0_[15] ),
        .I4(p_0_in[16]),
        .I5(\sect_cnt_reg_n_0_[16] ),
        .O(first_sect_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    first_sect_carry__0_i_3
       (.I0(p_0_in[13]),
        .I1(\sect_cnt_reg_n_0_[13] ),
        .I2(p_0_in[14]),
        .I3(\sect_cnt_reg_n_0_[14] ),
        .I4(\sect_cnt_reg_n_0_[12] ),
        .I5(p_0_in[12]),
        .O(first_sect_carry__0_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    first_sect_carry_i_1
       (.I0(p_0_in[10]),
        .I1(\sect_cnt_reg_n_0_[10] ),
        .I2(p_0_in[11]),
        .I3(\sect_cnt_reg_n_0_[11] ),
        .I4(\sect_cnt_reg_n_0_[9] ),
        .I5(p_0_in[9]),
        .O(first_sect_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    first_sect_carry_i_2
       (.I0(p_0_in[7]),
        .I1(\sect_cnt_reg_n_0_[7] ),
        .I2(p_0_in[8]),
        .I3(\sect_cnt_reg_n_0_[8] ),
        .I4(\sect_cnt_reg_n_0_[6] ),
        .I5(p_0_in[6]),
        .O(first_sect_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    first_sect_carry_i_3
       (.I0(p_0_in[5]),
        .I1(\sect_cnt_reg_n_0_[5] ),
        .I2(p_0_in[3]),
        .I3(\sect_cnt_reg_n_0_[3] ),
        .I4(p_0_in[4]),
        .I5(\sect_cnt_reg_n_0_[4] ),
        .O(first_sect_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    first_sect_carry_i_4
       (.I0(p_0_in[0]),
        .I1(\sect_cnt_reg_n_0_[0] ),
        .I2(p_0_in[1]),
        .I3(\sect_cnt_reg_n_0_[1] ),
        .I4(\sect_cnt_reg_n_0_[2] ),
        .I5(p_0_in[2]),
        .O(first_sect_carry_i_4_n_0));
  FDRE invalid_len_event_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(fifo_rreq_n_33),
        .Q(invalid_len_event),
        .R(SR));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 last_sect_carry
       (.CI(1'b0),
        .CO({last_sect_carry_n_0,last_sect_carry_n_1,last_sect_carry_n_2,last_sect_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_last_sect_carry_O_UNCONNECTED[3:0]),
        .S({fifo_rreq_n_25,fifo_rreq_n_26,fifo_rreq_n_27,fifo_rreq_n_28}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 last_sect_carry__0
       (.CI(last_sect_carry_n_0),
        .CO({NLW_last_sect_carry__0_CO_UNCONNECTED[3],last_sect,last_sect_carry__0_n_2,last_sect_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_last_sect_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,fifo_rreq_n_29,fifo_rreq_n_30,fifo_rreq_n_31}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_0_out_carry
       (.CI(1'b0),
        .CO({p_0_out_carry_n_0,p_0_out_carry_n_1,p_0_out_carry_n_2,p_0_out_carry_n_3}),
        .CYINIT(usedw_reg[0]),
        .DI({usedw_reg[3:1],usedw15_out}),
        .O({p_0_out_carry_n_4,p_0_out_carry_n_5,p_0_out_carry_n_6,p_0_out_carry_n_7}),
        .S({fifo_rdata_n_3,fifo_rdata_n_4,fifo_rdata_n_5,fifo_rdata_n_6}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_0_out_carry__0
       (.CI(p_0_out_carry_n_0),
        .CO({NLW_p_0_out_carry__0_CO_UNCONNECTED[3:2],p_0_out_carry__0_n_2,p_0_out_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,usedw_reg[5:4]}),
        .O({NLW_p_0_out_carry__0_O_UNCONNECTED[3],p_0_out_carry__0_n_5,p_0_out_carry__0_n_6,p_0_out_carry__0_n_7}),
        .S({1'b0,fifo_rdata_n_13,fifo_rdata_n_14,fifo_rdata_n_15}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry
       (.CI(1'b0),
        .CO({plusOp_carry_n_0,plusOp_carry_n_1,plusOp_carry_n_2,plusOp_carry_n_3}),
        .CYINIT(\sect_cnt_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(plusOp_0[4:1]),
        .S({\sect_cnt_reg_n_0_[4] ,\sect_cnt_reg_n_0_[3] ,\sect_cnt_reg_n_0_[2] ,\sect_cnt_reg_n_0_[1] }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry__0
       (.CI(plusOp_carry_n_0),
        .CO({plusOp_carry__0_n_0,plusOp_carry__0_n_1,plusOp_carry__0_n_2,plusOp_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(plusOp_0[8:5]),
        .S({\sect_cnt_reg_n_0_[8] ,\sect_cnt_reg_n_0_[7] ,\sect_cnt_reg_n_0_[6] ,\sect_cnt_reg_n_0_[5] }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry__1
       (.CI(plusOp_carry__0_n_0),
        .CO({plusOp_carry__1_n_0,plusOp_carry__1_n_1,plusOp_carry__1_n_2,plusOp_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(plusOp_0[12:9]),
        .S({\sect_cnt_reg_n_0_[12] ,\sect_cnt_reg_n_0_[11] ,\sect_cnt_reg_n_0_[10] ,\sect_cnt_reg_n_0_[9] }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry__2
       (.CI(plusOp_carry__1_n_0),
        .CO({plusOp_carry__2_n_0,plusOp_carry__2_n_1,plusOp_carry__2_n_2,plusOp_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(plusOp_0[16:13]),
        .S({\sect_cnt_reg_n_0_[16] ,\sect_cnt_reg_n_0_[15] ,\sect_cnt_reg_n_0_[14] ,\sect_cnt_reg_n_0_[13] }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry__3
       (.CI(plusOp_carry__2_n_0),
        .CO({NLW_plusOp_carry__3_CO_UNCONNECTED[3:2],plusOp_carry__3_n_2,plusOp_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_plusOp_carry__3_O_UNCONNECTED[3],plusOp_0[19:17]}),
        .S({1'b0,\sect_cnt_reg_n_0_[19] ,\sect_cnt_reg_n_0_[18] ,\sect_cnt_reg_n_0_[17] }));
  FDRE rreq_handling_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(fifo_rctl_n_23),
        .Q(rreq_handling_reg_n_0),
        .R(SR));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem0_m_axi_reg_slice__parameterized2 rs_rdata
       (.D(D[1]),
        .I_AWVALID(I_AWVALID),
        .Q(Q[2:1]),
        .SR(SR),
        .ap_clk(ap_clk),
        .ap_reg_ioackin_gmem1_AWREADY(ap_reg_ioackin_gmem1_AWREADY),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_0(ap_rst_n_0),
        .\bus_wide_gen.rdata_valid_t_reg (rs_rdata_n_5),
        .\data_p1_reg[7]_0 (\data_p1_reg[7] ),
        .\data_p2_reg[7]_0 (s_data),
        .gmem1_AWREADY(gmem1_AWREADY),
        .s_ready(s_ready),
        .s_ready_t_reg_0(\bus_wide_gen.rdata_valid_t_reg_n_0 ),
        .\state_reg[0]_0 (rdata_valid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem0_m_axi_reg_slice rs_rreq
       (.E(D[0]),
        .Q(Q[0]),
        .SR(SR),
        .ap_clk(ap_clk),
        .\data_p1_reg[31]_0 (rs2f_rreq_data),
        .\data_p2_reg[31]_0 (\data_p2_reg[31] ),
        .push(push_0),
        .rs2f_rreq_ack(rs2f_rreq_ack),
        .s_ready_t_reg_0(s_ready_t_reg),
        .\state_reg[0]_0 (rs2f_rreq_valid));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[0]_i_1 
       (.I0(first_sect),
        .I1(\start_addr_buf_reg_n_0_[0] ),
        .O(sect_addr[0]));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[10]_i_1 
       (.I0(first_sect),
        .I1(\start_addr_buf_reg_n_0_[10] ),
        .O(sect_addr[10]));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[11]_i_2 
       (.I0(first_sect),
        .I1(\start_addr_buf_reg_n_0_[11] ),
        .O(sect_addr[11]));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[12]_i_1 
       (.I0(p_0_in[0]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[0] ),
        .O(sect_addr[12]));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[13]_i_1 
       (.I0(p_0_in[1]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[1] ),
        .O(sect_addr[13]));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[14]_i_1 
       (.I0(p_0_in[2]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[2] ),
        .O(sect_addr[14]));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[15]_i_1 
       (.I0(p_0_in[3]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[3] ),
        .O(sect_addr[15]));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[16]_i_1 
       (.I0(p_0_in[4]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[4] ),
        .O(sect_addr[16]));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[17]_i_1 
       (.I0(p_0_in[5]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[5] ),
        .O(sect_addr[17]));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[18]_i_1 
       (.I0(p_0_in[6]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[6] ),
        .O(sect_addr[18]));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[19]_i_1 
       (.I0(p_0_in[7]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[7] ),
        .O(sect_addr[19]));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[1]_i_1 
       (.I0(first_sect),
        .I1(\start_addr_buf_reg_n_0_[1] ),
        .O(sect_addr[1]));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[20]_i_1 
       (.I0(p_0_in[8]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[8] ),
        .O(sect_addr[20]));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[21]_i_1 
       (.I0(p_0_in[9]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[9] ),
        .O(sect_addr[21]));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[22]_i_1 
       (.I0(p_0_in[10]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[10] ),
        .O(sect_addr[22]));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[23]_i_1 
       (.I0(p_0_in[11]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[11] ),
        .O(sect_addr[23]));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[24]_i_1 
       (.I0(p_0_in[12]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[12] ),
        .O(sect_addr[24]));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[25]_i_1 
       (.I0(p_0_in[13]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[13] ),
        .O(sect_addr[25]));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[26]_i_1 
       (.I0(p_0_in[14]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[14] ),
        .O(sect_addr[26]));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[27]_i_1 
       (.I0(p_0_in[15]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[15] ),
        .O(sect_addr[27]));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[28]_i_1 
       (.I0(p_0_in[16]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[16] ),
        .O(sect_addr[28]));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[29]_i_1 
       (.I0(p_0_in[17]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[17] ),
        .O(sect_addr[29]));
  (* SOFT_HLUTNM = "soft_lutpair131" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[2]_i_1 
       (.I0(first_sect),
        .I1(\start_addr_buf_reg_n_0_[2] ),
        .O(sect_addr[2]));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[30]_i_1 
       (.I0(p_0_in[18]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[18] ),
        .O(sect_addr[30]));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[31]_i_2 
       (.I0(p_0_in[19]),
        .I1(first_sect),
        .I2(\sect_cnt_reg_n_0_[19] ),
        .O(sect_addr[31]));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[3]_i_1 
       (.I0(first_sect),
        .I1(\start_addr_buf_reg_n_0_[3] ),
        .O(sect_addr[3]));
  (* SOFT_HLUTNM = "soft_lutpair133" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[4]_i_1 
       (.I0(first_sect),
        .I1(\start_addr_buf_reg_n_0_[4] ),
        .O(sect_addr[4]));
  (* SOFT_HLUTNM = "soft_lutpair132" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[5]_i_1 
       (.I0(first_sect),
        .I1(\start_addr_buf_reg_n_0_[5] ),
        .O(sect_addr[5]));
  (* SOFT_HLUTNM = "soft_lutpair133" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[6]_i_1 
       (.I0(first_sect),
        .I1(\start_addr_buf_reg_n_0_[6] ),
        .O(sect_addr[6]));
  (* SOFT_HLUTNM = "soft_lutpair132" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[7]_i_1 
       (.I0(first_sect),
        .I1(\start_addr_buf_reg_n_0_[7] ),
        .O(sect_addr[7]));
  (* SOFT_HLUTNM = "soft_lutpair131" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[8]_i_1 
       (.I0(first_sect),
        .I1(\start_addr_buf_reg_n_0_[8] ),
        .O(sect_addr[8]));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[9]_i_1 
       (.I0(first_sect),
        .I1(\start_addr_buf_reg_n_0_[9] ),
        .O(sect_addr[9]));
  FDRE \sect_addr_buf_reg[0] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[0]),
        .Q(\sect_addr_buf_reg_n_0_[0] ),
        .R(fifo_rctl_n_4));
  FDRE \sect_addr_buf_reg[10] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[10]),
        .Q(\sect_addr_buf_reg_n_0_[10] ),
        .R(fifo_rctl_n_4));
  FDRE \sect_addr_buf_reg[11] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[11]),
        .Q(\sect_addr_buf_reg_n_0_[11] ),
        .R(fifo_rctl_n_4));
  FDRE \sect_addr_buf_reg[12] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[12]),
        .Q(\sect_addr_buf_reg_n_0_[12] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[13] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[13]),
        .Q(\sect_addr_buf_reg_n_0_[13] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[14] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[14]),
        .Q(\sect_addr_buf_reg_n_0_[14] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[15] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[15]),
        .Q(\sect_addr_buf_reg_n_0_[15] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[16] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[16]),
        .Q(\sect_addr_buf_reg_n_0_[16] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[17] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[17]),
        .Q(\sect_addr_buf_reg_n_0_[17] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[18] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[18]),
        .Q(\sect_addr_buf_reg_n_0_[18] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[19] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[19]),
        .Q(\sect_addr_buf_reg_n_0_[19] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[1] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[1]),
        .Q(\sect_addr_buf_reg_n_0_[1] ),
        .R(fifo_rctl_n_4));
  FDRE \sect_addr_buf_reg[20] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[20]),
        .Q(\sect_addr_buf_reg_n_0_[20] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[21] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[21]),
        .Q(\sect_addr_buf_reg_n_0_[21] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[22] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[22]),
        .Q(\sect_addr_buf_reg_n_0_[22] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[23] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[23]),
        .Q(\sect_addr_buf_reg_n_0_[23] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[24] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[24]),
        .Q(\sect_addr_buf_reg_n_0_[24] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[25] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[25]),
        .Q(\sect_addr_buf_reg_n_0_[25] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[26] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[26]),
        .Q(\sect_addr_buf_reg_n_0_[26] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[27] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[27]),
        .Q(\sect_addr_buf_reg_n_0_[27] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[28] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[28]),
        .Q(\sect_addr_buf_reg_n_0_[28] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[29] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[29]),
        .Q(\sect_addr_buf_reg_n_0_[29] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[2] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[2]),
        .Q(\sect_addr_buf_reg_n_0_[2] ),
        .R(fifo_rctl_n_4));
  FDRE \sect_addr_buf_reg[30] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[30]),
        .Q(\sect_addr_buf_reg_n_0_[30] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[31] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[31]),
        .Q(\sect_addr_buf_reg_n_0_[31] ),
        .R(SR));
  FDRE \sect_addr_buf_reg[3] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[3]),
        .Q(\sect_addr_buf_reg_n_0_[3] ),
        .R(fifo_rctl_n_4));
  FDRE \sect_addr_buf_reg[4] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[4]),
        .Q(\sect_addr_buf_reg_n_0_[4] ),
        .R(fifo_rctl_n_4));
  FDRE \sect_addr_buf_reg[5] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[5]),
        .Q(\sect_addr_buf_reg_n_0_[5] ),
        .R(fifo_rctl_n_4));
  FDRE \sect_addr_buf_reg[6] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[6]),
        .Q(\sect_addr_buf_reg_n_0_[6] ),
        .R(fifo_rctl_n_4));
  FDRE \sect_addr_buf_reg[7] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[7]),
        .Q(\sect_addr_buf_reg_n_0_[7] ),
        .R(fifo_rctl_n_4));
  FDRE \sect_addr_buf_reg[8] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[8]),
        .Q(\sect_addr_buf_reg_n_0_[8] ),
        .R(fifo_rctl_n_4));
  FDRE \sect_addr_buf_reg[9] 
       (.C(ap_clk),
        .CE(p_27_in),
        .D(sect_addr[9]),
        .Q(\sect_addr_buf_reg_n_0_[9] ),
        .R(fifo_rctl_n_4));
  FDRE \sect_cnt_reg[0] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_23),
        .Q(\sect_cnt_reg_n_0_[0] ),
        .R(SR));
  FDRE \sect_cnt_reg[10] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_13),
        .Q(\sect_cnt_reg_n_0_[10] ),
        .R(SR));
  FDRE \sect_cnt_reg[11] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_12),
        .Q(\sect_cnt_reg_n_0_[11] ),
        .R(SR));
  FDRE \sect_cnt_reg[12] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_11),
        .Q(\sect_cnt_reg_n_0_[12] ),
        .R(SR));
  FDRE \sect_cnt_reg[13] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_10),
        .Q(\sect_cnt_reg_n_0_[13] ),
        .R(SR));
  FDRE \sect_cnt_reg[14] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_9),
        .Q(\sect_cnt_reg_n_0_[14] ),
        .R(SR));
  FDRE \sect_cnt_reg[15] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_8),
        .Q(\sect_cnt_reg_n_0_[15] ),
        .R(SR));
  FDRE \sect_cnt_reg[16] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_7),
        .Q(\sect_cnt_reg_n_0_[16] ),
        .R(SR));
  FDRE \sect_cnt_reg[17] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_6),
        .Q(\sect_cnt_reg_n_0_[17] ),
        .R(SR));
  FDRE \sect_cnt_reg[18] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_5),
        .Q(\sect_cnt_reg_n_0_[18] ),
        .R(SR));
  FDRE \sect_cnt_reg[19] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_4),
        .Q(\sect_cnt_reg_n_0_[19] ),
        .R(SR));
  FDRE \sect_cnt_reg[1] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_22),
        .Q(\sect_cnt_reg_n_0_[1] ),
        .R(SR));
  FDRE \sect_cnt_reg[2] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_21),
        .Q(\sect_cnt_reg_n_0_[2] ),
        .R(SR));
  FDRE \sect_cnt_reg[3] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_20),
        .Q(\sect_cnt_reg_n_0_[3] ),
        .R(SR));
  FDRE \sect_cnt_reg[4] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_19),
        .Q(\sect_cnt_reg_n_0_[4] ),
        .R(SR));
  FDRE \sect_cnt_reg[5] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_18),
        .Q(\sect_cnt_reg_n_0_[5] ),
        .R(SR));
  FDRE \sect_cnt_reg[6] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_17),
        .Q(\sect_cnt_reg_n_0_[6] ),
        .R(SR));
  FDRE \sect_cnt_reg[7] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_16),
        .Q(\sect_cnt_reg_n_0_[7] ),
        .R(SR));
  FDRE \sect_cnt_reg[8] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_15),
        .Q(\sect_cnt_reg_n_0_[8] ),
        .R(SR));
  FDRE \sect_cnt_reg[9] 
       (.C(ap_clk),
        .CE(fifo_rreq_n_2),
        .D(fifo_rreq_n_14),
        .Q(\sect_cnt_reg_n_0_[9] ),
        .R(SR));
  FDRE \sect_end_buf_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(fifo_rctl_n_18),
        .Q(\sect_end_buf_reg_n_0_[0] ),
        .R(SR));
  FDRE \sect_end_buf_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(fifo_rctl_n_17),
        .Q(\sect_end_buf_reg_n_0_[1] ),
        .R(SR));
  FDRE \sect_len_buf_reg[0] 
       (.C(ap_clk),
        .CE(fifo_rctl_n_6),
        .D(fifo_rctl_n_7),
        .Q(\sect_len_buf_reg_n_0_[0] ),
        .R(SR));
  FDRE \sect_len_buf_reg[1] 
       (.C(ap_clk),
        .CE(fifo_rctl_n_6),
        .D(fifo_rctl_n_8),
        .Q(\sect_len_buf_reg_n_0_[1] ),
        .R(SR));
  FDRE \sect_len_buf_reg[2] 
       (.C(ap_clk),
        .CE(fifo_rctl_n_6),
        .D(fifo_rctl_n_9),
        .Q(\sect_len_buf_reg_n_0_[2] ),
        .R(SR));
  FDRE \sect_len_buf_reg[3] 
       (.C(ap_clk),
        .CE(fifo_rctl_n_6),
        .D(fifo_rctl_n_10),
        .Q(\sect_len_buf_reg_n_0_[3] ),
        .R(SR));
  FDRE \sect_len_buf_reg[4] 
       (.C(ap_clk),
        .CE(fifo_rctl_n_6),
        .D(fifo_rctl_n_11),
        .Q(\sect_len_buf_reg_n_0_[4] ),
        .R(SR));
  FDRE \sect_len_buf_reg[5] 
       (.C(ap_clk),
        .CE(fifo_rctl_n_6),
        .D(fifo_rctl_n_12),
        .Q(\sect_len_buf_reg_n_0_[5] ),
        .R(SR));
  FDRE \sect_len_buf_reg[6] 
       (.C(ap_clk),
        .CE(fifo_rctl_n_6),
        .D(fifo_rctl_n_13),
        .Q(\sect_len_buf_reg_n_0_[6] ),
        .R(SR));
  FDRE \sect_len_buf_reg[7] 
       (.C(ap_clk),
        .CE(fifo_rctl_n_6),
        .D(fifo_rctl_n_14),
        .Q(\sect_len_buf_reg_n_0_[7] ),
        .R(SR));
  FDRE \sect_len_buf_reg[8] 
       (.C(ap_clk),
        .CE(fifo_rctl_n_6),
        .D(fifo_rctl_n_15),
        .Q(\sect_len_buf_reg_n_0_[8] ),
        .R(SR));
  FDRE \sect_len_buf_reg[9] 
       (.C(ap_clk),
        .CE(fifo_rctl_n_6),
        .D(fifo_rctl_n_16),
        .Q(\sect_len_buf_reg_n_0_[9] ),
        .R(SR));
  FDRE \start_addr_buf_reg[0] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[0] ),
        .Q(\start_addr_buf_reg_n_0_[0] ),
        .R(SR));
  FDRE \start_addr_buf_reg[10] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[10] ),
        .Q(\start_addr_buf_reg_n_0_[10] ),
        .R(SR));
  FDRE \start_addr_buf_reg[11] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[11] ),
        .Q(\start_addr_buf_reg_n_0_[11] ),
        .R(SR));
  FDRE \start_addr_buf_reg[12] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[12] ),
        .Q(p_0_in[0]),
        .R(SR));
  FDRE \start_addr_buf_reg[13] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[13] ),
        .Q(p_0_in[1]),
        .R(SR));
  FDRE \start_addr_buf_reg[14] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[14] ),
        .Q(p_0_in[2]),
        .R(SR));
  FDRE \start_addr_buf_reg[15] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[15] ),
        .Q(p_0_in[3]),
        .R(SR));
  FDRE \start_addr_buf_reg[16] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[16] ),
        .Q(p_0_in[4]),
        .R(SR));
  FDRE \start_addr_buf_reg[17] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[17] ),
        .Q(p_0_in[5]),
        .R(SR));
  FDRE \start_addr_buf_reg[18] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[18] ),
        .Q(p_0_in[6]),
        .R(SR));
  FDRE \start_addr_buf_reg[19] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[19] ),
        .Q(p_0_in[7]),
        .R(SR));
  FDRE \start_addr_buf_reg[1] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[1] ),
        .Q(\start_addr_buf_reg_n_0_[1] ),
        .R(SR));
  FDRE \start_addr_buf_reg[20] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[20] ),
        .Q(p_0_in[8]),
        .R(SR));
  FDRE \start_addr_buf_reg[21] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[21] ),
        .Q(p_0_in[9]),
        .R(SR));
  FDRE \start_addr_buf_reg[22] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[22] ),
        .Q(p_0_in[10]),
        .R(SR));
  FDRE \start_addr_buf_reg[23] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[23] ),
        .Q(p_0_in[11]),
        .R(SR));
  FDRE \start_addr_buf_reg[24] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[24] ),
        .Q(p_0_in[12]),
        .R(SR));
  FDRE \start_addr_buf_reg[25] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[25] ),
        .Q(p_0_in[13]),
        .R(SR));
  FDRE \start_addr_buf_reg[26] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[26] ),
        .Q(p_0_in[14]),
        .R(SR));
  FDRE \start_addr_buf_reg[27] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[27] ),
        .Q(p_0_in[15]),
        .R(SR));
  FDRE \start_addr_buf_reg[28] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[28] ),
        .Q(p_0_in[16]),
        .R(SR));
  FDRE \start_addr_buf_reg[29] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[29] ),
        .Q(p_0_in[17]),
        .R(SR));
  FDRE \start_addr_buf_reg[2] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[2] ),
        .Q(\start_addr_buf_reg_n_0_[2] ),
        .R(SR));
  FDRE \start_addr_buf_reg[30] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[30] ),
        .Q(p_0_in[18]),
        .R(SR));
  FDRE \start_addr_buf_reg[31] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[31] ),
        .Q(p_0_in[19]),
        .R(SR));
  FDRE \start_addr_buf_reg[3] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[3] ),
        .Q(\start_addr_buf_reg_n_0_[3] ),
        .R(SR));
  FDRE \start_addr_buf_reg[4] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[4] ),
        .Q(\start_addr_buf_reg_n_0_[4] ),
        .R(SR));
  FDRE \start_addr_buf_reg[5] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[5] ),
        .Q(\start_addr_buf_reg_n_0_[5] ),
        .R(SR));
  FDRE \start_addr_buf_reg[6] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[6] ),
        .Q(\start_addr_buf_reg_n_0_[6] ),
        .R(SR));
  FDRE \start_addr_buf_reg[7] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[7] ),
        .Q(\start_addr_buf_reg_n_0_[7] ),
        .R(SR));
  FDRE \start_addr_buf_reg[8] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[8] ),
        .Q(\start_addr_buf_reg_n_0_[8] ),
        .R(SR));
  FDRE \start_addr_buf_reg[9] 
       (.C(ap_clk),
        .CE(next_rreq),
        .D(\start_addr_reg_n_0_[9] ),
        .Q(\start_addr_buf_reg_n_0_[9] ),
        .R(SR));
  FDRE \start_addr_reg[0] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[0]),
        .Q(\start_addr_reg_n_0_[0] ),
        .R(SR));
  FDRE \start_addr_reg[10] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[10]),
        .Q(\start_addr_reg_n_0_[10] ),
        .R(SR));
  FDRE \start_addr_reg[11] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[11]),
        .Q(\start_addr_reg_n_0_[11] ),
        .R(SR));
  FDRE \start_addr_reg[12] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[12]),
        .Q(\start_addr_reg_n_0_[12] ),
        .R(SR));
  FDRE \start_addr_reg[13] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[13]),
        .Q(\start_addr_reg_n_0_[13] ),
        .R(SR));
  FDRE \start_addr_reg[14] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[14]),
        .Q(\start_addr_reg_n_0_[14] ),
        .R(SR));
  FDRE \start_addr_reg[15] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[15]),
        .Q(\start_addr_reg_n_0_[15] ),
        .R(SR));
  FDRE \start_addr_reg[16] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[16]),
        .Q(\start_addr_reg_n_0_[16] ),
        .R(SR));
  FDRE \start_addr_reg[17] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[17]),
        .Q(\start_addr_reg_n_0_[17] ),
        .R(SR));
  FDRE \start_addr_reg[18] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[18]),
        .Q(\start_addr_reg_n_0_[18] ),
        .R(SR));
  FDRE \start_addr_reg[19] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[19]),
        .Q(\start_addr_reg_n_0_[19] ),
        .R(SR));
  FDRE \start_addr_reg[1] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[1]),
        .Q(\start_addr_reg_n_0_[1] ),
        .R(SR));
  FDRE \start_addr_reg[20] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[20]),
        .Q(\start_addr_reg_n_0_[20] ),
        .R(SR));
  FDRE \start_addr_reg[21] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[21]),
        .Q(\start_addr_reg_n_0_[21] ),
        .R(SR));
  FDRE \start_addr_reg[22] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[22]),
        .Q(\start_addr_reg_n_0_[22] ),
        .R(SR));
  FDRE \start_addr_reg[23] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[23]),
        .Q(\start_addr_reg_n_0_[23] ),
        .R(SR));
  FDRE \start_addr_reg[24] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[24]),
        .Q(\start_addr_reg_n_0_[24] ),
        .R(SR));
  FDRE \start_addr_reg[25] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[25]),
        .Q(\start_addr_reg_n_0_[25] ),
        .R(SR));
  FDRE \start_addr_reg[26] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[26]),
        .Q(\start_addr_reg_n_0_[26] ),
        .R(SR));
  FDRE \start_addr_reg[27] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[27]),
        .Q(\start_addr_reg_n_0_[27] ),
        .R(SR));
  FDRE \start_addr_reg[28] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[28]),
        .Q(\start_addr_reg_n_0_[28] ),
        .R(SR));
  FDRE \start_addr_reg[29] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[29]),
        .Q(\start_addr_reg_n_0_[29] ),
        .R(SR));
  FDRE \start_addr_reg[2] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[2]),
        .Q(\start_addr_reg_n_0_[2] ),
        .R(SR));
  FDRE \start_addr_reg[30] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[30]),
        .Q(\start_addr_reg_n_0_[30] ),
        .R(SR));
  FDRE \start_addr_reg[31] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[31]),
        .Q(\start_addr_reg_n_0_[31] ),
        .R(SR));
  FDRE \start_addr_reg[3] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[3]),
        .Q(\start_addr_reg_n_0_[3] ),
        .R(SR));
  FDRE \start_addr_reg[4] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[4]),
        .Q(\start_addr_reg_n_0_[4] ),
        .R(SR));
  FDRE \start_addr_reg[5] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[5]),
        .Q(\start_addr_reg_n_0_[5] ),
        .R(SR));
  FDRE \start_addr_reg[6] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[6]),
        .Q(\start_addr_reg_n_0_[6] ),
        .R(SR));
  FDRE \start_addr_reg[7] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[7]),
        .Q(\start_addr_reg_n_0_[7] ),
        .R(SR));
  FDRE \start_addr_reg[8] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[8]),
        .Q(\start_addr_reg_n_0_[8] ),
        .R(SR));
  FDRE \start_addr_reg[9] 
       (.C(ap_clk),
        .CE(align_len),
        .D(q[9]),
        .Q(\start_addr_reg_n_0_[9] ),
        .R(SR));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem0_m_axi_reg_slice
   (s_ready_t_reg_0,
    E,
    \state_reg[0]_0 ,
    push,
    \data_p1_reg[31]_0 ,
    SR,
    ap_clk,
    Q,
    rs2f_rreq_ack,
    \data_p2_reg[31]_0 );
  output s_ready_t_reg_0;
  output [0:0]E;
  output [0:0]\state_reg[0]_0 ;
  output push;
  output [31:0]\data_p1_reg[31]_0 ;
  input [0:0]SR;
  input ap_clk;
  input [0:0]Q;
  input rs2f_rreq_ack;
  input [31:0]\data_p2_reg[31]_0 ;

  wire [0:0]E;
  wire [0:0]Q;
  wire [0:0]SR;
  wire ap_clk;
  wire \data_p1[0]_i_1_n_0 ;
  wire \data_p1[10]_i_1_n_0 ;
  wire \data_p1[11]_i_1_n_0 ;
  wire \data_p1[12]_i_1_n_0 ;
  wire \data_p1[13]_i_1_n_0 ;
  wire \data_p1[14]_i_1_n_0 ;
  wire \data_p1[15]_i_1_n_0 ;
  wire \data_p1[16]_i_1_n_0 ;
  wire \data_p1[17]_i_1_n_0 ;
  wire \data_p1[18]_i_1_n_0 ;
  wire \data_p1[19]_i_1_n_0 ;
  wire \data_p1[1]_i_1_n_0 ;
  wire \data_p1[20]_i_1_n_0 ;
  wire \data_p1[21]_i_1_n_0 ;
  wire \data_p1[22]_i_1_n_0 ;
  wire \data_p1[23]_i_1_n_0 ;
  wire \data_p1[24]_i_1_n_0 ;
  wire \data_p1[25]_i_1_n_0 ;
  wire \data_p1[26]_i_1_n_0 ;
  wire \data_p1[27]_i_1_n_0 ;
  wire \data_p1[28]_i_1_n_0 ;
  wire \data_p1[29]_i_1_n_0 ;
  wire \data_p1[2]_i_1_n_0 ;
  wire \data_p1[30]_i_1_n_0 ;
  wire \data_p1[31]_i_2_n_0 ;
  wire \data_p1[3]_i_1_n_0 ;
  wire \data_p1[4]_i_1_n_0 ;
  wire \data_p1[5]_i_1_n_0 ;
  wire \data_p1[6]_i_1_n_0 ;
  wire \data_p1[7]_i_1_n_0 ;
  wire \data_p1[8]_i_1_n_0 ;
  wire \data_p1[9]_i_1_n_0 ;
  wire [31:0]\data_p1_reg[31]_0 ;
  wire [31:0]data_p2;
  wire [31:0]\data_p2_reg[31]_0 ;
  wire load_p1;
  wire [1:0]next_st__0;
  wire push;
  wire rs2f_rreq_ack;
  wire s_ready_t_i_1_n_0;
  wire s_ready_t_reg_0;
  wire [1:1]state;
  wire \state[0]_i_1_n_0 ;
  wire \state[1]_i_1_n_0 ;
  wire [1:0]state__0;
  wire [0:0]\state_reg[0]_0 ;

  LUT4 #(
    .INIT(16'h0602)) 
    \FSM_sequential_state[0]_i_1 
       (.I0(state__0[0]),
        .I1(state__0[1]),
        .I2(rs2f_rreq_ack),
        .I3(Q),
        .O(next_st__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT5 #(
    .INIT(32'h3E020C30)) 
    \FSM_sequential_state[1]_i_1 
       (.I0(s_ready_t_reg_0),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(rs2f_rreq_ack),
        .I4(Q),
        .O(next_st__0[1]));
  (* FSM_ENCODED_STATES = "zero:00,two:01,one:10" *) 
  FDRE \FSM_sequential_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(next_st__0[0]),
        .Q(state__0[0]),
        .R(SR));
  (* FSM_ENCODED_STATES = "zero:00,two:01,one:10" *) 
  FDRE \FSM_sequential_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(next_st__0[1]),
        .Q(state__0[1]),
        .R(SR));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[0]_i_1 
       (.I0(data_p2[0]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [0]),
        .O(\data_p1[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[10]_i_1 
       (.I0(data_p2[10]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [10]),
        .O(\data_p1[10]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[11]_i_1 
       (.I0(data_p2[11]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [11]),
        .O(\data_p1[11]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[12]_i_1 
       (.I0(data_p2[12]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [12]),
        .O(\data_p1[12]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[13]_i_1 
       (.I0(data_p2[13]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [13]),
        .O(\data_p1[13]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[14]_i_1 
       (.I0(data_p2[14]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [14]),
        .O(\data_p1[14]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[15]_i_1 
       (.I0(data_p2[15]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [15]),
        .O(\data_p1[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[16]_i_1 
       (.I0(data_p2[16]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [16]),
        .O(\data_p1[16]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[17]_i_1 
       (.I0(data_p2[17]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [17]),
        .O(\data_p1[17]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[18]_i_1 
       (.I0(data_p2[18]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [18]),
        .O(\data_p1[18]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[19]_i_1 
       (.I0(data_p2[19]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [19]),
        .O(\data_p1[19]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[1]_i_1 
       (.I0(data_p2[1]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [1]),
        .O(\data_p1[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[20]_i_1 
       (.I0(data_p2[20]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [20]),
        .O(\data_p1[20]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[21]_i_1 
       (.I0(data_p2[21]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [21]),
        .O(\data_p1[21]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[22]_i_1 
       (.I0(data_p2[22]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [22]),
        .O(\data_p1[22]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[23]_i_1 
       (.I0(data_p2[23]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [23]),
        .O(\data_p1[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[24]_i_1 
       (.I0(data_p2[24]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [24]),
        .O(\data_p1[24]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[25]_i_1 
       (.I0(data_p2[25]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [25]),
        .O(\data_p1[25]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[26]_i_1 
       (.I0(data_p2[26]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [26]),
        .O(\data_p1[26]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[27]_i_1 
       (.I0(data_p2[27]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [27]),
        .O(\data_p1[27]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[28]_i_1 
       (.I0(data_p2[28]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [28]),
        .O(\data_p1[28]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[29]_i_1 
       (.I0(data_p2[29]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [29]),
        .O(\data_p1[29]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[2]_i_1 
       (.I0(data_p2[2]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [2]),
        .O(\data_p1[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[30]_i_1 
       (.I0(data_p2[30]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [30]),
        .O(\data_p1[30]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h4E04)) 
    \data_p1[31]_i_1 
       (.I0(state__0[0]),
        .I1(Q),
        .I2(state__0[1]),
        .I3(rs2f_rreq_ack),
        .O(load_p1));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[31]_i_2 
       (.I0(data_p2[31]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [31]),
        .O(\data_p1[31]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[3]_i_1 
       (.I0(data_p2[3]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [3]),
        .O(\data_p1[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[4]_i_1 
       (.I0(data_p2[4]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [4]),
        .O(\data_p1[4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[5]_i_1 
       (.I0(data_p2[5]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [5]),
        .O(\data_p1[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[6]_i_1 
       (.I0(data_p2[6]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [6]),
        .O(\data_p1[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[7]_i_1 
       (.I0(data_p2[7]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [7]),
        .O(\data_p1[7]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[8]_i_1 
       (.I0(data_p2[8]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [8]),
        .O(\data_p1[8]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[9]_i_1 
       (.I0(data_p2[9]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[31]_0 [9]),
        .O(\data_p1[9]_i_1_n_0 ));
  FDRE \data_p1_reg[0] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[0]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [0]),
        .R(1'b0));
  FDRE \data_p1_reg[10] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[10]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [10]),
        .R(1'b0));
  FDRE \data_p1_reg[11] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[11]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [11]),
        .R(1'b0));
  FDRE \data_p1_reg[12] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[12]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [12]),
        .R(1'b0));
  FDRE \data_p1_reg[13] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[13]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [13]),
        .R(1'b0));
  FDRE \data_p1_reg[14] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[14]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [14]),
        .R(1'b0));
  FDRE \data_p1_reg[15] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[15]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [15]),
        .R(1'b0));
  FDRE \data_p1_reg[16] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[16]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [16]),
        .R(1'b0));
  FDRE \data_p1_reg[17] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[17]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [17]),
        .R(1'b0));
  FDRE \data_p1_reg[18] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[18]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [18]),
        .R(1'b0));
  FDRE \data_p1_reg[19] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[19]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [19]),
        .R(1'b0));
  FDRE \data_p1_reg[1] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[1]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [1]),
        .R(1'b0));
  FDRE \data_p1_reg[20] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[20]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [20]),
        .R(1'b0));
  FDRE \data_p1_reg[21] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[21]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [21]),
        .R(1'b0));
  FDRE \data_p1_reg[22] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[22]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [22]),
        .R(1'b0));
  FDRE \data_p1_reg[23] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[23]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [23]),
        .R(1'b0));
  FDRE \data_p1_reg[24] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[24]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [24]),
        .R(1'b0));
  FDRE \data_p1_reg[25] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[25]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [25]),
        .R(1'b0));
  FDRE \data_p1_reg[26] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[26]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [26]),
        .R(1'b0));
  FDRE \data_p1_reg[27] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[27]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [27]),
        .R(1'b0));
  FDRE \data_p1_reg[28] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[28]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [28]),
        .R(1'b0));
  FDRE \data_p1_reg[29] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[29]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [29]),
        .R(1'b0));
  FDRE \data_p1_reg[2] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[2]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [2]),
        .R(1'b0));
  FDRE \data_p1_reg[30] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[30]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [30]),
        .R(1'b0));
  FDRE \data_p1_reg[31] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[31]_i_2_n_0 ),
        .Q(\data_p1_reg[31]_0 [31]),
        .R(1'b0));
  FDRE \data_p1_reg[3] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[3]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [3]),
        .R(1'b0));
  FDRE \data_p1_reg[4] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[4]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [4]),
        .R(1'b0));
  FDRE \data_p1_reg[5] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[5]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [5]),
        .R(1'b0));
  FDRE \data_p1_reg[6] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[6]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [6]),
        .R(1'b0));
  FDRE \data_p1_reg[7] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[7]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [7]),
        .R(1'b0));
  FDRE \data_p1_reg[8] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[8]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [8]),
        .R(1'b0));
  FDRE \data_p1_reg[9] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[9]_i_1_n_0 ),
        .Q(\data_p1_reg[31]_0 [9]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    \data_p2[31]_i_1 
       (.I0(Q),
        .I1(s_ready_t_reg_0),
        .O(E));
  FDRE \data_p2_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [0]),
        .Q(data_p2[0]),
        .R(1'b0));
  FDRE \data_p2_reg[10] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [10]),
        .Q(data_p2[10]),
        .R(1'b0));
  FDRE \data_p2_reg[11] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [11]),
        .Q(data_p2[11]),
        .R(1'b0));
  FDRE \data_p2_reg[12] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [12]),
        .Q(data_p2[12]),
        .R(1'b0));
  FDRE \data_p2_reg[13] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [13]),
        .Q(data_p2[13]),
        .R(1'b0));
  FDRE \data_p2_reg[14] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [14]),
        .Q(data_p2[14]),
        .R(1'b0));
  FDRE \data_p2_reg[15] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [15]),
        .Q(data_p2[15]),
        .R(1'b0));
  FDRE \data_p2_reg[16] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [16]),
        .Q(data_p2[16]),
        .R(1'b0));
  FDRE \data_p2_reg[17] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [17]),
        .Q(data_p2[17]),
        .R(1'b0));
  FDRE \data_p2_reg[18] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [18]),
        .Q(data_p2[18]),
        .R(1'b0));
  FDRE \data_p2_reg[19] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [19]),
        .Q(data_p2[19]),
        .R(1'b0));
  FDRE \data_p2_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [1]),
        .Q(data_p2[1]),
        .R(1'b0));
  FDRE \data_p2_reg[20] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [20]),
        .Q(data_p2[20]),
        .R(1'b0));
  FDRE \data_p2_reg[21] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [21]),
        .Q(data_p2[21]),
        .R(1'b0));
  FDRE \data_p2_reg[22] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [22]),
        .Q(data_p2[22]),
        .R(1'b0));
  FDRE \data_p2_reg[23] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [23]),
        .Q(data_p2[23]),
        .R(1'b0));
  FDRE \data_p2_reg[24] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [24]),
        .Q(data_p2[24]),
        .R(1'b0));
  FDRE \data_p2_reg[25] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [25]),
        .Q(data_p2[25]),
        .R(1'b0));
  FDRE \data_p2_reg[26] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [26]),
        .Q(data_p2[26]),
        .R(1'b0));
  FDRE \data_p2_reg[27] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [27]),
        .Q(data_p2[27]),
        .R(1'b0));
  FDRE \data_p2_reg[28] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [28]),
        .Q(data_p2[28]),
        .R(1'b0));
  FDRE \data_p2_reg[29] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [29]),
        .Q(data_p2[29]),
        .R(1'b0));
  FDRE \data_p2_reg[2] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [2]),
        .Q(data_p2[2]),
        .R(1'b0));
  FDRE \data_p2_reg[30] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [30]),
        .Q(data_p2[30]),
        .R(1'b0));
  FDRE \data_p2_reg[31] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [31]),
        .Q(data_p2[31]),
        .R(1'b0));
  FDRE \data_p2_reg[3] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [3]),
        .Q(data_p2[3]),
        .R(1'b0));
  FDRE \data_p2_reg[4] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [4]),
        .Q(data_p2[4]),
        .R(1'b0));
  FDRE \data_p2_reg[5] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [5]),
        .Q(data_p2[5]),
        .R(1'b0));
  FDRE \data_p2_reg[6] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [6]),
        .Q(data_p2[6]),
        .R(1'b0));
  FDRE \data_p2_reg[7] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [7]),
        .Q(data_p2[7]),
        .R(1'b0));
  FDRE \data_p2_reg[8] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [8]),
        .Q(data_p2[8]),
        .R(1'b0));
  FDRE \data_p2_reg[9] 
       (.C(ap_clk),
        .CE(E),
        .D(\data_p2_reg[31]_0 [9]),
        .Q(data_p2[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \mem_reg[4][0]_srl5_i_1 
       (.I0(\state_reg[0]_0 ),
        .I1(rs2f_rreq_ack),
        .O(push));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT5 #(
    .INIT(32'hFFBF0F05)) 
    s_ready_t_i_1
       (.I0(state__0[0]),
        .I1(Q),
        .I2(state__0[1]),
        .I3(rs2f_rreq_ack),
        .I4(s_ready_t_reg_0),
        .O(s_ready_t_i_1_n_0));
  FDRE s_ready_t_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(s_ready_t_i_1_n_0),
        .Q(s_ready_t_reg_0),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT5 #(
    .INIT(32'hFCCC4C4C)) 
    \state[0]_i_1 
       (.I0(rs2f_rreq_ack),
        .I1(\state_reg[0]_0 ),
        .I2(state),
        .I3(s_ready_t_reg_0),
        .I4(Q),
        .O(\state[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFF4F)) 
    \state[1]_i_1 
       (.I0(Q),
        .I1(state),
        .I2(\state_reg[0]_0 ),
        .I3(rs2f_rreq_ack),
        .O(\state[1]_i_1_n_0 ));
  FDRE \state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\state[0]_i_1_n_0 ),
        .Q(\state_reg[0]_0 ),
        .R(SR));
  FDSE \state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\state[1]_i_1_n_0 ),
        .Q(state),
        .S(SR));
endmodule

(* ORIG_REF_NAME = "filter_gmem0_m_axi_reg_slice" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem0_m_axi_reg_slice__parameterized2
   (s_ready,
    I_AWVALID,
    \state_reg[0]_0 ,
    D,
    ap_rst_n_0,
    \bus_wide_gen.rdata_valid_t_reg ,
    \data_p1_reg[7]_0 ,
    SR,
    ap_clk,
    Q,
    ap_reg_ioackin_gmem1_AWREADY,
    gmem1_AWREADY,
    ap_rst_n,
    s_ready_t_reg_0,
    \data_p2_reg[7]_0 );
  output s_ready;
  output I_AWVALID;
  output [0:0]\state_reg[0]_0 ;
  output [0:0]D;
  output ap_rst_n_0;
  output \bus_wide_gen.rdata_valid_t_reg ;
  output [7:0]\data_p1_reg[7]_0 ;
  input [0:0]SR;
  input ap_clk;
  input [1:0]Q;
  input ap_reg_ioackin_gmem1_AWREADY;
  input gmem1_AWREADY;
  input ap_rst_n;
  input s_ready_t_reg_0;
  input [7:0]\data_p2_reg[7]_0 ;

  wire [0:0]D;
  wire \FSM_sequential_state[1]_i_2__0_n_0 ;
  wire I_AWVALID;
  wire [1:0]Q;
  wire [0:0]SR;
  wire ap_clk;
  wire ap_reg_ioackin_gmem1_AWREADY;
  wire ap_rst_n;
  wire ap_rst_n_0;
  wire \bus_wide_gen.rdata_valid_t_reg ;
  wire \data_p1[0]_i_1__0_n_0 ;
  wire \data_p1[1]_i_1__0_n_0 ;
  wire \data_p1[2]_i_1__0_n_0 ;
  wire \data_p1[3]_i_1__0_n_0 ;
  wire \data_p1[4]_i_1__0_n_0 ;
  wire \data_p1[5]_i_1__0_n_0 ;
  wire \data_p1[6]_i_1__0_n_0 ;
  wire \data_p1[7]_i_2_n_0 ;
  wire [7:0]\data_p1_reg[7]_0 ;
  wire [7:0]\data_p2_reg[7]_0 ;
  wire \data_p2_reg_n_0_[0] ;
  wire \data_p2_reg_n_0_[1] ;
  wire \data_p2_reg_n_0_[2] ;
  wire \data_p2_reg_n_0_[3] ;
  wire \data_p2_reg_n_0_[4] ;
  wire \data_p2_reg_n_0_[5] ;
  wire \data_p2_reg_n_0_[6] ;
  wire \data_p2_reg_n_0_[7] ;
  wire gmem1_AWREADY;
  wire load_p1;
  wire load_p2;
  wire [1:0]next_st__0;
  wire s_ready;
  wire s_ready_t_i_1__0_n_0;
  wire s_ready_t_reg_0;
  wire [1:1]state;
  wire \state[0]_i_1__0_n_0 ;
  wire \state[1]_i_1__0_n_0 ;
  wire [1:0]state__0;
  wire [0:0]\state_reg[0]_0 ;

  LUT4 #(
    .INIT(16'h6020)) 
    \FSM_sequential_state[0]_i_1__0 
       (.I0(state__0[0]),
        .I1(state__0[1]),
        .I2(\FSM_sequential_state[1]_i_2__0_n_0 ),
        .I3(s_ready_t_reg_0),
        .O(next_st__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT5 #(
    .INIT(32'h023E300C)) 
    \FSM_sequential_state[1]_i_1__0 
       (.I0(s_ready),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\FSM_sequential_state[1]_i_2__0_n_0 ),
        .I4(s_ready_t_reg_0),
        .O(next_st__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_sequential_state[1]_i_2 
       (.I0(Q[1]),
        .I1(\state_reg[0]_0 ),
        .I2(ap_reg_ioackin_gmem1_AWREADY),
        .O(I_AWVALID));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT4 #(
    .INIT(16'h777F)) 
    \FSM_sequential_state[1]_i_2__0 
       (.I0(Q[1]),
        .I1(\state_reg[0]_0 ),
        .I2(gmem1_AWREADY),
        .I3(ap_reg_ioackin_gmem1_AWREADY),
        .O(\FSM_sequential_state[1]_i_2__0_n_0 ));
  (* FSM_ENCODED_STATES = "zero:00,two:01,one:10" *) 
  FDRE \FSM_sequential_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(next_st__0[0]),
        .Q(state__0[0]),
        .R(SR));
  (* FSM_ENCODED_STATES = "zero:00,two:01,one:10" *) 
  FDRE \FSM_sequential_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(next_st__0[1]),
        .Q(state__0[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT5 #(
    .INIT(32'hBBBFAAAA)) 
    \ap_CS_fsm[8]_i_1 
       (.I0(Q[0]),
        .I1(\state_reg[0]_0 ),
        .I2(ap_reg_ioackin_gmem1_AWREADY),
        .I3(gmem1_AWREADY),
        .I4(Q[1]),
        .O(D));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT4 #(
    .INIT(16'h0888)) 
    ap_reg_ioackin_gmem1_AWREADY_i_1
       (.I0(ap_rst_n),
        .I1(ap_reg_ioackin_gmem1_AWREADY),
        .I2(\state_reg[0]_0 ),
        .I3(Q[1]),
        .O(ap_rst_n_0));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \bus_wide_gen.len_cnt[7]_i_5 
       (.I0(s_ready_t_reg_0),
        .I1(s_ready),
        .O(\bus_wide_gen.rdata_valid_t_reg ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[0]_i_1__0 
       (.I0(\data_p2_reg_n_0_[0] ),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[7]_0 [0]),
        .O(\data_p1[0]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[1]_i_1__0 
       (.I0(\data_p2_reg_n_0_[1] ),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[7]_0 [1]),
        .O(\data_p1[1]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[2]_i_1__0 
       (.I0(\data_p2_reg_n_0_[2] ),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[7]_0 [2]),
        .O(\data_p1[2]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[3]_i_1__0 
       (.I0(\data_p2_reg_n_0_[3] ),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[7]_0 [3]),
        .O(\data_p1[3]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[4]_i_1__0 
       (.I0(\data_p2_reg_n_0_[4] ),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[7]_0 [4]),
        .O(\data_p1[4]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[5]_i_1__0 
       (.I0(\data_p2_reg_n_0_[5] ),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[7]_0 [5]),
        .O(\data_p1[5]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[6]_i_1__0 
       (.I0(\data_p2_reg_n_0_[6] ),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[7]_0 [6]),
        .O(\data_p1[6]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'h1170)) 
    \data_p1[7]_i_1__0 
       (.I0(state__0[1]),
        .I1(\FSM_sequential_state[1]_i_2__0_n_0 ),
        .I2(s_ready_t_reg_0),
        .I3(state__0[0]),
        .O(load_p1));
  LUT4 #(
    .INIT(16'hFB08)) 
    \data_p1[7]_i_2 
       (.I0(\data_p2_reg_n_0_[7] ),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\data_p2_reg[7]_0 [7]),
        .O(\data_p1[7]_i_2_n_0 ));
  FDRE \data_p1_reg[0] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[0]_i_1__0_n_0 ),
        .Q(\data_p1_reg[7]_0 [0]),
        .R(1'b0));
  FDRE \data_p1_reg[1] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[1]_i_1__0_n_0 ),
        .Q(\data_p1_reg[7]_0 [1]),
        .R(1'b0));
  FDRE \data_p1_reg[2] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[2]_i_1__0_n_0 ),
        .Q(\data_p1_reg[7]_0 [2]),
        .R(1'b0));
  FDRE \data_p1_reg[3] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[3]_i_1__0_n_0 ),
        .Q(\data_p1_reg[7]_0 [3]),
        .R(1'b0));
  FDRE \data_p1_reg[4] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[4]_i_1__0_n_0 ),
        .Q(\data_p1_reg[7]_0 [4]),
        .R(1'b0));
  FDRE \data_p1_reg[5] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[5]_i_1__0_n_0 ),
        .Q(\data_p1_reg[7]_0 [5]),
        .R(1'b0));
  FDRE \data_p1_reg[6] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[6]_i_1__0_n_0 ),
        .Q(\data_p1_reg[7]_0 [6]),
        .R(1'b0));
  FDRE \data_p1_reg[7] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[7]_i_2_n_0 ),
        .Q(\data_p1_reg[7]_0 [7]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    \data_p2[7]_i_1 
       (.I0(s_ready),
        .I1(s_ready_t_reg_0),
        .O(load_p2));
  FDRE \data_p2_reg[0] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[7]_0 [0]),
        .Q(\data_p2_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \data_p2_reg[1] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[7]_0 [1]),
        .Q(\data_p2_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \data_p2_reg[2] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[7]_0 [2]),
        .Q(\data_p2_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \data_p2_reg[3] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[7]_0 [3]),
        .Q(\data_p2_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \data_p2_reg[4] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[7]_0 [4]),
        .Q(\data_p2_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \data_p2_reg[5] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[7]_0 [5]),
        .Q(\data_p2_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \data_p2_reg[6] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[7]_0 [6]),
        .Q(\data_p2_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \data_p2_reg[7] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[7]_0 [7]),
        .Q(\data_p2_reg_n_0_[7] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT5 #(
    .INIT(32'hFF7F1155)) 
    s_ready_t_i_1__0
       (.I0(state__0[1]),
        .I1(\FSM_sequential_state[1]_i_2__0_n_0 ),
        .I2(s_ready_t_reg_0),
        .I3(state__0[0]),
        .I4(s_ready),
        .O(s_ready_t_i_1__0_n_0));
  FDRE s_ready_t_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(s_ready_t_i_1__0_n_0),
        .Q(s_ready),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT5 #(
    .INIT(32'hFC8CCC8C)) 
    \state[0]_i_1__0 
       (.I0(\FSM_sequential_state[1]_i_2__0_n_0 ),
        .I1(\state_reg[0]_0 ),
        .I2(state),
        .I3(s_ready_t_reg_0),
        .I4(s_ready),
        .O(\state[0]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hFF4FFF4FFF4F4F4F)) 
    \state[1]_i_1__0 
       (.I0(s_ready_t_reg_0),
        .I1(state),
        .I2(\state_reg[0]_0 ),
        .I3(Q[1]),
        .I4(gmem1_AWREADY),
        .I5(ap_reg_ioackin_gmem1_AWREADY),
        .O(\state[1]_i_1__0_n_0 ));
  FDRE \state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\state[0]_i_1__0_n_0 ),
        .Q(\state_reg[0]_0 ),
        .R(SR));
  FDSE \state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\state[1]_i_1__0_n_0 ),
        .Q(state),
        .S(SR));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi
   (D,
    gmem1_AWREADY,
    E,
    empty_n_tmp_reg,
    I_BVALID,
    RREADY,
    m_axi_gmem1_AWADDR,
    AWLEN,
    m_axi_gmem1_WDATA,
    m_axi_gmem1_WSTRB,
    \bus_equal_gen.WVALID_Dummy_reg ,
    m_axi_gmem1_AWVALID,
    full_n_tmp_reg,
    m_axi_gmem1_WLAST,
    Q,
    ap_reg_ioackin_gmem1_AWREADY,
    \data_p2_reg[0] ,
    ap_rst_n,
    m_axi_gmem1_RVALID,
    ARESET,
    ap_clk,
    \q_tmp_reg[31] ,
    \data_p2_reg[29] ,
    m_axi_gmem1_WREADY,
    m_axi_gmem1_AWREADY,
    I_AWVALID,
    m_axi_gmem1_BVALID);
  output [2:0]D;
  output gmem1_AWREADY;
  output [0:0]E;
  output [0:0]empty_n_tmp_reg;
  output I_BVALID;
  output RREADY;
  output [29:0]m_axi_gmem1_AWADDR;
  output [3:0]AWLEN;
  output [31:0]m_axi_gmem1_WDATA;
  output [3:0]m_axi_gmem1_WSTRB;
  output \bus_equal_gen.WVALID_Dummy_reg ;
  output m_axi_gmem1_AWVALID;
  output full_n_tmp_reg;
  output m_axi_gmem1_WLAST;
  input [3:0]Q;
  input ap_reg_ioackin_gmem1_AWREADY;
  input [0:0]\data_p2_reg[0] ;
  input ap_rst_n;
  input m_axi_gmem1_RVALID;
  input ARESET;
  input ap_clk;
  input [7:0]\q_tmp_reg[31] ;
  input [29:0]\data_p2_reg[29] ;
  input m_axi_gmem1_WREADY;
  input m_axi_gmem1_AWREADY;
  input I_AWVALID;
  input m_axi_gmem1_BVALID;

  wire ARESET;
  wire [3:0]AWLEN;
  wire AWVALID_Dummy;
  wire [2:0]D;
  wire [0:0]E;
  wire I_AWVALID;
  wire I_BVALID;
  wire [3:0]Q;
  wire RREADY;
  wire ap_clk;
  wire ap_reg_ioackin_gmem1_AWREADY;
  wire ap_rst_n;
  wire \bus_equal_gen.WVALID_Dummy_reg ;
  wire bus_write_n_47;
  wire bus_write_n_48;
  wire [1:0]\conservative_gen.throttl_cnt_reg ;
  wire [0:0]\data_p2_reg[0] ;
  wire [29:0]\data_p2_reg[29] ;
  wire [0:0]empty_n_tmp_reg;
  wire full_n_tmp_reg;
  wire gmem1_AWREADY;
  wire [29:0]m_axi_gmem1_AWADDR;
  wire m_axi_gmem1_AWREADY;
  wire m_axi_gmem1_AWVALID;
  wire m_axi_gmem1_BVALID;
  wire m_axi_gmem1_RVALID;
  wire [31:0]m_axi_gmem1_WDATA;
  wire m_axi_gmem1_WLAST;
  wire m_axi_gmem1_WREADY;
  wire [3:0]m_axi_gmem1_WSTRB;
  wire [1:0]p_0_in;
  wire [7:0]\q_tmp_reg[31] ;
  wire wreq_throttl_n_3;
  wire wreq_throttl_n_4;
  wire wreq_throttl_n_5;
  wire wreq_throttl_n_6;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_read bus_read
       (.ARESET(ARESET),
        .ap_clk(ap_clk),
        .full_n_reg(RREADY),
        .m_axi_gmem1_RVALID(m_axi_gmem1_RVALID));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_write bus_write
       (.ARESET(ARESET),
        .AWVALID_Dummy(AWVALID_Dummy),
        .D(D),
        .E(E),
        .I_AWVALID(I_AWVALID),
        .Q(Q),
        .ap_clk(ap_clk),
        .ap_reg_ioackin_gmem1_AWREADY(ap_reg_ioackin_gmem1_AWREADY),
        .ap_rst_n(ap_rst_n),
        .\bus_equal_gen.WVALID_Dummy_reg_0 (\bus_equal_gen.WVALID_Dummy_reg ),
        .\bus_equal_gen.WVALID_Dummy_reg_1 (bus_write_n_48),
        .\conservative_gen.throttl_cnt_reg[1] (\conservative_gen.throttl_cnt_reg ),
        .\conservative_gen.throttl_cnt_reg[7] (wreq_throttl_n_5),
        .\could_multi_bursts.AWVALID_Dummy_reg_0 (bus_write_n_47),
        .\could_multi_bursts.AWVALID_Dummy_reg_1 (wreq_throttl_n_4),
        .\could_multi_bursts.awlen_buf_reg[1]_0 (p_0_in),
        .\could_multi_bursts.awlen_buf_reg[3]_0 (AWLEN),
        .\could_multi_bursts.loop_cnt_reg[0]_0 (wreq_throttl_n_6),
        .\could_multi_bursts.loop_cnt_reg[0]_1 (wreq_throttl_n_3),
        .\data_p2_reg[0] (\data_p2_reg[0] ),
        .\data_p2_reg[29] (\data_p2_reg[29] ),
        .empty_n_tmp_reg(I_BVALID),
        .empty_n_tmp_reg_0(empty_n_tmp_reg),
        .full_n_tmp_reg(full_n_tmp_reg),
        .m_axi_gmem1_AWADDR(m_axi_gmem1_AWADDR),
        .m_axi_gmem1_AWREADY(m_axi_gmem1_AWREADY),
        .m_axi_gmem1_BVALID(m_axi_gmem1_BVALID),
        .m_axi_gmem1_WDATA(m_axi_gmem1_WDATA),
        .m_axi_gmem1_WLAST(m_axi_gmem1_WLAST),
        .m_axi_gmem1_WREADY(m_axi_gmem1_WREADY),
        .m_axi_gmem1_WSTRB(m_axi_gmem1_WSTRB),
        .\q_tmp_reg[31] (\q_tmp_reg[31] ),
        .s_ready_t_reg(gmem1_AWREADY));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_throttl wreq_throttl
       (.ARESET(ARESET),
        .AWLEN(AWLEN[3:2]),
        .AWVALID_Dummy(AWVALID_Dummy),
        .D(p_0_in),
        .E(bus_write_n_48),
        .Q(\conservative_gen.throttl_cnt_reg ),
        .ap_clk(ap_clk),
        .\conservative_gen.throttl_cnt_reg[1]_0 (wreq_throttl_n_3),
        .\conservative_gen.throttl_cnt_reg[2]_0 (bus_write_n_47),
        .\conservative_gen.throttl_cnt_reg[4]_0 (wreq_throttl_n_5),
        .\conservative_gen.throttl_cnt_reg[6]_0 (wreq_throttl_n_6),
        .m_axi_gmem1_AWREADY(m_axi_gmem1_AWREADY),
        .m_axi_gmem1_AWREADY_0(wreq_throttl_n_4),
        .m_axi_gmem1_AWVALID(m_axi_gmem1_AWVALID));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_buffer
   (if_empty_n,
    D,
    \dout_buf_reg[35]_0 ,
    ap_clk,
    \q_tmp_reg[31]_0 ,
    Q,
    ARESET,
    ap_reg_ioackin_gmem1_AWREADY,
    \ap_CS_fsm_reg[9] ,
    \ap_CS_fsm_reg[9]_0 ,
    burst_valid,
    m_axi_gmem1_WREADY,
    dout_valid_reg_0);
  output if_empty_n;
  output [1:0]D;
  output [35:0]\dout_buf_reg[35]_0 ;
  input ap_clk;
  input [7:0]\q_tmp_reg[31]_0 ;
  input [1:0]Q;
  input ARESET;
  input ap_reg_ioackin_gmem1_AWREADY;
  input \ap_CS_fsm_reg[9] ;
  input [0:0]\ap_CS_fsm_reg[9]_0 ;
  input burst_valid;
  input m_axi_gmem1_WREADY;
  input dout_valid_reg_0;

  wire ARESET;
  wire [1:0]D;
  wire [1:0]Q;
  wire \ap_CS_fsm_reg[9] ;
  wire [0:0]\ap_CS_fsm_reg[9]_0 ;
  wire ap_clk;
  wire ap_reg_ioackin_gmem1_AWREADY;
  wire burst_valid;
  wire \dout_buf[0]_i_1_n_0 ;
  wire \dout_buf[10]_i_1_n_0 ;
  wire \dout_buf[11]_i_1_n_0 ;
  wire \dout_buf[12]_i_1_n_0 ;
  wire \dout_buf[13]_i_1_n_0 ;
  wire \dout_buf[14]_i_1_n_0 ;
  wire \dout_buf[15]_i_1_n_0 ;
  wire \dout_buf[16]_i_1_n_0 ;
  wire \dout_buf[17]_i_1_n_0 ;
  wire \dout_buf[18]_i_1_n_0 ;
  wire \dout_buf[19]_i_1_n_0 ;
  wire \dout_buf[1]_i_1_n_0 ;
  wire \dout_buf[20]_i_1_n_0 ;
  wire \dout_buf[21]_i_1_n_0 ;
  wire \dout_buf[22]_i_1_n_0 ;
  wire \dout_buf[23]_i_1_n_0 ;
  wire \dout_buf[24]_i_1_n_0 ;
  wire \dout_buf[25]_i_1_n_0 ;
  wire \dout_buf[26]_i_1_n_0 ;
  wire \dout_buf[27]_i_1_n_0 ;
  wire \dout_buf[28]_i_1_n_0 ;
  wire \dout_buf[29]_i_1_n_0 ;
  wire \dout_buf[2]_i_1_n_0 ;
  wire \dout_buf[30]_i_1_n_0 ;
  wire \dout_buf[31]_i_1_n_0 ;
  wire \dout_buf[32]_i_1_n_0 ;
  wire \dout_buf[33]_i_1_n_0 ;
  wire \dout_buf[34]_i_1_n_0 ;
  wire \dout_buf[35]_i_1_n_0 ;
  wire \dout_buf[3]_i_1_n_0 ;
  wire \dout_buf[4]_i_1_n_0 ;
  wire \dout_buf[5]_i_1_n_0 ;
  wire \dout_buf[6]_i_1_n_0 ;
  wire \dout_buf[7]_i_1_n_0 ;
  wire \dout_buf[8]_i_1_n_0 ;
  wire \dout_buf[9]_i_1_n_0 ;
  wire [35:0]\dout_buf_reg[35]_0 ;
  wire dout_valid_i_1__0_n_0;
  wire dout_valid_reg_0;
  wire empty_n;
  wire empty_n0;
  wire empty_n_i_3__0_n_0;
  wire empty_n_i_4_n_0;
  wire empty_n_reg_n_0;
  wire full_n0;
  wire full_n_i_2__0_n_0;
  wire full_n_i_3__0_n_0;
  wire gmem1_WREADY;
  wire if_empty_n;
  wire m_axi_gmem1_WREADY;
  wire mem_reg_i_10__0_n_0;
  wire mem_reg_i_1__0_n_0;
  wire mem_reg_i_2__0_n_0;
  wire mem_reg_i_3__0_n_0;
  wire mem_reg_i_4__0_n_0;
  wire mem_reg_i_5__0_n_0;
  wire mem_reg_i_6__0_n_0;
  wire mem_reg_i_7__0_n_0;
  wire mem_reg_i_8__0_n_0;
  wire mem_reg_i_9__0_n_0;
  wire pop;
  wire [35:0]q_buf;
  wire [35:24]q_tmp;
  wire [7:0]\q_tmp_reg[31]_0 ;
  wire [7:0]raddr;
  wire \raddr[1]_i_1_n_0 ;
  wire \raddr[3]_i_1_n_0 ;
  wire \raddr[4]_i_1_n_0 ;
  wire \raddr[7]_i_2_n_0 ;
  wire show_ahead;
  wire show_ahead0;
  wire usedw15_out;
  wire \usedw[0]_i_1__0_n_0 ;
  wire \usedw[4]_i_3_n_0 ;
  wire \usedw[4]_i_4_n_0 ;
  wire \usedw[4]_i_5__0_n_0 ;
  wire \usedw[4]_i_6__0_n_0 ;
  wire \usedw[7]_i_2_n_0 ;
  wire \usedw[7]_i_3_n_0 ;
  wire \usedw[7]_i_4_n_0 ;
  wire \usedw_reg[4]_i_1_n_0 ;
  wire \usedw_reg[4]_i_1_n_1 ;
  wire \usedw_reg[4]_i_1_n_2 ;
  wire \usedw_reg[4]_i_1_n_3 ;
  wire \usedw_reg[4]_i_1_n_4 ;
  wire \usedw_reg[4]_i_1_n_5 ;
  wire \usedw_reg[4]_i_1_n_6 ;
  wire \usedw_reg[4]_i_1_n_7 ;
  wire \usedw_reg[7]_i_1_n_2 ;
  wire \usedw_reg[7]_i_1_n_3 ;
  wire \usedw_reg[7]_i_1_n_5 ;
  wire \usedw_reg[7]_i_1_n_6 ;
  wire \usedw_reg[7]_i_1_n_7 ;
  wire [7:0]usedw_reg__0;
  wire [7:0]waddr;
  wire \waddr[6]_i_2__0_n_0 ;
  wire \waddr[7]_i_3__0_n_0 ;
  wire \waddr[7]_i_4__0_n_0 ;
  wire [7:0]wnext;
  wire [3:2]\NLW_usedw_reg[7]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_usedw_reg[7]_i_1_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hFFF0000044444444)) 
    \ap_CS_fsm[9]_i_1 
       (.I0(gmem1_WREADY),
        .I1(Q[1]),
        .I2(ap_reg_ioackin_gmem1_AWREADY),
        .I3(\ap_CS_fsm_reg[9] ),
        .I4(\ap_CS_fsm_reg[9]_0 ),
        .I5(Q[0]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair147" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[0]_i_1 
       (.I0(q_tmp[24]),
        .I1(q_buf[0]),
        .I2(show_ahead),
        .O(\dout_buf[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair151" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[10]_i_1 
       (.I0(q_tmp[26]),
        .I1(q_buf[10]),
        .I2(show_ahead),
        .O(\dout_buf[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair157" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[11]_i_1 
       (.I0(q_tmp[27]),
        .I1(q_buf[11]),
        .I2(show_ahead),
        .O(\dout_buf[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair159" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[12]_i_1 
       (.I0(q_tmp[28]),
        .I1(q_buf[12]),
        .I2(show_ahead),
        .O(\dout_buf[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair155" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[13]_i_1 
       (.I0(q_tmp[29]),
        .I1(q_buf[13]),
        .I2(show_ahead),
        .O(\dout_buf[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair150" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[14]_i_1 
       (.I0(q_tmp[30]),
        .I1(q_buf[14]),
        .I2(show_ahead),
        .O(\dout_buf[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair160" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[15]_i_1 
       (.I0(q_tmp[31]),
        .I1(q_buf[15]),
        .I2(show_ahead),
        .O(\dout_buf[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair158" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[16]_i_1 
       (.I0(q_tmp[24]),
        .I1(q_buf[16]),
        .I2(show_ahead),
        .O(\dout_buf[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair148" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[17]_i_1 
       (.I0(q_tmp[25]),
        .I1(q_buf[17]),
        .I2(show_ahead),
        .O(\dout_buf[17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair151" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[18]_i_1 
       (.I0(q_tmp[26]),
        .I1(q_buf[18]),
        .I2(show_ahead),
        .O(\dout_buf[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair143" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[19]_i_1 
       (.I0(q_tmp[27]),
        .I1(q_buf[19]),
        .I2(show_ahead),
        .O(\dout_buf[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair148" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[1]_i_1 
       (.I0(q_tmp[25]),
        .I1(q_buf[1]),
        .I2(show_ahead),
        .O(\dout_buf[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair144" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[20]_i_1 
       (.I0(q_tmp[28]),
        .I1(q_buf[20]),
        .I2(show_ahead),
        .O(\dout_buf[20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair146" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[21]_i_1 
       (.I0(q_tmp[29]),
        .I1(q_buf[21]),
        .I2(show_ahead),
        .O(\dout_buf[21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair145" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[22]_i_1 
       (.I0(q_tmp[30]),
        .I1(q_buf[22]),
        .I2(show_ahead),
        .O(\dout_buf[22]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair152" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[23]_i_1 
       (.I0(q_tmp[31]),
        .I1(q_buf[23]),
        .I2(show_ahead),
        .O(\dout_buf[23]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair147" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[24]_i_1 
       (.I0(q_tmp[24]),
        .I1(q_buf[24]),
        .I2(show_ahead),
        .O(\dout_buf[24]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair153" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[25]_i_1 
       (.I0(q_tmp[25]),
        .I1(q_buf[25]),
        .I2(show_ahead),
        .O(\dout_buf[25]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair149" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[26]_i_1 
       (.I0(q_tmp[26]),
        .I1(q_buf[26]),
        .I2(show_ahead),
        .O(\dout_buf[26]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair157" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[27]_i_1 
       (.I0(q_tmp[27]),
        .I1(q_buf[27]),
        .I2(show_ahead),
        .O(\dout_buf[27]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair159" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[28]_i_1 
       (.I0(q_tmp[28]),
        .I1(q_buf[28]),
        .I2(show_ahead),
        .O(\dout_buf[28]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair155" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[29]_i_1 
       (.I0(q_tmp[29]),
        .I1(q_buf[29]),
        .I2(show_ahead),
        .O(\dout_buf[29]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair149" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[2]_i_1 
       (.I0(q_tmp[26]),
        .I1(q_buf[2]),
        .I2(show_ahead),
        .O(\dout_buf[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair150" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[30]_i_1 
       (.I0(q_tmp[30]),
        .I1(q_buf[30]),
        .I2(show_ahead),
        .O(\dout_buf[30]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair160" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[31]_i_1 
       (.I0(q_tmp[31]),
        .I1(q_buf[31]),
        .I2(show_ahead),
        .O(\dout_buf[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair154" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[32]_i_1 
       (.I0(q_tmp[35]),
        .I1(q_buf[32]),
        .I2(show_ahead),
        .O(\dout_buf[32]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair154" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[33]_i_1 
       (.I0(q_tmp[35]),
        .I1(q_buf[33]),
        .I2(show_ahead),
        .O(\dout_buf[33]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair156" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[34]_i_1 
       (.I0(q_tmp[35]),
        .I1(q_buf[34]),
        .I2(show_ahead),
        .O(\dout_buf[34]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair156" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[35]_i_1 
       (.I0(q_tmp[35]),
        .I1(q_buf[35]),
        .I2(show_ahead),
        .O(\dout_buf[35]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair143" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[3]_i_1 
       (.I0(q_tmp[27]),
        .I1(q_buf[3]),
        .I2(show_ahead),
        .O(\dout_buf[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair144" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[4]_i_1 
       (.I0(q_tmp[28]),
        .I1(q_buf[4]),
        .I2(show_ahead),
        .O(\dout_buf[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair146" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[5]_i_1 
       (.I0(q_tmp[29]),
        .I1(q_buf[5]),
        .I2(show_ahead),
        .O(\dout_buf[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair145" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[6]_i_1 
       (.I0(q_tmp[30]),
        .I1(q_buf[6]),
        .I2(show_ahead),
        .O(\dout_buf[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair152" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[7]_i_1 
       (.I0(q_tmp[31]),
        .I1(q_buf[7]),
        .I2(show_ahead),
        .O(\dout_buf[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair158" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[8]_i_1 
       (.I0(q_tmp[24]),
        .I1(q_buf[8]),
        .I2(show_ahead),
        .O(\dout_buf[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair153" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \dout_buf[9]_i_1 
       (.I0(q_tmp[25]),
        .I1(q_buf[9]),
        .I2(show_ahead),
        .O(\dout_buf[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[0] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[0]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [0]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[10] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[10]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [10]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[11] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[11]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [11]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[12] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[12]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [12]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[13] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[13]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [13]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[14] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[14]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [14]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[15] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[15]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [15]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[16] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[16]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [16]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[17] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[17]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [17]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[18] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[18]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [18]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[19] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[19]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [19]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[1] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[1]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [1]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[20] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[20]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [20]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[21] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[21]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [21]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[22] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[22]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [22]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[23] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[23]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [23]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[24] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[24]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [24]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[25] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[25]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [25]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[26] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[26]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [26]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[27] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[27]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [27]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[28] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[28]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [28]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[29] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[29]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [29]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[2] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[2]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [2]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[30] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[30]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [30]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[31] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[31]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [31]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[32] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[32]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [32]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[33] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[33]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [33]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[34] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[34]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [34]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[35] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[35]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [35]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[3] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[3]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [3]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[4] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[4]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [4]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[5] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[5]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [5]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[6] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[6]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [6]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[7] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[7]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [7]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[8] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[8]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [8]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \dout_buf_reg[9] 
       (.C(ap_clk),
        .CE(pop),
        .D(\dout_buf[9]_i_1_n_0 ),
        .Q(\dout_buf_reg[35]_0 [9]),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hFFFF2A22)) 
    dout_valid_i_1__0
       (.I0(if_empty_n),
        .I1(burst_valid),
        .I2(m_axi_gmem1_WREADY),
        .I3(dout_valid_reg_0),
        .I4(empty_n_reg_n_0),
        .O(dout_valid_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    dout_valid_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(dout_valid_i_1__0_n_0),
        .Q(if_empty_n),
        .R(ARESET));
  LUT3 #(
    .INIT(8'h6A)) 
    empty_n_i_1__0
       (.I0(pop),
        .I1(gmem1_WREADY),
        .I2(Q[1]),
        .O(empty_n));
  (* SOFT_HLUTNM = "soft_lutpair140" *) 
  LUT5 #(
    .INIT(32'hFFFF40FF)) 
    empty_n_i_2__0
       (.I0(pop),
        .I1(Q[1]),
        .I2(gmem1_WREADY),
        .I3(usedw_reg__0[0]),
        .I4(empty_n_i_3__0_n_0),
        .O(empty_n0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    empty_n_i_3__0
       (.I0(empty_n_i_4_n_0),
        .I1(usedw_reg__0[1]),
        .I2(usedw_reg__0[3]),
        .I3(usedw_reg__0[2]),
        .O(empty_n_i_3__0_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    empty_n_i_4
       (.I0(usedw_reg__0[7]),
        .I1(usedw_reg__0[6]),
        .I2(usedw_reg__0[5]),
        .I3(usedw_reg__0[4]),
        .O(empty_n_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    empty_n_reg
       (.C(ap_clk),
        .CE(empty_n),
        .D(empty_n0),
        .Q(empty_n_reg_n_0),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hFF7FFFFF)) 
    full_n_i_1__1
       (.I0(usedw_reg__0[2]),
        .I1(usedw_reg__0[4]),
        .I2(usedw_reg__0[5]),
        .I3(full_n_i_2__0_n_0),
        .I4(full_n_i_3__0_n_0),
        .O(full_n0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    full_n_i_2__0
       (.I0(usedw_reg__0[7]),
        .I1(usedw_reg__0[6]),
        .I2(usedw_reg__0[3]),
        .I3(usedw_reg__0[1]),
        .O(full_n_i_2__0_n_0));
  LUT6 #(
    .INIT(64'h08880808AAAAAAAA)) 
    full_n_i_3__0
       (.I0(usedw_reg__0[0]),
        .I1(if_empty_n),
        .I2(burst_valid),
        .I3(m_axi_gmem1_WREADY),
        .I4(dout_valid_reg_0),
        .I5(empty_n_reg_n_0),
        .O(full_n_i_3__0_n_0));
  FDSE #(
    .INIT(1'b1)) 
    full_n_reg
       (.C(ap_clk),
        .CE(empty_n),
        .D(full_n0),
        .Q(gmem1_WREADY),
        .S(ARESET));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p4_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p4_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "9216" *) 
  (* RTL_RAM_NAME = "mem" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "35" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "511" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "35" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    mem_reg
       (.ADDRARDADDR({1'b1,mem_reg_i_1__0_n_0,mem_reg_i_2__0_n_0,mem_reg_i_3__0_n_0,mem_reg_i_4__0_n_0,mem_reg_i_5__0_n_0,mem_reg_i_6__0_n_0,mem_reg_i_7__0_n_0,mem_reg_i_8__0_n_0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,waddr,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(ap_clk),
        .CLKBWRCLK(ap_clk),
        .DIADI({\q_tmp_reg[31]_0 ,\q_tmp_reg[31]_0 }),
        .DIBDI({\q_tmp_reg[31]_0 ,\q_tmp_reg[31]_0 }),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(q_buf[15:0]),
        .DOBDO(q_buf[31:16]),
        .DOPADOP(q_buf[33:32]),
        .DOPBDOP(q_buf[35:34]),
        .ENARDEN(1'b1),
        .ENBWREN(gmem1_WREADY),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({Q[1],Q[1],Q[1],Q[1]}));
  (* SOFT_HLUTNM = "soft_lutpair141" *) 
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    mem_reg_i_10__0
       (.I0(raddr[2]),
        .I1(raddr[1]),
        .I2(raddr[0]),
        .I3(raddr[3]),
        .I4(raddr[4]),
        .O(mem_reg_i_10__0_n_0));
  LUT4 #(
    .INIT(16'hBF40)) 
    mem_reg_i_1__0
       (.I0(mem_reg_i_9__0_n_0),
        .I1(raddr[6]),
        .I2(pop),
        .I3(raddr[7]),
        .O(mem_reg_i_1__0_n_0));
  LUT3 #(
    .INIT(8'h9A)) 
    mem_reg_i_2__0
       (.I0(raddr[6]),
        .I1(mem_reg_i_9__0_n_0),
        .I2(pop),
        .O(mem_reg_i_2__0_n_0));
  LUT3 #(
    .INIT(8'h9A)) 
    mem_reg_i_3__0
       (.I0(raddr[5]),
        .I1(mem_reg_i_10__0_n_0),
        .I2(pop),
        .O(mem_reg_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    mem_reg_i_4__0
       (.I0(raddr[2]),
        .I1(raddr[1]),
        .I2(raddr[0]),
        .I3(raddr[3]),
        .I4(pop),
        .I5(raddr[4]),
        .O(mem_reg_i_4__0_n_0));
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    mem_reg_i_5__0
       (.I0(raddr[0]),
        .I1(raddr[1]),
        .I2(raddr[2]),
        .I3(pop),
        .I4(raddr[3]),
        .O(mem_reg_i_5__0_n_0));
  LUT4 #(
    .INIT(16'h6AAA)) 
    mem_reg_i_6__0
       (.I0(raddr[2]),
        .I1(raddr[0]),
        .I2(raddr[1]),
        .I3(pop),
        .O(mem_reg_i_6__0_n_0));
  LUT3 #(
    .INIT(8'h78)) 
    mem_reg_i_7__0
       (.I0(raddr[0]),
        .I1(pop),
        .I2(raddr[1]),
        .O(mem_reg_i_7__0_n_0));
  LUT6 #(
    .INIT(64'h59995959AAAAAAAA)) 
    mem_reg_i_8__0
       (.I0(raddr[0]),
        .I1(if_empty_n),
        .I2(burst_valid),
        .I3(m_axi_gmem1_WREADY),
        .I4(dout_valid_reg_0),
        .I5(empty_n_reg_n_0),
        .O(mem_reg_i_8__0_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    mem_reg_i_9__0
       (.I0(raddr[4]),
        .I1(raddr[3]),
        .I2(raddr[0]),
        .I3(raddr[1]),
        .I4(raddr[2]),
        .I5(raddr[5]),
        .O(mem_reg_i_9__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[24] 
       (.C(ap_clk),
        .CE(D[1]),
        .D(\q_tmp_reg[31]_0 [0]),
        .Q(q_tmp[24]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[25] 
       (.C(ap_clk),
        .CE(D[1]),
        .D(\q_tmp_reg[31]_0 [1]),
        .Q(q_tmp[25]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[26] 
       (.C(ap_clk),
        .CE(D[1]),
        .D(\q_tmp_reg[31]_0 [2]),
        .Q(q_tmp[26]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[27] 
       (.C(ap_clk),
        .CE(D[1]),
        .D(\q_tmp_reg[31]_0 [3]),
        .Q(q_tmp[27]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[28] 
       (.C(ap_clk),
        .CE(D[1]),
        .D(\q_tmp_reg[31]_0 [4]),
        .Q(q_tmp[28]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[29] 
       (.C(ap_clk),
        .CE(D[1]),
        .D(\q_tmp_reg[31]_0 [5]),
        .Q(q_tmp[29]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[30] 
       (.C(ap_clk),
        .CE(D[1]),
        .D(\q_tmp_reg[31]_0 [6]),
        .Q(q_tmp[30]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[31] 
       (.C(ap_clk),
        .CE(D[1]),
        .D(\q_tmp_reg[31]_0 [7]),
        .Q(q_tmp[31]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \q_tmp_reg[35] 
       (.C(ap_clk),
        .CE(D[1]),
        .D(1'b1),
        .Q(q_tmp[35]),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair142" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \raddr[1]_i_1 
       (.I0(raddr[0]),
        .I1(raddr[1]),
        .O(\raddr[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair142" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \raddr[3]_i_1 
       (.I0(raddr[3]),
        .I1(raddr[0]),
        .I2(raddr[1]),
        .I3(raddr[2]),
        .O(\raddr[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair141" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \raddr[4]_i_1 
       (.I0(raddr[4]),
        .I1(raddr[2]),
        .I2(raddr[1]),
        .I3(raddr[0]),
        .I4(raddr[3]),
        .O(\raddr[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hA200AAAA)) 
    \raddr[7]_i_1 
       (.I0(empty_n_reg_n_0),
        .I1(dout_valid_reg_0),
        .I2(m_axi_gmem1_WREADY),
        .I3(burst_valid),
        .I4(if_empty_n),
        .O(pop));
  LUT3 #(
    .INIT(8'h9A)) 
    \raddr[7]_i_2 
       (.I0(raddr[7]),
        .I1(mem_reg_i_9__0_n_0),
        .I2(raddr[6]),
        .O(\raddr[7]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \raddr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(mem_reg_i_8__0_n_0),
        .Q(raddr[0]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \raddr_reg[1] 
       (.C(ap_clk),
        .CE(pop),
        .D(\raddr[1]_i_1_n_0 ),
        .Q(raddr[1]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \raddr_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(mem_reg_i_6__0_n_0),
        .Q(raddr[2]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \raddr_reg[3] 
       (.C(ap_clk),
        .CE(pop),
        .D(\raddr[3]_i_1_n_0 ),
        .Q(raddr[3]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \raddr_reg[4] 
       (.C(ap_clk),
        .CE(pop),
        .D(\raddr[4]_i_1_n_0 ),
        .Q(raddr[4]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \raddr_reg[5] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(mem_reg_i_3__0_n_0),
        .Q(raddr[5]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \raddr_reg[6] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(mem_reg_i_2__0_n_0),
        .Q(raddr[6]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \raddr_reg[7] 
       (.C(ap_clk),
        .CE(pop),
        .D(\raddr[7]_i_2_n_0 ),
        .Q(raddr[7]),
        .R(ARESET));
  LUT5 #(
    .INIT(32'h40004040)) 
    show_ahead_i_1__0
       (.I0(empty_n_i_3__0_n_0),
        .I1(Q[1]),
        .I2(gmem1_WREADY),
        .I3(pop),
        .I4(usedw_reg__0[0]),
        .O(show_ahead0));
  FDRE #(
    .INIT(1'b0)) 
    show_ahead_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(show_ahead0),
        .Q(show_ahead),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair140" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \usedw[0]_i_1__0 
       (.I0(usedw_reg__0[0]),
        .O(\usedw[0]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \usedw[4]_i_2 
       (.I0(gmem1_WREADY),
        .I1(Q[1]),
        .I2(pop),
        .O(usedw15_out));
  LUT2 #(
    .INIT(4'h9)) 
    \usedw[4]_i_3 
       (.I0(usedw_reg__0[3]),
        .I1(usedw_reg__0[4]),
        .O(\usedw[4]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \usedw[4]_i_4 
       (.I0(usedw_reg__0[2]),
        .I1(usedw_reg__0[3]),
        .O(\usedw[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \usedw[4]_i_5__0 
       (.I0(usedw_reg__0[1]),
        .I1(usedw_reg__0[2]),
        .O(\usedw[4]_i_5__0_n_0 ));
  LUT4 #(
    .INIT(16'h6555)) 
    \usedw[4]_i_6__0 
       (.I0(usedw_reg__0[1]),
        .I1(pop),
        .I2(Q[1]),
        .I3(gmem1_WREADY),
        .O(\usedw[4]_i_6__0_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \usedw[7]_i_2 
       (.I0(usedw_reg__0[6]),
        .I1(usedw_reg__0[7]),
        .O(\usedw[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \usedw[7]_i_3 
       (.I0(usedw_reg__0[5]),
        .I1(usedw_reg__0[6]),
        .O(\usedw[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \usedw[7]_i_4 
       (.I0(usedw_reg__0[4]),
        .I1(usedw_reg__0[5]),
        .O(\usedw[7]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[0] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw[0]_i_1__0_n_0 ),
        .Q(usedw_reg__0[0]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[1] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[4]_i_1_n_7 ),
        .Q(usedw_reg__0[1]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[2] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[4]_i_1_n_6 ),
        .Q(usedw_reg__0[2]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[3] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[4]_i_1_n_5 ),
        .Q(usedw_reg__0[3]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[4] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[4]_i_1_n_4 ),
        .Q(usedw_reg__0[4]),
        .R(ARESET));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \usedw_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\usedw_reg[4]_i_1_n_0 ,\usedw_reg[4]_i_1_n_1 ,\usedw_reg[4]_i_1_n_2 ,\usedw_reg[4]_i_1_n_3 }),
        .CYINIT(usedw_reg__0[0]),
        .DI({usedw_reg__0[3:1],usedw15_out}),
        .O({\usedw_reg[4]_i_1_n_4 ,\usedw_reg[4]_i_1_n_5 ,\usedw_reg[4]_i_1_n_6 ,\usedw_reg[4]_i_1_n_7 }),
        .S({\usedw[4]_i_3_n_0 ,\usedw[4]_i_4_n_0 ,\usedw[4]_i_5__0_n_0 ,\usedw[4]_i_6__0_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[5] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[7]_i_1_n_7 ),
        .Q(usedw_reg__0[5]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[6] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[7]_i_1_n_6 ),
        .Q(usedw_reg__0[6]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[7] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[7]_i_1_n_5 ),
        .Q(usedw_reg__0[7]),
        .R(ARESET));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \usedw_reg[7]_i_1 
       (.CI(\usedw_reg[4]_i_1_n_0 ),
        .CO({\NLW_usedw_reg[7]_i_1_CO_UNCONNECTED [3:2],\usedw_reg[7]_i_1_n_2 ,\usedw_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,usedw_reg__0[5:4]}),
        .O({\NLW_usedw_reg[7]_i_1_O_UNCONNECTED [3],\usedw_reg[7]_i_1_n_5 ,\usedw_reg[7]_i_1_n_6 ,\usedw_reg[7]_i_1_n_7 }),
        .S({1'b0,\usedw[7]_i_2_n_0 ,\usedw[7]_i_3_n_0 ,\usedw[7]_i_4_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair162" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \waddr[0]_i_1__0 
       (.I0(waddr[0]),
        .O(wnext[0]));
  (* SOFT_HLUTNM = "soft_lutpair162" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \waddr[1]_i_1__0 
       (.I0(waddr[0]),
        .I1(waddr[1]),
        .O(wnext[1]));
  (* SOFT_HLUTNM = "soft_lutpair161" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \waddr[2]_i_1__0 
       (.I0(waddr[2]),
        .I1(waddr[0]),
        .I2(waddr[1]),
        .O(wnext[2]));
  (* SOFT_HLUTNM = "soft_lutpair139" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \waddr[3]_i_1__0 
       (.I0(waddr[3]),
        .I1(waddr[0]),
        .I2(waddr[1]),
        .I3(waddr[2]),
        .O(wnext[3]));
  (* SOFT_HLUTNM = "soft_lutpair139" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \waddr[4]_i_1__1 
       (.I0(waddr[4]),
        .I1(waddr[2]),
        .I2(waddr[1]),
        .I3(waddr[0]),
        .I4(waddr[3]),
        .O(wnext[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \waddr[5]_i_1__0 
       (.I0(waddr[5]),
        .I1(waddr[3]),
        .I2(waddr[0]),
        .I3(waddr[1]),
        .I4(waddr[2]),
        .I5(waddr[4]),
        .O(wnext[5]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \waddr[6]_i_1__0 
       (.I0(waddr[6]),
        .I1(waddr[4]),
        .I2(waddr[2]),
        .I3(\waddr[6]_i_2__0_n_0 ),
        .I4(waddr[3]),
        .I5(waddr[5]),
        .O(wnext[6]));
  (* SOFT_HLUTNM = "soft_lutpair161" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \waddr[6]_i_2__0 
       (.I0(waddr[1]),
        .I1(waddr[0]),
        .O(\waddr[6]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \waddr[7]_i_1__0 
       (.I0(Q[1]),
        .I1(gmem1_WREADY),
        .O(D[1]));
  LUT4 #(
    .INIT(16'hB8CC)) 
    \waddr[7]_i_2__0 
       (.I0(\waddr[7]_i_3__0_n_0 ),
        .I1(waddr[7]),
        .I2(\waddr[7]_i_4__0_n_0 ),
        .I3(waddr[6]),
        .O(wnext[7]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \waddr[7]_i_3__0 
       (.I0(waddr[4]),
        .I1(waddr[2]),
        .I2(waddr[0]),
        .I3(waddr[1]),
        .I4(waddr[3]),
        .I5(waddr[5]),
        .O(\waddr[7]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \waddr[7]_i_4__0 
       (.I0(waddr[4]),
        .I1(waddr[2]),
        .I2(waddr[1]),
        .I3(waddr[0]),
        .I4(waddr[3]),
        .I5(waddr[5]),
        .O(\waddr[7]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \waddr_reg[0] 
       (.C(ap_clk),
        .CE(D[1]),
        .D(wnext[0]),
        .Q(waddr[0]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \waddr_reg[1] 
       (.C(ap_clk),
        .CE(D[1]),
        .D(wnext[1]),
        .Q(waddr[1]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \waddr_reg[2] 
       (.C(ap_clk),
        .CE(D[1]),
        .D(wnext[2]),
        .Q(waddr[2]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \waddr_reg[3] 
       (.C(ap_clk),
        .CE(D[1]),
        .D(wnext[3]),
        .Q(waddr[3]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \waddr_reg[4] 
       (.C(ap_clk),
        .CE(D[1]),
        .D(wnext[4]),
        .Q(waddr[4]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \waddr_reg[5] 
       (.C(ap_clk),
        .CE(D[1]),
        .D(wnext[5]),
        .Q(waddr[5]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \waddr_reg[6] 
       (.C(ap_clk),
        .CE(D[1]),
        .D(wnext[6]),
        .Q(waddr[6]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \waddr_reg[7] 
       (.C(ap_clk),
        .CE(D[1]),
        .D(wnext[7]),
        .Q(waddr[7]),
        .R(ARESET));
endmodule

(* ORIG_REF_NAME = "filter_gmem1_m_axi_buffer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_buffer__parameterized1
   (full_n_reg_0,
    dout_valid_reg_0,
    ARESET,
    ap_clk,
    m_axi_gmem1_RVALID,
    dout_valid_reg_1,
    s_ready);
  output full_n_reg_0;
  output dout_valid_reg_0;
  input ARESET;
  input ap_clk;
  input m_axi_gmem1_RVALID;
  input dout_valid_reg_1;
  input s_ready;

  wire ARESET;
  wire ap_clk;
  wire beat_valid;
  wire dout_valid_i_1__1_n_0;
  wire dout_valid_reg_0;
  wire dout_valid_reg_1;
  wire empty_n;
  wire empty_n_i_1__1_n_0;
  wire empty_n_i_3__1_n_0;
  wire empty_n_reg_n_0;
  wire full_n_i_2__1_n_0;
  wire full_n_i_3__1_n_0;
  wire full_n_i_4__0_n_0;
  wire full_n_reg_0;
  wire m_axi_gmem1_RVALID;
  wire pop;
  wire s_ready;
  wire usedw15_out;
  wire \usedw[0]_i_1__1_n_0 ;
  wire \usedw[4]_i_2__0_n_0 ;
  wire \usedw[4]_i_3__0_n_0 ;
  wire \usedw[4]_i_4__0_n_0 ;
  wire \usedw[4]_i_5_n_0 ;
  wire \usedw[7]_i_2__0_n_0 ;
  wire \usedw[7]_i_3__0_n_0 ;
  wire \usedw[7]_i_4__0_n_0 ;
  wire \usedw_reg[4]_i_1__0_n_0 ;
  wire \usedw_reg[4]_i_1__0_n_1 ;
  wire \usedw_reg[4]_i_1__0_n_2 ;
  wire \usedw_reg[4]_i_1__0_n_3 ;
  wire \usedw_reg[4]_i_1__0_n_4 ;
  wire \usedw_reg[4]_i_1__0_n_5 ;
  wire \usedw_reg[4]_i_1__0_n_6 ;
  wire \usedw_reg[4]_i_1__0_n_7 ;
  wire \usedw_reg[7]_i_1__0_n_2 ;
  wire \usedw_reg[7]_i_1__0_n_3 ;
  wire \usedw_reg[7]_i_1__0_n_5 ;
  wire \usedw_reg[7]_i_1__0_n_6 ;
  wire \usedw_reg[7]_i_1__0_n_7 ;
  wire [7:0]usedw_reg__0;
  wire [3:2]\NLW_usedw_reg[7]_i_1__0_CO_UNCONNECTED ;
  wire [3:3]\NLW_usedw_reg[7]_i_1__0_O_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair137" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \bus_equal_gen.rdata_valid_t_i_1 
       (.I0(beat_valid),
        .I1(s_ready),
        .I2(dout_valid_reg_1),
        .O(dout_valid_reg_0));
  (* SOFT_HLUTNM = "soft_lutpair137" *) 
  LUT4 #(
    .INIT(16'hFF08)) 
    dout_valid_i_1__1
       (.I0(beat_valid),
        .I1(dout_valid_reg_1),
        .I2(s_ready),
        .I3(empty_n_reg_n_0),
        .O(dout_valid_i_1__1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    dout_valid_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(dout_valid_i_1__1_n_0),
        .Q(beat_valid),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    empty_n_i_1__1
       (.I0(usedw15_out),
        .I1(empty_n_i_3__1_n_0),
        .I2(usedw_reg__0[5]),
        .I3(usedw_reg__0[2]),
        .I4(usedw_reg__0[0]),
        .I5(usedw_reg__0[1]),
        .O(empty_n_i_1__1_n_0));
  LUT6 #(
    .INIT(64'h0000800088888888)) 
    empty_n_i_2__1
       (.I0(full_n_reg_0),
        .I1(m_axi_gmem1_RVALID),
        .I2(beat_valid),
        .I3(dout_valid_reg_1),
        .I4(s_ready),
        .I5(empty_n_reg_n_0),
        .O(usedw15_out));
  (* SOFT_HLUTNM = "soft_lutpair136" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    empty_n_i_3__1
       (.I0(usedw_reg__0[7]),
        .I1(usedw_reg__0[6]),
        .I2(usedw_reg__0[4]),
        .I3(usedw_reg__0[3]),
        .O(empty_n_i_3__1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    empty_n_reg
       (.C(ap_clk),
        .CE(empty_n),
        .D(empty_n_i_1__1_n_0),
        .Q(empty_n_reg_n_0),
        .R(ARESET));
  LUT6 #(
    .INIT(64'h08FFF700F700F700)) 
    full_n_i_1__0
       (.I0(beat_valid),
        .I1(dout_valid_reg_1),
        .I2(s_ready),
        .I3(empty_n_reg_n_0),
        .I4(full_n_reg_0),
        .I5(m_axi_gmem1_RVALID),
        .O(empty_n));
  (* SOFT_HLUTNM = "soft_lutpair135" *) 
  LUT5 #(
    .INIT(32'h8AAAFFFF)) 
    full_n_i_2__1
       (.I0(empty_n_reg_n_0),
        .I1(s_ready),
        .I2(dout_valid_reg_1),
        .I3(beat_valid),
        .I4(full_n_i_3__1_n_0),
        .O(full_n_i_2__1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair134" *) 
  LUT5 #(
    .INIT(32'h00008000)) 
    full_n_i_3__1
       (.I0(usedw_reg__0[2]),
        .I1(usedw_reg__0[3]),
        .I2(usedw_reg__0[1]),
        .I3(usedw_reg__0[0]),
        .I4(full_n_i_4__0_n_0),
        .O(full_n_i_3__1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair136" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    full_n_i_4__0
       (.I0(usedw_reg__0[7]),
        .I1(usedw_reg__0[6]),
        .I2(usedw_reg__0[5]),
        .I3(usedw_reg__0[4]),
        .O(full_n_i_4__0_n_0));
  FDSE #(
    .INIT(1'b1)) 
    full_n_reg
       (.C(ap_clk),
        .CE(empty_n),
        .D(full_n_i_2__1_n_0),
        .Q(full_n_reg_0),
        .S(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair134" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \usedw[0]_i_1__1 
       (.I0(usedw_reg__0[0]),
        .O(\usedw[0]_i_1__1_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \usedw[4]_i_2__0 
       (.I0(usedw_reg__0[3]),
        .I1(usedw_reg__0[4]),
        .O(\usedw[4]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \usedw[4]_i_3__0 
       (.I0(usedw_reg__0[2]),
        .I1(usedw_reg__0[3]),
        .O(\usedw[4]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \usedw[4]_i_4__0 
       (.I0(usedw_reg__0[1]),
        .I1(usedw_reg__0[2]),
        .O(\usedw[4]_i_4__0_n_0 ));
  LUT4 #(
    .INIT(16'h6555)) 
    \usedw[4]_i_5 
       (.I0(usedw_reg__0[1]),
        .I1(pop),
        .I2(m_axi_gmem1_RVALID),
        .I3(full_n_reg_0),
        .O(\usedw[4]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair135" *) 
  LUT4 #(
    .INIT(16'h8AAA)) 
    \usedw[4]_i_6 
       (.I0(empty_n_reg_n_0),
        .I1(s_ready),
        .I2(dout_valid_reg_1),
        .I3(beat_valid),
        .O(pop));
  LUT2 #(
    .INIT(4'h9)) 
    \usedw[7]_i_2__0 
       (.I0(usedw_reg__0[6]),
        .I1(usedw_reg__0[7]),
        .O(\usedw[7]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \usedw[7]_i_3__0 
       (.I0(usedw_reg__0[5]),
        .I1(usedw_reg__0[6]),
        .O(\usedw[7]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \usedw[7]_i_4__0 
       (.I0(usedw_reg__0[4]),
        .I1(usedw_reg__0[5]),
        .O(\usedw[7]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[0] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw[0]_i_1__1_n_0 ),
        .Q(usedw_reg__0[0]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[1] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[4]_i_1__0_n_7 ),
        .Q(usedw_reg__0[1]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[2] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[4]_i_1__0_n_6 ),
        .Q(usedw_reg__0[2]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[3] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[4]_i_1__0_n_5 ),
        .Q(usedw_reg__0[3]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[4] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[4]_i_1__0_n_4 ),
        .Q(usedw_reg__0[4]),
        .R(ARESET));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \usedw_reg[4]_i_1__0 
       (.CI(1'b0),
        .CO({\usedw_reg[4]_i_1__0_n_0 ,\usedw_reg[4]_i_1__0_n_1 ,\usedw_reg[4]_i_1__0_n_2 ,\usedw_reg[4]_i_1__0_n_3 }),
        .CYINIT(usedw_reg__0[0]),
        .DI({usedw_reg__0[3:1],usedw15_out}),
        .O({\usedw_reg[4]_i_1__0_n_4 ,\usedw_reg[4]_i_1__0_n_5 ,\usedw_reg[4]_i_1__0_n_6 ,\usedw_reg[4]_i_1__0_n_7 }),
        .S({\usedw[4]_i_2__0_n_0 ,\usedw[4]_i_3__0_n_0 ,\usedw[4]_i_4__0_n_0 ,\usedw[4]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[5] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[7]_i_1__0_n_7 ),
        .Q(usedw_reg__0[5]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[6] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[7]_i_1__0_n_6 ),
        .Q(usedw_reg__0[6]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \usedw_reg[7] 
       (.C(ap_clk),
        .CE(empty_n),
        .D(\usedw_reg[7]_i_1__0_n_5 ),
        .Q(usedw_reg__0[7]),
        .R(ARESET));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \usedw_reg[7]_i_1__0 
       (.CI(\usedw_reg[4]_i_1__0_n_0 ),
        .CO({\NLW_usedw_reg[7]_i_1__0_CO_UNCONNECTED [3:2],\usedw_reg[7]_i_1__0_n_2 ,\usedw_reg[7]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,usedw_reg__0[5:4]}),
        .O({\NLW_usedw_reg[7]_i_1__0_O_UNCONNECTED [3],\usedw_reg[7]_i_1__0_n_5 ,\usedw_reg[7]_i_1__0_n_6 ,\usedw_reg[7]_i_1__0_n_7 }),
        .S({1'b0,\usedw[7]_i_2__0_n_0 ,\usedw[7]_i_3__0_n_0 ,\usedw[7]_i_4__0_n_0 }));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_fifo
   (fifo_wreq_valid,
    rs2f_wreq_ack,
    SR,
    Q,
    E,
    empty_n_tmp_reg_0,
    S,
    \end_addr_buf_reg[31] ,
    \q_reg[32]_0 ,
    empty_n_tmp_reg_1,
    ARESET,
    ap_clk,
    last_sect_buf,
    \align_len_reg[31] ,
    \align_len_reg[31]_0 ,
    ap_rst_n,
    \pout_reg[2]_0 ,
    wrreq24_out,
    empty_n_tmp_reg_2,
    empty_n_tmp_reg_3,
    last_sect_carry__0,
    last_sect_carry__0_0,
    \sect_cnt_reg[0] ,
    \q_reg[29]_0 );
  output fifo_wreq_valid;
  output rs2f_wreq_ack;
  output [0:0]SR;
  output [30:0]Q;
  output [0:0]E;
  output empty_n_tmp_reg_0;
  output [3:0]S;
  output [2:0]\end_addr_buf_reg[31] ;
  output [0:0]\q_reg[32]_0 ;
  output [0:0]empty_n_tmp_reg_1;
  input ARESET;
  input ap_clk;
  input last_sect_buf;
  input [0:0]\align_len_reg[31] ;
  input \align_len_reg[31]_0 ;
  input ap_rst_n;
  input [0:0]\pout_reg[2]_0 ;
  input wrreq24_out;
  input empty_n_tmp_reg_2;
  input empty_n_tmp_reg_3;
  input [19:0]last_sect_carry__0;
  input [19:0]last_sect_carry__0_0;
  input \sect_cnt_reg[0] ;
  input [29:0]\q_reg[29]_0 ;

  wire ARESET;
  wire [0:0]E;
  wire [30:0]Q;
  wire [3:0]S;
  wire [0:0]SR;
  wire [0:0]\align_len_reg[31] ;
  wire \align_len_reg[31]_0 ;
  wire ap_clk;
  wire ap_rst_n;
  wire data_vld_i_1__2_n_0;
  wire data_vld_reg_n_0;
  wire empty_n_tmp_i_1__1_n_0;
  wire empty_n_tmp_reg_0;
  wire [0:0]empty_n_tmp_reg_1;
  wire empty_n_tmp_reg_2;
  wire empty_n_tmp_reg_3;
  wire [2:0]\end_addr_buf_reg[31] ;
  wire fifo_wreq_valid;
  wire full_n_tmp_i_1__2_n_0;
  wire full_n_tmp_i_2__2_n_0;
  wire full_n_tmp_i_3__0_n_0;
  wire full_n_tmp_i_4__0_n_0;
  wire last_sect_buf;
  wire [19:0]last_sect_carry__0;
  wire [19:0]last_sect_carry__0_0;
  wire \mem_reg[4][0]_srl5_n_0 ;
  wire \mem_reg[4][10]_srl5_n_0 ;
  wire \mem_reg[4][11]_srl5_n_0 ;
  wire \mem_reg[4][12]_srl5_n_0 ;
  wire \mem_reg[4][13]_srl5_n_0 ;
  wire \mem_reg[4][14]_srl5_n_0 ;
  wire \mem_reg[4][15]_srl5_n_0 ;
  wire \mem_reg[4][16]_srl5_n_0 ;
  wire \mem_reg[4][17]_srl5_n_0 ;
  wire \mem_reg[4][18]_srl5_n_0 ;
  wire \mem_reg[4][19]_srl5_n_0 ;
  wire \mem_reg[4][1]_srl5_n_0 ;
  wire \mem_reg[4][20]_srl5_n_0 ;
  wire \mem_reg[4][21]_srl5_n_0 ;
  wire \mem_reg[4][22]_srl5_n_0 ;
  wire \mem_reg[4][23]_srl5_n_0 ;
  wire \mem_reg[4][24]_srl5_n_0 ;
  wire \mem_reg[4][25]_srl5_n_0 ;
  wire \mem_reg[4][26]_srl5_n_0 ;
  wire \mem_reg[4][27]_srl5_n_0 ;
  wire \mem_reg[4][28]_srl5_n_0 ;
  wire \mem_reg[4][29]_srl5_n_0 ;
  wire \mem_reg[4][2]_srl5_n_0 ;
  wire \mem_reg[4][32]_srl5_n_0 ;
  wire \mem_reg[4][3]_srl5_n_0 ;
  wire \mem_reg[4][4]_srl5_n_0 ;
  wire \mem_reg[4][5]_srl5_n_0 ;
  wire \mem_reg[4][6]_srl5_n_0 ;
  wire \mem_reg[4][7]_srl5_n_0 ;
  wire \mem_reg[4][8]_srl5_n_0 ;
  wire \mem_reg[4][9]_srl5_n_0 ;
  wire \pout[0]_i_1_n_0 ;
  wire \pout[1]_i_1_n_0 ;
  wire \pout[2]_i_1_n_0 ;
  wire \pout[2]_i_2__0_n_0 ;
  wire \pout[2]_i_3_n_0 ;
  wire \pout[2]_i_4_n_0 ;
  wire [0:0]\pout_reg[2]_0 ;
  wire \pout_reg_n_0_[0] ;
  wire \pout_reg_n_0_[1] ;
  wire \pout_reg_n_0_[2] ;
  wire push;
  wire [29:0]\q_reg[29]_0 ;
  wire [0:0]\q_reg[32]_0 ;
  wire rs2f_wreq_ack;
  wire \sect_cnt_reg[0] ;
  wire wrreq24_out;

  LUT6 #(
    .INIT(64'h000080AAFFFFFFFF)) 
    \align_len[31]_i_1__0 
       (.I0(fifo_wreq_valid),
        .I1(last_sect_buf),
        .I2(\align_len_reg[31] ),
        .I3(\align_len_reg[31]_0 ),
        .I4(Q[30]),
        .I5(ap_rst_n),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair189" *) 
  LUT4 #(
    .INIT(16'h80AA)) 
    \align_len[31]_i_2 
       (.I0(fifo_wreq_valid),
        .I1(last_sect_buf),
        .I2(\align_len_reg[31] ),
        .I3(\align_len_reg[31]_0 ),
        .O(E));
  LUT6 #(
    .INIT(64'hFFFEFFFFAAAAAAAA)) 
    data_vld_i_1__2
       (.I0(push),
        .I1(\pout_reg_n_0_[1] ),
        .I2(\pout_reg_n_0_[0] ),
        .I3(\pout_reg_n_0_[2] ),
        .I4(full_n_tmp_i_2__2_n_0),
        .I5(data_vld_reg_n_0),
        .O(data_vld_i_1__2_n_0));
  FDRE data_vld_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(data_vld_i_1__2_n_0),
        .Q(data_vld_reg_n_0),
        .R(ARESET));
  LUT6 #(
    .INIT(64'h5DFF5555FFFFFFFF)) 
    empty_n_tmp_i_1__1
       (.I0(fifo_wreq_valid),
        .I1(wrreq24_out),
        .I2(empty_n_tmp_reg_2),
        .I3(empty_n_tmp_reg_3),
        .I4(\align_len_reg[31] ),
        .I5(\align_len_reg[31]_0 ),
        .O(empty_n_tmp_i_1__1_n_0));
  FDRE empty_n_tmp_reg
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(data_vld_reg_n_0),
        .Q(fifo_wreq_valid),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFBBBFBFBFBFBFBFB)) 
    full_n_tmp_i_1__2
       (.I0(full_n_tmp_i_2__2_n_0),
        .I1(ap_rst_n),
        .I2(rs2f_wreq_ack),
        .I3(\pout_reg_n_0_[2] ),
        .I4(full_n_tmp_i_3__0_n_0),
        .I5(full_n_tmp_i_4__0_n_0),
        .O(full_n_tmp_i_1__2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair189" *) 
  LUT5 #(
    .INIT(32'h80AAAAAA)) 
    full_n_tmp_i_2__2
       (.I0(data_vld_reg_n_0),
        .I1(last_sect_buf),
        .I2(\align_len_reg[31] ),
        .I3(\align_len_reg[31]_0 ),
        .I4(fifo_wreq_valid),
        .O(full_n_tmp_i_2__2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair188" *) 
  LUT2 #(
    .INIT(4'h8)) 
    full_n_tmp_i_3__0
       (.I0(\pout_reg_n_0_[0] ),
        .I1(\pout_reg_n_0_[1] ),
        .O(full_n_tmp_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h7000000000000000)) 
    full_n_tmp_i_4__0
       (.I0(last_sect_buf),
        .I1(\align_len_reg[31] ),
        .I2(\align_len_reg[31]_0 ),
        .I3(fifo_wreq_valid),
        .I4(push),
        .I5(data_vld_reg_n_0),
        .O(full_n_tmp_i_4__0_n_0));
  FDRE full_n_tmp_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(full_n_tmp_i_1__2_n_0),
        .Q(rs2f_wreq_ack),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair190" *) 
  LUT2 #(
    .INIT(4'h2)) 
    invalid_len_event_i_1__0
       (.I0(fifo_wreq_valid),
        .I1(Q[30]),
        .O(empty_n_tmp_reg_0));
  LUT4 #(
    .INIT(16'h9009)) 
    last_sect_carry__0_i_1__0
       (.I0(last_sect_carry__0[19]),
        .I1(last_sect_carry__0_0[19]),
        .I2(last_sect_carry__0[18]),
        .I3(last_sect_carry__0_0[18]),
        .O(\end_addr_buf_reg[31] [2]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    last_sect_carry__0_i_2__0
       (.I0(last_sect_carry__0[17]),
        .I1(last_sect_carry__0_0[17]),
        .I2(last_sect_carry__0_0[15]),
        .I3(last_sect_carry__0[15]),
        .I4(last_sect_carry__0_0[16]),
        .I5(last_sect_carry__0[16]),
        .O(\end_addr_buf_reg[31] [1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    last_sect_carry__0_i_3__0
       (.I0(last_sect_carry__0[14]),
        .I1(last_sect_carry__0_0[14]),
        .I2(last_sect_carry__0_0[12]),
        .I3(last_sect_carry__0[12]),
        .I4(last_sect_carry__0_0[13]),
        .I5(last_sect_carry__0[13]),
        .O(\end_addr_buf_reg[31] [0]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    last_sect_carry_i_1__0
       (.I0(last_sect_carry__0[11]),
        .I1(last_sect_carry__0_0[11]),
        .I2(last_sect_carry__0_0[10]),
        .I3(last_sect_carry__0[10]),
        .I4(last_sect_carry__0_0[9]),
        .I5(last_sect_carry__0[9]),
        .O(S[3]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    last_sect_carry_i_2__0
       (.I0(last_sect_carry__0_0[6]),
        .I1(last_sect_carry__0[6]),
        .I2(last_sect_carry__0_0[7]),
        .I3(last_sect_carry__0[7]),
        .I4(last_sect_carry__0[8]),
        .I5(last_sect_carry__0_0[8]),
        .O(S[2]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    last_sect_carry_i_3__0
       (.I0(last_sect_carry__0[5]),
        .I1(last_sect_carry__0_0[5]),
        .I2(last_sect_carry__0_0[3]),
        .I3(last_sect_carry__0[3]),
        .I4(last_sect_carry__0_0[4]),
        .I5(last_sect_carry__0[4]),
        .O(S[1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    last_sect_carry_i_4__0
       (.I0(last_sect_carry__0[2]),
        .I1(last_sect_carry__0_0[2]),
        .I2(last_sect_carry__0_0[0]),
        .I3(last_sect_carry__0[0]),
        .I4(last_sect_carry__0_0[1]),
        .I5(last_sect_carry__0[1]),
        .O(S[0]));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][0]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][0]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [0]),
        .Q(\mem_reg[4][0]_srl5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \mem_reg[4][0]_srl5_i_1__1 
       (.I0(rs2f_wreq_ack),
        .I1(\pout_reg[2]_0 ),
        .O(push));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][10]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][10]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [10]),
        .Q(\mem_reg[4][10]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][11]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][11]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [11]),
        .Q(\mem_reg[4][11]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][12]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][12]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [12]),
        .Q(\mem_reg[4][12]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][13]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][13]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [13]),
        .Q(\mem_reg[4][13]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][14]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][14]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [14]),
        .Q(\mem_reg[4][14]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][15]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][15]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [15]),
        .Q(\mem_reg[4][15]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][16]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][16]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [16]),
        .Q(\mem_reg[4][16]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][17]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][17]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [17]),
        .Q(\mem_reg[4][17]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][18]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][18]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [18]),
        .Q(\mem_reg[4][18]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][19]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][19]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [19]),
        .Q(\mem_reg[4][19]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][1]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][1]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [1]),
        .Q(\mem_reg[4][1]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][20]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][20]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [20]),
        .Q(\mem_reg[4][20]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][21]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][21]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [21]),
        .Q(\mem_reg[4][21]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][22]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][22]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [22]),
        .Q(\mem_reg[4][22]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][23]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][23]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [23]),
        .Q(\mem_reg[4][23]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][24]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][24]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [24]),
        .Q(\mem_reg[4][24]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][25]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][25]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [25]),
        .Q(\mem_reg[4][25]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][26]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][26]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [26]),
        .Q(\mem_reg[4][26]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][27]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][27]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [27]),
        .Q(\mem_reg[4][27]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][28]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][28]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [28]),
        .Q(\mem_reg[4][28]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][29]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][29]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [29]),
        .Q(\mem_reg[4][29]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][2]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][2]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [2]),
        .Q(\mem_reg[4][2]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][32]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][32]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(1'b1),
        .Q(\mem_reg[4][32]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][3]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][3]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [3]),
        .Q(\mem_reg[4][3]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][4]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][4]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [4]),
        .Q(\mem_reg[4][4]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][5]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][5]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [5]),
        .Q(\mem_reg[4][5]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][6]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][6]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [6]),
        .Q(\mem_reg[4][6]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][7]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][7]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [7]),
        .Q(\mem_reg[4][7]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][8]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][8]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [8]),
        .Q(\mem_reg[4][8]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_wreq/mem_reg[4][9]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][9]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(\q_reg[29]_0 [9]),
        .Q(\mem_reg[4][9]_srl5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    minusOp_carry_i_1
       (.I0(Q[30]),
        .O(\q_reg[32]_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \pout[0]_i_1 
       (.I0(\pout[2]_i_3_n_0 ),
        .I1(\pout_reg_n_0_[0] ),
        .O(\pout[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF708FFFF08F70000)) 
    \pout[1]_i_1 
       (.I0(data_vld_reg_n_0),
        .I1(push),
        .I2(empty_n_tmp_i_1__1_n_0),
        .I3(\pout_reg_n_0_[0] ),
        .I4(\pout[2]_i_3_n_0 ),
        .I5(\pout_reg_n_0_[1] ),
        .O(\pout[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair188" *) 
  LUT5 #(
    .INIT(32'hBDFF4200)) 
    \pout[2]_i_1 
       (.I0(\pout[2]_i_2__0_n_0 ),
        .I1(\pout_reg_n_0_[1] ),
        .I2(\pout_reg_n_0_[0] ),
        .I3(\pout[2]_i_3_n_0 ),
        .I4(\pout_reg_n_0_[2] ),
        .O(\pout[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hBFFF)) 
    \pout[2]_i_2__0 
       (.I0(empty_n_tmp_i_1__1_n_0),
        .I1(rs2f_wreq_ack),
        .I2(\pout_reg[2]_0 ),
        .I3(data_vld_reg_n_0),
        .O(\pout[2]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA000055540000)) 
    \pout[2]_i_3 
       (.I0(push),
        .I1(\pout_reg_n_0_[2] ),
        .I2(\pout_reg_n_0_[0] ),
        .I3(\pout_reg_n_0_[1] ),
        .I4(data_vld_reg_n_0),
        .I5(\pout[2]_i_4_n_0 ),
        .O(\pout[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8808080888088808)) 
    \pout[2]_i_4 
       (.I0(fifo_wreq_valid),
        .I1(\align_len_reg[31]_0 ),
        .I2(\align_len_reg[31] ),
        .I3(empty_n_tmp_reg_3),
        .I4(empty_n_tmp_reg_2),
        .I5(wrreq24_out),
        .O(\pout[2]_i_4_n_0 ));
  FDRE \pout_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\pout[0]_i_1_n_0 ),
        .Q(\pout_reg_n_0_[0] ),
        .R(ARESET));
  FDRE \pout_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\pout[1]_i_1_n_0 ),
        .Q(\pout_reg_n_0_[1] ),
        .R(ARESET));
  FDRE \pout_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\pout[2]_i_1_n_0 ),
        .Q(\pout_reg_n_0_[2] ),
        .R(ARESET));
  FDRE \q_reg[0] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][0]_srl5_n_0 ),
        .Q(Q[0]),
        .R(ARESET));
  FDRE \q_reg[10] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][10]_srl5_n_0 ),
        .Q(Q[10]),
        .R(ARESET));
  FDRE \q_reg[11] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][11]_srl5_n_0 ),
        .Q(Q[11]),
        .R(ARESET));
  FDRE \q_reg[12] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][12]_srl5_n_0 ),
        .Q(Q[12]),
        .R(ARESET));
  FDRE \q_reg[13] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][13]_srl5_n_0 ),
        .Q(Q[13]),
        .R(ARESET));
  FDRE \q_reg[14] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][14]_srl5_n_0 ),
        .Q(Q[14]),
        .R(ARESET));
  FDRE \q_reg[15] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][15]_srl5_n_0 ),
        .Q(Q[15]),
        .R(ARESET));
  FDRE \q_reg[16] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][16]_srl5_n_0 ),
        .Q(Q[16]),
        .R(ARESET));
  FDRE \q_reg[17] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][17]_srl5_n_0 ),
        .Q(Q[17]),
        .R(ARESET));
  FDRE \q_reg[18] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][18]_srl5_n_0 ),
        .Q(Q[18]),
        .R(ARESET));
  FDRE \q_reg[19] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][19]_srl5_n_0 ),
        .Q(Q[19]),
        .R(ARESET));
  FDRE \q_reg[1] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][1]_srl5_n_0 ),
        .Q(Q[1]),
        .R(ARESET));
  FDRE \q_reg[20] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][20]_srl5_n_0 ),
        .Q(Q[20]),
        .R(ARESET));
  FDRE \q_reg[21] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][21]_srl5_n_0 ),
        .Q(Q[21]),
        .R(ARESET));
  FDRE \q_reg[22] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][22]_srl5_n_0 ),
        .Q(Q[22]),
        .R(ARESET));
  FDRE \q_reg[23] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][23]_srl5_n_0 ),
        .Q(Q[23]),
        .R(ARESET));
  FDRE \q_reg[24] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][24]_srl5_n_0 ),
        .Q(Q[24]),
        .R(ARESET));
  FDRE \q_reg[25] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][25]_srl5_n_0 ),
        .Q(Q[25]),
        .R(ARESET));
  FDRE \q_reg[26] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][26]_srl5_n_0 ),
        .Q(Q[26]),
        .R(ARESET));
  FDRE \q_reg[27] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][27]_srl5_n_0 ),
        .Q(Q[27]),
        .R(ARESET));
  FDRE \q_reg[28] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][28]_srl5_n_0 ),
        .Q(Q[28]),
        .R(ARESET));
  FDRE \q_reg[29] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][29]_srl5_n_0 ),
        .Q(Q[29]),
        .R(ARESET));
  FDRE \q_reg[2] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][2]_srl5_n_0 ),
        .Q(Q[2]),
        .R(ARESET));
  FDRE \q_reg[32] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][32]_srl5_n_0 ),
        .Q(Q[30]),
        .R(ARESET));
  FDRE \q_reg[3] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][3]_srl5_n_0 ),
        .Q(Q[3]),
        .R(ARESET));
  FDRE \q_reg[4] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][4]_srl5_n_0 ),
        .Q(Q[4]),
        .R(ARESET));
  FDRE \q_reg[5] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][5]_srl5_n_0 ),
        .Q(Q[5]),
        .R(ARESET));
  FDRE \q_reg[6] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][6]_srl5_n_0 ),
        .Q(Q[6]),
        .R(ARESET));
  FDRE \q_reg[7] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][7]_srl5_n_0 ),
        .Q(Q[7]),
        .R(ARESET));
  FDRE \q_reg[8] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][8]_srl5_n_0 ),
        .Q(Q[8]),
        .R(ARESET));
  FDRE \q_reg[9] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__1_n_0),
        .D(\mem_reg[4][9]_srl5_n_0 ),
        .Q(Q[9]),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair190" *) 
  LUT4 #(
    .INIT(16'hF0FE)) 
    \sect_cnt[19]_i_1 
       (.I0(fifo_wreq_valid),
        .I1(\sect_cnt_reg[0] ),
        .I2(last_sect_buf),
        .I3(\align_len_reg[31]_0 ),
        .O(empty_n_tmp_reg_1));
endmodule

(* ORIG_REF_NAME = "filter_gmem1_m_axi_fifo" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_fifo__parameterized1
   (burst_valid,
    fifo_burst_ready,
    SR,
    \could_multi_bursts.AWVALID_Dummy_reg ,
    in,
    \sect_len_buf_reg[7] ,
    E,
    empty_n_tmp_reg_0,
    m_axi_gmem1_WREADY_0,
    ARESET,
    ap_clk,
    ap_rst_n,
    \could_multi_bursts.AWVALID_Dummy_reg_0 ,
    \could_multi_bursts.AWVALID_Dummy_reg_1 ,
    \could_multi_bursts.AWVALID_Dummy_reg_2 ,
    full_n0_in,
    invalid_len_event_2,
    wrreq24_out,
    Q,
    \could_multi_bursts.awlen_buf[3]_i_2_0 ,
    empty_n_tmp_reg_1,
    \bus_equal_gen.WVALID_Dummy_reg ,
    m_axi_gmem1_WREADY,
    if_empty_n,
    push,
    m_axi_gmem1_WLAST);
  output burst_valid;
  output fifo_burst_ready;
  output [0:0]SR;
  output \could_multi_bursts.AWVALID_Dummy_reg ;
  output [3:0]in;
  output \sect_len_buf_reg[7] ;
  output [0:0]E;
  output empty_n_tmp_reg_0;
  output m_axi_gmem1_WREADY_0;
  input ARESET;
  input ap_clk;
  input ap_rst_n;
  input \could_multi_bursts.AWVALID_Dummy_reg_0 ;
  input \could_multi_bursts.AWVALID_Dummy_reg_1 ;
  input \could_multi_bursts.AWVALID_Dummy_reg_2 ;
  input full_n0_in;
  input invalid_len_event_2;
  input wrreq24_out;
  input [9:0]Q;
  input [5:0]\could_multi_bursts.awlen_buf[3]_i_2_0 ;
  input [7:0]empty_n_tmp_reg_1;
  input \bus_equal_gen.WVALID_Dummy_reg ;
  input m_axi_gmem1_WREADY;
  input if_empty_n;
  input push;
  input m_axi_gmem1_WLAST;

  wire ARESET;
  wire [0:0]E;
  wire [9:0]Q;
  wire [0:0]SR;
  wire ap_clk;
  wire ap_rst_n;
  wire burst_valid;
  wire \bus_equal_gen.WLAST_Dummy_i_3_n_0 ;
  wire \bus_equal_gen.WLAST_Dummy_i_4_n_0 ;
  wire \bus_equal_gen.WVALID_Dummy_reg ;
  wire \could_multi_bursts.AWVALID_Dummy_reg ;
  wire \could_multi_bursts.AWVALID_Dummy_reg_0 ;
  wire \could_multi_bursts.AWVALID_Dummy_reg_1 ;
  wire \could_multi_bursts.AWVALID_Dummy_reg_2 ;
  wire [5:0]\could_multi_bursts.awlen_buf[3]_i_2_0 ;
  wire \could_multi_bursts.awlen_buf[3]_i_3_n_0 ;
  wire \could_multi_bursts.awlen_buf[3]_i_4_n_0 ;
  wire data_vld_i_1__5_n_0;
  wire data_vld_reg_n_0;
  wire empty_n_tmp_i_1__2_n_0;
  wire empty_n_tmp_reg_0;
  wire [7:0]empty_n_tmp_reg_1;
  wire fifo_burst_ready;
  wire full_n0_in;
  wire full_n_tmp_i_1__3_n_0;
  wire full_n_tmp_i_2__5_n_0;
  wire full_n_tmp_i_3__2_n_0;
  wire full_n_tmp_i_4__1_n_0;
  wire if_empty_n;
  wire [3:0]in;
  wire invalid_len_event_2;
  wire m_axi_gmem1_WLAST;
  wire m_axi_gmem1_WREADY;
  wire m_axi_gmem1_WREADY_0;
  wire \mem_reg[4][0]_srl5_n_0 ;
  wire \mem_reg[4][1]_srl5_n_0 ;
  wire \mem_reg[4][2]_srl5_n_0 ;
  wire \mem_reg[4][3]_srl5_n_0 ;
  wire \pout[0]_i_1_n_0 ;
  wire \pout[1]_i_1_n_0 ;
  wire \pout[2]_i_1_n_0 ;
  wire \pout_reg_n_0_[0] ;
  wire \pout_reg_n_0_[1] ;
  wire \pout_reg_n_0_[2] ;
  wire push;
  wire [3:0]q__0;
  wire rdreq;
  wire \sect_len_buf_reg[7] ;
  wire wrreq24_out;

  (* SOFT_HLUTNM = "soft_lutpair165" *) 
  LUT4 #(
    .INIT(16'hBA8A)) 
    \bus_equal_gen.WLAST_Dummy_i_1 
       (.I0(rdreq),
        .I1(m_axi_gmem1_WREADY),
        .I2(\bus_equal_gen.WVALID_Dummy_reg ),
        .I3(m_axi_gmem1_WLAST),
        .O(m_axi_gmem1_WREADY_0));
  LUT5 #(
    .INIT(32'h00000002)) 
    \bus_equal_gen.WLAST_Dummy_i_2 
       (.I0(E),
        .I1(\bus_equal_gen.WLAST_Dummy_i_3_n_0 ),
        .I2(\bus_equal_gen.WLAST_Dummy_i_4_n_0 ),
        .I3(empty_n_tmp_reg_1[6]),
        .I4(empty_n_tmp_reg_1[7]),
        .O(rdreq));
  LUT4 #(
    .INIT(16'hEFFE)) 
    \bus_equal_gen.WLAST_Dummy_i_3 
       (.I0(empty_n_tmp_reg_1[5]),
        .I1(empty_n_tmp_reg_1[4]),
        .I2(q__0[3]),
        .I3(empty_n_tmp_reg_1[3]),
        .O(\bus_equal_gen.WLAST_Dummy_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    \bus_equal_gen.WLAST_Dummy_i_4 
       (.I0(q__0[0]),
        .I1(empty_n_tmp_reg_1[0]),
        .I2(empty_n_tmp_reg_1[1]),
        .I3(q__0[1]),
        .I4(empty_n_tmp_reg_1[2]),
        .I5(q__0[2]),
        .O(\bus_equal_gen.WLAST_Dummy_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair163" *) 
  LUT4 #(
    .INIT(16'h8F88)) 
    \bus_equal_gen.WVALID_Dummy_i_1 
       (.I0(burst_valid),
        .I1(if_empty_n),
        .I2(m_axi_gmem1_WREADY),
        .I3(\bus_equal_gen.WVALID_Dummy_reg ),
        .O(empty_n_tmp_reg_0));
  (* SOFT_HLUTNM = "soft_lutpair163" *) 
  LUT4 #(
    .INIT(16'hD000)) 
    \bus_equal_gen.data_buf[31]_i_1 
       (.I0(\bus_equal_gen.WVALID_Dummy_reg ),
        .I1(m_axi_gmem1_WREADY),
        .I2(burst_valid),
        .I3(if_empty_n),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair165" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \bus_equal_gen.len_cnt[7]_i_1 
       (.I0(rdreq),
        .I1(ap_rst_n),
        .O(SR));
  LUT6 #(
    .INIT(64'h00000000F2222222)) 
    \could_multi_bursts.AWVALID_Dummy_i_1 
       (.I0(\could_multi_bursts.AWVALID_Dummy_reg_0 ),
        .I1(\could_multi_bursts.AWVALID_Dummy_reg_1 ),
        .I2(fifo_burst_ready),
        .I3(\could_multi_bursts.AWVALID_Dummy_reg_2 ),
        .I4(full_n0_in),
        .I5(invalid_len_event_2),
        .O(\could_multi_bursts.AWVALID_Dummy_reg ));
  (* SOFT_HLUTNM = "soft_lutpair166" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \could_multi_bursts.awlen_buf[0]_i_1 
       (.I0(Q[0]),
        .I1(\sect_len_buf_reg[7] ),
        .O(in[0]));
  (* SOFT_HLUTNM = "soft_lutpair167" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \could_multi_bursts.awlen_buf[1]_i_1 
       (.I0(Q[1]),
        .I1(\sect_len_buf_reg[7] ),
        .O(in[1]));
  (* SOFT_HLUTNM = "soft_lutpair167" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \could_multi_bursts.awlen_buf[2]_i_1 
       (.I0(Q[2]),
        .I1(\sect_len_buf_reg[7] ),
        .O(in[2]));
  (* SOFT_HLUTNM = "soft_lutpair166" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \could_multi_bursts.awlen_buf[3]_i_1 
       (.I0(Q[3]),
        .I1(\sect_len_buf_reg[7] ),
        .O(in[3]));
  LUT2 #(
    .INIT(4'hE)) 
    \could_multi_bursts.awlen_buf[3]_i_2 
       (.I0(\could_multi_bursts.awlen_buf[3]_i_3_n_0 ),
        .I1(\could_multi_bursts.awlen_buf[3]_i_4_n_0 ),
        .O(\sect_len_buf_reg[7] ));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    \could_multi_bursts.awlen_buf[3]_i_3 
       (.I0(Q[7]),
        .I1(\could_multi_bursts.awlen_buf[3]_i_2_0 [3]),
        .I2(\could_multi_bursts.awlen_buf[3]_i_2_0 [4]),
        .I3(Q[8]),
        .I4(\could_multi_bursts.awlen_buf[3]_i_2_0 [5]),
        .I5(Q[9]),
        .O(\could_multi_bursts.awlen_buf[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    \could_multi_bursts.awlen_buf[3]_i_4 
       (.I0(Q[4]),
        .I1(\could_multi_bursts.awlen_buf[3]_i_2_0 [0]),
        .I2(\could_multi_bursts.awlen_buf[3]_i_2_0 [1]),
        .I3(Q[5]),
        .I4(\could_multi_bursts.awlen_buf[3]_i_2_0 [2]),
        .I5(Q[6]),
        .O(\could_multi_bursts.awlen_buf[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFEFFFFAAAAAAAA)) 
    data_vld_i_1__5
       (.I0(push),
        .I1(\pout_reg_n_0_[1] ),
        .I2(\pout_reg_n_0_[0] ),
        .I3(\pout_reg_n_0_[2] ),
        .I4(empty_n_tmp_i_1__2_n_0),
        .I5(data_vld_reg_n_0),
        .O(data_vld_i_1__5_n_0));
  FDRE data_vld_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(data_vld_i_1__5_n_0),
        .Q(data_vld_reg_n_0),
        .R(ARESET));
  LUT2 #(
    .INIT(4'hB)) 
    empty_n_tmp_i_1__2
       (.I0(rdreq),
        .I1(burst_valid),
        .O(empty_n_tmp_i_1__2_n_0));
  FDRE empty_n_tmp_reg
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__2_n_0),
        .D(data_vld_reg_n_0),
        .Q(burst_valid),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFBFBFBFBFBBBFBFB)) 
    full_n_tmp_i_1__3
       (.I0(full_n_tmp_i_2__5_n_0),
        .I1(ap_rst_n),
        .I2(fifo_burst_ready),
        .I3(\pout_reg_n_0_[2] ),
        .I4(full_n_tmp_i_3__2_n_0),
        .I5(full_n_tmp_i_4__1_n_0),
        .O(full_n_tmp_i_1__3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair164" *) 
  LUT2 #(
    .INIT(4'h8)) 
    full_n_tmp_i_2__5
       (.I0(empty_n_tmp_i_1__2_n_0),
        .I1(data_vld_reg_n_0),
        .O(full_n_tmp_i_2__5_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    full_n_tmp_i_3__2
       (.I0(\pout_reg_n_0_[0] ),
        .I1(\pout_reg_n_0_[1] ),
        .O(full_n_tmp_i_3__2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair164" *) 
  LUT4 #(
    .INIT(16'hFBFF)) 
    full_n_tmp_i_4__1
       (.I0(invalid_len_event_2),
        .I1(wrreq24_out),
        .I2(empty_n_tmp_i_1__2_n_0),
        .I3(data_vld_reg_n_0),
        .O(full_n_tmp_i_4__1_n_0));
  FDRE full_n_tmp_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(full_n_tmp_i_1__3_n_0),
        .Q(fifo_burst_ready),
        .R(1'b0));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/bus_equal_gen.fifo_burst/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/bus_equal_gen.fifo_burst/mem_reg[4][0]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][0]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(in[0]),
        .Q(\mem_reg[4][0]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/bus_equal_gen.fifo_burst/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/bus_equal_gen.fifo_burst/mem_reg[4][1]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][1]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(in[1]),
        .Q(\mem_reg[4][1]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/bus_equal_gen.fifo_burst/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/bus_equal_gen.fifo_burst/mem_reg[4][2]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][2]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(in[2]),
        .Q(\mem_reg[4][2]_srl5_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/bus_equal_gen.fifo_burst/mem_reg[4] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/bus_equal_gen.fifo_burst/mem_reg[4][3]_srl5 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[4][3]_srl5 
       (.A0(\pout_reg_n_0_[0] ),
        .A1(\pout_reg_n_0_[1] ),
        .A2(\pout_reg_n_0_[2] ),
        .A3(1'b0),
        .CE(push),
        .CLK(ap_clk),
        .D(in[3]),
        .Q(\mem_reg[4][3]_srl5_n_0 ));
  LUT6 #(
    .INIT(64'hF0FF0FFF0F00E000)) 
    \pout[0]_i_1 
       (.I0(\pout_reg_n_0_[1] ),
        .I1(\pout_reg_n_0_[2] ),
        .I2(empty_n_tmp_i_1__2_n_0),
        .I3(data_vld_reg_n_0),
        .I4(push),
        .I5(\pout_reg_n_0_[0] ),
        .O(\pout[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF7F7BFBF08084000)) 
    \pout[1]_i_1 
       (.I0(push),
        .I1(data_vld_reg_n_0),
        .I2(empty_n_tmp_i_1__2_n_0),
        .I3(\pout_reg_n_0_[2] ),
        .I4(\pout_reg_n_0_[0] ),
        .I5(\pout_reg_n_0_[1] ),
        .O(\pout[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF708FF00FF00BF00)) 
    \pout[2]_i_1 
       (.I0(push),
        .I1(data_vld_reg_n_0),
        .I2(empty_n_tmp_i_1__2_n_0),
        .I3(\pout_reg_n_0_[2] ),
        .I4(\pout_reg_n_0_[0] ),
        .I5(\pout_reg_n_0_[1] ),
        .O(\pout[2]_i_1_n_0 ));
  FDRE \pout_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\pout[0]_i_1_n_0 ),
        .Q(\pout_reg_n_0_[0] ),
        .R(ARESET));
  FDRE \pout_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\pout[1]_i_1_n_0 ),
        .Q(\pout_reg_n_0_[1] ),
        .R(ARESET));
  FDRE \pout_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\pout[2]_i_1_n_0 ),
        .Q(\pout_reg_n_0_[2] ),
        .R(ARESET));
  FDRE \q_reg[0] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__2_n_0),
        .D(\mem_reg[4][0]_srl5_n_0 ),
        .Q(q__0[0]),
        .R(ARESET));
  FDRE \q_reg[1] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__2_n_0),
        .D(\mem_reg[4][1]_srl5_n_0 ),
        .Q(q__0[1]),
        .R(ARESET));
  FDRE \q_reg[2] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__2_n_0),
        .D(\mem_reg[4][2]_srl5_n_0 ),
        .Q(q__0[2]),
        .R(ARESET));
  FDRE \q_reg[3] 
       (.C(ap_clk),
        .CE(empty_n_tmp_i_1__2_n_0),
        .D(\mem_reg[4][3]_srl5_n_0 ),
        .Q(q__0[3]),
        .R(ARESET));
endmodule

(* ORIG_REF_NAME = "filter_gmem1_m_axi_fifo" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_fifo__parameterized3
   (full_n0_in,
    SR,
    last_sect_buf,
    ap_rst_n_0,
    wrreq24_out,
    D,
    rdreq33_out,
    push,
    next_resp0,
    push_0,
    wreq_handling_reg,
    \could_multi_bursts.sect_handling_reg ,
    \could_multi_bursts.last_sect_buf_reg ,
    ap_clk,
    ARESET,
    ap_rst_n,
    CO,
    next_resp,
    Q,
    plusOp__1,
    \sect_cnt_reg[0] ,
    wreq_handling_reg_0,
    wreq_handling_reg_1,
    wreq_handling_reg_2,
    fifo_wreq_valid,
    \could_multi_bursts.sect_handling_reg_0 ,
    \could_multi_bursts.sect_handling_reg_1 ,
    in,
    \could_multi_bursts.loop_cnt_reg[0] ,
    m_axi_gmem1_AWREADY,
    \could_multi_bursts.loop_cnt_reg[0]_0 ,
    \could_multi_bursts.loop_cnt_reg[0]_1 ,
    \could_multi_bursts.last_sect_buf_reg_0 ,
    fifo_burst_ready,
    m_axi_gmem1_BVALID,
    next_resp_reg);
  output full_n0_in;
  output [0:0]SR;
  output last_sect_buf;
  output [0:0]ap_rst_n_0;
  output wrreq24_out;
  output [19:0]D;
  output rdreq33_out;
  output push;
  output next_resp0;
  output push_0;
  output wreq_handling_reg;
  output \could_multi_bursts.sect_handling_reg ;
  output \could_multi_bursts.last_sect_buf_reg ;
  input ap_clk;
  input ARESET;
  input ap_rst_n;
  input [0:0]CO;
  input next_resp;
  input [19:0]Q;
  input [18:0]plusOp__1;
  input [0:0]\sect_cnt_reg[0] ;
  input wreq_handling_reg_0;
  input [0:0]wreq_handling_reg_1;
  input wreq_handling_reg_2;
  input fifo_wreq_valid;
  input \could_multi_bursts.sect_handling_reg_0 ;
  input \could_multi_bursts.sect_handling_reg_1 ;
  input [0:0]in;
  input \could_multi_bursts.loop_cnt_reg[0] ;
  input m_axi_gmem1_AWREADY;
  input \could_multi_bursts.loop_cnt_reg[0]_0 ;
  input \could_multi_bursts.loop_cnt_reg[0]_1 ;
  input \could_multi_bursts.last_sect_buf_reg_0 ;
  input fifo_burst_ready;
  input m_axi_gmem1_BVALID;
  input next_resp_reg;

  wire ARESET;
  wire [0:0]CO;
  wire [19:0]D;
  wire [19:0]Q;
  wire [0:0]SR;
  wire ap_clk;
  wire ap_rst_n;
  wire [0:0]ap_rst_n_0;
  wire aw2b_awdata1;
  wire [1:0]aw2b_bdata;
  wire \could_multi_bursts.awaddr_buf[31]_i_4_n_0 ;
  wire \could_multi_bursts.last_sect_buf_reg ;
  wire \could_multi_bursts.last_sect_buf_reg_0 ;
  wire \could_multi_bursts.loop_cnt_reg[0] ;
  wire \could_multi_bursts.loop_cnt_reg[0]_0 ;
  wire \could_multi_bursts.loop_cnt_reg[0]_1 ;
  wire \could_multi_bursts.sect_handling_reg ;
  wire \could_multi_bursts.sect_handling_reg_0 ;
  wire \could_multi_bursts.sect_handling_reg_1 ;
  wire data_vld_i_1__3_n_0;
  wire data_vld_reg_n_0;
  wire empty_n_tmp_i_1__4_n_0;
  wire fifo_burst_ready;
  wire fifo_wreq_valid;
  wire full_n0_in;
  wire full_n_tmp_i_1__4_n_0;
  wire full_n_tmp_i_2__4_n_0;
  wire [0:0]in;
  wire last_sect_buf;
  wire m_axi_gmem1_AWREADY;
  wire m_axi_gmem1_BVALID;
  wire \mem_reg[14][0]_srl15_n_0 ;
  wire \mem_reg[14][1]_srl15_n_0 ;
  wire need_wrsp;
  wire next_resp;
  wire next_resp0;
  wire next_resp_reg;
  wire [18:0]plusOp__1;
  wire \pout[0]_i_1__0_n_0 ;
  wire \pout[1]_i_1__0_n_0 ;
  wire \pout[2]_i_1__0_n_0 ;
  wire \pout[3]_i_1_n_0 ;
  wire \pout[3]_i_2__0_n_0 ;
  wire \pout[3]_i_3__0_n_0 ;
  wire \pout[3]_i_4__0_n_0 ;
  wire [3:0]pout_reg__0;
  wire push;
  wire push_0;
  wire \q[1]_i_1_n_0 ;
  wire rdreq33_out;
  wire [0:0]\sect_cnt_reg[0] ;
  wire wreq_handling_reg;
  wire wreq_handling_reg_0;
  wire [0:0]wreq_handling_reg_1;
  wire wreq_handling_reg_2;
  wire wrreq24_out;

  LUT5 #(
    .INIT(32'h0000555D)) 
    \could_multi_bursts.awaddr_buf[31]_i_1 
       (.I0(\could_multi_bursts.loop_cnt_reg[0] ),
        .I1(m_axi_gmem1_AWREADY),
        .I2(\could_multi_bursts.loop_cnt_reg[0]_0 ),
        .I3(\could_multi_bursts.loop_cnt_reg[0]_1 ),
        .I4(\could_multi_bursts.awaddr_buf[31]_i_4_n_0 ),
        .O(wrreq24_out));
  LUT3 #(
    .INIT(8'h7F)) 
    \could_multi_bursts.awaddr_buf[31]_i_4 
       (.I0(full_n0_in),
        .I1(\could_multi_bursts.sect_handling_reg_1 ),
        .I2(fifo_burst_ready),
        .O(\could_multi_bursts.awaddr_buf[31]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair183" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.last_sect_buf_i_1 
       (.I0(wreq_handling_reg_1),
        .I1(last_sect_buf),
        .I2(\could_multi_bursts.last_sect_buf_reg_0 ),
        .O(\could_multi_bursts.last_sect_buf_reg ));
  (* SOFT_HLUTNM = "soft_lutpair182" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \could_multi_bursts.loop_cnt[5]_i_1__0 
       (.I0(last_sect_buf),
        .I1(ap_rst_n),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair172" *) 
  LUT4 #(
    .INIT(16'hEECE)) 
    \could_multi_bursts.sect_handling_i_1__0 
       (.I0(\could_multi_bursts.sect_handling_reg_1 ),
        .I1(wreq_handling_reg_0),
        .I2(wrreq24_out),
        .I3(\could_multi_bursts.sect_handling_reg_0 ),
        .O(\could_multi_bursts.sect_handling_reg ));
  (* SOFT_HLUTNM = "soft_lutpair171" *) 
  LUT5 #(
    .INIT(32'hBABAFABA)) 
    data_vld_i_1__3
       (.I0(wrreq24_out),
        .I1(\pout[3]_i_3__0_n_0 ),
        .I2(data_vld_reg_n_0),
        .I3(need_wrsp),
        .I4(next_resp),
        .O(data_vld_i_1__3_n_0));
  FDRE data_vld_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(data_vld_i_1__3_n_0),
        .Q(data_vld_reg_n_0),
        .R(ARESET));
  LUT3 #(
    .INIT(8'hBA)) 
    empty_n_tmp_i_1__4
       (.I0(data_vld_reg_n_0),
        .I1(next_resp),
        .I2(need_wrsp),
        .O(empty_n_tmp_i_1__4_n_0));
  FDRE empty_n_tmp_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(empty_n_tmp_i_1__4_n_0),
        .Q(need_wrsp),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair168" *) 
  LUT5 #(
    .INIT(32'hD5D5D500)) 
    fifo_wreq_valid_buf_i_1
       (.I0(wreq_handling_reg_0),
        .I1(wreq_handling_reg_1),
        .I2(last_sect_buf),
        .I3(wreq_handling_reg_2),
        .I4(fifo_wreq_valid),
        .O(rdreq33_out));
  LUT5 #(
    .INIT(32'hFFFFB0FF)) 
    full_n_tmp_i_1__4
       (.I0(next_resp),
        .I1(need_wrsp),
        .I2(data_vld_reg_n_0),
        .I3(ap_rst_n),
        .I4(full_n_tmp_i_2__4_n_0),
        .O(full_n_tmp_i_1__4_n_0));
  LUT6 #(
    .INIT(64'hAA8AAAAAAAAAAAAA)) 
    full_n_tmp_i_2__4
       (.I0(full_n0_in),
        .I1(\pout[3]_i_4__0_n_0 ),
        .I2(pout_reg__0[0]),
        .I3(pout_reg__0[1]),
        .I4(pout_reg__0[3]),
        .I5(pout_reg__0[2]),
        .O(full_n_tmp_i_2__4_n_0));
  FDRE full_n_tmp_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(full_n_tmp_i_1__4_n_0),
        .Q(full_n0_in),
        .R(1'b0));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_resp/mem_reg[14] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_resp/mem_reg[14][0]_srl15 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[14][0]_srl15 
       (.A0(pout_reg__0[0]),
        .A1(pout_reg__0[1]),
        .A2(pout_reg__0[2]),
        .A3(pout_reg__0[3]),
        .CE(wrreq24_out),
        .CLK(ap_clk),
        .D(in),
        .Q(\mem_reg[14][0]_srl15_n_0 ));
  (* srl_bus_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_resp/mem_reg[14] " *) 
  (* srl_name = "U0/\filter_gmem1_m_axi_U/bus_write/fifo_resp/mem_reg[14][1]_srl15 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    \mem_reg[14][1]_srl15 
       (.A0(pout_reg__0[0]),
        .A1(pout_reg__0[1]),
        .A2(pout_reg__0[2]),
        .A3(pout_reg__0[3]),
        .CE(wrreq24_out),
        .CLK(ap_clk),
        .D(aw2b_awdata1),
        .Q(\mem_reg[14][1]_srl15_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair183" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \mem_reg[14][1]_srl15_i_1 
       (.I0(\could_multi_bursts.last_sect_buf_reg_0 ),
        .I1(\could_multi_bursts.sect_handling_reg_0 ),
        .O(aw2b_awdata1));
  LUT2 #(
    .INIT(4'h2)) 
    \mem_reg[4][0]_srl5_i_1__0 
       (.I0(wrreq24_out),
        .I1(in),
        .O(push));
  LUT5 #(
    .INIT(32'hFF404040)) 
    next_resp_i_1
       (.I0(next_resp),
        .I1(need_wrsp),
        .I2(aw2b_bdata[0]),
        .I3(m_axi_gmem1_BVALID),
        .I4(next_resp_reg),
        .O(next_resp0));
  (* SOFT_HLUTNM = "soft_lutpair169" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \pout[0]_i_1__0 
       (.I0(pout_reg__0[0]),
        .O(\pout[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair169" *) 
  LUT5 #(
    .INIT(32'hDF2020DF)) 
    \pout[1]_i_1__0 
       (.I0(need_wrsp),
        .I1(next_resp),
        .I2(wrreq24_out),
        .I3(pout_reg__0[0]),
        .I4(pout_reg__0[1]),
        .O(\pout[1]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hB4F0F04BF0F0F00F)) 
    \pout[2]_i_1__0 
       (.I0(next_resp),
        .I1(need_wrsp),
        .I2(pout_reg__0[2]),
        .I3(pout_reg__0[1]),
        .I4(pout_reg__0[0]),
        .I5(wrreq24_out),
        .O(\pout[2]_i_1__0_n_0 ));
  LUT5 #(
    .INIT(32'hE0000000)) 
    \pout[2]_i_2__1 
       (.I0(aw2b_bdata[1]),
        .I1(aw2b_bdata[0]),
        .I2(next_resp_reg),
        .I3(next_resp),
        .I4(need_wrsp),
        .O(push_0));
  LUT5 #(
    .INIT(32'h20006500)) 
    \pout[3]_i_1 
       (.I0(wrreq24_out),
        .I1(next_resp),
        .I2(need_wrsp),
        .I3(data_vld_reg_n_0),
        .I4(\pout[3]_i_3__0_n_0 ),
        .O(\pout[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair170" *) 
  LUT5 #(
    .INIT(32'h9AAAAAA6)) 
    \pout[3]_i_2__0 
       (.I0(pout_reg__0[3]),
        .I1(\pout[3]_i_4__0_n_0 ),
        .I2(pout_reg__0[0]),
        .I3(pout_reg__0[1]),
        .I4(pout_reg__0[2]),
        .O(\pout[3]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair170" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \pout[3]_i_3__0 
       (.I0(pout_reg__0[0]),
        .I1(pout_reg__0[1]),
        .I2(pout_reg__0[3]),
        .I3(pout_reg__0[2]),
        .O(\pout[3]_i_3__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair171" *) 
  LUT4 #(
    .INIT(16'hDFFF)) 
    \pout[3]_i_4__0 
       (.I0(wrreq24_out),
        .I1(next_resp),
        .I2(need_wrsp),
        .I3(data_vld_reg_n_0),
        .O(\pout[3]_i_4__0_n_0 ));
  FDRE \pout_reg[0] 
       (.C(ap_clk),
        .CE(\pout[3]_i_1_n_0 ),
        .D(\pout[0]_i_1__0_n_0 ),
        .Q(pout_reg__0[0]),
        .R(ARESET));
  FDRE \pout_reg[1] 
       (.C(ap_clk),
        .CE(\pout[3]_i_1_n_0 ),
        .D(\pout[1]_i_1__0_n_0 ),
        .Q(pout_reg__0[1]),
        .R(ARESET));
  FDRE \pout_reg[2] 
       (.C(ap_clk),
        .CE(\pout[3]_i_1_n_0 ),
        .D(\pout[2]_i_1__0_n_0 ),
        .Q(pout_reg__0[2]),
        .R(ARESET));
  FDRE \pout_reg[3] 
       (.C(ap_clk),
        .CE(\pout[3]_i_1_n_0 ),
        .D(\pout[3]_i_2__0_n_0 ),
        .Q(pout_reg__0[3]),
        .R(ARESET));
  LUT2 #(
    .INIT(4'hB)) 
    \q[1]_i_1 
       (.I0(next_resp),
        .I1(need_wrsp),
        .O(\q[1]_i_1_n_0 ));
  FDRE \q_reg[0] 
       (.C(ap_clk),
        .CE(\q[1]_i_1_n_0 ),
        .D(\mem_reg[14][0]_srl15_n_0 ),
        .Q(aw2b_bdata[0]),
        .R(ARESET));
  FDRE \q_reg[1] 
       (.C(ap_clk),
        .CE(\q[1]_i_1_n_0 ),
        .D(\mem_reg[14][1]_srl15_n_0 ),
        .Q(aw2b_bdata[1]),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair182" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \sect_addr_buf[11]_i_1__0 
       (.I0(CO),
        .I1(last_sect_buf),
        .I2(ap_rst_n),
        .O(ap_rst_n_0));
  (* SOFT_HLUTNM = "soft_lutpair181" *) 
  LUT3 #(
    .INIT(8'h8B)) 
    \sect_cnt[0]_i_1__0 
       (.I0(Q[0]),
        .I1(rdreq33_out),
        .I2(\sect_cnt_reg[0] ),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair177" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[10]_i_1__0 
       (.I0(Q[10]),
        .I1(rdreq33_out),
        .I2(plusOp__1[9]),
        .O(D[10]));
  (* SOFT_HLUTNM = "soft_lutpair176" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[11]_i_1__0 
       (.I0(Q[11]),
        .I1(rdreq33_out),
        .I2(plusOp__1[10]),
        .O(D[11]));
  (* SOFT_HLUTNM = "soft_lutpair180" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[12]_i_1__0 
       (.I0(Q[12]),
        .I1(rdreq33_out),
        .I2(plusOp__1[11]),
        .O(D[12]));
  (* SOFT_HLUTNM = "soft_lutpair179" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[13]_i_1__0 
       (.I0(Q[13]),
        .I1(rdreq33_out),
        .I2(plusOp__1[12]),
        .O(D[13]));
  (* SOFT_HLUTNM = "soft_lutpair178" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[14]_i_1__0 
       (.I0(Q[14]),
        .I1(rdreq33_out),
        .I2(plusOp__1[13]),
        .O(D[14]));
  (* SOFT_HLUTNM = "soft_lutpair177" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[15]_i_1__0 
       (.I0(Q[15]),
        .I1(rdreq33_out),
        .I2(plusOp__1[14]),
        .O(D[15]));
  (* SOFT_HLUTNM = "soft_lutpair176" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[16]_i_1__0 
       (.I0(Q[16]),
        .I1(rdreq33_out),
        .I2(plusOp__1[15]),
        .O(D[16]));
  (* SOFT_HLUTNM = "soft_lutpair175" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[17]_i_1__0 
       (.I0(Q[17]),
        .I1(rdreq33_out),
        .I2(plusOp__1[16]),
        .O(D[17]));
  (* SOFT_HLUTNM = "soft_lutpair174" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[18]_i_1__0 
       (.I0(Q[18]),
        .I1(rdreq33_out),
        .I2(plusOp__1[17]),
        .O(D[18]));
  (* SOFT_HLUTNM = "soft_lutpair173" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[19]_i_2__0 
       (.I0(Q[19]),
        .I1(rdreq33_out),
        .I2(plusOp__1[18]),
        .O(D[19]));
  (* SOFT_HLUTNM = "soft_lutpair184" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[1]_i_1__0 
       (.I0(Q[1]),
        .I1(rdreq33_out),
        .I2(plusOp__1[0]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair179" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[2]_i_1__0 
       (.I0(Q[2]),
        .I1(rdreq33_out),
        .I2(plusOp__1[1]),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair184" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[3]_i_1__0 
       (.I0(Q[3]),
        .I1(rdreq33_out),
        .I2(plusOp__1[2]),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair181" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[4]_i_1__0 
       (.I0(Q[4]),
        .I1(rdreq33_out),
        .I2(plusOp__1[3]),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair180" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[5]_i_1__0 
       (.I0(Q[5]),
        .I1(rdreq33_out),
        .I2(plusOp__1[4]),
        .O(D[5]));
  (* SOFT_HLUTNM = "soft_lutpair175" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[6]_i_1__0 
       (.I0(Q[6]),
        .I1(rdreq33_out),
        .I2(plusOp__1[5]),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair178" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[7]_i_1__0 
       (.I0(Q[7]),
        .I1(rdreq33_out),
        .I2(plusOp__1[6]),
        .O(D[7]));
  (* SOFT_HLUTNM = "soft_lutpair173" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[8]_i_1__0 
       (.I0(Q[8]),
        .I1(rdreq33_out),
        .I2(plusOp__1[7]),
        .O(D[8]));
  (* SOFT_HLUTNM = "soft_lutpair174" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_cnt[9]_i_1__0 
       (.I0(Q[9]),
        .I1(rdreq33_out),
        .I2(plusOp__1[8]),
        .O(D[9]));
  (* SOFT_HLUTNM = "soft_lutpair172" *) 
  LUT4 #(
    .INIT(16'h08AA)) 
    \sect_len_buf[9]_i_1 
       (.I0(wreq_handling_reg_0),
        .I1(wrreq24_out),
        .I2(\could_multi_bursts.sect_handling_reg_0 ),
        .I3(\could_multi_bursts.sect_handling_reg_1 ),
        .O(last_sect_buf));
  (* SOFT_HLUTNM = "soft_lutpair168" *) 
  LUT4 #(
    .INIT(16'hCEEE)) 
    wreq_handling_i_1
       (.I0(wreq_handling_reg_0),
        .I1(wreq_handling_reg_2),
        .I2(wreq_handling_reg_1),
        .I3(last_sect_buf),
        .O(wreq_handling_reg));
endmodule

(* ORIG_REF_NAME = "filter_gmem1_m_axi_fifo" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_fifo__parameterized5
   (full_n_tmp_reg_0,
    empty_n_tmp_reg_0,
    empty_n_tmp_reg_1,
    D,
    ap_clk,
    ARESET,
    Q,
    push,
    ap_rst_n);
  output full_n_tmp_reg_0;
  output empty_n_tmp_reg_0;
  output [0:0]empty_n_tmp_reg_1;
  output [0:0]D;
  input ap_clk;
  input ARESET;
  input [1:0]Q;
  input push;
  input ap_rst_n;

  wire ARESET;
  wire [0:0]D;
  wire [1:0]Q;
  wire ap_clk;
  wire ap_rst_n;
  wire data_vld_i_1__4_n_0;
  wire data_vld_reg_n_0;
  wire empty_n_tmp_i_1__3_n_0;
  wire empty_n_tmp_reg_0;
  wire [0:0]empty_n_tmp_reg_1;
  wire full_n_tmp_i_1__5_n_0;
  wire full_n_tmp_i_2__3_n_0;
  wire full_n_tmp_i_3__1_n_0;
  wire full_n_tmp_i_4__2_n_0;
  wire full_n_tmp_reg_0;
  wire \pout[0]_i_1__1_n_0 ;
  wire \pout[1]_i_1_n_0 ;
  wire \pout[2]_i_1_n_0 ;
  wire \pout[2]_i_3__0_n_0 ;
  wire \pout_reg_n_0_[0] ;
  wire \pout_reg_n_0_[1] ;
  wire \pout_reg_n_0_[2] ;
  wire push;

  (* SOFT_HLUTNM = "soft_lutpair187" *) 
  LUT3 #(
    .INIT(8'hF4)) 
    \ap_CS_fsm[14]_i_1 
       (.I0(empty_n_tmp_reg_0),
        .I1(Q[1]),
        .I2(Q[0]),
        .O(D));
  LUT6 #(
    .INIT(64'hFFFEFFFFAAAAAAAA)) 
    data_vld_i_1__4
       (.I0(push),
        .I1(\pout_reg_n_0_[1] ),
        .I2(\pout_reg_n_0_[0] ),
        .I3(\pout_reg_n_0_[2] ),
        .I4(full_n_tmp_i_2__3_n_0),
        .I5(data_vld_reg_n_0),
        .O(data_vld_i_1__4_n_0));
  FDRE data_vld_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(data_vld_i_1__4_n_0),
        .Q(data_vld_reg_n_0),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair186" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    empty_n_tmp_i_1__3
       (.I0(data_vld_reg_n_0),
        .I1(Q[1]),
        .I2(empty_n_tmp_reg_0),
        .O(empty_n_tmp_i_1__3_n_0));
  FDRE empty_n_tmp_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(empty_n_tmp_i_1__3_n_0),
        .Q(empty_n_tmp_reg_0),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFBBBFBFBFBFBFBFB)) 
    full_n_tmp_i_1__5
       (.I0(full_n_tmp_i_2__3_n_0),
        .I1(ap_rst_n),
        .I2(full_n_tmp_reg_0),
        .I3(\pout_reg_n_0_[2] ),
        .I4(full_n_tmp_i_3__1_n_0),
        .I5(full_n_tmp_i_4__2_n_0),
        .O(full_n_tmp_i_1__5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair185" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    full_n_tmp_i_2__3
       (.I0(data_vld_reg_n_0),
        .I1(empty_n_tmp_reg_0),
        .I2(Q[1]),
        .O(full_n_tmp_i_2__3_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    full_n_tmp_i_3__1
       (.I0(\pout_reg_n_0_[0] ),
        .I1(\pout_reg_n_0_[1] ),
        .O(full_n_tmp_i_3__1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair185" *) 
  LUT4 #(
    .INIT(16'h2000)) 
    full_n_tmp_i_4__2
       (.I0(push),
        .I1(Q[1]),
        .I2(empty_n_tmp_reg_0),
        .I3(data_vld_reg_n_0),
        .O(full_n_tmp_i_4__2_n_0));
  FDRE full_n_tmp_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(full_n_tmp_i_1__5_n_0),
        .Q(full_n_tmp_reg_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair186" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \j_reg_104[29]_i_1 
       (.I0(empty_n_tmp_reg_0),
        .I1(Q[1]),
        .O(empty_n_tmp_reg_1));
  LUT6 #(
    .INIT(64'h9F9F60609F9F6020)) 
    \pout[0]_i_1__1 
       (.I0(push),
        .I1(\pout[2]_i_3__0_n_0 ),
        .I2(data_vld_reg_n_0),
        .I3(\pout_reg_n_0_[2] ),
        .I4(\pout_reg_n_0_[0] ),
        .I5(\pout_reg_n_0_[1] ),
        .O(\pout[0]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'hDFDFBFBF20204000)) 
    \pout[1]_i_1 
       (.I0(push),
        .I1(\pout[2]_i_3__0_n_0 ),
        .I2(data_vld_reg_n_0),
        .I3(\pout_reg_n_0_[2] ),
        .I4(\pout_reg_n_0_[0] ),
        .I5(\pout_reg_n_0_[1] ),
        .O(\pout[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hDF20FF00FF00BF00)) 
    \pout[2]_i_1 
       (.I0(push),
        .I1(\pout[2]_i_3__0_n_0 ),
        .I2(data_vld_reg_n_0),
        .I3(\pout_reg_n_0_[2] ),
        .I4(\pout_reg_n_0_[0] ),
        .I5(\pout_reg_n_0_[1] ),
        .O(\pout[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair187" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \pout[2]_i_3__0 
       (.I0(Q[1]),
        .I1(empty_n_tmp_reg_0),
        .O(\pout[2]_i_3__0_n_0 ));
  FDRE \pout_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\pout[0]_i_1__1_n_0 ),
        .Q(\pout_reg_n_0_[0] ),
        .R(ARESET));
  FDRE \pout_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\pout[1]_i_1_n_0 ),
        .Q(\pout_reg_n_0_[1] ),
        .R(ARESET));
  FDRE \pout_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\pout[2]_i_1_n_0 ),
        .Q(\pout_reg_n_0_[2] ),
        .R(ARESET));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_read
   (full_n_reg,
    ARESET,
    ap_clk,
    m_axi_gmem1_RVALID);
  output full_n_reg;
  input ARESET;
  input ap_clk;
  input m_axi_gmem1_RVALID;

  wire ARESET;
  wire ap_clk;
  wire \bus_equal_gen.rdata_valid_t_reg_n_0 ;
  wire fifo_rdata_n_1;
  wire full_n_reg;
  wire m_axi_gmem1_RVALID;
  wire s_ready;

  FDRE \bus_equal_gen.rdata_valid_t_reg 
       (.C(ap_clk),
        .CE(1'b1),
        .D(fifo_rdata_n_1),
        .Q(\bus_equal_gen.rdata_valid_t_reg_n_0 ),
        .R(ARESET));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_buffer__parameterized1 fifo_rdata
       (.ARESET(ARESET),
        .ap_clk(ap_clk),
        .dout_valid_reg_0(fifo_rdata_n_1),
        .dout_valid_reg_1(\bus_equal_gen.rdata_valid_t_reg_n_0 ),
        .full_n_reg_0(full_n_reg),
        .m_axi_gmem1_RVALID(m_axi_gmem1_RVALID),
        .s_ready(s_ready));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_reg_slice__parameterized2 rs_rdata
       (.ARESET(ARESET),
        .ap_clk(ap_clk),
        .s_ready(s_ready),
        .s_ready_t_reg_0(\bus_equal_gen.rdata_valid_t_reg_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_reg_slice
   (s_ready_t_reg_0,
    E,
    \state_reg[0]_0 ,
    \data_p1_reg[29]_0 ,
    ARESET,
    ap_clk,
    ap_reg_ioackin_gmem1_AWREADY,
    \data_p2_reg[0]_0 ,
    Q,
    I_AWVALID,
    rs2f_wreq_ack,
    \data_p2_reg[29]_0 );
  output s_ready_t_reg_0;
  output [0:0]E;
  output [0:0]\state_reg[0]_0 ;
  output [29:0]\data_p1_reg[29]_0 ;
  input ARESET;
  input ap_clk;
  input ap_reg_ioackin_gmem1_AWREADY;
  input [0:0]\data_p2_reg[0]_0 ;
  input [0:0]Q;
  input I_AWVALID;
  input rs2f_wreq_ack;
  input [29:0]\data_p2_reg[29]_0 ;

  wire ARESET;
  wire [0:0]E;
  wire I_AWVALID;
  wire [0:0]Q;
  wire ap_clk;
  wire ap_reg_ioackin_gmem1_AWREADY;
  wire \data_p1[0]_i_1__1_n_0 ;
  wire \data_p1[10]_i_1__0_n_0 ;
  wire \data_p1[11]_i_1__0_n_0 ;
  wire \data_p1[12]_i_1__0_n_0 ;
  wire \data_p1[13]_i_1__0_n_0 ;
  wire \data_p1[14]_i_1__0_n_0 ;
  wire \data_p1[15]_i_1__0_n_0 ;
  wire \data_p1[16]_i_1__0_n_0 ;
  wire \data_p1[17]_i_1__0_n_0 ;
  wire \data_p1[18]_i_1__0_n_0 ;
  wire \data_p1[19]_i_1__0_n_0 ;
  wire \data_p1[1]_i_1__1_n_0 ;
  wire \data_p1[20]_i_1__0_n_0 ;
  wire \data_p1[21]_i_1__0_n_0 ;
  wire \data_p1[22]_i_1__0_n_0 ;
  wire \data_p1[23]_i_1__0_n_0 ;
  wire \data_p1[24]_i_1__0_n_0 ;
  wire \data_p1[25]_i_1__0_n_0 ;
  wire \data_p1[26]_i_1__0_n_0 ;
  wire \data_p1[27]_i_1__0_n_0 ;
  wire \data_p1[28]_i_1__0_n_0 ;
  wire \data_p1[29]_i_2_n_0 ;
  wire \data_p1[2]_i_1__1_n_0 ;
  wire \data_p1[3]_i_1__1_n_0 ;
  wire \data_p1[4]_i_1__1_n_0 ;
  wire \data_p1[5]_i_1__1_n_0 ;
  wire \data_p1[6]_i_1__1_n_0 ;
  wire \data_p1[7]_i_1__1_n_0 ;
  wire \data_p1[8]_i_1__0_n_0 ;
  wire \data_p1[9]_i_1__0_n_0 ;
  wire [29:0]\data_p1_reg[29]_0 ;
  wire [29:0]data_p2;
  wire [0:0]\data_p2_reg[0]_0 ;
  wire [29:0]\data_p2_reg[29]_0 ;
  wire load_p1;
  wire load_p2;
  wire [1:0]next_st__0;
  wire rs2f_wreq_ack;
  wire s_ready_t_i_1__1_n_0;
  wire s_ready_t_reg_0;
  wire [1:1]state;
  wire \state[0]_i_1__1_n_0 ;
  wire \state[1]_i_1__1_n_0 ;
  wire [1:0]state__0;
  wire [0:0]\state_reg[0]_0 ;

  LUT4 #(
    .INIT(16'h002C)) 
    \FSM_sequential_state[0]_i_1__2 
       (.I0(I_AWVALID),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(rs2f_wreq_ack),
        .O(next_st__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair191" *) 
  LUT5 #(
    .INIT(32'h0CF80308)) 
    \FSM_sequential_state[1]_i_1__1 
       (.I0(s_ready_t_reg_0),
        .I1(I_AWVALID),
        .I2(state__0[0]),
        .I3(state__0[1]),
        .I4(rs2f_wreq_ack),
        .O(next_st__0[1]));
  (* FSM_ENCODED_STATES = "zero:00,two:01,one:10" *) 
  FDRE \FSM_sequential_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(next_st__0[0]),
        .Q(state__0[0]),
        .R(ARESET));
  (* FSM_ENCODED_STATES = "zero:00,two:01,one:10" *) 
  FDRE \FSM_sequential_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(next_st__0[1]),
        .Q(state__0[1]),
        .R(ARESET));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[0]_i_1__1 
       (.I0(\data_p2_reg[29]_0 [0]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[0]),
        .O(\data_p1[0]_i_1__1_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[10]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [10]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[10]),
        .O(\data_p1[10]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[11]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [11]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[11]),
        .O(\data_p1[11]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[12]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [12]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[12]),
        .O(\data_p1[12]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[13]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [13]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[13]),
        .O(\data_p1[13]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[14]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [14]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[14]),
        .O(\data_p1[14]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[15]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [15]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[15]),
        .O(\data_p1[15]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[16]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [16]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[16]),
        .O(\data_p1[16]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[17]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [17]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[17]),
        .O(\data_p1[17]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[18]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [18]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[18]),
        .O(\data_p1[18]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[19]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [19]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[19]),
        .O(\data_p1[19]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[1]_i_1__1 
       (.I0(\data_p2_reg[29]_0 [1]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[1]),
        .O(\data_p1[1]_i_1__1_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[20]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [20]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[20]),
        .O(\data_p1[20]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[21]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [21]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[21]),
        .O(\data_p1[21]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[22]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [22]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[22]),
        .O(\data_p1[22]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[23]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [23]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[23]),
        .O(\data_p1[23]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[24]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [24]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[24]),
        .O(\data_p1[24]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[25]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [25]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[25]),
        .O(\data_p1[25]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[26]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [26]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[26]),
        .O(\data_p1[26]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[27]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [27]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[27]),
        .O(\data_p1[27]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[28]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [28]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[28]),
        .O(\data_p1[28]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h404D404040404040)) 
    \data_p1[29]_i_1__0 
       (.I0(state__0[1]),
        .I1(rs2f_wreq_ack),
        .I2(state__0[0]),
        .I3(ap_reg_ioackin_gmem1_AWREADY),
        .I4(\data_p2_reg[0]_0 ),
        .I5(Q),
        .O(load_p1));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[29]_i_2 
       (.I0(\data_p2_reg[29]_0 [29]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[29]),
        .O(\data_p1[29]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[2]_i_1__1 
       (.I0(\data_p2_reg[29]_0 [2]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[2]),
        .O(\data_p1[2]_i_1__1_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[3]_i_1__1 
       (.I0(\data_p2_reg[29]_0 [3]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[3]),
        .O(\data_p1[3]_i_1__1_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[4]_i_1__1 
       (.I0(\data_p2_reg[29]_0 [4]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[4]),
        .O(\data_p1[4]_i_1__1_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[5]_i_1__1 
       (.I0(\data_p2_reg[29]_0 [5]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[5]),
        .O(\data_p1[5]_i_1__1_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[6]_i_1__1 
       (.I0(\data_p2_reg[29]_0 [6]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[6]),
        .O(\data_p1[6]_i_1__1_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[7]_i_1__1 
       (.I0(\data_p2_reg[29]_0 [7]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[7]),
        .O(\data_p1[7]_i_1__1_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[8]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [8]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[8]),
        .O(\data_p1[8]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \data_p1[9]_i_1__0 
       (.I0(\data_p2_reg[29]_0 [9]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(data_p2[9]),
        .O(\data_p1[9]_i_1__0_n_0 ));
  FDRE \data_p1_reg[0] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[0]_i_1__1_n_0 ),
        .Q(\data_p1_reg[29]_0 [0]),
        .R(1'b0));
  FDRE \data_p1_reg[10] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[10]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [10]),
        .R(1'b0));
  FDRE \data_p1_reg[11] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[11]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [11]),
        .R(1'b0));
  FDRE \data_p1_reg[12] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[12]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [12]),
        .R(1'b0));
  FDRE \data_p1_reg[13] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[13]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [13]),
        .R(1'b0));
  FDRE \data_p1_reg[14] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[14]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [14]),
        .R(1'b0));
  FDRE \data_p1_reg[15] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[15]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [15]),
        .R(1'b0));
  FDRE \data_p1_reg[16] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[16]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [16]),
        .R(1'b0));
  FDRE \data_p1_reg[17] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[17]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [17]),
        .R(1'b0));
  FDRE \data_p1_reg[18] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[18]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [18]),
        .R(1'b0));
  FDRE \data_p1_reg[19] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[19]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [19]),
        .R(1'b0));
  FDRE \data_p1_reg[1] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[1]_i_1__1_n_0 ),
        .Q(\data_p1_reg[29]_0 [1]),
        .R(1'b0));
  FDRE \data_p1_reg[20] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[20]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [20]),
        .R(1'b0));
  FDRE \data_p1_reg[21] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[21]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [21]),
        .R(1'b0));
  FDRE \data_p1_reg[22] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[22]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [22]),
        .R(1'b0));
  FDRE \data_p1_reg[23] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[23]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [23]),
        .R(1'b0));
  FDRE \data_p1_reg[24] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[24]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [24]),
        .R(1'b0));
  FDRE \data_p1_reg[25] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[25]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [25]),
        .R(1'b0));
  FDRE \data_p1_reg[26] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[26]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [26]),
        .R(1'b0));
  FDRE \data_p1_reg[27] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[27]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [27]),
        .R(1'b0));
  FDRE \data_p1_reg[28] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[28]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [28]),
        .R(1'b0));
  FDRE \data_p1_reg[29] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[29]_i_2_n_0 ),
        .Q(\data_p1_reg[29]_0 [29]),
        .R(1'b0));
  FDRE \data_p1_reg[2] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[2]_i_1__1_n_0 ),
        .Q(\data_p1_reg[29]_0 [2]),
        .R(1'b0));
  FDRE \data_p1_reg[3] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[3]_i_1__1_n_0 ),
        .Q(\data_p1_reg[29]_0 [3]),
        .R(1'b0));
  FDRE \data_p1_reg[4] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[4]_i_1__1_n_0 ),
        .Q(\data_p1_reg[29]_0 [4]),
        .R(1'b0));
  FDRE \data_p1_reg[5] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[5]_i_1__1_n_0 ),
        .Q(\data_p1_reg[29]_0 [5]),
        .R(1'b0));
  FDRE \data_p1_reg[6] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[6]_i_1__1_n_0 ),
        .Q(\data_p1_reg[29]_0 [6]),
        .R(1'b0));
  FDRE \data_p1_reg[7] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[7]_i_1__1_n_0 ),
        .Q(\data_p1_reg[29]_0 [7]),
        .R(1'b0));
  FDRE \data_p1_reg[8] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[8]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [8]),
        .R(1'b0));
  FDRE \data_p1_reg[9] 
       (.C(ap_clk),
        .CE(load_p1),
        .D(\data_p1[9]_i_1__0_n_0 ),
        .Q(\data_p1_reg[29]_0 [9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h2000)) 
    \data_p2[29]_i_1 
       (.I0(s_ready_t_reg_0),
        .I1(ap_reg_ioackin_gmem1_AWREADY),
        .I2(\data_p2_reg[0]_0 ),
        .I3(Q),
        .O(load_p2));
  FDRE \data_p2_reg[0] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [0]),
        .Q(data_p2[0]),
        .R(1'b0));
  FDRE \data_p2_reg[10] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [10]),
        .Q(data_p2[10]),
        .R(1'b0));
  FDRE \data_p2_reg[11] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [11]),
        .Q(data_p2[11]),
        .R(1'b0));
  FDRE \data_p2_reg[12] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [12]),
        .Q(data_p2[12]),
        .R(1'b0));
  FDRE \data_p2_reg[13] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [13]),
        .Q(data_p2[13]),
        .R(1'b0));
  FDRE \data_p2_reg[14] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [14]),
        .Q(data_p2[14]),
        .R(1'b0));
  FDRE \data_p2_reg[15] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [15]),
        .Q(data_p2[15]),
        .R(1'b0));
  FDRE \data_p2_reg[16] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [16]),
        .Q(data_p2[16]),
        .R(1'b0));
  FDRE \data_p2_reg[17] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [17]),
        .Q(data_p2[17]),
        .R(1'b0));
  FDRE \data_p2_reg[18] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [18]),
        .Q(data_p2[18]),
        .R(1'b0));
  FDRE \data_p2_reg[19] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [19]),
        .Q(data_p2[19]),
        .R(1'b0));
  FDRE \data_p2_reg[1] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [1]),
        .Q(data_p2[1]),
        .R(1'b0));
  FDRE \data_p2_reg[20] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [20]),
        .Q(data_p2[20]),
        .R(1'b0));
  FDRE \data_p2_reg[21] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [21]),
        .Q(data_p2[21]),
        .R(1'b0));
  FDRE \data_p2_reg[22] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [22]),
        .Q(data_p2[22]),
        .R(1'b0));
  FDRE \data_p2_reg[23] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [23]),
        .Q(data_p2[23]),
        .R(1'b0));
  FDRE \data_p2_reg[24] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [24]),
        .Q(data_p2[24]),
        .R(1'b0));
  FDRE \data_p2_reg[25] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [25]),
        .Q(data_p2[25]),
        .R(1'b0));
  FDRE \data_p2_reg[26] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [26]),
        .Q(data_p2[26]),
        .R(1'b0));
  FDRE \data_p2_reg[27] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [27]),
        .Q(data_p2[27]),
        .R(1'b0));
  FDRE \data_p2_reg[28] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [28]),
        .Q(data_p2[28]),
        .R(1'b0));
  FDRE \data_p2_reg[29] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [29]),
        .Q(data_p2[29]),
        .R(1'b0));
  FDRE \data_p2_reg[2] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [2]),
        .Q(data_p2[2]),
        .R(1'b0));
  FDRE \data_p2_reg[3] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [3]),
        .Q(data_p2[3]),
        .R(1'b0));
  FDRE \data_p2_reg[4] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [4]),
        .Q(data_p2[4]),
        .R(1'b0));
  FDRE \data_p2_reg[5] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [5]),
        .Q(data_p2[5]),
        .R(1'b0));
  FDRE \data_p2_reg[6] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [6]),
        .Q(data_p2[6]),
        .R(1'b0));
  FDRE \data_p2_reg[7] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [7]),
        .Q(data_p2[7]),
        .R(1'b0));
  FDRE \data_p2_reg[8] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [8]),
        .Q(data_p2[8]),
        .R(1'b0));
  FDRE \data_p2_reg[9] 
       (.C(ap_clk),
        .CE(load_p2),
        .D(\data_p2_reg[29]_0 [9]),
        .Q(data_p2[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hE000)) 
    \gmem0_addr_read_reg_195[7]_i_1 
       (.I0(ap_reg_ioackin_gmem1_AWREADY),
        .I1(s_ready_t_reg_0),
        .I2(\data_p2_reg[0]_0 ),
        .I3(Q),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair191" *) 
  LUT5 #(
    .INIT(32'hFFF73033)) 
    s_ready_t_i_1__1
       (.I0(I_AWVALID),
        .I1(state__0[1]),
        .I2(rs2f_wreq_ack),
        .I3(state__0[0]),
        .I4(s_ready_t_reg_0),
        .O(s_ready_t_i_1__1_n_0));
  FDRE s_ready_t_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(s_ready_t_i_1__1_n_0),
        .Q(s_ready_t_reg_0),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hFC4CCC4C)) 
    \state[0]_i_1__1 
       (.I0(rs2f_wreq_ack),
        .I1(\state_reg[0]_0 ),
        .I2(state),
        .I3(I_AWVALID),
        .I4(s_ready_t_reg_0),
        .O(\state[0]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFF700FFFF)) 
    \state[1]_i_1__1 
       (.I0(Q),
        .I1(\data_p2_reg[0]_0 ),
        .I2(ap_reg_ioackin_gmem1_AWREADY),
        .I3(state),
        .I4(\state_reg[0]_0 ),
        .I5(rs2f_wreq_ack),
        .O(\state[1]_i_1__1_n_0 ));
  FDRE \state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\state[0]_i_1__1_n_0 ),
        .Q(\state_reg[0]_0 ),
        .R(ARESET));
  FDSE \state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\state[1]_i_1__1_n_0 ),
        .Q(state),
        .S(ARESET));
endmodule

(* ORIG_REF_NAME = "filter_gmem1_m_axi_reg_slice" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_reg_slice__parameterized2
   (s_ready,
    ARESET,
    ap_clk,
    s_ready_t_reg_0);
  output s_ready;
  input ARESET;
  input ap_clk;
  input s_ready_t_reg_0;

  wire ARESET;
  wire ap_clk;
  wire [1:0]next_st__0;
  wire s_ready;
  wire s_ready_t_i_1__2_n_0;
  wire s_ready_t_reg_0;
  wire [1:0]state__0;

  LUT3 #(
    .INIT(8'h2C)) 
    \FSM_sequential_state[0]_i_1__1 
       (.I0(s_ready_t_reg_0),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .O(next_st__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair138" *) 
  LUT4 #(
    .INIT(16'h1140)) 
    \FSM_sequential_state[1]_i_1__2 
       (.I0(state__0[0]),
        .I1(s_ready_t_reg_0),
        .I2(s_ready),
        .I3(state__0[1]),
        .O(next_st__0[1]));
  (* FSM_ENCODED_STATES = "zero:00,two:01,one:10" *) 
  FDRE \FSM_sequential_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(next_st__0[0]),
        .Q(state__0[0]),
        .R(ARESET));
  (* FSM_ENCODED_STATES = "zero:00,two:01,one:10" *) 
  FDRE \FSM_sequential_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(next_st__0[1]),
        .Q(state__0[1]),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair138" *) 
  LUT4 #(
    .INIT(16'hBF05)) 
    s_ready_t_i_1__2
       (.I0(state__0[0]),
        .I1(s_ready_t_reg_0),
        .I2(state__0[1]),
        .I3(s_ready),
        .O(s_ready_t_i_1__2_n_0));
  FDRE s_ready_t_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(s_ready_t_i_1__2_n_0),
        .Q(s_ready),
        .R(ARESET));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_throttl
   (Q,
    m_axi_gmem1_AWVALID,
    \conservative_gen.throttl_cnt_reg[1]_0 ,
    m_axi_gmem1_AWREADY_0,
    \conservative_gen.throttl_cnt_reg[4]_0 ,
    \conservative_gen.throttl_cnt_reg[6]_0 ,
    D,
    \conservative_gen.throttl_cnt_reg[2]_0 ,
    AWLEN,
    AWVALID_Dummy,
    m_axi_gmem1_AWREADY,
    ARESET,
    E,
    ap_clk);
  output [1:0]Q;
  output m_axi_gmem1_AWVALID;
  output \conservative_gen.throttl_cnt_reg[1]_0 ;
  output m_axi_gmem1_AWREADY_0;
  output \conservative_gen.throttl_cnt_reg[4]_0 ;
  output \conservative_gen.throttl_cnt_reg[6]_0 ;
  input [1:0]D;
  input \conservative_gen.throttl_cnt_reg[2]_0 ;
  input [1:0]AWLEN;
  input AWVALID_Dummy;
  input m_axi_gmem1_AWREADY;
  input ARESET;
  input [0:0]E;
  input ap_clk;

  wire ARESET;
  wire [1:0]AWLEN;
  wire AWVALID_Dummy;
  wire [1:0]D;
  wire [0:0]E;
  wire [1:0]Q;
  wire ap_clk;
  wire \conservative_gen.throttl_cnt[7]_i_5_n_0 ;
  wire \conservative_gen.throttl_cnt_reg[1]_0 ;
  wire \conservative_gen.throttl_cnt_reg[2]_0 ;
  wire \conservative_gen.throttl_cnt_reg[4]_0 ;
  wire \conservative_gen.throttl_cnt_reg[6]_0 ;
  wire [7:2]\conservative_gen.throttl_cnt_reg__0 ;
  wire m_axi_gmem1_AWREADY;
  wire m_axi_gmem1_AWREADY_0;
  wire m_axi_gmem1_AWVALID;
  wire [7:2]p_0_in;

  LUT5 #(
    .INIT(32'hDDD0000D)) 
    \conservative_gen.throttl_cnt[2]_i_1 
       (.I0(\conservative_gen.throttl_cnt_reg[2]_0 ),
        .I1(AWLEN[0]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(\conservative_gen.throttl_cnt_reg__0 [2]),
        .O(p_0_in[2]));
  LUT6 #(
    .INIT(64'hFE010000FE01FE01)) 
    \conservative_gen.throttl_cnt[3]_i_1 
       (.I0(\conservative_gen.throttl_cnt_reg__0 [2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\conservative_gen.throttl_cnt_reg__0 [3]),
        .I4(AWLEN[1]),
        .I5(\conservative_gen.throttl_cnt_reg[2]_0 ),
        .O(p_0_in[3]));
  LUT6 #(
    .INIT(64'h00000000FFFE0001)) 
    \conservative_gen.throttl_cnt[4]_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(\conservative_gen.throttl_cnt_reg__0 [3]),
        .I3(\conservative_gen.throttl_cnt_reg__0 [2]),
        .I4(\conservative_gen.throttl_cnt_reg__0 [4]),
        .I5(\conservative_gen.throttl_cnt_reg[2]_0 ),
        .O(p_0_in[4]));
  LUT3 #(
    .INIT(8'h06)) 
    \conservative_gen.throttl_cnt[5]_i_1 
       (.I0(\conservative_gen.throttl_cnt[7]_i_5_n_0 ),
        .I1(\conservative_gen.throttl_cnt_reg__0 [5]),
        .I2(\conservative_gen.throttl_cnt_reg[2]_0 ),
        .O(p_0_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair229" *) 
  LUT4 #(
    .INIT(16'h00D2)) 
    \conservative_gen.throttl_cnt[6]_i_1 
       (.I0(\conservative_gen.throttl_cnt[7]_i_5_n_0 ),
        .I1(\conservative_gen.throttl_cnt_reg__0 [5]),
        .I2(\conservative_gen.throttl_cnt_reg__0 [6]),
        .I3(\conservative_gen.throttl_cnt_reg[2]_0 ),
        .O(p_0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair229" *) 
  LUT5 #(
    .INIT(32'h0000FD02)) 
    \conservative_gen.throttl_cnt[7]_i_2 
       (.I0(\conservative_gen.throttl_cnt[7]_i_5_n_0 ),
        .I1(\conservative_gen.throttl_cnt_reg__0 [6]),
        .I2(\conservative_gen.throttl_cnt_reg__0 [5]),
        .I3(\conservative_gen.throttl_cnt_reg__0 [7]),
        .I4(\conservative_gen.throttl_cnt_reg[2]_0 ),
        .O(p_0_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair228" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \conservative_gen.throttl_cnt[7]_i_3 
       (.I0(\conservative_gen.throttl_cnt_reg[1]_0 ),
        .I1(\conservative_gen.throttl_cnt_reg__0 [4]),
        .I2(\conservative_gen.throttl_cnt_reg__0 [7]),
        .I3(\conservative_gen.throttl_cnt_reg__0 [5]),
        .I4(\conservative_gen.throttl_cnt_reg__0 [6]),
        .O(\conservative_gen.throttl_cnt_reg[4]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair230" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \conservative_gen.throttl_cnt[7]_i_5 
       (.I0(\conservative_gen.throttl_cnt_reg__0 [4]),
        .I1(\conservative_gen.throttl_cnt_reg__0 [2]),
        .I2(\conservative_gen.throttl_cnt_reg__0 [3]),
        .I3(Q[0]),
        .I4(Q[1]),
        .O(\conservative_gen.throttl_cnt[7]_i_5_n_0 ));
  FDRE \conservative_gen.throttl_cnt_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(D[0]),
        .Q(Q[0]),
        .R(ARESET));
  FDRE \conservative_gen.throttl_cnt_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(D[1]),
        .Q(Q[1]),
        .R(ARESET));
  FDRE \conservative_gen.throttl_cnt_reg[2] 
       (.C(ap_clk),
        .CE(E),
        .D(p_0_in[2]),
        .Q(\conservative_gen.throttl_cnt_reg__0 [2]),
        .R(ARESET));
  FDRE \conservative_gen.throttl_cnt_reg[3] 
       (.C(ap_clk),
        .CE(E),
        .D(p_0_in[3]),
        .Q(\conservative_gen.throttl_cnt_reg__0 [3]),
        .R(ARESET));
  FDRE \conservative_gen.throttl_cnt_reg[4] 
       (.C(ap_clk),
        .CE(E),
        .D(p_0_in[4]),
        .Q(\conservative_gen.throttl_cnt_reg__0 [4]),
        .R(ARESET));
  FDRE \conservative_gen.throttl_cnt_reg[5] 
       (.C(ap_clk),
        .CE(E),
        .D(p_0_in[5]),
        .Q(\conservative_gen.throttl_cnt_reg__0 [5]),
        .R(ARESET));
  FDRE \conservative_gen.throttl_cnt_reg[6] 
       (.C(ap_clk),
        .CE(E),
        .D(p_0_in[6]),
        .Q(\conservative_gen.throttl_cnt_reg__0 [6]),
        .R(ARESET));
  FDRE \conservative_gen.throttl_cnt_reg[7] 
       (.C(ap_clk),
        .CE(E),
        .D(p_0_in[7]),
        .Q(\conservative_gen.throttl_cnt_reg__0 [7]),
        .R(ARESET));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \could_multi_bursts.AWVALID_Dummy_i_2 
       (.I0(m_axi_gmem1_AWREADY),
        .I1(\conservative_gen.throttl_cnt_reg__0 [6]),
        .I2(\conservative_gen.throttl_cnt_reg__0 [5]),
        .I3(\conservative_gen.throttl_cnt_reg__0 [7]),
        .I4(\conservative_gen.throttl_cnt_reg__0 [4]),
        .I5(\conservative_gen.throttl_cnt_reg[1]_0 ),
        .O(m_axi_gmem1_AWREADY_0));
  (* SOFT_HLUTNM = "soft_lutpair228" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \could_multi_bursts.awaddr_buf[31]_i_3 
       (.I0(\conservative_gen.throttl_cnt_reg__0 [6]),
        .I1(\conservative_gen.throttl_cnt_reg__0 [5]),
        .I2(\conservative_gen.throttl_cnt_reg__0 [7]),
        .I3(\conservative_gen.throttl_cnt_reg__0 [4]),
        .O(\conservative_gen.throttl_cnt_reg[6]_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    m_axi_gmem1_AWVALID_INST_0
       (.I0(AWVALID_Dummy),
        .I1(\conservative_gen.throttl_cnt_reg__0 [6]),
        .I2(\conservative_gen.throttl_cnt_reg__0 [5]),
        .I3(\conservative_gen.throttl_cnt_reg__0 [7]),
        .I4(\conservative_gen.throttl_cnt_reg__0 [4]),
        .I5(\conservative_gen.throttl_cnt_reg[1]_0 ),
        .O(m_axi_gmem1_AWVALID));
  (* SOFT_HLUTNM = "soft_lutpair230" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    m_axi_gmem1_AWVALID_INST_0_i_1
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(\conservative_gen.throttl_cnt_reg__0 [3]),
        .I3(\conservative_gen.throttl_cnt_reg__0 [2]),
        .O(\conservative_gen.throttl_cnt_reg[1]_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_write
   (s_ready_t_reg,
    full_n_tmp_reg,
    empty_n_tmp_reg,
    \bus_equal_gen.WVALID_Dummy_reg_0 ,
    m_axi_gmem1_WLAST,
    AWVALID_Dummy,
    D,
    E,
    empty_n_tmp_reg_0,
    m_axi_gmem1_AWADDR,
    \could_multi_bursts.awlen_buf_reg[3]_0 ,
    \could_multi_bursts.awlen_buf_reg[1]_0 ,
    \could_multi_bursts.AWVALID_Dummy_reg_0 ,
    \bus_equal_gen.WVALID_Dummy_reg_1 ,
    m_axi_gmem1_WDATA,
    m_axi_gmem1_WSTRB,
    ap_clk,
    \q_tmp_reg[31] ,
    Q,
    ARESET,
    ap_reg_ioackin_gmem1_AWREADY,
    \data_p2_reg[0] ,
    ap_rst_n,
    \could_multi_bursts.AWVALID_Dummy_reg_1 ,
    \conservative_gen.throttl_cnt_reg[1] ,
    m_axi_gmem1_WREADY,
    \conservative_gen.throttl_cnt_reg[7] ,
    m_axi_gmem1_AWREADY,
    \could_multi_bursts.loop_cnt_reg[0]_0 ,
    \could_multi_bursts.loop_cnt_reg[0]_1 ,
    I_AWVALID,
    m_axi_gmem1_BVALID,
    \data_p2_reg[29] );
  output s_ready_t_reg;
  output full_n_tmp_reg;
  output empty_n_tmp_reg;
  output \bus_equal_gen.WVALID_Dummy_reg_0 ;
  output m_axi_gmem1_WLAST;
  output AWVALID_Dummy;
  output [2:0]D;
  output [0:0]E;
  output [0:0]empty_n_tmp_reg_0;
  output [29:0]m_axi_gmem1_AWADDR;
  output [3:0]\could_multi_bursts.awlen_buf_reg[3]_0 ;
  output [1:0]\could_multi_bursts.awlen_buf_reg[1]_0 ;
  output \could_multi_bursts.AWVALID_Dummy_reg_0 ;
  output [0:0]\bus_equal_gen.WVALID_Dummy_reg_1 ;
  output [31:0]m_axi_gmem1_WDATA;
  output [3:0]m_axi_gmem1_WSTRB;
  input ap_clk;
  input [7:0]\q_tmp_reg[31] ;
  input [3:0]Q;
  input ARESET;
  input ap_reg_ioackin_gmem1_AWREADY;
  input [0:0]\data_p2_reg[0] ;
  input ap_rst_n;
  input \could_multi_bursts.AWVALID_Dummy_reg_1 ;
  input [1:0]\conservative_gen.throttl_cnt_reg[1] ;
  input m_axi_gmem1_WREADY;
  input \conservative_gen.throttl_cnt_reg[7] ;
  input m_axi_gmem1_AWREADY;
  input \could_multi_bursts.loop_cnt_reg[0]_0 ;
  input \could_multi_bursts.loop_cnt_reg[0]_1 ;
  input I_AWVALID;
  input m_axi_gmem1_BVALID;
  input [29:0]\data_p2_reg[29] ;

  wire ARESET;
  wire AWVALID_Dummy;
  wire [2:0]D;
  wire [0:0]E;
  wire I_AWVALID;
  wire [3:0]Q;
  wire align_len0;
  wire align_len2;
  wire \align_len_reg_n_0_[2] ;
  wire \align_len_reg_n_0_[31] ;
  wire ap_clk;
  wire ap_reg_ioackin_gmem1_AWREADY;
  wire ap_rst_n;
  wire [31:2]awaddr_tmp;
  wire [3:0]beat_len_buf;
  wire buff_wdata_n_10;
  wire buff_wdata_n_11;
  wire buff_wdata_n_12;
  wire buff_wdata_n_13;
  wire buff_wdata_n_14;
  wire buff_wdata_n_15;
  wire buff_wdata_n_16;
  wire buff_wdata_n_17;
  wire buff_wdata_n_18;
  wire buff_wdata_n_19;
  wire buff_wdata_n_20;
  wire buff_wdata_n_21;
  wire buff_wdata_n_22;
  wire buff_wdata_n_23;
  wire buff_wdata_n_24;
  wire buff_wdata_n_25;
  wire buff_wdata_n_26;
  wire buff_wdata_n_27;
  wire buff_wdata_n_28;
  wire buff_wdata_n_29;
  wire buff_wdata_n_30;
  wire buff_wdata_n_31;
  wire buff_wdata_n_32;
  wire buff_wdata_n_33;
  wire buff_wdata_n_34;
  wire buff_wdata_n_35;
  wire buff_wdata_n_36;
  wire buff_wdata_n_37;
  wire buff_wdata_n_38;
  wire buff_wdata_n_7;
  wire buff_wdata_n_8;
  wire buff_wdata_n_9;
  wire burst_valid;
  wire \bus_equal_gen.WVALID_Dummy_reg_0 ;
  wire [0:0]\bus_equal_gen.WVALID_Dummy_reg_1 ;
  wire \bus_equal_gen.fifo_burst_n_10 ;
  wire \bus_equal_gen.fifo_burst_n_11 ;
  wire \bus_equal_gen.fifo_burst_n_2 ;
  wire \bus_equal_gen.fifo_burst_n_3 ;
  wire \bus_equal_gen.fifo_burst_n_8 ;
  wire \bus_equal_gen.len_cnt[7]_i_3_n_0 ;
  wire [7:0]\bus_equal_gen.len_cnt_reg__0 ;
  wire [1:0]\conservative_gen.throttl_cnt_reg[1] ;
  wire \conservative_gen.throttl_cnt_reg[7] ;
  wire \could_multi_bursts.AWVALID_Dummy_reg_0 ;
  wire \could_multi_bursts.AWVALID_Dummy_reg_1 ;
  wire \could_multi_bursts.awaddr_buf[31]_i_5_n_0 ;
  wire \could_multi_bursts.awaddr_buf[4]_i_3_n_0 ;
  wire \could_multi_bursts.awaddr_buf[4]_i_4_n_0 ;
  wire \could_multi_bursts.awaddr_buf[4]_i_5_n_0 ;
  wire \could_multi_bursts.awaddr_buf[8]_i_3_n_0 ;
  wire \could_multi_bursts.awaddr_buf[8]_i_4_n_0 ;
  wire \could_multi_bursts.awaddr_buf_reg[12]_i_2_n_0 ;
  wire \could_multi_bursts.awaddr_buf_reg[12]_i_2_n_1 ;
  wire \could_multi_bursts.awaddr_buf_reg[12]_i_2_n_2 ;
  wire \could_multi_bursts.awaddr_buf_reg[12]_i_2_n_3 ;
  wire \could_multi_bursts.awaddr_buf_reg[16]_i_2_n_0 ;
  wire \could_multi_bursts.awaddr_buf_reg[16]_i_2_n_1 ;
  wire \could_multi_bursts.awaddr_buf_reg[16]_i_2_n_2 ;
  wire \could_multi_bursts.awaddr_buf_reg[16]_i_2_n_3 ;
  wire \could_multi_bursts.awaddr_buf_reg[20]_i_2_n_0 ;
  wire \could_multi_bursts.awaddr_buf_reg[20]_i_2_n_1 ;
  wire \could_multi_bursts.awaddr_buf_reg[20]_i_2_n_2 ;
  wire \could_multi_bursts.awaddr_buf_reg[20]_i_2_n_3 ;
  wire \could_multi_bursts.awaddr_buf_reg[24]_i_2_n_0 ;
  wire \could_multi_bursts.awaddr_buf_reg[24]_i_2_n_1 ;
  wire \could_multi_bursts.awaddr_buf_reg[24]_i_2_n_2 ;
  wire \could_multi_bursts.awaddr_buf_reg[24]_i_2_n_3 ;
  wire \could_multi_bursts.awaddr_buf_reg[28]_i_2_n_0 ;
  wire \could_multi_bursts.awaddr_buf_reg[28]_i_2_n_1 ;
  wire \could_multi_bursts.awaddr_buf_reg[28]_i_2_n_2 ;
  wire \could_multi_bursts.awaddr_buf_reg[28]_i_2_n_3 ;
  wire \could_multi_bursts.awaddr_buf_reg[31]_i_6_n_2 ;
  wire \could_multi_bursts.awaddr_buf_reg[31]_i_6_n_3 ;
  wire \could_multi_bursts.awaddr_buf_reg[4]_i_2_n_0 ;
  wire \could_multi_bursts.awaddr_buf_reg[4]_i_2_n_1 ;
  wire \could_multi_bursts.awaddr_buf_reg[4]_i_2_n_2 ;
  wire \could_multi_bursts.awaddr_buf_reg[4]_i_2_n_3 ;
  wire \could_multi_bursts.awaddr_buf_reg[8]_i_2_n_0 ;
  wire \could_multi_bursts.awaddr_buf_reg[8]_i_2_n_1 ;
  wire \could_multi_bursts.awaddr_buf_reg[8]_i_2_n_2 ;
  wire \could_multi_bursts.awaddr_buf_reg[8]_i_2_n_3 ;
  wire [1:0]\could_multi_bursts.awlen_buf_reg[1]_0 ;
  wire [3:0]\could_multi_bursts.awlen_buf_reg[3]_0 ;
  wire \could_multi_bursts.last_sect_buf_reg_n_0 ;
  wire \could_multi_bursts.loop_cnt_reg[0]_0 ;
  wire \could_multi_bursts.loop_cnt_reg[0]_1 ;
  wire [5:0]\could_multi_bursts.loop_cnt_reg__0 ;
  wire \could_multi_bursts.sect_handling_reg_n_0 ;
  wire [3:0]data;
  wire [31:2]data1;
  wire [0:0]\data_p2_reg[0] ;
  wire [29:0]\data_p2_reg[29] ;
  wire empty_n_tmp_reg;
  wire [0:0]empty_n_tmp_reg_0;
  wire [31:2]end_addr;
  wire \end_addr_buf_reg_n_0_[10] ;
  wire \end_addr_buf_reg_n_0_[11] ;
  wire \end_addr_buf_reg_n_0_[2] ;
  wire \end_addr_buf_reg_n_0_[3] ;
  wire \end_addr_buf_reg_n_0_[4] ;
  wire \end_addr_buf_reg_n_0_[5] ;
  wire \end_addr_buf_reg_n_0_[6] ;
  wire \end_addr_buf_reg_n_0_[7] ;
  wire \end_addr_buf_reg_n_0_[8] ;
  wire \end_addr_buf_reg_n_0_[9] ;
  wire end_addr_carry__0_i_1__0_n_0;
  wire end_addr_carry__0_i_2__0_n_0;
  wire end_addr_carry__0_i_3__0_n_0;
  wire end_addr_carry__0_i_4__0_n_0;
  wire end_addr_carry__0_n_0;
  wire end_addr_carry__0_n_1;
  wire end_addr_carry__0_n_2;
  wire end_addr_carry__0_n_3;
  wire end_addr_carry__1_i_1__0_n_0;
  wire end_addr_carry__1_i_2__0_n_0;
  wire end_addr_carry__1_i_3__0_n_0;
  wire end_addr_carry__1_i_4__0_n_0;
  wire end_addr_carry__1_n_0;
  wire end_addr_carry__1_n_1;
  wire end_addr_carry__1_n_2;
  wire end_addr_carry__1_n_3;
  wire end_addr_carry__2_i_1__0_n_0;
  wire end_addr_carry__2_i_2__0_n_0;
  wire end_addr_carry__2_i_3__0_n_0;
  wire end_addr_carry__2_i_4__0_n_0;
  wire end_addr_carry__2_n_0;
  wire end_addr_carry__2_n_1;
  wire end_addr_carry__2_n_2;
  wire end_addr_carry__2_n_3;
  wire end_addr_carry__3_i_1__0_n_0;
  wire end_addr_carry__3_i_2__0_n_0;
  wire end_addr_carry__3_i_3__0_n_0;
  wire end_addr_carry__3_i_4__0_n_0;
  wire end_addr_carry__3_n_0;
  wire end_addr_carry__3_n_1;
  wire end_addr_carry__3_n_2;
  wire end_addr_carry__3_n_3;
  wire end_addr_carry__4_i_1__0_n_0;
  wire end_addr_carry__4_i_2__0_n_0;
  wire end_addr_carry__4_i_3__0_n_0;
  wire end_addr_carry__4_i_4__0_n_0;
  wire end_addr_carry__4_n_0;
  wire end_addr_carry__4_n_1;
  wire end_addr_carry__4_n_2;
  wire end_addr_carry__4_n_3;
  wire end_addr_carry__5_i_1__0_n_0;
  wire end_addr_carry__5_i_2__0_n_0;
  wire end_addr_carry__5_i_3__0_n_0;
  wire end_addr_carry__5_i_4__0_n_0;
  wire end_addr_carry__5_n_0;
  wire end_addr_carry__5_n_1;
  wire end_addr_carry__5_n_2;
  wire end_addr_carry__5_n_3;
  wire end_addr_carry__6_i_1__0_n_0;
  wire end_addr_carry__6_i_2__0_n_0;
  wire end_addr_carry__6_n_3;
  wire end_addr_carry_i_1__0_n_0;
  wire end_addr_carry_i_2__0_n_0;
  wire end_addr_carry_i_3__0_n_0;
  wire end_addr_carry_i_4__0_n_0;
  wire end_addr_carry_n_0;
  wire end_addr_carry_n_1;
  wire end_addr_carry_n_2;
  wire end_addr_carry_n_3;
  wire fifo_burst_ready;
  wire fifo_resp_n_1;
  wire fifo_resp_n_10;
  wire fifo_resp_n_11;
  wire fifo_resp_n_12;
  wire fifo_resp_n_13;
  wire fifo_resp_n_14;
  wire fifo_resp_n_15;
  wire fifo_resp_n_16;
  wire fifo_resp_n_17;
  wire fifo_resp_n_18;
  wire fifo_resp_n_19;
  wire fifo_resp_n_20;
  wire fifo_resp_n_21;
  wire fifo_resp_n_22;
  wire fifo_resp_n_23;
  wire fifo_resp_n_24;
  wire fifo_resp_n_29;
  wire fifo_resp_n_3;
  wire fifo_resp_n_30;
  wire fifo_resp_n_31;
  wire fifo_resp_n_5;
  wire fifo_resp_n_6;
  wire fifo_resp_n_7;
  wire fifo_resp_n_8;
  wire fifo_resp_n_9;
  wire [32:32]fifo_wreq_data;
  wire fifo_wreq_n_2;
  wire fifo_wreq_n_35;
  wire fifo_wreq_n_36;
  wire fifo_wreq_n_37;
  wire fifo_wreq_n_38;
  wire fifo_wreq_n_39;
  wire fifo_wreq_n_40;
  wire fifo_wreq_n_41;
  wire fifo_wreq_n_42;
  wire fifo_wreq_n_44;
  wire fifo_wreq_valid;
  wire fifo_wreq_valid_buf_reg_n_0;
  wire first_sect;
  wire first_sect_carry__0_i_1__0_n_0;
  wire first_sect_carry__0_i_2__0_n_0;
  wire first_sect_carry__0_i_3__0_n_0;
  wire first_sect_carry__0_n_2;
  wire first_sect_carry__0_n_3;
  wire first_sect_carry_i_1__0_n_0;
  wire first_sect_carry_i_2__0_n_0;
  wire first_sect_carry_i_3__0_n_0;
  wire first_sect_carry_i_4__0_n_0;
  wire first_sect_carry_n_0;
  wire first_sect_carry_n_1;
  wire first_sect_carry_n_2;
  wire first_sect_carry_n_3;
  wire full_n0_in;
  wire full_n_tmp_reg;
  wire if_empty_n;
  wire invalid_len_event;
  wire invalid_len_event_1;
  wire invalid_len_event_2;
  wire last_sect;
  wire last_sect_buf;
  wire last_sect_carry__0_n_2;
  wire last_sect_carry__0_n_3;
  wire last_sect_carry_n_0;
  wire last_sect_carry_n_1;
  wire last_sect_carry_n_2;
  wire last_sect_carry_n_3;
  wire [29:0]m_axi_gmem1_AWADDR;
  wire m_axi_gmem1_AWREADY;
  wire m_axi_gmem1_BVALID;
  wire [31:0]m_axi_gmem1_WDATA;
  wire m_axi_gmem1_WLAST;
  wire m_axi_gmem1_WREADY;
  wire [3:0]m_axi_gmem1_WSTRB;
  wire [31:2]minusOp;
  wire minusOp_carry_n_2;
  wire minusOp_carry_n_3;
  wire next_resp;
  wire next_resp0;
  wire [19:0]p_0_in0_in;
  wire p_13_in;
  wire [5:0]plusOp;
  wire [7:0]plusOp__0;
  wire [19:1]plusOp__1;
  wire plusOp_carry__0_n_0;
  wire plusOp_carry__0_n_1;
  wire plusOp_carry__0_n_2;
  wire plusOp_carry__0_n_3;
  wire plusOp_carry__1_n_0;
  wire plusOp_carry__1_n_1;
  wire plusOp_carry__1_n_2;
  wire plusOp_carry__1_n_3;
  wire plusOp_carry__2_n_0;
  wire plusOp_carry__2_n_1;
  wire plusOp_carry__2_n_2;
  wire plusOp_carry__2_n_3;
  wire plusOp_carry__3_n_2;
  wire plusOp_carry__3_n_3;
  wire plusOp_carry_n_0;
  wire plusOp_carry_n_1;
  wire plusOp_carry_n_2;
  wire plusOp_carry_n_3;
  wire push;
  wire push_0;
  wire [29:0]q;
  wire [7:0]\q_tmp_reg[31] ;
  wire rdreq33_out;
  wire rs2f_wreq_ack;
  wire [29:0]rs2f_wreq_data;
  wire rs2f_wreq_valid;
  wire s_ready_t_reg;
  wire [31:2]sect_addr;
  wire \sect_addr_buf_reg_n_0_[10] ;
  wire \sect_addr_buf_reg_n_0_[11] ;
  wire \sect_addr_buf_reg_n_0_[12] ;
  wire \sect_addr_buf_reg_n_0_[13] ;
  wire \sect_addr_buf_reg_n_0_[14] ;
  wire \sect_addr_buf_reg_n_0_[15] ;
  wire \sect_addr_buf_reg_n_0_[16] ;
  wire \sect_addr_buf_reg_n_0_[17] ;
  wire \sect_addr_buf_reg_n_0_[18] ;
  wire \sect_addr_buf_reg_n_0_[19] ;
  wire \sect_addr_buf_reg_n_0_[20] ;
  wire \sect_addr_buf_reg_n_0_[21] ;
  wire \sect_addr_buf_reg_n_0_[22] ;
  wire \sect_addr_buf_reg_n_0_[23] ;
  wire \sect_addr_buf_reg_n_0_[24] ;
  wire \sect_addr_buf_reg_n_0_[25] ;
  wire \sect_addr_buf_reg_n_0_[26] ;
  wire \sect_addr_buf_reg_n_0_[27] ;
  wire \sect_addr_buf_reg_n_0_[28] ;
  wire \sect_addr_buf_reg_n_0_[29] ;
  wire \sect_addr_buf_reg_n_0_[2] ;
  wire \sect_addr_buf_reg_n_0_[30] ;
  wire \sect_addr_buf_reg_n_0_[31] ;
  wire \sect_addr_buf_reg_n_0_[3] ;
  wire \sect_addr_buf_reg_n_0_[4] ;
  wire \sect_addr_buf_reg_n_0_[5] ;
  wire \sect_addr_buf_reg_n_0_[6] ;
  wire \sect_addr_buf_reg_n_0_[7] ;
  wire \sect_addr_buf_reg_n_0_[8] ;
  wire \sect_addr_buf_reg_n_0_[9] ;
  wire [19:0]sect_cnt;
  wire [9:4]sect_len_buf;
  wire \sect_len_buf[0]_i_1_n_0 ;
  wire \sect_len_buf[1]_i_1_n_0 ;
  wire \sect_len_buf[2]_i_1_n_0 ;
  wire \sect_len_buf[3]_i_1_n_0 ;
  wire \sect_len_buf[4]_i_1_n_0 ;
  wire \sect_len_buf[5]_i_1_n_0 ;
  wire \sect_len_buf[6]_i_1_n_0 ;
  wire \sect_len_buf[7]_i_1_n_0 ;
  wire \sect_len_buf[8]_i_1_n_0 ;
  wire \sect_len_buf[9]_i_2_n_0 ;
  wire \sect_len_buf_reg_n_0_[0] ;
  wire \sect_len_buf_reg_n_0_[1] ;
  wire \sect_len_buf_reg_n_0_[2] ;
  wire \sect_len_buf_reg_n_0_[3] ;
  wire [31:2]start_addr_buf;
  wire \start_addr_reg_n_0_[10] ;
  wire \start_addr_reg_n_0_[11] ;
  wire \start_addr_reg_n_0_[12] ;
  wire \start_addr_reg_n_0_[13] ;
  wire \start_addr_reg_n_0_[14] ;
  wire \start_addr_reg_n_0_[15] ;
  wire \start_addr_reg_n_0_[16] ;
  wire \start_addr_reg_n_0_[17] ;
  wire \start_addr_reg_n_0_[18] ;
  wire \start_addr_reg_n_0_[19] ;
  wire \start_addr_reg_n_0_[20] ;
  wire \start_addr_reg_n_0_[21] ;
  wire \start_addr_reg_n_0_[22] ;
  wire \start_addr_reg_n_0_[23] ;
  wire \start_addr_reg_n_0_[24] ;
  wire \start_addr_reg_n_0_[25] ;
  wire \start_addr_reg_n_0_[26] ;
  wire \start_addr_reg_n_0_[27] ;
  wire \start_addr_reg_n_0_[28] ;
  wire \start_addr_reg_n_0_[29] ;
  wire \start_addr_reg_n_0_[2] ;
  wire \start_addr_reg_n_0_[30] ;
  wire \start_addr_reg_n_0_[31] ;
  wire \start_addr_reg_n_0_[3] ;
  wire \start_addr_reg_n_0_[4] ;
  wire \start_addr_reg_n_0_[5] ;
  wire \start_addr_reg_n_0_[6] ;
  wire \start_addr_reg_n_0_[7] ;
  wire \start_addr_reg_n_0_[8] ;
  wire \start_addr_reg_n_0_[9] ;
  wire [3:0]tmp_strb;
  wire wreq_handling_reg_n_0;
  wire wrreq24_out;
  wire [3:2]\NLW_could_multi_bursts.awaddr_buf_reg[31]_i_6_CO_UNCONNECTED ;
  wire [3:3]\NLW_could_multi_bursts.awaddr_buf_reg[31]_i_6_O_UNCONNECTED ;
  wire [0:0]\NLW_could_multi_bursts.awaddr_buf_reg[4]_i_2_O_UNCONNECTED ;
  wire [0:0]NLW_end_addr_carry_O_UNCONNECTED;
  wire [3:1]NLW_end_addr_carry__6_CO_UNCONNECTED;
  wire [3:2]NLW_end_addr_carry__6_O_UNCONNECTED;
  wire [3:0]NLW_first_sect_carry_O_UNCONNECTED;
  wire [3:3]NLW_first_sect_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_first_sect_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_last_sect_carry_O_UNCONNECTED;
  wire [3:3]NLW_last_sect_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_last_sect_carry__0_O_UNCONNECTED;
  wire [3:2]NLW_minusOp_carry_CO_UNCONNECTED;
  wire [3:0]NLW_minusOp_carry_O_UNCONNECTED;
  wire [3:2]NLW_plusOp_carry__3_CO_UNCONNECTED;
  wire [3:3]NLW_plusOp_carry__3_O_UNCONNECTED;

  FDRE \align_len_reg[2] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(minusOp[2]),
        .Q(\align_len_reg_n_0_[2] ),
        .R(fifo_wreq_n_2));
  FDRE \align_len_reg[31] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(minusOp[31]),
        .Q(\align_len_reg_n_0_[31] ),
        .R(fifo_wreq_n_2));
  FDRE \beat_len_buf_reg[0] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\align_len_reg_n_0_[2] ),
        .Q(beat_len_buf[0]),
        .R(ARESET));
  FDRE \beat_len_buf_reg[3] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\align_len_reg_n_0_[31] ),
        .Q(beat_len_buf[3]),
        .R(ARESET));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_buffer buff_wdata
       (.ARESET(ARESET),
        .D(D[1:0]),
        .Q(Q[1:0]),
        .\ap_CS_fsm_reg[9] (s_ready_t_reg),
        .\ap_CS_fsm_reg[9]_0 (\data_p2_reg[0] ),
        .ap_clk(ap_clk),
        .ap_reg_ioackin_gmem1_AWREADY(ap_reg_ioackin_gmem1_AWREADY),
        .burst_valid(burst_valid),
        .\dout_buf_reg[35]_0 ({tmp_strb,buff_wdata_n_7,buff_wdata_n_8,buff_wdata_n_9,buff_wdata_n_10,buff_wdata_n_11,buff_wdata_n_12,buff_wdata_n_13,buff_wdata_n_14,buff_wdata_n_15,buff_wdata_n_16,buff_wdata_n_17,buff_wdata_n_18,buff_wdata_n_19,buff_wdata_n_20,buff_wdata_n_21,buff_wdata_n_22,buff_wdata_n_23,buff_wdata_n_24,buff_wdata_n_25,buff_wdata_n_26,buff_wdata_n_27,buff_wdata_n_28,buff_wdata_n_29,buff_wdata_n_30,buff_wdata_n_31,buff_wdata_n_32,buff_wdata_n_33,buff_wdata_n_34,buff_wdata_n_35,buff_wdata_n_36,buff_wdata_n_37,buff_wdata_n_38}),
        .dout_valid_reg_0(\bus_equal_gen.WVALID_Dummy_reg_0 ),
        .if_empty_n(if_empty_n),
        .m_axi_gmem1_WREADY(m_axi_gmem1_WREADY),
        .\q_tmp_reg[31]_0 (\q_tmp_reg[31] ));
  FDRE \bus_equal_gen.WLAST_Dummy_reg 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\bus_equal_gen.fifo_burst_n_11 ),
        .Q(m_axi_gmem1_WLAST),
        .R(ARESET));
  FDRE \bus_equal_gen.WVALID_Dummy_reg 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\bus_equal_gen.fifo_burst_n_10 ),
        .Q(\bus_equal_gen.WVALID_Dummy_reg_0 ),
        .R(ARESET));
  FDRE \bus_equal_gen.data_buf_reg[0] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_38),
        .Q(m_axi_gmem1_WDATA[0]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[10] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_28),
        .Q(m_axi_gmem1_WDATA[10]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[11] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_27),
        .Q(m_axi_gmem1_WDATA[11]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[12] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_26),
        .Q(m_axi_gmem1_WDATA[12]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[13] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_25),
        .Q(m_axi_gmem1_WDATA[13]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[14] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_24),
        .Q(m_axi_gmem1_WDATA[14]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[15] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_23),
        .Q(m_axi_gmem1_WDATA[15]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[16] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_22),
        .Q(m_axi_gmem1_WDATA[16]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[17] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_21),
        .Q(m_axi_gmem1_WDATA[17]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[18] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_20),
        .Q(m_axi_gmem1_WDATA[18]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[19] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_19),
        .Q(m_axi_gmem1_WDATA[19]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[1] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_37),
        .Q(m_axi_gmem1_WDATA[1]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[20] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_18),
        .Q(m_axi_gmem1_WDATA[20]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[21] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_17),
        .Q(m_axi_gmem1_WDATA[21]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[22] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_16),
        .Q(m_axi_gmem1_WDATA[22]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[23] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_15),
        .Q(m_axi_gmem1_WDATA[23]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[24] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_14),
        .Q(m_axi_gmem1_WDATA[24]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[25] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_13),
        .Q(m_axi_gmem1_WDATA[25]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[26] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_12),
        .Q(m_axi_gmem1_WDATA[26]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[27] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_11),
        .Q(m_axi_gmem1_WDATA[27]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[28] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_10),
        .Q(m_axi_gmem1_WDATA[28]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[29] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_9),
        .Q(m_axi_gmem1_WDATA[29]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[2] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_36),
        .Q(m_axi_gmem1_WDATA[2]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[30] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_8),
        .Q(m_axi_gmem1_WDATA[30]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[31] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_7),
        .Q(m_axi_gmem1_WDATA[31]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[3] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_35),
        .Q(m_axi_gmem1_WDATA[3]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[4] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_34),
        .Q(m_axi_gmem1_WDATA[4]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[5] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_33),
        .Q(m_axi_gmem1_WDATA[5]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[6] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_32),
        .Q(m_axi_gmem1_WDATA[6]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[7] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_31),
        .Q(m_axi_gmem1_WDATA[7]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[8] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_30),
        .Q(m_axi_gmem1_WDATA[8]),
        .R(1'b0));
  FDRE \bus_equal_gen.data_buf_reg[9] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(buff_wdata_n_29),
        .Q(m_axi_gmem1_WDATA[9]),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_fifo__parameterized1 \bus_equal_gen.fifo_burst 
       (.ARESET(ARESET),
        .E(p_13_in),
        .Q({sect_len_buf,\sect_len_buf_reg_n_0_[3] ,\sect_len_buf_reg_n_0_[2] ,\sect_len_buf_reg_n_0_[1] ,\sect_len_buf_reg_n_0_[0] }),
        .SR(\bus_equal_gen.fifo_burst_n_2 ),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .burst_valid(burst_valid),
        .\bus_equal_gen.WVALID_Dummy_reg (\bus_equal_gen.WVALID_Dummy_reg_0 ),
        .\could_multi_bursts.AWVALID_Dummy_reg (\bus_equal_gen.fifo_burst_n_3 ),
        .\could_multi_bursts.AWVALID_Dummy_reg_0 (AWVALID_Dummy),
        .\could_multi_bursts.AWVALID_Dummy_reg_1 (\could_multi_bursts.AWVALID_Dummy_reg_1 ),
        .\could_multi_bursts.AWVALID_Dummy_reg_2 (\could_multi_bursts.sect_handling_reg_n_0 ),
        .\could_multi_bursts.awlen_buf[3]_i_2_0 (\could_multi_bursts.loop_cnt_reg__0 ),
        .empty_n_tmp_reg_0(\bus_equal_gen.fifo_burst_n_10 ),
        .empty_n_tmp_reg_1(\bus_equal_gen.len_cnt_reg__0 ),
        .fifo_burst_ready(fifo_burst_ready),
        .full_n0_in(full_n0_in),
        .if_empty_n(if_empty_n),
        .in(data),
        .invalid_len_event_2(invalid_len_event_2),
        .m_axi_gmem1_WLAST(m_axi_gmem1_WLAST),
        .m_axi_gmem1_WREADY(m_axi_gmem1_WREADY),
        .m_axi_gmem1_WREADY_0(\bus_equal_gen.fifo_burst_n_11 ),
        .push(push_0),
        .\sect_len_buf_reg[7] (\bus_equal_gen.fifo_burst_n_8 ),
        .wrreq24_out(wrreq24_out));
  LUT1 #(
    .INIT(2'h1)) 
    \bus_equal_gen.len_cnt[0]_i_1 
       (.I0(\bus_equal_gen.len_cnt_reg__0 [0]),
        .O(plusOp__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair222" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \bus_equal_gen.len_cnt[1]_i_1 
       (.I0(\bus_equal_gen.len_cnt_reg__0 [0]),
        .I1(\bus_equal_gen.len_cnt_reg__0 [1]),
        .O(plusOp__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair222" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \bus_equal_gen.len_cnt[2]_i_1 
       (.I0(\bus_equal_gen.len_cnt_reg__0 [2]),
        .I1(\bus_equal_gen.len_cnt_reg__0 [1]),
        .I2(\bus_equal_gen.len_cnt_reg__0 [0]),
        .O(plusOp__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair193" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \bus_equal_gen.len_cnt[3]_i_1 
       (.I0(\bus_equal_gen.len_cnt_reg__0 [3]),
        .I1(\bus_equal_gen.len_cnt_reg__0 [0]),
        .I2(\bus_equal_gen.len_cnt_reg__0 [1]),
        .I3(\bus_equal_gen.len_cnt_reg__0 [2]),
        .O(plusOp__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair193" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \bus_equal_gen.len_cnt[4]_i_1 
       (.I0(\bus_equal_gen.len_cnt_reg__0 [4]),
        .I1(\bus_equal_gen.len_cnt_reg__0 [2]),
        .I2(\bus_equal_gen.len_cnt_reg__0 [1]),
        .I3(\bus_equal_gen.len_cnt_reg__0 [0]),
        .I4(\bus_equal_gen.len_cnt_reg__0 [3]),
        .O(plusOp__0[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \bus_equal_gen.len_cnt[5]_i_1 
       (.I0(\bus_equal_gen.len_cnt_reg__0 [5]),
        .I1(\bus_equal_gen.len_cnt_reg__0 [3]),
        .I2(\bus_equal_gen.len_cnt_reg__0 [0]),
        .I3(\bus_equal_gen.len_cnt_reg__0 [1]),
        .I4(\bus_equal_gen.len_cnt_reg__0 [2]),
        .I5(\bus_equal_gen.len_cnt_reg__0 [4]),
        .O(plusOp__0[5]));
  (* SOFT_HLUTNM = "soft_lutpair220" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \bus_equal_gen.len_cnt[6]_i_1 
       (.I0(\bus_equal_gen.len_cnt_reg__0 [6]),
        .I1(\bus_equal_gen.len_cnt[7]_i_3_n_0 ),
        .O(plusOp__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair220" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \bus_equal_gen.len_cnt[7]_i_2 
       (.I0(\bus_equal_gen.len_cnt_reg__0 [7]),
        .I1(\bus_equal_gen.len_cnt[7]_i_3_n_0 ),
        .I2(\bus_equal_gen.len_cnt_reg__0 [6]),
        .O(plusOp__0[7]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \bus_equal_gen.len_cnt[7]_i_3 
       (.I0(\bus_equal_gen.len_cnt_reg__0 [5]),
        .I1(\bus_equal_gen.len_cnt_reg__0 [3]),
        .I2(\bus_equal_gen.len_cnt_reg__0 [0]),
        .I3(\bus_equal_gen.len_cnt_reg__0 [1]),
        .I4(\bus_equal_gen.len_cnt_reg__0 [2]),
        .I5(\bus_equal_gen.len_cnt_reg__0 [4]),
        .O(\bus_equal_gen.len_cnt[7]_i_3_n_0 ));
  FDRE \bus_equal_gen.len_cnt_reg[0] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(plusOp__0[0]),
        .Q(\bus_equal_gen.len_cnt_reg__0 [0]),
        .R(\bus_equal_gen.fifo_burst_n_2 ));
  FDRE \bus_equal_gen.len_cnt_reg[1] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(plusOp__0[1]),
        .Q(\bus_equal_gen.len_cnt_reg__0 [1]),
        .R(\bus_equal_gen.fifo_burst_n_2 ));
  FDRE \bus_equal_gen.len_cnt_reg[2] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(plusOp__0[2]),
        .Q(\bus_equal_gen.len_cnt_reg__0 [2]),
        .R(\bus_equal_gen.fifo_burst_n_2 ));
  FDRE \bus_equal_gen.len_cnt_reg[3] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(plusOp__0[3]),
        .Q(\bus_equal_gen.len_cnt_reg__0 [3]),
        .R(\bus_equal_gen.fifo_burst_n_2 ));
  FDRE \bus_equal_gen.len_cnt_reg[4] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(plusOp__0[4]),
        .Q(\bus_equal_gen.len_cnt_reg__0 [4]),
        .R(\bus_equal_gen.fifo_burst_n_2 ));
  FDRE \bus_equal_gen.len_cnt_reg[5] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(plusOp__0[5]),
        .Q(\bus_equal_gen.len_cnt_reg__0 [5]),
        .R(\bus_equal_gen.fifo_burst_n_2 ));
  FDRE \bus_equal_gen.len_cnt_reg[6] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(plusOp__0[6]),
        .Q(\bus_equal_gen.len_cnt_reg__0 [6]),
        .R(\bus_equal_gen.fifo_burst_n_2 ));
  FDRE \bus_equal_gen.len_cnt_reg[7] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(plusOp__0[7]),
        .Q(\bus_equal_gen.len_cnt_reg__0 [7]),
        .R(\bus_equal_gen.fifo_burst_n_2 ));
  FDRE \bus_equal_gen.strb_buf_reg[0] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(tmp_strb[0]),
        .Q(m_axi_gmem1_WSTRB[0]),
        .R(ARESET));
  FDRE \bus_equal_gen.strb_buf_reg[1] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(tmp_strb[1]),
        .Q(m_axi_gmem1_WSTRB[1]),
        .R(ARESET));
  FDRE \bus_equal_gen.strb_buf_reg[2] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(tmp_strb[2]),
        .Q(m_axi_gmem1_WSTRB[2]),
        .R(ARESET));
  FDRE \bus_equal_gen.strb_buf_reg[3] 
       (.C(ap_clk),
        .CE(p_13_in),
        .D(tmp_strb[3]),
        .Q(m_axi_gmem1_WSTRB[3]),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair194" *) 
  LUT3 #(
    .INIT(8'h8B)) 
    \conservative_gen.throttl_cnt[0]_i_1 
       (.I0(\could_multi_bursts.awlen_buf_reg[3]_0 [0]),
        .I1(\could_multi_bursts.AWVALID_Dummy_reg_0 ),
        .I2(\conservative_gen.throttl_cnt_reg[1] [0]),
        .O(\could_multi_bursts.awlen_buf_reg[1]_0 [0]));
  (* SOFT_HLUTNM = "soft_lutpair194" *) 
  LUT4 #(
    .INIT(16'hB88B)) 
    \conservative_gen.throttl_cnt[1]_i_1 
       (.I0(\could_multi_bursts.awlen_buf_reg[3]_0 [1]),
        .I1(\could_multi_bursts.AWVALID_Dummy_reg_0 ),
        .I2(\conservative_gen.throttl_cnt_reg[1] [0]),
        .I3(\conservative_gen.throttl_cnt_reg[1] [1]),
        .O(\could_multi_bursts.awlen_buf_reg[1]_0 [1]));
  LUT4 #(
    .INIT(16'hFF80)) 
    \conservative_gen.throttl_cnt[7]_i_1 
       (.I0(\bus_equal_gen.WVALID_Dummy_reg_0 ),
        .I1(m_axi_gmem1_WREADY),
        .I2(\conservative_gen.throttl_cnt_reg[7] ),
        .I3(\could_multi_bursts.AWVALID_Dummy_reg_0 ),
        .O(\bus_equal_gen.WVALID_Dummy_reg_1 ));
  LUT6 #(
    .INIT(64'h8888888888888880)) 
    \conservative_gen.throttl_cnt[7]_i_4 
       (.I0(\could_multi_bursts.AWVALID_Dummy_reg_1 ),
        .I1(AWVALID_Dummy),
        .I2(\could_multi_bursts.awlen_buf_reg[3]_0 [1]),
        .I3(\could_multi_bursts.awlen_buf_reg[3]_0 [0]),
        .I4(\could_multi_bursts.awlen_buf_reg[3]_0 [3]),
        .I5(\could_multi_bursts.awlen_buf_reg[3]_0 [2]),
        .O(\could_multi_bursts.AWVALID_Dummy_reg_0 ));
  FDRE \could_multi_bursts.AWVALID_Dummy_reg 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\bus_equal_gen.fifo_burst_n_3 ),
        .Q(AWVALID_Dummy),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair196" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[10]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[10] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[10]),
        .O(awaddr_tmp[10]));
  (* SOFT_HLUTNM = "soft_lutpair198" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[11]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[11] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[11]),
        .O(awaddr_tmp[11]));
  (* SOFT_HLUTNM = "soft_lutpair197" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[12]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[12] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[12]),
        .O(awaddr_tmp[12]));
  (* SOFT_HLUTNM = "soft_lutpair195" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[13]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[13] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[13]),
        .O(awaddr_tmp[13]));
  (* SOFT_HLUTNM = "soft_lutpair214" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[14]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[14] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[14]),
        .O(awaddr_tmp[14]));
  (* SOFT_HLUTNM = "soft_lutpair204" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[15]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[15] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[15]),
        .O(awaddr_tmp[15]));
  (* SOFT_HLUTNM = "soft_lutpair215" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[16]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[16] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[16]),
        .O(awaddr_tmp[16]));
  (* SOFT_HLUTNM = "soft_lutpair216" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[17]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[17] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[17]),
        .O(awaddr_tmp[17]));
  (* SOFT_HLUTNM = "soft_lutpair216" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[18]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[18] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[18]),
        .O(awaddr_tmp[18]));
  (* SOFT_HLUTNM = "soft_lutpair217" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[19]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[19] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[19]),
        .O(awaddr_tmp[19]));
  (* SOFT_HLUTNM = "soft_lutpair217" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[20]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[20] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[20]),
        .O(awaddr_tmp[20]));
  (* SOFT_HLUTNM = "soft_lutpair215" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[21]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[21] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[21]),
        .O(awaddr_tmp[21]));
  (* SOFT_HLUTNM = "soft_lutpair204" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[22]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[22] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[22]),
        .O(awaddr_tmp[22]));
  (* SOFT_HLUTNM = "soft_lutpair214" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[23]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[23] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[23]),
        .O(awaddr_tmp[23]));
  (* SOFT_HLUTNM = "soft_lutpair205" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[24]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[24] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[24]),
        .O(awaddr_tmp[24]));
  (* SOFT_HLUTNM = "soft_lutpair201" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[25]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[25] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[25]),
        .O(awaddr_tmp[25]));
  (* SOFT_HLUTNM = "soft_lutpair198" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[26]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[26] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[26]),
        .O(awaddr_tmp[26]));
  (* SOFT_HLUTNM = "soft_lutpair200" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[27]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[27] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[27]),
        .O(awaddr_tmp[27]));
  (* SOFT_HLUTNM = "soft_lutpair199" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[28]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[28] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[28]),
        .O(awaddr_tmp[28]));
  (* SOFT_HLUTNM = "soft_lutpair197" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[29]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[29] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[29]),
        .O(awaddr_tmp[29]));
  (* SOFT_HLUTNM = "soft_lutpair219" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[2]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[2] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[2]),
        .O(awaddr_tmp[2]));
  (* SOFT_HLUTNM = "soft_lutpair195" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[30]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[30] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[30]),
        .O(awaddr_tmp[30]));
  (* SOFT_HLUTNM = "soft_lutpair196" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[31]_i_2 
       (.I0(\sect_addr_buf_reg_n_0_[31] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[31]),
        .O(awaddr_tmp[31]));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \could_multi_bursts.awaddr_buf[31]_i_5 
       (.I0(\could_multi_bursts.loop_cnt_reg__0 [1]),
        .I1(\could_multi_bursts.loop_cnt_reg__0 [0]),
        .I2(\could_multi_bursts.loop_cnt_reg__0 [2]),
        .I3(\could_multi_bursts.loop_cnt_reg__0 [3]),
        .I4(\could_multi_bursts.loop_cnt_reg__0 [4]),
        .I5(\could_multi_bursts.loop_cnt_reg__0 [5]),
        .O(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair218" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[3]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[3] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[3]),
        .O(awaddr_tmp[3]));
  (* SOFT_HLUTNM = "soft_lutpair219" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[4]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[4] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[4]),
        .O(awaddr_tmp[4]));
  LUT4 #(
    .INIT(16'h956A)) 
    \could_multi_bursts.awaddr_buf[4]_i_3 
       (.I0(m_axi_gmem1_AWADDR[2]),
        .I1(\could_multi_bursts.awlen_buf_reg[3]_0 [0]),
        .I2(\could_multi_bursts.awlen_buf_reg[3]_0 [1]),
        .I3(\could_multi_bursts.awlen_buf_reg[3]_0 [2]),
        .O(\could_multi_bursts.awaddr_buf[4]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h96)) 
    \could_multi_bursts.awaddr_buf[4]_i_4 
       (.I0(m_axi_gmem1_AWADDR[1]),
        .I1(\could_multi_bursts.awlen_buf_reg[3]_0 [1]),
        .I2(\could_multi_bursts.awlen_buf_reg[3]_0 [0]),
        .O(\could_multi_bursts.awaddr_buf[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \could_multi_bursts.awaddr_buf[4]_i_5 
       (.I0(m_axi_gmem1_AWADDR[0]),
        .I1(\could_multi_bursts.awlen_buf_reg[3]_0 [0]),
        .O(\could_multi_bursts.awaddr_buf[4]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair218" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[5]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[5] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[5]),
        .O(awaddr_tmp[5]));
  (* SOFT_HLUTNM = "soft_lutpair200" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[6]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[6] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[6]),
        .O(awaddr_tmp[6]));
  (* SOFT_HLUTNM = "soft_lutpair201" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[7]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[7] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[7]),
        .O(awaddr_tmp[7]));
  (* SOFT_HLUTNM = "soft_lutpair205" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[8]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[8] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[8]),
        .O(awaddr_tmp[8]));
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \could_multi_bursts.awaddr_buf[8]_i_3 
       (.I0(m_axi_gmem1_AWADDR[4]),
        .I1(\could_multi_bursts.awlen_buf_reg[3]_0 [1]),
        .I2(\could_multi_bursts.awlen_buf_reg[3]_0 [0]),
        .I3(\could_multi_bursts.awlen_buf_reg[3]_0 [2]),
        .I4(\could_multi_bursts.awlen_buf_reg[3]_0 [3]),
        .O(\could_multi_bursts.awaddr_buf[8]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h95556AAA)) 
    \could_multi_bursts.awaddr_buf[8]_i_4 
       (.I0(m_axi_gmem1_AWADDR[3]),
        .I1(\could_multi_bursts.awlen_buf_reg[3]_0 [1]),
        .I2(\could_multi_bursts.awlen_buf_reg[3]_0 [0]),
        .I3(\could_multi_bursts.awlen_buf_reg[3]_0 [2]),
        .I4(\could_multi_bursts.awlen_buf_reg[3]_0 [3]),
        .O(\could_multi_bursts.awaddr_buf[8]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair199" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \could_multi_bursts.awaddr_buf[9]_i_1 
       (.I0(\sect_addr_buf_reg_n_0_[9] ),
        .I1(\could_multi_bursts.awaddr_buf[31]_i_5_n_0 ),
        .I2(data1[9]),
        .O(awaddr_tmp[9]));
  FDRE \could_multi_bursts.awaddr_buf_reg[10] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[10]),
        .Q(m_axi_gmem1_AWADDR[8]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[11] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[11]),
        .Q(m_axi_gmem1_AWADDR[9]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[12] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[12]),
        .Q(m_axi_gmem1_AWADDR[10]),
        .R(ARESET));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \could_multi_bursts.awaddr_buf_reg[12]_i_2 
       (.CI(\could_multi_bursts.awaddr_buf_reg[8]_i_2_n_0 ),
        .CO({\could_multi_bursts.awaddr_buf_reg[12]_i_2_n_0 ,\could_multi_bursts.awaddr_buf_reg[12]_i_2_n_1 ,\could_multi_bursts.awaddr_buf_reg[12]_i_2_n_2 ,\could_multi_bursts.awaddr_buf_reg[12]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data1[12:9]),
        .S(m_axi_gmem1_AWADDR[10:7]));
  FDRE \could_multi_bursts.awaddr_buf_reg[13] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[13]),
        .Q(m_axi_gmem1_AWADDR[11]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[14] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[14]),
        .Q(m_axi_gmem1_AWADDR[12]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[15] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[15]),
        .Q(m_axi_gmem1_AWADDR[13]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[16] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[16]),
        .Q(m_axi_gmem1_AWADDR[14]),
        .R(ARESET));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \could_multi_bursts.awaddr_buf_reg[16]_i_2 
       (.CI(\could_multi_bursts.awaddr_buf_reg[12]_i_2_n_0 ),
        .CO({\could_multi_bursts.awaddr_buf_reg[16]_i_2_n_0 ,\could_multi_bursts.awaddr_buf_reg[16]_i_2_n_1 ,\could_multi_bursts.awaddr_buf_reg[16]_i_2_n_2 ,\could_multi_bursts.awaddr_buf_reg[16]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data1[16:13]),
        .S(m_axi_gmem1_AWADDR[14:11]));
  FDRE \could_multi_bursts.awaddr_buf_reg[17] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[17]),
        .Q(m_axi_gmem1_AWADDR[15]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[18] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[18]),
        .Q(m_axi_gmem1_AWADDR[16]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[19] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[19]),
        .Q(m_axi_gmem1_AWADDR[17]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[20] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[20]),
        .Q(m_axi_gmem1_AWADDR[18]),
        .R(ARESET));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \could_multi_bursts.awaddr_buf_reg[20]_i_2 
       (.CI(\could_multi_bursts.awaddr_buf_reg[16]_i_2_n_0 ),
        .CO({\could_multi_bursts.awaddr_buf_reg[20]_i_2_n_0 ,\could_multi_bursts.awaddr_buf_reg[20]_i_2_n_1 ,\could_multi_bursts.awaddr_buf_reg[20]_i_2_n_2 ,\could_multi_bursts.awaddr_buf_reg[20]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data1[20:17]),
        .S(m_axi_gmem1_AWADDR[18:15]));
  FDRE \could_multi_bursts.awaddr_buf_reg[21] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[21]),
        .Q(m_axi_gmem1_AWADDR[19]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[22] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[22]),
        .Q(m_axi_gmem1_AWADDR[20]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[23] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[23]),
        .Q(m_axi_gmem1_AWADDR[21]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[24] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[24]),
        .Q(m_axi_gmem1_AWADDR[22]),
        .R(ARESET));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \could_multi_bursts.awaddr_buf_reg[24]_i_2 
       (.CI(\could_multi_bursts.awaddr_buf_reg[20]_i_2_n_0 ),
        .CO({\could_multi_bursts.awaddr_buf_reg[24]_i_2_n_0 ,\could_multi_bursts.awaddr_buf_reg[24]_i_2_n_1 ,\could_multi_bursts.awaddr_buf_reg[24]_i_2_n_2 ,\could_multi_bursts.awaddr_buf_reg[24]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data1[24:21]),
        .S(m_axi_gmem1_AWADDR[22:19]));
  FDRE \could_multi_bursts.awaddr_buf_reg[25] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[25]),
        .Q(m_axi_gmem1_AWADDR[23]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[26] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[26]),
        .Q(m_axi_gmem1_AWADDR[24]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[27] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[27]),
        .Q(m_axi_gmem1_AWADDR[25]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[28] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[28]),
        .Q(m_axi_gmem1_AWADDR[26]),
        .R(ARESET));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \could_multi_bursts.awaddr_buf_reg[28]_i_2 
       (.CI(\could_multi_bursts.awaddr_buf_reg[24]_i_2_n_0 ),
        .CO({\could_multi_bursts.awaddr_buf_reg[28]_i_2_n_0 ,\could_multi_bursts.awaddr_buf_reg[28]_i_2_n_1 ,\could_multi_bursts.awaddr_buf_reg[28]_i_2_n_2 ,\could_multi_bursts.awaddr_buf_reg[28]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data1[28:25]),
        .S(m_axi_gmem1_AWADDR[26:23]));
  FDRE \could_multi_bursts.awaddr_buf_reg[29] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[29]),
        .Q(m_axi_gmem1_AWADDR[27]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[2] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[2]),
        .Q(m_axi_gmem1_AWADDR[0]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[30] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[30]),
        .Q(m_axi_gmem1_AWADDR[28]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[31] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[31]),
        .Q(m_axi_gmem1_AWADDR[29]),
        .R(ARESET));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \could_multi_bursts.awaddr_buf_reg[31]_i_6 
       (.CI(\could_multi_bursts.awaddr_buf_reg[28]_i_2_n_0 ),
        .CO({\NLW_could_multi_bursts.awaddr_buf_reg[31]_i_6_CO_UNCONNECTED [3:2],\could_multi_bursts.awaddr_buf_reg[31]_i_6_n_2 ,\could_multi_bursts.awaddr_buf_reg[31]_i_6_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_could_multi_bursts.awaddr_buf_reg[31]_i_6_O_UNCONNECTED [3],data1[31:29]}),
        .S({1'b0,m_axi_gmem1_AWADDR[29:27]}));
  FDRE \could_multi_bursts.awaddr_buf_reg[3] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[3]),
        .Q(m_axi_gmem1_AWADDR[1]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[4] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[4]),
        .Q(m_axi_gmem1_AWADDR[2]),
        .R(ARESET));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \could_multi_bursts.awaddr_buf_reg[4]_i_2 
       (.CI(1'b0),
        .CO({\could_multi_bursts.awaddr_buf_reg[4]_i_2_n_0 ,\could_multi_bursts.awaddr_buf_reg[4]_i_2_n_1 ,\could_multi_bursts.awaddr_buf_reg[4]_i_2_n_2 ,\could_multi_bursts.awaddr_buf_reg[4]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({m_axi_gmem1_AWADDR[2:0],1'b0}),
        .O({data1[4:2],\NLW_could_multi_bursts.awaddr_buf_reg[4]_i_2_O_UNCONNECTED [0]}),
        .S({\could_multi_bursts.awaddr_buf[4]_i_3_n_0 ,\could_multi_bursts.awaddr_buf[4]_i_4_n_0 ,\could_multi_bursts.awaddr_buf[4]_i_5_n_0 ,1'b0}));
  FDRE \could_multi_bursts.awaddr_buf_reg[5] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[5]),
        .Q(m_axi_gmem1_AWADDR[3]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[6] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[6]),
        .Q(m_axi_gmem1_AWADDR[4]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[7] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[7]),
        .Q(m_axi_gmem1_AWADDR[5]),
        .R(ARESET));
  FDRE \could_multi_bursts.awaddr_buf_reg[8] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[8]),
        .Q(m_axi_gmem1_AWADDR[6]),
        .R(ARESET));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \could_multi_bursts.awaddr_buf_reg[8]_i_2 
       (.CI(\could_multi_bursts.awaddr_buf_reg[4]_i_2_n_0 ),
        .CO({\could_multi_bursts.awaddr_buf_reg[8]_i_2_n_0 ,\could_multi_bursts.awaddr_buf_reg[8]_i_2_n_1 ,\could_multi_bursts.awaddr_buf_reg[8]_i_2_n_2 ,\could_multi_bursts.awaddr_buf_reg[8]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,m_axi_gmem1_AWADDR[4:3]}),
        .O(data1[8:5]),
        .S({m_axi_gmem1_AWADDR[6:5],\could_multi_bursts.awaddr_buf[8]_i_3_n_0 ,\could_multi_bursts.awaddr_buf[8]_i_4_n_0 }));
  FDRE \could_multi_bursts.awaddr_buf_reg[9] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(awaddr_tmp[9]),
        .Q(m_axi_gmem1_AWADDR[7]),
        .R(ARESET));
  FDRE \could_multi_bursts.awlen_buf_reg[0] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(data[0]),
        .Q(\could_multi_bursts.awlen_buf_reg[3]_0 [0]),
        .R(ARESET));
  FDRE \could_multi_bursts.awlen_buf_reg[1] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(data[1]),
        .Q(\could_multi_bursts.awlen_buf_reg[3]_0 [1]),
        .R(ARESET));
  FDRE \could_multi_bursts.awlen_buf_reg[2] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(data[2]),
        .Q(\could_multi_bursts.awlen_buf_reg[3]_0 [2]),
        .R(ARESET));
  FDRE \could_multi_bursts.awlen_buf_reg[3] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(data[3]),
        .Q(\could_multi_bursts.awlen_buf_reg[3]_0 [3]),
        .R(ARESET));
  FDRE \could_multi_bursts.last_sect_buf_reg 
       (.C(ap_clk),
        .CE(1'b1),
        .D(fifo_resp_n_31),
        .Q(\could_multi_bursts.last_sect_buf_reg_n_0 ),
        .R(ARESET));
  LUT1 #(
    .INIT(2'h1)) 
    \could_multi_bursts.loop_cnt[0]_i_1__0 
       (.I0(\could_multi_bursts.loop_cnt_reg__0 [0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair221" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \could_multi_bursts.loop_cnt[1]_i_1__0 
       (.I0(\could_multi_bursts.loop_cnt_reg__0 [0]),
        .I1(\could_multi_bursts.loop_cnt_reg__0 [1]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair221" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \could_multi_bursts.loop_cnt[2]_i_1__0 
       (.I0(\could_multi_bursts.loop_cnt_reg__0 [2]),
        .I1(\could_multi_bursts.loop_cnt_reg__0 [1]),
        .I2(\could_multi_bursts.loop_cnt_reg__0 [0]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair192" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \could_multi_bursts.loop_cnt[3]_i_1__0 
       (.I0(\could_multi_bursts.loop_cnt_reg__0 [3]),
        .I1(\could_multi_bursts.loop_cnt_reg__0 [0]),
        .I2(\could_multi_bursts.loop_cnt_reg__0 [1]),
        .I3(\could_multi_bursts.loop_cnt_reg__0 [2]),
        .O(plusOp[3]));
  (* SOFT_HLUTNM = "soft_lutpair192" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \could_multi_bursts.loop_cnt[4]_i_1__0 
       (.I0(\could_multi_bursts.loop_cnt_reg__0 [4]),
        .I1(\could_multi_bursts.loop_cnt_reg__0 [2]),
        .I2(\could_multi_bursts.loop_cnt_reg__0 [1]),
        .I3(\could_multi_bursts.loop_cnt_reg__0 [0]),
        .I4(\could_multi_bursts.loop_cnt_reg__0 [3]),
        .O(plusOp[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \could_multi_bursts.loop_cnt[5]_i_2__0 
       (.I0(\could_multi_bursts.loop_cnt_reg__0 [5]),
        .I1(\could_multi_bursts.loop_cnt_reg__0 [3]),
        .I2(\could_multi_bursts.loop_cnt_reg__0 [0]),
        .I3(\could_multi_bursts.loop_cnt_reg__0 [1]),
        .I4(\could_multi_bursts.loop_cnt_reg__0 [2]),
        .I5(\could_multi_bursts.loop_cnt_reg__0 [4]),
        .O(plusOp[5]));
  FDRE \could_multi_bursts.loop_cnt_reg[0] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(plusOp[0]),
        .Q(\could_multi_bursts.loop_cnt_reg__0 [0]),
        .R(fifo_resp_n_1));
  FDRE \could_multi_bursts.loop_cnt_reg[1] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(plusOp[1]),
        .Q(\could_multi_bursts.loop_cnt_reg__0 [1]),
        .R(fifo_resp_n_1));
  FDRE \could_multi_bursts.loop_cnt_reg[2] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(plusOp[2]),
        .Q(\could_multi_bursts.loop_cnt_reg__0 [2]),
        .R(fifo_resp_n_1));
  FDRE \could_multi_bursts.loop_cnt_reg[3] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(plusOp[3]),
        .Q(\could_multi_bursts.loop_cnt_reg__0 [3]),
        .R(fifo_resp_n_1));
  FDRE \could_multi_bursts.loop_cnt_reg[4] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(plusOp[4]),
        .Q(\could_multi_bursts.loop_cnt_reg__0 [4]),
        .R(fifo_resp_n_1));
  FDRE \could_multi_bursts.loop_cnt_reg[5] 
       (.C(ap_clk),
        .CE(wrreq24_out),
        .D(plusOp[5]),
        .Q(\could_multi_bursts.loop_cnt_reg__0 [5]),
        .R(fifo_resp_n_1));
  FDRE \could_multi_bursts.sect_handling_reg 
       (.C(ap_clk),
        .CE(1'b1),
        .D(fifo_resp_n_30),
        .Q(\could_multi_bursts.sect_handling_reg_n_0 ),
        .R(ARESET));
  LUT2 #(
    .INIT(4'h6)) 
    \end_addr_buf[2]_i_1 
       (.I0(\start_addr_reg_n_0_[2] ),
        .I1(\align_len_reg_n_0_[2] ),
        .O(end_addr[2]));
  FDRE \end_addr_buf_reg[10] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[10]),
        .Q(\end_addr_buf_reg_n_0_[10] ),
        .R(ARESET));
  FDRE \end_addr_buf_reg[11] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[11]),
        .Q(\end_addr_buf_reg_n_0_[11] ),
        .R(ARESET));
  FDRE \end_addr_buf_reg[12] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[12]),
        .Q(p_0_in0_in[0]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[13] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[13]),
        .Q(p_0_in0_in[1]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[14] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[14]),
        .Q(p_0_in0_in[2]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[15] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[15]),
        .Q(p_0_in0_in[3]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[16] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[16]),
        .Q(p_0_in0_in[4]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[17] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[17]),
        .Q(p_0_in0_in[5]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[18] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[18]),
        .Q(p_0_in0_in[6]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[19] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[19]),
        .Q(p_0_in0_in[7]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[20] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[20]),
        .Q(p_0_in0_in[8]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[21] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[21]),
        .Q(p_0_in0_in[9]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[22] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[22]),
        .Q(p_0_in0_in[10]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[23] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[23]),
        .Q(p_0_in0_in[11]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[24] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[24]),
        .Q(p_0_in0_in[12]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[25] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[25]),
        .Q(p_0_in0_in[13]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[26] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[26]),
        .Q(p_0_in0_in[14]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[27] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[27]),
        .Q(p_0_in0_in[15]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[28] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[28]),
        .Q(p_0_in0_in[16]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[29] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[29]),
        .Q(p_0_in0_in[17]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[2] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[2]),
        .Q(\end_addr_buf_reg_n_0_[2] ),
        .R(ARESET));
  FDRE \end_addr_buf_reg[30] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[30]),
        .Q(p_0_in0_in[18]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[31] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[31]),
        .Q(p_0_in0_in[19]),
        .R(ARESET));
  FDRE \end_addr_buf_reg[3] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[3]),
        .Q(\end_addr_buf_reg_n_0_[3] ),
        .R(ARESET));
  FDRE \end_addr_buf_reg[4] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[4]),
        .Q(\end_addr_buf_reg_n_0_[4] ),
        .R(ARESET));
  FDRE \end_addr_buf_reg[5] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[5]),
        .Q(\end_addr_buf_reg_n_0_[5] ),
        .R(ARESET));
  FDRE \end_addr_buf_reg[6] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[6]),
        .Q(\end_addr_buf_reg_n_0_[6] ),
        .R(ARESET));
  FDRE \end_addr_buf_reg[7] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[7]),
        .Q(\end_addr_buf_reg_n_0_[7] ),
        .R(ARESET));
  FDRE \end_addr_buf_reg[8] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[8]),
        .Q(\end_addr_buf_reg_n_0_[8] ),
        .R(ARESET));
  FDRE \end_addr_buf_reg[9] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(end_addr[9]),
        .Q(\end_addr_buf_reg_n_0_[9] ),
        .R(ARESET));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 end_addr_carry
       (.CI(1'b0),
        .CO({end_addr_carry_n_0,end_addr_carry_n_1,end_addr_carry_n_2,end_addr_carry_n_3}),
        .CYINIT(1'b0),
        .DI({\start_addr_reg_n_0_[5] ,\start_addr_reg_n_0_[4] ,\start_addr_reg_n_0_[3] ,\start_addr_reg_n_0_[2] }),
        .O({end_addr[5:3],NLW_end_addr_carry_O_UNCONNECTED[0]}),
        .S({end_addr_carry_i_1__0_n_0,end_addr_carry_i_2__0_n_0,end_addr_carry_i_3__0_n_0,end_addr_carry_i_4__0_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 end_addr_carry__0
       (.CI(end_addr_carry_n_0),
        .CO({end_addr_carry__0_n_0,end_addr_carry__0_n_1,end_addr_carry__0_n_2,end_addr_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({\start_addr_reg_n_0_[9] ,\start_addr_reg_n_0_[8] ,\start_addr_reg_n_0_[7] ,\start_addr_reg_n_0_[6] }),
        .O(end_addr[9:6]),
        .S({end_addr_carry__0_i_1__0_n_0,end_addr_carry__0_i_2__0_n_0,end_addr_carry__0_i_3__0_n_0,end_addr_carry__0_i_4__0_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__0_i_1__0
       (.I0(\start_addr_reg_n_0_[9] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__0_i_1__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__0_i_2__0
       (.I0(\start_addr_reg_n_0_[8] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__0_i_2__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__0_i_3__0
       (.I0(\start_addr_reg_n_0_[7] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__0_i_3__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__0_i_4__0
       (.I0(\start_addr_reg_n_0_[6] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__0_i_4__0_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 end_addr_carry__1
       (.CI(end_addr_carry__0_n_0),
        .CO({end_addr_carry__1_n_0,end_addr_carry__1_n_1,end_addr_carry__1_n_2,end_addr_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({\start_addr_reg_n_0_[13] ,\start_addr_reg_n_0_[12] ,\start_addr_reg_n_0_[11] ,\start_addr_reg_n_0_[10] }),
        .O(end_addr[13:10]),
        .S({end_addr_carry__1_i_1__0_n_0,end_addr_carry__1_i_2__0_n_0,end_addr_carry__1_i_3__0_n_0,end_addr_carry__1_i_4__0_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__1_i_1__0
       (.I0(\start_addr_reg_n_0_[13] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__1_i_1__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__1_i_2__0
       (.I0(\start_addr_reg_n_0_[12] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__1_i_2__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__1_i_3__0
       (.I0(\start_addr_reg_n_0_[11] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__1_i_3__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__1_i_4__0
       (.I0(\start_addr_reg_n_0_[10] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__1_i_4__0_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 end_addr_carry__2
       (.CI(end_addr_carry__1_n_0),
        .CO({end_addr_carry__2_n_0,end_addr_carry__2_n_1,end_addr_carry__2_n_2,end_addr_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({\start_addr_reg_n_0_[17] ,\start_addr_reg_n_0_[16] ,\start_addr_reg_n_0_[15] ,\start_addr_reg_n_0_[14] }),
        .O(end_addr[17:14]),
        .S({end_addr_carry__2_i_1__0_n_0,end_addr_carry__2_i_2__0_n_0,end_addr_carry__2_i_3__0_n_0,end_addr_carry__2_i_4__0_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__2_i_1__0
       (.I0(\start_addr_reg_n_0_[17] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__2_i_1__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__2_i_2__0
       (.I0(\start_addr_reg_n_0_[16] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__2_i_2__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__2_i_3__0
       (.I0(\start_addr_reg_n_0_[15] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__2_i_3__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__2_i_4__0
       (.I0(\start_addr_reg_n_0_[14] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__2_i_4__0_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 end_addr_carry__3
       (.CI(end_addr_carry__2_n_0),
        .CO({end_addr_carry__3_n_0,end_addr_carry__3_n_1,end_addr_carry__3_n_2,end_addr_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({\start_addr_reg_n_0_[21] ,\start_addr_reg_n_0_[20] ,\start_addr_reg_n_0_[19] ,\start_addr_reg_n_0_[18] }),
        .O(end_addr[21:18]),
        .S({end_addr_carry__3_i_1__0_n_0,end_addr_carry__3_i_2__0_n_0,end_addr_carry__3_i_3__0_n_0,end_addr_carry__3_i_4__0_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__3_i_1__0
       (.I0(\start_addr_reg_n_0_[21] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__3_i_1__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__3_i_2__0
       (.I0(\start_addr_reg_n_0_[20] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__3_i_2__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__3_i_3__0
       (.I0(\start_addr_reg_n_0_[19] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__3_i_3__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__3_i_4__0
       (.I0(\start_addr_reg_n_0_[18] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__3_i_4__0_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 end_addr_carry__4
       (.CI(end_addr_carry__3_n_0),
        .CO({end_addr_carry__4_n_0,end_addr_carry__4_n_1,end_addr_carry__4_n_2,end_addr_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({\start_addr_reg_n_0_[25] ,\start_addr_reg_n_0_[24] ,\start_addr_reg_n_0_[23] ,\start_addr_reg_n_0_[22] }),
        .O(end_addr[25:22]),
        .S({end_addr_carry__4_i_1__0_n_0,end_addr_carry__4_i_2__0_n_0,end_addr_carry__4_i_3__0_n_0,end_addr_carry__4_i_4__0_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__4_i_1__0
       (.I0(\start_addr_reg_n_0_[25] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__4_i_1__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__4_i_2__0
       (.I0(\start_addr_reg_n_0_[24] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__4_i_2__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__4_i_3__0
       (.I0(\start_addr_reg_n_0_[23] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__4_i_3__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__4_i_4__0
       (.I0(\start_addr_reg_n_0_[22] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__4_i_4__0_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 end_addr_carry__5
       (.CI(end_addr_carry__4_n_0),
        .CO({end_addr_carry__5_n_0,end_addr_carry__5_n_1,end_addr_carry__5_n_2,end_addr_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({\start_addr_reg_n_0_[29] ,\start_addr_reg_n_0_[28] ,\start_addr_reg_n_0_[27] ,\start_addr_reg_n_0_[26] }),
        .O(end_addr[29:26]),
        .S({end_addr_carry__5_i_1__0_n_0,end_addr_carry__5_i_2__0_n_0,end_addr_carry__5_i_3__0_n_0,end_addr_carry__5_i_4__0_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__5_i_1__0
       (.I0(\start_addr_reg_n_0_[29] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__5_i_1__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__5_i_2__0
       (.I0(\start_addr_reg_n_0_[28] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__5_i_2__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__5_i_3__0
       (.I0(\start_addr_reg_n_0_[27] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__5_i_3__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__5_i_4__0
       (.I0(\start_addr_reg_n_0_[26] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__5_i_4__0_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 end_addr_carry__6
       (.CI(end_addr_carry__5_n_0),
        .CO({NLW_end_addr_carry__6_CO_UNCONNECTED[3:1],end_addr_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\start_addr_reg_n_0_[30] }),
        .O({NLW_end_addr_carry__6_O_UNCONNECTED[3:2],end_addr[31:30]}),
        .S({1'b0,1'b0,end_addr_carry__6_i_1__0_n_0,end_addr_carry__6_i_2__0_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__6_i_1__0
       (.I0(\align_len_reg_n_0_[31] ),
        .I1(\start_addr_reg_n_0_[31] ),
        .O(end_addr_carry__6_i_1__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry__6_i_2__0
       (.I0(\start_addr_reg_n_0_[30] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry__6_i_2__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry_i_1__0
       (.I0(\start_addr_reg_n_0_[5] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry_i_1__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry_i_2__0
       (.I0(\start_addr_reg_n_0_[4] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry_i_2__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry_i_3__0
       (.I0(\start_addr_reg_n_0_[3] ),
        .I1(\align_len_reg_n_0_[31] ),
        .O(end_addr_carry_i_3__0_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    end_addr_carry_i_4__0
       (.I0(\start_addr_reg_n_0_[2] ),
        .I1(\align_len_reg_n_0_[2] ),
        .O(end_addr_carry_i_4__0_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_fifo__parameterized3 fifo_resp
       (.ARESET(ARESET),
        .CO(first_sect),
        .D({fifo_resp_n_5,fifo_resp_n_6,fifo_resp_n_7,fifo_resp_n_8,fifo_resp_n_9,fifo_resp_n_10,fifo_resp_n_11,fifo_resp_n_12,fifo_resp_n_13,fifo_resp_n_14,fifo_resp_n_15,fifo_resp_n_16,fifo_resp_n_17,fifo_resp_n_18,fifo_resp_n_19,fifo_resp_n_20,fifo_resp_n_21,fifo_resp_n_22,fifo_resp_n_23,fifo_resp_n_24}),
        .Q({\start_addr_reg_n_0_[31] ,\start_addr_reg_n_0_[30] ,\start_addr_reg_n_0_[29] ,\start_addr_reg_n_0_[28] ,\start_addr_reg_n_0_[27] ,\start_addr_reg_n_0_[26] ,\start_addr_reg_n_0_[25] ,\start_addr_reg_n_0_[24] ,\start_addr_reg_n_0_[23] ,\start_addr_reg_n_0_[22] ,\start_addr_reg_n_0_[21] ,\start_addr_reg_n_0_[20] ,\start_addr_reg_n_0_[19] ,\start_addr_reg_n_0_[18] ,\start_addr_reg_n_0_[17] ,\start_addr_reg_n_0_[16] ,\start_addr_reg_n_0_[15] ,\start_addr_reg_n_0_[14] ,\start_addr_reg_n_0_[13] ,\start_addr_reg_n_0_[12] }),
        .SR(fifo_resp_n_1),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_0(fifo_resp_n_3),
        .\could_multi_bursts.last_sect_buf_reg (fifo_resp_n_31),
        .\could_multi_bursts.last_sect_buf_reg_0 (\could_multi_bursts.last_sect_buf_reg_n_0 ),
        .\could_multi_bursts.loop_cnt_reg[0] (AWVALID_Dummy),
        .\could_multi_bursts.loop_cnt_reg[0]_0 (\could_multi_bursts.loop_cnt_reg[0]_0 ),
        .\could_multi_bursts.loop_cnt_reg[0]_1 (\could_multi_bursts.loop_cnt_reg[0]_1 ),
        .\could_multi_bursts.sect_handling_reg (fifo_resp_n_30),
        .\could_multi_bursts.sect_handling_reg_0 (\bus_equal_gen.fifo_burst_n_8 ),
        .\could_multi_bursts.sect_handling_reg_1 (\could_multi_bursts.sect_handling_reg_n_0 ),
        .fifo_burst_ready(fifo_burst_ready),
        .fifo_wreq_valid(fifo_wreq_valid),
        .full_n0_in(full_n0_in),
        .in(invalid_len_event_2),
        .last_sect_buf(last_sect_buf),
        .m_axi_gmem1_AWREADY(m_axi_gmem1_AWREADY),
        .m_axi_gmem1_BVALID(m_axi_gmem1_BVALID),
        .next_resp(next_resp),
        .next_resp0(next_resp0),
        .next_resp_reg(full_n_tmp_reg),
        .plusOp__1(plusOp__1),
        .push(push_0),
        .push_0(push),
        .rdreq33_out(rdreq33_out),
        .\sect_cnt_reg[0] (sect_cnt[0]),
        .wreq_handling_reg(fifo_resp_n_29),
        .wreq_handling_reg_0(wreq_handling_reg_n_0),
        .wreq_handling_reg_1(last_sect),
        .wreq_handling_reg_2(fifo_wreq_valid_buf_reg_n_0),
        .wrreq24_out(wrreq24_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_fifo__parameterized5 fifo_resp_to_user
       (.ARESET(ARESET),
        .D(D[2]),
        .Q(Q[3:2]),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .empty_n_tmp_reg_0(empty_n_tmp_reg),
        .empty_n_tmp_reg_1(empty_n_tmp_reg_0),
        .full_n_tmp_reg_0(full_n_tmp_reg),
        .push(push));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_fifo fifo_wreq
       (.ARESET(ARESET),
        .E(align_len0),
        .Q({fifo_wreq_data,q}),
        .S({fifo_wreq_n_36,fifo_wreq_n_37,fifo_wreq_n_38,fifo_wreq_n_39}),
        .SR(fifo_wreq_n_2),
        .\align_len_reg[31] (last_sect),
        .\align_len_reg[31]_0 (wreq_handling_reg_n_0),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .empty_n_tmp_reg_0(fifo_wreq_n_35),
        .empty_n_tmp_reg_1(fifo_wreq_n_44),
        .empty_n_tmp_reg_2(\bus_equal_gen.fifo_burst_n_8 ),
        .empty_n_tmp_reg_3(\could_multi_bursts.sect_handling_reg_n_0 ),
        .\end_addr_buf_reg[31] ({fifo_wreq_n_40,fifo_wreq_n_41,fifo_wreq_n_42}),
        .fifo_wreq_valid(fifo_wreq_valid),
        .last_sect_buf(last_sect_buf),
        .last_sect_carry__0(p_0_in0_in),
        .last_sect_carry__0_0(sect_cnt),
        .\pout_reg[2]_0 (rs2f_wreq_valid),
        .\q_reg[29]_0 (rs2f_wreq_data),
        .\q_reg[32]_0 (align_len2),
        .rs2f_wreq_ack(rs2f_wreq_ack),
        .\sect_cnt_reg[0] (fifo_wreq_valid_buf_reg_n_0),
        .wrreq24_out(wrreq24_out));
  FDRE fifo_wreq_valid_buf_reg
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(fifo_wreq_valid),
        .Q(fifo_wreq_valid_buf_reg_n_0),
        .R(ARESET));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 first_sect_carry
       (.CI(1'b0),
        .CO({first_sect_carry_n_0,first_sect_carry_n_1,first_sect_carry_n_2,first_sect_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_first_sect_carry_O_UNCONNECTED[3:0]),
        .S({first_sect_carry_i_1__0_n_0,first_sect_carry_i_2__0_n_0,first_sect_carry_i_3__0_n_0,first_sect_carry_i_4__0_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 first_sect_carry__0
       (.CI(first_sect_carry_n_0),
        .CO({NLW_first_sect_carry__0_CO_UNCONNECTED[3],first_sect,first_sect_carry__0_n_2,first_sect_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_first_sect_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,first_sect_carry__0_i_1__0_n_0,first_sect_carry__0_i_2__0_n_0,first_sect_carry__0_i_3__0_n_0}));
  LUT4 #(
    .INIT(16'h9009)) 
    first_sect_carry__0_i_1__0
       (.I0(start_addr_buf[31]),
        .I1(sect_cnt[19]),
        .I2(start_addr_buf[30]),
        .I3(sect_cnt[18]),
        .O(first_sect_carry__0_i_1__0_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    first_sect_carry__0_i_2__0
       (.I0(start_addr_buf[29]),
        .I1(sect_cnt[17]),
        .I2(sect_cnt[15]),
        .I3(start_addr_buf[27]),
        .I4(sect_cnt[16]),
        .I5(start_addr_buf[28]),
        .O(first_sect_carry__0_i_2__0_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    first_sect_carry__0_i_3__0
       (.I0(start_addr_buf[26]),
        .I1(sect_cnt[14]),
        .I2(sect_cnt[12]),
        .I3(start_addr_buf[24]),
        .I4(sect_cnt[13]),
        .I5(start_addr_buf[25]),
        .O(first_sect_carry__0_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    first_sect_carry_i_1__0
       (.I0(start_addr_buf[23]),
        .I1(sect_cnt[11]),
        .I2(sect_cnt[9]),
        .I3(start_addr_buf[21]),
        .I4(sect_cnt[10]),
        .I5(start_addr_buf[22]),
        .O(first_sect_carry_i_1__0_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    first_sect_carry_i_2__0
       (.I0(sect_cnt[8]),
        .I1(start_addr_buf[20]),
        .I2(sect_cnt[6]),
        .I3(start_addr_buf[18]),
        .I4(start_addr_buf[19]),
        .I5(sect_cnt[7]),
        .O(first_sect_carry_i_2__0_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    first_sect_carry_i_3__0
       (.I0(start_addr_buf[17]),
        .I1(sect_cnt[5]),
        .I2(sect_cnt[4]),
        .I3(start_addr_buf[16]),
        .I4(sect_cnt[3]),
        .I5(start_addr_buf[15]),
        .O(first_sect_carry_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    first_sect_carry_i_4__0
       (.I0(start_addr_buf[14]),
        .I1(sect_cnt[2]),
        .I2(sect_cnt[1]),
        .I3(start_addr_buf[13]),
        .I4(sect_cnt[0]),
        .I5(start_addr_buf[12]),
        .O(first_sect_carry_i_4__0_n_0));
  FDRE invalid_len_event_1_reg
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(invalid_len_event),
        .Q(invalid_len_event_1),
        .R(ARESET));
  FDRE invalid_len_event_2_reg
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(invalid_len_event_1),
        .Q(invalid_len_event_2),
        .R(ARESET));
  FDRE invalid_len_event_reg
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(fifo_wreq_n_35),
        .Q(invalid_len_event),
        .R(ARESET));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 last_sect_carry
       (.CI(1'b0),
        .CO({last_sect_carry_n_0,last_sect_carry_n_1,last_sect_carry_n_2,last_sect_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_last_sect_carry_O_UNCONNECTED[3:0]),
        .S({fifo_wreq_n_36,fifo_wreq_n_37,fifo_wreq_n_38,fifo_wreq_n_39}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 last_sect_carry__0
       (.CI(last_sect_carry_n_0),
        .CO({NLW_last_sect_carry__0_CO_UNCONNECTED[3],last_sect,last_sect_carry__0_n_2,last_sect_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_last_sect_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,fifo_wreq_n_40,fifo_wreq_n_41,fifo_wreq_n_42}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 minusOp_carry
       (.CI(1'b0),
        .CO({NLW_minusOp_carry_CO_UNCONNECTED[3:2],minusOp_carry_n_2,minusOp_carry_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,fifo_wreq_data,1'b0}),
        .O({NLW_minusOp_carry_O_UNCONNECTED[3],minusOp[31],minusOp[2],NLW_minusOp_carry_O_UNCONNECTED[0]}),
        .S({1'b0,1'b1,align_len2,1'b1}));
  FDRE next_resp_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(next_resp0),
        .Q(next_resp),
        .R(ARESET));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry
       (.CI(1'b0),
        .CO({plusOp_carry_n_0,plusOp_carry_n_1,plusOp_carry_n_2,plusOp_carry_n_3}),
        .CYINIT(sect_cnt[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(plusOp__1[4:1]),
        .S(sect_cnt[4:1]));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry__0
       (.CI(plusOp_carry_n_0),
        .CO({plusOp_carry__0_n_0,plusOp_carry__0_n_1,plusOp_carry__0_n_2,plusOp_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(plusOp__1[8:5]),
        .S(sect_cnt[8:5]));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry__1
       (.CI(plusOp_carry__0_n_0),
        .CO({plusOp_carry__1_n_0,plusOp_carry__1_n_1,plusOp_carry__1_n_2,plusOp_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(plusOp__1[12:9]),
        .S(sect_cnt[12:9]));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry__2
       (.CI(plusOp_carry__1_n_0),
        .CO({plusOp_carry__2_n_0,plusOp_carry__2_n_1,plusOp_carry__2_n_2,plusOp_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(plusOp__1[16:13]),
        .S(sect_cnt[16:13]));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry__3
       (.CI(plusOp_carry__2_n_0),
        .CO({NLW_plusOp_carry__3_CO_UNCONNECTED[3:2],plusOp_carry__3_n_2,plusOp_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_plusOp_carry__3_O_UNCONNECTED[3],plusOp__1[19:17]}),
        .S({1'b0,sect_cnt[19:17]}));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_filter_gmem1_m_axi_reg_slice rs_wreq
       (.ARESET(ARESET),
        .E(E),
        .I_AWVALID(I_AWVALID),
        .Q(Q[0]),
        .ap_clk(ap_clk),
        .ap_reg_ioackin_gmem1_AWREADY(ap_reg_ioackin_gmem1_AWREADY),
        .\data_p1_reg[29]_0 (rs2f_wreq_data),
        .\data_p2_reg[0]_0 (\data_p2_reg[0] ),
        .\data_p2_reg[29]_0 (\data_p2_reg[29] ),
        .rs2f_wreq_ack(rs2f_wreq_ack),
        .s_ready_t_reg_0(s_ready_t_reg),
        .\state_reg[0]_0 (rs2f_wreq_valid));
  (* SOFT_HLUTNM = "soft_lutpair224" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[10]_i_1__0 
       (.I0(first_sect),
        .I1(start_addr_buf[10]),
        .O(sect_addr[10]));
  (* SOFT_HLUTNM = "soft_lutpair223" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[11]_i_2__0 
       (.I0(first_sect),
        .I1(start_addr_buf[11]),
        .O(sect_addr[11]));
  (* SOFT_HLUTNM = "soft_lutpair213" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[12]_i_1__0 
       (.I0(start_addr_buf[12]),
        .I1(first_sect),
        .I2(sect_cnt[0]),
        .O(sect_addr[12]));
  (* SOFT_HLUTNM = "soft_lutpair213" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[13]_i_1__0 
       (.I0(start_addr_buf[13]),
        .I1(first_sect),
        .I2(sect_cnt[1]),
        .O(sect_addr[13]));
  (* SOFT_HLUTNM = "soft_lutpair212" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[14]_i_1__0 
       (.I0(start_addr_buf[14]),
        .I1(first_sect),
        .I2(sect_cnt[2]),
        .O(sect_addr[14]));
  (* SOFT_HLUTNM = "soft_lutpair212" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[15]_i_1__0 
       (.I0(start_addr_buf[15]),
        .I1(first_sect),
        .I2(sect_cnt[3]),
        .O(sect_addr[15]));
  (* SOFT_HLUTNM = "soft_lutpair211" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[16]_i_1__0 
       (.I0(start_addr_buf[16]),
        .I1(first_sect),
        .I2(sect_cnt[4]),
        .O(sect_addr[16]));
  (* SOFT_HLUTNM = "soft_lutpair211" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[17]_i_1__0 
       (.I0(start_addr_buf[17]),
        .I1(first_sect),
        .I2(sect_cnt[5]),
        .O(sect_addr[17]));
  (* SOFT_HLUTNM = "soft_lutpair210" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[18]_i_1__0 
       (.I0(start_addr_buf[18]),
        .I1(first_sect),
        .I2(sect_cnt[6]),
        .O(sect_addr[18]));
  (* SOFT_HLUTNM = "soft_lutpair210" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[19]_i_1__0 
       (.I0(start_addr_buf[19]),
        .I1(first_sect),
        .I2(sect_cnt[7]),
        .O(sect_addr[19]));
  (* SOFT_HLUTNM = "soft_lutpair209" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[20]_i_1__0 
       (.I0(start_addr_buf[20]),
        .I1(first_sect),
        .I2(sect_cnt[8]),
        .O(sect_addr[20]));
  (* SOFT_HLUTNM = "soft_lutpair209" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[21]_i_1__0 
       (.I0(start_addr_buf[21]),
        .I1(first_sect),
        .I2(sect_cnt[9]),
        .O(sect_addr[21]));
  (* SOFT_HLUTNM = "soft_lutpair208" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[22]_i_1__0 
       (.I0(start_addr_buf[22]),
        .I1(first_sect),
        .I2(sect_cnt[10]),
        .O(sect_addr[22]));
  (* SOFT_HLUTNM = "soft_lutpair208" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[23]_i_1__0 
       (.I0(start_addr_buf[23]),
        .I1(first_sect),
        .I2(sect_cnt[11]),
        .O(sect_addr[23]));
  (* SOFT_HLUTNM = "soft_lutpair206" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[24]_i_1__0 
       (.I0(start_addr_buf[24]),
        .I1(first_sect),
        .I2(sect_cnt[12]),
        .O(sect_addr[24]));
  (* SOFT_HLUTNM = "soft_lutpair207" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[25]_i_1__0 
       (.I0(start_addr_buf[25]),
        .I1(first_sect),
        .I2(sect_cnt[13]),
        .O(sect_addr[25]));
  (* SOFT_HLUTNM = "soft_lutpair207" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[26]_i_1__0 
       (.I0(start_addr_buf[26]),
        .I1(first_sect),
        .I2(sect_cnt[14]),
        .O(sect_addr[26]));
  (* SOFT_HLUTNM = "soft_lutpair206" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[27]_i_1__0 
       (.I0(start_addr_buf[27]),
        .I1(first_sect),
        .I2(sect_cnt[15]),
        .O(sect_addr[27]));
  (* SOFT_HLUTNM = "soft_lutpair203" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[28]_i_1__0 
       (.I0(start_addr_buf[28]),
        .I1(first_sect),
        .I2(sect_cnt[16]),
        .O(sect_addr[28]));
  (* SOFT_HLUTNM = "soft_lutpair203" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[29]_i_1__0 
       (.I0(start_addr_buf[29]),
        .I1(first_sect),
        .I2(sect_cnt[17]),
        .O(sect_addr[29]));
  (* SOFT_HLUTNM = "soft_lutpair226" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[2]_i_1__0 
       (.I0(first_sect),
        .I1(start_addr_buf[2]),
        .O(sect_addr[2]));
  (* SOFT_HLUTNM = "soft_lutpair202" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[30]_i_1__0 
       (.I0(start_addr_buf[30]),
        .I1(first_sect),
        .I2(sect_cnt[18]),
        .O(sect_addr[30]));
  (* SOFT_HLUTNM = "soft_lutpair202" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \sect_addr_buf[31]_i_1__0 
       (.I0(start_addr_buf[31]),
        .I1(first_sect),
        .I2(sect_cnt[19]),
        .O(sect_addr[31]));
  (* SOFT_HLUTNM = "soft_lutpair223" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[3]_i_1__0 
       (.I0(first_sect),
        .I1(start_addr_buf[3]),
        .O(sect_addr[3]));
  (* SOFT_HLUTNM = "soft_lutpair224" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[4]_i_1__0 
       (.I0(first_sect),
        .I1(start_addr_buf[4]),
        .O(sect_addr[4]));
  (* SOFT_HLUTNM = "soft_lutpair225" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[5]_i_1__0 
       (.I0(first_sect),
        .I1(start_addr_buf[5]),
        .O(sect_addr[5]));
  (* SOFT_HLUTNM = "soft_lutpair227" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[6]_i_1__0 
       (.I0(first_sect),
        .I1(start_addr_buf[6]),
        .O(sect_addr[6]));
  (* SOFT_HLUTNM = "soft_lutpair227" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[7]_i_1__0 
       (.I0(first_sect),
        .I1(start_addr_buf[7]),
        .O(sect_addr[7]));
  (* SOFT_HLUTNM = "soft_lutpair226" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[8]_i_1__0 
       (.I0(first_sect),
        .I1(start_addr_buf[8]),
        .O(sect_addr[8]));
  (* SOFT_HLUTNM = "soft_lutpair225" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sect_addr_buf[9]_i_1__0 
       (.I0(first_sect),
        .I1(start_addr_buf[9]),
        .O(sect_addr[9]));
  FDRE \sect_addr_buf_reg[10] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[10]),
        .Q(\sect_addr_buf_reg_n_0_[10] ),
        .R(fifo_resp_n_3));
  FDRE \sect_addr_buf_reg[11] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[11]),
        .Q(\sect_addr_buf_reg_n_0_[11] ),
        .R(fifo_resp_n_3));
  FDRE \sect_addr_buf_reg[12] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[12]),
        .Q(\sect_addr_buf_reg_n_0_[12] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[13] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[13]),
        .Q(\sect_addr_buf_reg_n_0_[13] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[14] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[14]),
        .Q(\sect_addr_buf_reg_n_0_[14] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[15] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[15]),
        .Q(\sect_addr_buf_reg_n_0_[15] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[16] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[16]),
        .Q(\sect_addr_buf_reg_n_0_[16] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[17] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[17]),
        .Q(\sect_addr_buf_reg_n_0_[17] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[18] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[18]),
        .Q(\sect_addr_buf_reg_n_0_[18] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[19] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[19]),
        .Q(\sect_addr_buf_reg_n_0_[19] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[20] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[20]),
        .Q(\sect_addr_buf_reg_n_0_[20] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[21] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[21]),
        .Q(\sect_addr_buf_reg_n_0_[21] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[22] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[22]),
        .Q(\sect_addr_buf_reg_n_0_[22] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[23] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[23]),
        .Q(\sect_addr_buf_reg_n_0_[23] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[24] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[24]),
        .Q(\sect_addr_buf_reg_n_0_[24] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[25] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[25]),
        .Q(\sect_addr_buf_reg_n_0_[25] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[26] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[26]),
        .Q(\sect_addr_buf_reg_n_0_[26] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[27] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[27]),
        .Q(\sect_addr_buf_reg_n_0_[27] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[28] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[28]),
        .Q(\sect_addr_buf_reg_n_0_[28] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[29] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[29]),
        .Q(\sect_addr_buf_reg_n_0_[29] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[2] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[2]),
        .Q(\sect_addr_buf_reg_n_0_[2] ),
        .R(fifo_resp_n_3));
  FDRE \sect_addr_buf_reg[30] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[30]),
        .Q(\sect_addr_buf_reg_n_0_[30] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[31] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[31]),
        .Q(\sect_addr_buf_reg_n_0_[31] ),
        .R(ARESET));
  FDRE \sect_addr_buf_reg[3] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[3]),
        .Q(\sect_addr_buf_reg_n_0_[3] ),
        .R(fifo_resp_n_3));
  FDRE \sect_addr_buf_reg[4] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[4]),
        .Q(\sect_addr_buf_reg_n_0_[4] ),
        .R(fifo_resp_n_3));
  FDRE \sect_addr_buf_reg[5] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[5]),
        .Q(\sect_addr_buf_reg_n_0_[5] ),
        .R(fifo_resp_n_3));
  FDRE \sect_addr_buf_reg[6] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[6]),
        .Q(\sect_addr_buf_reg_n_0_[6] ),
        .R(fifo_resp_n_3));
  FDRE \sect_addr_buf_reg[7] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[7]),
        .Q(\sect_addr_buf_reg_n_0_[7] ),
        .R(fifo_resp_n_3));
  FDRE \sect_addr_buf_reg[8] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[8]),
        .Q(\sect_addr_buf_reg_n_0_[8] ),
        .R(fifo_resp_n_3));
  FDRE \sect_addr_buf_reg[9] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(sect_addr[9]),
        .Q(\sect_addr_buf_reg_n_0_[9] ),
        .R(fifo_resp_n_3));
  FDRE \sect_cnt_reg[0] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_24),
        .Q(sect_cnt[0]),
        .R(ARESET));
  FDRE \sect_cnt_reg[10] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_14),
        .Q(sect_cnt[10]),
        .R(ARESET));
  FDRE \sect_cnt_reg[11] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_13),
        .Q(sect_cnt[11]),
        .R(ARESET));
  FDRE \sect_cnt_reg[12] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_12),
        .Q(sect_cnt[12]),
        .R(ARESET));
  FDRE \sect_cnt_reg[13] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_11),
        .Q(sect_cnt[13]),
        .R(ARESET));
  FDRE \sect_cnt_reg[14] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_10),
        .Q(sect_cnt[14]),
        .R(ARESET));
  FDRE \sect_cnt_reg[15] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_9),
        .Q(sect_cnt[15]),
        .R(ARESET));
  FDRE \sect_cnt_reg[16] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_8),
        .Q(sect_cnt[16]),
        .R(ARESET));
  FDRE \sect_cnt_reg[17] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_7),
        .Q(sect_cnt[17]),
        .R(ARESET));
  FDRE \sect_cnt_reg[18] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_6),
        .Q(sect_cnt[18]),
        .R(ARESET));
  FDRE \sect_cnt_reg[19] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_5),
        .Q(sect_cnt[19]),
        .R(ARESET));
  FDRE \sect_cnt_reg[1] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_23),
        .Q(sect_cnt[1]),
        .R(ARESET));
  FDRE \sect_cnt_reg[2] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_22),
        .Q(sect_cnt[2]),
        .R(ARESET));
  FDRE \sect_cnt_reg[3] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_21),
        .Q(sect_cnt[3]),
        .R(ARESET));
  FDRE \sect_cnt_reg[4] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_20),
        .Q(sect_cnt[4]),
        .R(ARESET));
  FDRE \sect_cnt_reg[5] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_19),
        .Q(sect_cnt[5]),
        .R(ARESET));
  FDRE \sect_cnt_reg[6] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_18),
        .Q(sect_cnt[6]),
        .R(ARESET));
  FDRE \sect_cnt_reg[7] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_17),
        .Q(sect_cnt[7]),
        .R(ARESET));
  FDRE \sect_cnt_reg[8] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_16),
        .Q(sect_cnt[8]),
        .R(ARESET));
  FDRE \sect_cnt_reg[9] 
       (.C(ap_clk),
        .CE(fifo_wreq_n_44),
        .D(fifo_resp_n_15),
        .Q(sect_cnt[9]),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hF055CCFF)) 
    \sect_len_buf[0]_i_1 
       (.I0(start_addr_buf[2]),
        .I1(\end_addr_buf_reg_n_0_[2] ),
        .I2(beat_len_buf[0]),
        .I3(last_sect),
        .I4(first_sect),
        .O(\sect_len_buf[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF055CCFF)) 
    \sect_len_buf[1]_i_1 
       (.I0(start_addr_buf[3]),
        .I1(\end_addr_buf_reg_n_0_[3] ),
        .I2(beat_len_buf[3]),
        .I3(last_sect),
        .I4(first_sect),
        .O(\sect_len_buf[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF055CCFF)) 
    \sect_len_buf[2]_i_1 
       (.I0(start_addr_buf[4]),
        .I1(\end_addr_buf_reg_n_0_[4] ),
        .I2(beat_len_buf[3]),
        .I3(last_sect),
        .I4(first_sect),
        .O(\sect_len_buf[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF055CCFF)) 
    \sect_len_buf[3]_i_1 
       (.I0(start_addr_buf[5]),
        .I1(\end_addr_buf_reg_n_0_[5] ),
        .I2(beat_len_buf[3]),
        .I3(last_sect),
        .I4(first_sect),
        .O(\sect_len_buf[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF055CCFF)) 
    \sect_len_buf[4]_i_1 
       (.I0(start_addr_buf[6]),
        .I1(\end_addr_buf_reg_n_0_[6] ),
        .I2(beat_len_buf[3]),
        .I3(last_sect),
        .I4(first_sect),
        .O(\sect_len_buf[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF055CCFF)) 
    \sect_len_buf[5]_i_1 
       (.I0(start_addr_buf[7]),
        .I1(\end_addr_buf_reg_n_0_[7] ),
        .I2(beat_len_buf[3]),
        .I3(last_sect),
        .I4(first_sect),
        .O(\sect_len_buf[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF055CCFF)) 
    \sect_len_buf[6]_i_1 
       (.I0(start_addr_buf[8]),
        .I1(\end_addr_buf_reg_n_0_[8] ),
        .I2(beat_len_buf[3]),
        .I3(last_sect),
        .I4(first_sect),
        .O(\sect_len_buf[6]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF055CCFF)) 
    \sect_len_buf[7]_i_1 
       (.I0(start_addr_buf[9]),
        .I1(\end_addr_buf_reg_n_0_[9] ),
        .I2(beat_len_buf[3]),
        .I3(last_sect),
        .I4(first_sect),
        .O(\sect_len_buf[7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF055CCFF)) 
    \sect_len_buf[8]_i_1 
       (.I0(start_addr_buf[10]),
        .I1(\end_addr_buf_reg_n_0_[10] ),
        .I2(beat_len_buf[3]),
        .I3(last_sect),
        .I4(first_sect),
        .O(\sect_len_buf[8]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF055CCFF)) 
    \sect_len_buf[9]_i_2 
       (.I0(start_addr_buf[11]),
        .I1(\end_addr_buf_reg_n_0_[11] ),
        .I2(beat_len_buf[3]),
        .I3(last_sect),
        .I4(first_sect),
        .O(\sect_len_buf[9]_i_2_n_0 ));
  FDRE \sect_len_buf_reg[0] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(\sect_len_buf[0]_i_1_n_0 ),
        .Q(\sect_len_buf_reg_n_0_[0] ),
        .R(ARESET));
  FDRE \sect_len_buf_reg[1] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(\sect_len_buf[1]_i_1_n_0 ),
        .Q(\sect_len_buf_reg_n_0_[1] ),
        .R(ARESET));
  FDRE \sect_len_buf_reg[2] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(\sect_len_buf[2]_i_1_n_0 ),
        .Q(\sect_len_buf_reg_n_0_[2] ),
        .R(ARESET));
  FDRE \sect_len_buf_reg[3] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(\sect_len_buf[3]_i_1_n_0 ),
        .Q(\sect_len_buf_reg_n_0_[3] ),
        .R(ARESET));
  FDRE \sect_len_buf_reg[4] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(\sect_len_buf[4]_i_1_n_0 ),
        .Q(sect_len_buf[4]),
        .R(ARESET));
  FDRE \sect_len_buf_reg[5] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(\sect_len_buf[5]_i_1_n_0 ),
        .Q(sect_len_buf[5]),
        .R(ARESET));
  FDRE \sect_len_buf_reg[6] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(\sect_len_buf[6]_i_1_n_0 ),
        .Q(sect_len_buf[6]),
        .R(ARESET));
  FDRE \sect_len_buf_reg[7] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(\sect_len_buf[7]_i_1_n_0 ),
        .Q(sect_len_buf[7]),
        .R(ARESET));
  FDRE \sect_len_buf_reg[8] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(\sect_len_buf[8]_i_1_n_0 ),
        .Q(sect_len_buf[8]),
        .R(ARESET));
  FDRE \sect_len_buf_reg[9] 
       (.C(ap_clk),
        .CE(last_sect_buf),
        .D(\sect_len_buf[9]_i_2_n_0 ),
        .Q(sect_len_buf[9]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[10] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[10] ),
        .Q(start_addr_buf[10]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[11] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[11] ),
        .Q(start_addr_buf[11]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[12] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[12] ),
        .Q(start_addr_buf[12]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[13] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[13] ),
        .Q(start_addr_buf[13]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[14] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[14] ),
        .Q(start_addr_buf[14]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[15] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[15] ),
        .Q(start_addr_buf[15]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[16] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[16] ),
        .Q(start_addr_buf[16]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[17] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[17] ),
        .Q(start_addr_buf[17]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[18] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[18] ),
        .Q(start_addr_buf[18]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[19] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[19] ),
        .Q(start_addr_buf[19]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[20] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[20] ),
        .Q(start_addr_buf[20]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[21] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[21] ),
        .Q(start_addr_buf[21]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[22] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[22] ),
        .Q(start_addr_buf[22]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[23] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[23] ),
        .Q(start_addr_buf[23]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[24] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[24] ),
        .Q(start_addr_buf[24]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[25] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[25] ),
        .Q(start_addr_buf[25]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[26] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[26] ),
        .Q(start_addr_buf[26]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[27] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[27] ),
        .Q(start_addr_buf[27]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[28] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[28] ),
        .Q(start_addr_buf[28]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[29] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[29] ),
        .Q(start_addr_buf[29]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[2] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[2] ),
        .Q(start_addr_buf[2]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[30] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[30] ),
        .Q(start_addr_buf[30]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[31] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[31] ),
        .Q(start_addr_buf[31]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[3] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[3] ),
        .Q(start_addr_buf[3]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[4] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[4] ),
        .Q(start_addr_buf[4]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[5] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[5] ),
        .Q(start_addr_buf[5]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[6] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[6] ),
        .Q(start_addr_buf[6]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[7] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[7] ),
        .Q(start_addr_buf[7]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[8] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[8] ),
        .Q(start_addr_buf[8]),
        .R(ARESET));
  FDRE \start_addr_buf_reg[9] 
       (.C(ap_clk),
        .CE(rdreq33_out),
        .D(\start_addr_reg_n_0_[9] ),
        .Q(start_addr_buf[9]),
        .R(ARESET));
  FDRE \start_addr_reg[10] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[8]),
        .Q(\start_addr_reg_n_0_[10] ),
        .R(ARESET));
  FDRE \start_addr_reg[11] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[9]),
        .Q(\start_addr_reg_n_0_[11] ),
        .R(ARESET));
  FDRE \start_addr_reg[12] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[10]),
        .Q(\start_addr_reg_n_0_[12] ),
        .R(ARESET));
  FDRE \start_addr_reg[13] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[11]),
        .Q(\start_addr_reg_n_0_[13] ),
        .R(ARESET));
  FDRE \start_addr_reg[14] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[12]),
        .Q(\start_addr_reg_n_0_[14] ),
        .R(ARESET));
  FDRE \start_addr_reg[15] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[13]),
        .Q(\start_addr_reg_n_0_[15] ),
        .R(ARESET));
  FDRE \start_addr_reg[16] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[14]),
        .Q(\start_addr_reg_n_0_[16] ),
        .R(ARESET));
  FDRE \start_addr_reg[17] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[15]),
        .Q(\start_addr_reg_n_0_[17] ),
        .R(ARESET));
  FDRE \start_addr_reg[18] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[16]),
        .Q(\start_addr_reg_n_0_[18] ),
        .R(ARESET));
  FDRE \start_addr_reg[19] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[17]),
        .Q(\start_addr_reg_n_0_[19] ),
        .R(ARESET));
  FDRE \start_addr_reg[20] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[18]),
        .Q(\start_addr_reg_n_0_[20] ),
        .R(ARESET));
  FDRE \start_addr_reg[21] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[19]),
        .Q(\start_addr_reg_n_0_[21] ),
        .R(ARESET));
  FDRE \start_addr_reg[22] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[20]),
        .Q(\start_addr_reg_n_0_[22] ),
        .R(ARESET));
  FDRE \start_addr_reg[23] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[21]),
        .Q(\start_addr_reg_n_0_[23] ),
        .R(ARESET));
  FDRE \start_addr_reg[24] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[22]),
        .Q(\start_addr_reg_n_0_[24] ),
        .R(ARESET));
  FDRE \start_addr_reg[25] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[23]),
        .Q(\start_addr_reg_n_0_[25] ),
        .R(ARESET));
  FDRE \start_addr_reg[26] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[24]),
        .Q(\start_addr_reg_n_0_[26] ),
        .R(ARESET));
  FDRE \start_addr_reg[27] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[25]),
        .Q(\start_addr_reg_n_0_[27] ),
        .R(ARESET));
  FDRE \start_addr_reg[28] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[26]),
        .Q(\start_addr_reg_n_0_[28] ),
        .R(ARESET));
  FDRE \start_addr_reg[29] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[27]),
        .Q(\start_addr_reg_n_0_[29] ),
        .R(ARESET));
  FDRE \start_addr_reg[2] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[0]),
        .Q(\start_addr_reg_n_0_[2] ),
        .R(ARESET));
  FDRE \start_addr_reg[30] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[28]),
        .Q(\start_addr_reg_n_0_[30] ),
        .R(ARESET));
  FDRE \start_addr_reg[31] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[29]),
        .Q(\start_addr_reg_n_0_[31] ),
        .R(ARESET));
  FDRE \start_addr_reg[3] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[1]),
        .Q(\start_addr_reg_n_0_[3] ),
        .R(ARESET));
  FDRE \start_addr_reg[4] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[2]),
        .Q(\start_addr_reg_n_0_[4] ),
        .R(ARESET));
  FDRE \start_addr_reg[5] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[3]),
        .Q(\start_addr_reg_n_0_[5] ),
        .R(ARESET));
  FDRE \start_addr_reg[6] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[4]),
        .Q(\start_addr_reg_n_0_[6] ),
        .R(ARESET));
  FDRE \start_addr_reg[7] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[5]),
        .Q(\start_addr_reg_n_0_[7] ),
        .R(ARESET));
  FDRE \start_addr_reg[8] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[6]),
        .Q(\start_addr_reg_n_0_[8] ),
        .R(ARESET));
  FDRE \start_addr_reg[9] 
       (.C(ap_clk),
        .CE(align_len0),
        .D(q[7]),
        .Q(\start_addr_reg_n_0_[9] ),
        .R(ARESET));
  FDRE wreq_handling_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(fifo_resp_n_29),
        .Q(wreq_handling_reg_n_0),
        .R(ARESET));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
