#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <inttypes.h>
#include <xil_cache.h>
#include <xparameters.h>
#include <xtime_l.h>
#include <ff.h>
#include "platform.h"
#include "hdmi/zed_hdmi_display.h"
//#include "Filter.h"			//d�commentez lorsqu'il existera

void hdmiInit(zed_hdmi_display_t * hdmiConfig)
{
	hdmiConfig->uBaseAddr_IIC_HdmiOut = XPAR_ZED_HDMI_IIC_0_BASEADDR;
	hdmiConfig->uDeviceId_VTC_HdmioGenerator = XPAR_ZED_HDMI_DISPLAY_V_TC_0_DEVICE_ID;
	hdmiConfig->uDeviceId_VDMA_HdmiDisplay = XPAR_ZED_HDMI_DISPLAY_AXI_VDMA_0_DEVICE_ID;
	hdmiConfig->uBaseAddr_MEM_HdmiDisplay = XPAR_DDR_MEM_BASEADDR + 0x1E000000;		// Les derniers 32 Mo de la DDR sont r�serv�s pour l'HDMI
	hdmiConfig->uNumFrames_HdmiDisplay = XPAR_AXIVDMA_0_NUM_FSTORES;
	zed_hdmi_display_init(hdmiConfig);
}

// Exemple de fonction permettant d'envoyer votre vid�o lue. Vous devez l'appliquer pour vos images
void show_video( zed_hdmi_display_t *pDemo, const uint8_t * frame, int frameSize)
{
	for (int i = 0; i < frameSize; ++i) {
		typedef union {
			uint8_t pix[4];
			unsigned full;
		} pix;
		_Static_assert(sizeof(pix) == 4, "");
		pix mypix = { .pix = { frame[i], frame[i], frame[i], frame[i] } };
		*(unsigned*)(pDemo->uBaseAddr_MEM_HdmiDisplay + i*4) = mypix.full;
	}

}

// Fonction Filter qui d�mare le mat�riel. Prenez les mesures de temps pour vous rendre compte de l'efficacit� de votre impl�mentation.
void doFilter(/* param�tres � passer ici */)
{
	XTime before, after;

	/* Configurez votre filtre ici */

	printf("Starting Filter\n");
	XTime_GetTime(&before);

	// D�marrez votre filtre ici

	while(/*Attendez que votre filtre termine*/ true);

	XTime_GetTime(&after);

	double elapsed = (double)(after-before)/COUNTS_PER_SECOND;
	printf("Done in %fs\n", elapsed);
}


// Fonction Filter qui d�mare le logiciel. Prenez les mesures de temps pour vous rendre compte de l'efficacit� de votre impl�mentation.
void doFilterSW(uint8_t * img_in, unsigned * img_out)
{
	XTime before, after;

	/* Configurez votre filtre ici */

	printf("Starting Filter\n");
	XTime_GetTime(&before);

	// D�commentez une fois votre code import�
	//filter(img_in, img_out);

	XTime_GetTime(&after);

	double elapsed = (double)(after-before)/COUNTS_PER_SECOND;
	printf("Done in %fs\n", elapsed);
}

int main()
{
	init_platform();

	zed_hdmi_display_t hdmiConfig;
	hdmiInit(&hdmiConfig);

	FILINFO fInfo = { 0 };
	uint8_t * data = 0; 		// Tableau representant votre image. Les valeur vont de 0 � 255 pour ne traiter que des images en noirs et blanc. 
	Xil_DCacheFlush();		// On flush la cache pour s'assurer que tout le fichier retourner est dans la DDR et non seulement dans la cache.

	// � compl�ter: Initialisation du filtre de Sobel mat�riel/logiciel selon ce que devez tester

	XTime_SetTime(0);
	uint8_t * output = 0; // Tableau representant votre image de sorties. Les valeur vont de 0 � 255 pour ne traiter que des images en noirs et blanc. 
	while(1) {
		for (int i = 0; i < fInfo.fsize; i += 1920*1080) {
			//doFilter( /* param�tres � compl�ter ici */);  ou  //doFilterSW( data, output);
			show_video(&hdmiConfig, &output[i], 1920*1080);
		}
	}
	cleanup_platform();
	return 0;
}
