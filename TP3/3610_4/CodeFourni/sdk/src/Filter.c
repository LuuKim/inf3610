#include "Filter.h"
#include <string.h>

#define ABS(x)          ((x>0)? x : -x)

typedef union {
	uint8_t pix[4];
	unsigned full;
} OneToFourPixels;


uint8_t filter_operator(const int fullIndex, uint8_t * image)
{
#pragma HLS inline			// Inliner la fonction lui permet d'etre "copie-coller" lorsqu elle est appelle
							// et ainsi faciliter le pipelinage de la boucle principale
	/*la fonction peut avoir 3 signatures diff�rentes, selon vos diff�rentes modifications:
	 * uint8_t filter_operator(const int fullIndex, uint8_t * image)
	 * uint8_t filter_operator(const int fullIndex, uint8_t image[IMG_HEIGHT * IMG_WIDTH])
	 * uint8_t filter_operator(const int col, const int row, uint8_t image[IMG_HEIGHT][IMG_WIDTH])
	 *
	 * Les deux premieres sont assez equivalentes, mais la derniere permet d'acceder � l'image comme un
	 * tableau 2D. Par contre, un tableau 2D doit alors lui etre passe, ce qui n'est pas evident considerant
	 * que les entrees de la fonction filtrer() sont 1D. Cependant, si pour une raison ou une autre
	 * un buffer-cache intermediaire etait utilise, celui-ci pourrait etre 2D...
	 */
	return 0;
}


void filter(uint8_t inter_pix[IMG_WIDTH * IMG_HEIGHT], unsigned out_pix[IMG_WIDTH * IMG_HEIGHT])
{
	/* On demande � HLS de nous synthetiser des maitres AXI que l'on connectera � la memoire principale.
	 * Ainsi, le CPU n'a pas besoin de transferer l'image au filtre: c'est le filtre qui va chercher l'image
	 * dans la memoire principale (DDR de la carte) et ecrit le resultat dans cette meme memoire.
	 * Un esclave AXI-Lite est aussi cree, accessible par le CPU, pour informer le filtre des adresses
	 * auxquelles il doit aller chercher et ecrire l'image, lui dire de demarrer ou d'arreter, etc.
	 */
	// ***** LES 3 LIGNES SUIVANTES DOIVENT ETRE DECOMMENTEES UNE FOIS LES QUESTIONS INITIALES COMPLETE!! ******
//#pragma HLS INTERFACE m_axi port=inter_pix offset=slave bundle=gmem0
//#pragma HLS INTERFACE m_axi port=out_pix offset=slave bundle=gmem1
//#pragma HLS INTERFACE s_axilite port=return

	//� remplacer par votre fonction *apres* avoir repondu aux questions initiales
IMG: for (int i = 0; i < IMG_WIDTH * IMG_HEIGHT; ++i) {
		uint8_t val = inter_pix[i];
		OneToFourPixels fourWide;
OneTo4:	for (int j = 0; j < 4; ++j)
			fourWide.pix[j] = val;
		out_pix[i] = fourWide.full;
	}
}
