// ==============================================================
// File generated on Sun Dec 15 20:21:25 -0500 2019
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:38:27 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XFILTER_H
#define XFILTER_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xfilter_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
#else
typedef struct {
    u16 DeviceId;
    u32 Axilites_BaseAddress;
} XFilter_Config;
#endif

typedef struct {
    u32 Axilites_BaseAddress;
    u32 IsReady;
} XFilter;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XFilter_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XFilter_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XFilter_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XFilter_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XFilter_Initialize(XFilter *InstancePtr, u16 DeviceId);
XFilter_Config* XFilter_LookupConfig(u16 DeviceId);
int XFilter_CfgInitialize(XFilter *InstancePtr, XFilter_Config *ConfigPtr);
#else
int XFilter_Initialize(XFilter *InstancePtr, const char* InstanceName);
int XFilter_Release(XFilter *InstancePtr);
#endif

void XFilter_Start(XFilter *InstancePtr);
u32 XFilter_IsDone(XFilter *InstancePtr);
u32 XFilter_IsIdle(XFilter *InstancePtr);
u32 XFilter_IsReady(XFilter *InstancePtr);
void XFilter_EnableAutoRestart(XFilter *InstancePtr);
void XFilter_DisableAutoRestart(XFilter *InstancePtr);

void XFilter_Set_inter_pix(XFilter *InstancePtr, u32 Data);
u32 XFilter_Get_inter_pix(XFilter *InstancePtr);
void XFilter_Set_out_pix(XFilter *InstancePtr, u32 Data);
u32 XFilter_Get_out_pix(XFilter *InstancePtr);

void XFilter_InterruptGlobalEnable(XFilter *InstancePtr);
void XFilter_InterruptGlobalDisable(XFilter *InstancePtr);
void XFilter_InterruptEnable(XFilter *InstancePtr, u32 Mask);
void XFilter_InterruptDisable(XFilter *InstancePtr, u32 Mask);
void XFilter_InterruptClear(XFilter *InstancePtr, u32 Mask);
u32 XFilter_InterruptGetEnabled(XFilter *InstancePtr);
u32 XFilter_InterruptGetStatus(XFilter *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
