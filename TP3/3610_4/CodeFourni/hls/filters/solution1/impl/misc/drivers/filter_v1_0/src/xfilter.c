// ==============================================================
// File generated on Sun Dec 15 20:21:25 -0500 2019
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:38:27 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xfilter.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XFilter_CfgInitialize(XFilter *InstancePtr, XFilter_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Axilites_BaseAddress = ConfigPtr->Axilites_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XFilter_Start(XFilter *InstancePtr) {
    u32 Data;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XFilter_ReadReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_AP_CTRL) & 0x80;
    XFilter_WriteReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_AP_CTRL, Data | 0x01);
}

u32 XFilter_IsDone(XFilter *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XFilter_ReadReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_AP_CTRL);
    return (Data >> 1) & 0x1;
}

u32 XFilter_IsIdle(XFilter *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XFilter_ReadReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_AP_CTRL);
    return (Data >> 2) & 0x1;
}

u32 XFilter_IsReady(XFilter *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XFilter_ReadReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_AP_CTRL);
    // check ap_start to see if the pcore is ready for next input
    return !(Data & 0x1);
}

void XFilter_EnableAutoRestart(XFilter *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XFilter_WriteReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_AP_CTRL, 0x80);
}

void XFilter_DisableAutoRestart(XFilter *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XFilter_WriteReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_AP_CTRL, 0);
}

void XFilter_Set_inter_pix(XFilter *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XFilter_WriteReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_INTER_PIX_DATA, Data);
}

u32 XFilter_Get_inter_pix(XFilter *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XFilter_ReadReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_INTER_PIX_DATA);
    return Data;
}

void XFilter_Set_out_pix(XFilter *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XFilter_WriteReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_OUT_PIX_DATA, Data);
}

u32 XFilter_Get_out_pix(XFilter *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XFilter_ReadReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_OUT_PIX_DATA);
    return Data;
}

void XFilter_InterruptGlobalEnable(XFilter *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XFilter_WriteReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_GIE, 1);
}

void XFilter_InterruptGlobalDisable(XFilter *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XFilter_WriteReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_GIE, 0);
}

void XFilter_InterruptEnable(XFilter *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XFilter_ReadReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_IER);
    XFilter_WriteReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_IER, Register | Mask);
}

void XFilter_InterruptDisable(XFilter *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XFilter_ReadReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_IER);
    XFilter_WriteReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_IER, Register & (~Mask));
}

void XFilter_InterruptClear(XFilter *InstancePtr, u32 Mask) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XFilter_WriteReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_ISR, Mask);
}

u32 XFilter_InterruptGetEnabled(XFilter *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XFilter_ReadReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_IER);
}

u32 XFilter_InterruptGetStatus(XFilter *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XFilter_ReadReg(InstancePtr->Axilites_BaseAddress, XFILTER_AXILITES_ADDR_ISR);
}

