############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
############################################################
open_project filters
set_top filter
add_files filters/Filter.cpp
open_solution "solution1"
set_part {xc7z020clg484-1} -tool vivado
create_clock -period 10 -name default
config_compile -no_signed_zeros=0 -unsafe_math_optimizations=0
config_export -description Filter -display_name Filter -format ip_catalog -rtl vhdl -vendor polymtl.ca -vivado_phys_opt place -vivado_report_level 0
#source "./filters/solution1/directives.tcl"
#csim_design -O -compiler gcc
csynth_design
#cosim_design
export_design -rtl vhdl -format ip_catalog -description "Filter" -vendor "polymtl.ca" -display_name "Filter"
