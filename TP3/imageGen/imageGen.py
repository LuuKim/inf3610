from PIL import Image

f = open("results.txt", "w")

def calculate(r, g, b):
   return ((66 * r + 129 * g + 25 * b + 128) >> 8) + 16;

img = Image.open("test.bmp")
width, height = img.size
new = Image.new(mode = "RGB", size = (width, height))
pixels = img.load()

for y in range(height):
   for x in range(width):
       r, g, b = img.getpixel((x, y))
       result = calculate(r, g, b)
       f.write(str(result) + ", ")
       pixels[x, y] = (result, result, result)

f.close()
img.show()
img.save("testBW.bmp")
