#include "Filter.h"
#include <string.h>

#define ABS(x)          ((x>0)? x : -x)

typedef union {
	uint8_t pix[4];
	unsigned full;
} OneToFourPixels;


unsigned convert(uint8_t inter_pix)
{
	OneToFourPixels fourWide;
	for(int j = 0; j < 4; ++j) {
		fourWide.pix[j] = inter_pix;
	}
	return fourWide.full;

}


void filter(uint8_t inter_pix[IMG_WIDTH * IMG_HEIGHT], unsigned out_pix[IMG_WIDTH * IMG_HEIGHT])
{
	/* On demande � HLS de nous synthetiser des maitres AXI que l'on connectera � la memoire principale.
	 * Ainsi, le CPU n'a pas besoin de transferer l'image au filtre: c'est le filtre qui va chercher l'image
	 * dans la memoire principale (DDR de la carte) et ecrit le resultat dans cette meme memoire.
	 * Un esclave AXI-Lite est aussi cree, accessible par le CPU, pour informer le filtre des adresses
	 * auxquelles il doit aller chercher et ecrire l'image, lui dire de demarrer ou d'arreter, etc.
	 */
	// ***** LES 3 LIGNES SUIVANTES DOIVENT ETRE DECOMMENTEES UNE FOIS LES QUESTIONS INITIALES COMPLETE!! ******
#pragma HLS INTERFACE m_axi port=inter_pix offset=slave bundle=gmem0
#pragma HLS INTERFACE m_axi port=out_pix offset=slave bundle=gmem1
#pragma HLS INTERFACE s_axilite port=return

	//� remplacer par votre fonction *apres* avoir repondu aux questions initiales
/*IMG: for (int i = 0; i < IMG_WIDTH * IMG_HEIGHT; ++i) {
		uint8_t val = inter_pix[i];
		OneToFourPixels fourWide;
OneTo4:	for (int j = 0; j < 4; ++j)
			fourWide.pix[j] = val;
		out_pix[i] = fourWide.full;
	}*/
	double kernel[3][3] = {{1.0/9.0, 1.0/9.0, 1.0/9.0}, {1.0/9.0, 1.0/9.0, 1.0/9.0}, {1.0/9.0, 1.0/9.0, 1.0/9.0}};
	int translation = 1;
	for(int i = 0; i < IMG_WIDTH; i++) {
		for(int j = 0; j < IMG_HEIGHT; j++) {
		# pragma HLS PIPELINE
		# pragma HLS loop_flatten off
			int R = 0;
			int translationH = j - translation ;
			for(int k = 0; k <= 3; k++) {
				int translationW = i - translation;
				for(int l = 0; l <= 3; l++) {
					R += kernel[k][l]*inter_pix[translationW+translationH*IMG_WIDTH];
					translationW++;
				}
				translationH++;
		}
			out_pix[i+j*IMG_WIDTH] = convert((uint8_t) R);
		}
	}
}
