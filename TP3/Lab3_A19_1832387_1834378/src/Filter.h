#pragma once

#include <inttypes.h>


#define IMG_WIDTH 1920
#define IMG_HEIGHT 1080
#define IMG_SIZE (IMG_WIDTH * IMG_HEIGHT)
#define BOX_BLUR 1
#define GAUSSIAN_BLUR 2
#define SOBEL_OPERATOR 3
#define LAPLACIAN_OPERATOR 4
#define LAPLACIAN_OF_GAUSSIAN 5

unsigned convert(uint8_t inter_pix);
void simple_box_blur(uint8_t * pixel, unsigned * pixel_out);
void gaussian_blur(uint8_t * pixel, unsigned * pixel_out);
void sobel(uint8_t * pixel, unsigned * pixel_out);
void laplacian_operator(uint8_t * pixel, unsigned * pixel_out);
void laplacien_of_gaussian(uint8_t * pixel, unsigned * pixel_out);
