/*
*********************************************************************************************************
*                                                 uC/OS-II
*                                          The Real-Time Kernel
*                                               PORT Windows
*
*
*            		          	Arnaud Desaulty, Frederic Fortier, Eva Terriault
*                                  Ecole Polytechnique de Montreal, Qc, CANADA
*                                                  08/2017
*
* File : simulateur.c
*
*********************************************************************************************************
*/

// Main include of ï¿½C-II
#include "simAvion.h"

#include "bsp_init.h"
#include <stdlib.h>
#include <stdbool.h>
#include <xil_printf.h>
#include <xgpio.h>
///////////////////////////////////////////////////////////////////////////////////////
//								Routines d'interruptions
///////////////////////////////////////////////////////////////////////////////////////



/**
* Ecrivez les routines d'interruption des deux fit_timer (celui qui s'execute a chaque seconde et celui qui s'execute a chaque 3 secondes) et du gpio
*/

void handlerTimer1(void* InstancePtr) {
	OS_ERR err;

	OSSemPost(&timer1_to_generation_sem, OS_OPT_POST_1, &err);
	errMsg(err, "erreur Post Sem : generation_to_timer1_sem\n");
}

void handlerTimer2(void* InstancePtr) {
	OS_ERR err;

	OSSemPost(&timer2_to_verification_sem, OS_OPT_POST_1, &err);
	errMsg(err, "erreur Post Sem : verification_to_timer2_sem\n");


}

void handlerGpio(void* InstancePtr) {
	OS_ERR err;
	OSSemPost(&gpio_to_statistiques_sem, OS_OPT_POST_1, &err);
	errMsg(err, "erreur Post Sem : generation_to_timer1_sem\n");
	XGpio_InterruptClear(&gpSwitch, XGPIO_IR_MASK);
}

/*
*********************************************************************************************************
*                                                  MAIN
*********************************************************************************************************
*/

int main(void* arg)
{

	OS_ERR  os_err;
	UCOS_LowLevelInit();

	CPU_Init();
	Mem_Init();
	OSInit(&os_err);
	create_application();

	init_interrupt();
	connect_axi();
	OSStart(&os_err);

	return 0;
}

void create_application() {
	int error;

	error = create_tasks();
	if (error != 0)
		xil_printf("Erreur %d lors de la creation d'une tache\n", error);

	error = create_events();
	if (error != 0)
		xil_printf("Erreur %d lors de la creation d'un event\n", error);
}

int create_tasks() {
	// Stacks
	OS_ERR  os_err;
	static CPU_STK generationStk[TASK_STK_SIZE]; //Stack of each task
	static CPU_STK atterrissage0Stk[TASK_STK_SIZE];
	static CPU_STK terminal0Stk[TASK_STK_SIZE];
	static CPU_STK terminal1Stk[TASK_STK_SIZE];
	static CPU_STK decollageStk[TASK_STK_SIZE];
	static CPU_STK statistiquesStk[TASK_STK_SIZE];
	static CPU_STK verificationStk[TASK_STK_SIZE];

	/**
	* TODO : Creer les differentes taches
	*/
	OSTaskCreate(&generationTCB, "generation", generation, 0, GENERATION_PRIO, &generationStk[0], TASK_STK_SIZE / 2, TASK_STK_SIZE, 0, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &os_err);
	errMsg(os_err, "erreur lors de la creation de la tache generation\n");

	OSTaskCreate(&atterrissageTCB, "atterrissage", atterrissage, 0, ATTERRISSAGE_PRIO, &atterrissage0Stk[0], TASK_STK_SIZE / 2, TASK_STK_SIZE, 0, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &os_err);
	errMsg(os_err, "erreur lors de la creation de la tache atterrissage\n");

	OSTaskCreate(&terminal0TCB, "terminal0", terminal, (void*)1, TERMINAL0_PRIO, &terminal0Stk[0], TASK_STK_SIZE / 2, TASK_STK_SIZE, 0, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &os_err);
	errMsg(os_err, "erreur lors de la creation de la tache terminal0\n");

	OSTaskCreate(&terminal1TCB, "terminal1", terminal, (void*)2, TERMINAL1_PRIO, &terminal1Stk[0], TASK_STK_SIZE / 2, TASK_STK_SIZE, 0, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &os_err);
	errMsg(os_err, "erreur lors de la creation de la tache terminal1\n");

	OSTaskCreate(&decollageTCB, "decollage", decollage, 0, DECOLLAGE_PRIO, &decollageStk[0], TASK_STK_SIZE / 2, TASK_STK_SIZE, 0, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &os_err);
	errMsg(os_err, "erreur lors de la creation de la tache decollage\n");

	OSTaskCreate(&statistiquesTCB, "statistiques", statistiques, 0, STATISTIQUES_PRIO, &statistiquesStk[0], TASK_STK_SIZE / 2, TASK_STK_SIZE, 0, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &os_err);
	errMsg(os_err, "erreur lors de la creation de la tache statistiques\n");

	OSTaskCreate(&verificationTCB, "verification", verification, 0, VERIFICATION_PRIO, &verificationStk[0], TASK_STK_SIZE / 2, TASK_STK_SIZE, 0, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &os_err);
	errMsg(os_err, "erreur lors de la creation de la tache verification\n");

	return 0;
}

int create_events() {
	OS_ERR err;


	/**
	* TODO : Creer les mutex, files, semaphores que vous utilisez dans ce devoir
	*/

	//Mutex
	OSMutexCreate(&print_mutex, "print_mutex", &err);
	errMsg(err, "erreur lors de la creation du mutex print_mutex\n");

	OSMutexCreate(&malloc_mutex, "malloc_mutex", &err);
	errMsg(err, "erreur lors de la creation du mutex malloc_mutex\n");

	OSMutexCreate(&free_mutex, "free_mutex", &err);
	errMsg(err, "erreur lors de la creation du mutex free_mutex\n");

	OSMutexCreate(&terminalAvailable_mutex, "terminalAvailable_mutex", &err);
	errMsg(err, "erreur lors de la creation du mutex terminalAvailable_mutex\n");

	OSMutexCreate(&terminal1_mutex, "terminal1_mutex", &err);
	errMsg(err, "erreur lors de la creation du mutex terminal1_mutex\n");

	OSMutexCreate(&terminal2_mutex, "terminal2_mutex", &err);
	errMsg(err, "erreur lors de la creation du mutex terminal2_mutex\n");

	OSMutexCreate(&nbAttenteAtterrissageHigh_mutex, "nbAttenteAtterrissageHigh_mutex", &err);
	errMsg(err, "erreur lors de la creation du mutex nbAttenteAtterrissageHigh_mutex\n");

	OSMutexCreate(&nbAttenteAtterrissageMedium_mutex, "nbAttenteAtterrissageMedium_mutex", &err);
	errMsg(err, "erreur lors de la creation du mutex nbAttenteAtterrissageMedium_mutex\n");

	OSMutexCreate(&nbAttenteAtterrissageLow_mutex, "nbAttenteAtterrissageLow_mutex", &err);
	errMsg(err, "erreur lors de la creation du mutex nbAttenteAtterrissageLow_mutex\n");

	OSMutexCreate(&nbAttenteDecollage_mutex, "nbAttenteDecollage_mutex", &err);
	errMsg(err, "erreur lors de la creation du mutex nbAttenteDecollage_mutex\n");

	OSMutexCreate(&debordement_mutex, "debordement_mutex", &err);
	errMsg(err, "erreur lors de la creation du mutex debordement_mutex\n");

	OSMutexCreate(&affichage_mutex, "affichage_mutex", &err);
	errMsg(err, "erreur lors de la creation du mutex affichage_mutex\n");

	//Sem
	OSSemCreate(&timer1_to_generation_sem, "timer1_to_atterrissage_sem", 0, &err);
	errMsg(err, "erreur lors de la creation de timer1_to_atterrissage_sem\n");

	OSSemCreate(&timer2_to_verification_sem, "timer2_to_verification_sem", 0, &err);
	errMsg(err, "erreur lors de la creation de timer2_to_verification_sem\n");

	OSSemCreate(&gpio_to_statistiques_sem, "gpio_to_statistiques_sem", 0, &err);
	errMsg(err, "erreur lors de la creation de gpio_to_statistiques_sem\n");

	OSSemCreate(&atterrissage_to_terminal_sem, "atterrissage_to_terminal_sem", 0, &err);
	errMsg(err, "erreur lors de la creation de atterrissage_to_terminal_sem\n");

	OSSemCreate(&terminal_to_atterrissage_sem, "terminal_to_atterrissage_sem", 2, &err);
	errMsg(err, "erreur lors de la creation de terminal_to_atterrissage_sem\n");

	//Queue
	OSQCreate(&Q_high_priority, "Q_high_priority", sizeof(Avion), &err);
	errMsg(err, "erreur lors de la creation de la file Q_high_priority\n");

	OSQCreate(&Q_medium_priority, "Q_medium_priority", sizeof(Avion), &err);
	errMsg(err, "erreur lors de la creation de la file Q_medium_priority\n");

	OSQCreate(&Q_low_priority, "Q_low_priority", sizeof(Avion), &err);
	errMsg(err, "erreur lors de la creation de la file Q_low_priority\n");

	OSQCreate(&Q_terminal0, "Q_terminal0", sizeof(Avion), &err);
	errMsg(err, "erreur lors de la creation de la file Q_terminal0\n");

	OSQCreate(&Q_terminal1, "Q_terminal1", sizeof(Avion), &err);
	errMsg(err, "erreur lors de la creation de la file Q_terminal1\n");

	OSQCreate(&Q_decollage, "Q_decollage", sizeof(Avion), &err);
	errMsg(err, "erreur lors de la creation de la file Q_decollage\n");

	return 0;
}

/*
*********************************************************************************************************
*                                            TASK FUNCTIONS
*********************************************************************************************************
*/
void generation(void* data) {
	OS_ERR err;
	CPU_TS ts;

	int nbAvionsCrees = 0;
	safe_print("Tache generation lancee\n");
	int skipGen = 0;
	int seed = 34;
	while (DEF_ON) {

		/*TODO: Synchronisation unilaterale timer 1s*/
		OSSemPend(&timer1_to_generation_sem, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
		errMsg(err, "erreur Pend Sem : generation_to_timer1_sem\n");

		srand(seed);
		skipGen = rand() % 5; //On saute la generation 1 fois sur 6
		if (skipGen != 0) {
			Avion* avion;

			safe_malloc(avion);

			avion->id = nbAvionsCrees;
			remplirAvion(avion);

			nbAvionsCrees++;

			/*TODO: Envoi des avions dans les files appropriees*/
			switch (priority(avion->retard)) {
			case HIGH_PRIORITY:
				OSQPost(&Q_high_priority, avion, sizeof(Avion), OS_OPT_POST_FIFO, &err);
				errMsg(err, "erreur Post Queue : Q_high_priority\n");
				qIsFull(err);
				break;

			case MEDIUM_PRIORITY:
				OSQPost(&Q_medium_priority, avion, sizeof(Avion), OS_OPT_POST_FIFO, &err);
				errMsg(err, "erreur Post Queue : Q_medium_priority\n");
				qIsFull(err);
				break;

			case LOW_PRIORITY:
				OSQPost(&Q_low_priority, avion, sizeof(Avion), OS_OPT_POST_FIFO, &err);
				errMsg(err, "erreur Post Queue : Q_low_priority\n");
				qIsFull(err);
				break;

			default:
				safe_print("Impossible de determiner la priorite de l'avion.\n");
			}
			updateNbAttenteAtterrissage(priority(avion->retard), true);
		}
		else {
			safe_print("Generation avion a ete skippe!\n");
		}
		seed++;
	}
}

void atterrissage(void* data)
{
	OS_ERR err;
	CPU_TS ts;
	OS_MSG_SIZE msg_size;

	Avion* avion = NULL;
	safe_print("Tache atterrissage lancee\n");
	while (DEF_ON) {

		/*TODO: Mise en attente des 3 files en fonction de leur priorité*/
		avion = (Avion*)OSQPend(&Q_high_priority, 0, OS_OPT_PEND_NON_BLOCKING, &msg_size, &ts, &err);

		if (avion == NULL) {
			avion = (Avion*)OSQPend(&Q_medium_priority, 0, OS_OPT_PEND_NON_BLOCKING, &msg_size, &ts, &err);
			if (avion == NULL) {
				avion = (Avion*)OSQPend(&Q_low_priority, 0, OS_OPT_PEND_NON_BLOCKING, &msg_size, &ts, &err);
				if (avion != NULL) {
					updateNbAttenteAtterrissage(LOW_PRIORITY, false);
				}
			} else {
				updateNbAttenteAtterrissage(MEDIUM_PRIORITY, false);
			}
		} else {
			updateNbAttenteAtterrissage(HIGH_PRIORITY, false);
		}

		safe_print("[ATTERRISSAGE] Debut atterrissage\n");
		OSTimeDly(15000, OS_OPT_TIME_DLY, &err); //Temps pour que l'avion atterrisse
		safe_print("[ATTERRISSAGE] Attente terminal libre\n");

		/*TODO: Mise en attente d'un terminal libre (mecanisme a votre choix)*/

        /*TODO: Envoi de l'avion au terminal choisi (mecanisme de votre choix)*/
		OSSemPend(&terminal_to_atterrissage_sem, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
		errMsg(err, "erreur Pend Sem : terminal_to_atterrissage_sem\n");

		enAttenteTerminalLibre(avion);
		OSSemPost(&atterrissage_to_terminal_sem, OS_OPT_POST_1, &err);
		errMsg(err, "erreur Post Sem : atterrissage_to_terminal_sem\n");
	}
}


void terminal(void* data)
{
	OS_ERR err;
	CPU_TS ts;
	OS_MSG_SIZE msg_size;
	int numTerminal = (int*)data;
	Avion* avion = NULL;

	safe_print_int("TERMINAL %d: Tache lancee\n", numTerminal);

	while (DEF_ON) {
		/*TODO: Mise en attente d'un avion venant de la piste d'atterrissage*/

		OSSemPend(&atterrissage_to_terminal_sem, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
		errMsg(err, "erreur Pend Sem : atterrissage_to_terminal_sem\n");
		if (numTerminal == 2) {

			avion = (Avion*)OSQPend(&Q_terminal1, 0, OS_OPT_PEND_BLOCKING, &msg_size, &ts, &err);
			errMsg(err, "erreur Pend Queue : Q_terminal1\n");

		}
		else if (numTerminal == 1) {

			avion = (Avion*)OSQPend(&Q_terminal0, 0, OS_OPT_PEND_BLOCKING, &msg_size, &ts, &err);
			errMsg(err, "erreur Pend Queue : Q_terminal0\n");
		}

		safe_print_int("TERMINAL %d: Obtention avion\n", numTerminal);

		//Attente pour le vidage, le nettoyage et le remplissage de l'avion
		OSTimeDly(16000, OS_OPT_TIME_DLY, &err);

		remplirAvion(avion);

		/*TODO: Envoi de l'avion pour le piste de decollage*/
		safe_print_int("[TERMINAL %d] Liberation avion\n", numTerminal);

		OSQPost(&Q_decollage, avion, sizeof(Avion), OS_OPT_POST_FIFO, &err);
		errMsg(err, "erreur Post Queue : Q_decollage\n");

		updateNbAttenteDecollage(true);

		OSMutexPend(&terminalAvailable_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
		errMsg(err, "erreur Pend Mutex: terminalAvailable_mutex\n");

		terminalSeLibere(numTerminal);

		OSMutexPost(&terminalAvailable_mutex, OS_OPT_POST_NONE, &err);
		errMsg(err, "erreur Post Mutex: terminalAvailable_mutex\n");
		/*TODO: Notifier que le terminal est libre (mecanisme de votre choix)*/
		OSSemPost(&terminal_to_atterrissage_sem, OS_OPT_POST_1, &err);
		errMsg(err, "erreur Post Sem : terminal_to_atterrissage_sem\n");

	}

}

void decollage(void* data)
{
	OS_ERR err;
	CPU_TS ts;
	OS_MSG_SIZE msg_size;

	Avion* avion = NULL;
	safe_print("Tache decollage lancee\n");

	while (DEF_ON) {
		/*TODO: Mise en attente d'un avion pret pour le decollage*/
		avion = (Avion*)OSQPend(&Q_decollage, 0, OS_OPT_PEND_BLOCKING, &msg_size, &ts, &err);
		errMsg(err, "erreur Post Queue : Q_decollage\n");

		OSTimeDly(30000, OS_OPT_TIME_DLY, &err); //Temps pour que l'avion decolle
		safe_print("DECOLLAGE: decollage avion\n");

		updateNbAttenteDecollage(false);

		/*TODO: Destruction de l'avion*/
		safe_free(avion);

	}
}


void statistiques(void* data) {
	OS_ERR err;
	CPU_TS ts;
	safe_print("[STATISTIQUES] Tache lancee\n");

	while (DEF_ON) {
		/*TODO: Synchronisation unilaterale switches*/
		OSSemPend(&gpio_to_statistiques_sem, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
		errMsg(err, "erreur Pend Sem : gpio_to_statistiques_sem\n");

		OSMutexPend(&affichage_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
		errMsg(err, "erreur Pend Mutex : affichage_mutex\n");

		safe_print("\n------------------ Affichage des statistiques ------------------\n");

		/*TODO: Obtenir statistiques pour les files d'atterrissage*/

		safe_print_int("Nb d'avions en attente d'atterrissage de type High : %d\n", nbAttenteAtterrissageHigh);
		safe_print_int("Nb d'avions en attente d'atterrissage de type Medium : %d\n", nbAttenteAtterrissageMedium);
		safe_print_int("Nb d'avions en attente d'atterrissage de type Low : %d\n", nbAttenteAtterrissageLow);

		/*TODO: Obtenir statistiques pour la file de decollage*/
		safe_print_int("Nb d'avions en attente de decollage : %d\n", nbAttenteDecollage);

		/*TODO: Obtenir statut des terminaux*/
		safe_print("Terminal 1 ");
		if(terminal1 == 1) {
			safe_print("LIBRE\n");
		} else {
			safe_print("OCCUPE\n");
		}

		safe_print("Terminal 2 ");
		if(terminal2 == 1) {
			safe_print("LIBRE\n");
		} else {
			safe_print("OCCUPE\n");
		}

		OSMutexPost(&affichage_mutex, OS_OPT_POST_NONE, &err);
		errMsg(err, "erreur Post Mutex : affichage_mutex\n");
	}

}

void verification(void* data) {
	OS_ERR err;
	CPU_TS ts;
	safe_print("[VERIFICATION] Tache lancee\n");
	while (1) {
		/*TODO: Synchronisation unilaterale avec timer 3s*/
		OSSemPend(&timer2_to_verification_sem, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
		errMsg(err, "erreur Pend Sem : timer2_to_verification_sem\n");

		if (stopSimDebordement) {
			/*TODO: Suspension de toutes les taches de la simulation*/
			OSTaskSuspend(&generationTCB, &err);
			errMsg(err, "erreur OSTaskSuspend : generationTCB\n");
			OSTaskSuspend(&atterrissageTCB, &err);
			errMsg(err, "erreur OSTaskSuspend : atterrissageTCB\n");
			OSTaskSuspend(&terminal0TCB, &err);
			errMsg(err, "erreur OSTaskSuspend : terminal0TCB\n");
			OSTaskSuspend(&terminal1TCB, &err);
			errMsg(err, "erreur OSTaskSuspend : terminal1TCB\n");
			OSTaskSuspend(&decollageTCB, &err);
			errMsg(err, "erreur OSTaskSuspend : decollageTCB\n");
			OSTaskSuspend(&statistiquesTCB, &err);
			errMsg(err, "erreur OSTaskSuspend : statistiquesTCB\n");
			OSTaskSuspend(&verificationTCB, &err);
			errMsg(err, "erreur OSTaskSuspend : verificationTCB\n");
		}
	}
}

void remplirAvion(Avion* avion) {
	OS_ERR err;

	srand(OSTimeGet(&err));
	avion->retard = rand() % BORNE_SUP_HIGH;
	avion->origine = rand() % NB_AEROPORTS;
	do { avion->destination = rand() % NB_AEROPORTS; } while (avion->origine == avion->destination);
	safe_print_int("\nAvion retard = %d\n", avion->retard);
	safe_print_int("Avion origine = %d\n", avion->origine);
	safe_print_int("Avion destination = %d\n", avion->destination);
}


int priority(int retard) {
	if (retard >= BORNE_INF_HIGH && retard <= BORNE_SUP_HIGH) {
		return HIGH_PRIORITY;
	}
	else if (retard >= BORNE_INF_MEDIUM && retard <= BORNE_SUP_MEDIUM) {
		return MEDIUM_PRIORITY;
	}
	else if (retard >= BORNE_INF_LOW && retard <= BORNE_SUP_LOW) {
		return LOW_PRIORITY;
	}
	return -1;
}


void safe_print(char* message) {
	OS_ERR err;
	CPU_TS ts;

	OSMutexPend(&print_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
	errMsg(err, "erreur Pend Mutex : print_mutex\n");

	xil_printf(message);

	OSMutexPost(&print_mutex, OS_OPT_POST_NONE, &err);
	errMsg(err, "erreur Post Mutex : print_mutex\n");
}

void safe_print_int(char* message, int i) {
	OS_ERR err;
	CPU_TS ts;

	OSMutexPend(&print_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
	errMsg(err, "erreur Pend Mutex : print_mutex\n");

	xil_printf(message, i);

	OSMutexPost(&print_mutex, OS_OPT_POST_NONE, &err);
	errMsg(err, "erreur Post Mutex : print_mutex\n");
}

void safe_malloc(Avion* avion) {
	OS_ERR err;
	CPU_TS ts;

	OSMutexPend(&malloc_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
	errMsg(err, "erreur Pend Mutex : malloc_mutex\n");

	avion = malloc(sizeof(Avion));

	OSMutexPost(&malloc_mutex, OS_OPT_POST_NONE, &err);
	errMsg(err, "erreur Post Mutex : malloc_mutex\n");

}

void safe_free(Avion* avion) {
	OS_ERR err;
	CPU_TS ts;

	OSMutexPend(&free_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
	errMsg(err, "erreur Pend Mutex : free_mutex\n");

	free(avion);
	OSMutexPost(&free_mutex, OS_OPT_POST_NONE, &err);
	errMsg(err, "erreur Post Mutex : free_mutex\n");

}

void errMsg(OS_ERR err, char* errMsg)
{
	if (err != OS_ERR_NONE)
	{
		safe_print(errMsg);
		exit(1);
	}
}

void qIsFull(OS_ERR err)
{
	CPU_TS ts;

	OSMutexPend(&debordement_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
	errMsg(err, "erreur Pend Mutex : debordement_mutex\n");

	if (err == OS_ERR_Q_MAX) {
		stopSimDebordement = true;
	}

	OSMutexPost(&debordement_mutex, OS_OPT_POST_NONE, &err);
	errMsg(err, "erreur Post Mutex : debordement_mutex\n");
}

void updateNbAttenteAtterrissage(int type, bool incrementation) {
	OS_ERR err;
	CPU_TS ts;
	switch (type) {
		case HIGH_PRIORITY :
			OSMutexPend(&nbAttenteAtterrissageHigh_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
			errMsg(err, "erreur Pend Mutex : nbAttenteAtterrissageHigh_mutex\n");

			if(incrementation) {
				nbAttenteAtterrissageHigh++;
			} else {
				if(nbAttenteAtterrissageHigh > 0) {
					nbAttenteAtterrissageHigh--;
				}
			}

			OSMutexPost(&nbAttenteAtterrissageHigh_mutex, OS_OPT_POST_NONE, &err);
			errMsg(err, "erreur Post Mutex : nbAttenteAtterrissageHigh_mutex\n");
			break;

		case MEDIUM_PRIORITY :
			OSMutexPend(&nbAttenteAtterrissageMedium_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
			errMsg(err, "erreur Pend Mutex : nbAttenteAtterrissageMedium_mutex\n");

			if(incrementation) {
				nbAttenteAtterrissageMedium++;
			} else {
				if(nbAttenteAtterrissageMedium > 0) {
					nbAttenteAtterrissageMedium--;
				}
			}

			OSMutexPost(&nbAttenteAtterrissageMedium_mutex, OS_OPT_POST_NONE, &err);
			errMsg(err, "erreur Post Mutex : nbAttenteAtterrissageMedium_mutex\n");
			break;

		case LOW_PRIORITY :
			OSMutexPend(&nbAttenteAtterrissageLow_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
			errMsg(err, "erreur Pend Mutex : nbAttenteAtterrissageLow_mutex\n");

			if(incrementation) {
				nbAttenteAtterrissageLow++;
			} else {
				if(nbAttenteAtterrissageLow > 0) {
					nbAttenteAtterrissageLow--;
				}
			}

			OSMutexPost(&nbAttenteAtterrissageLow_mutex, OS_OPT_POST_NONE, &err);
			errMsg(err, "erreur Post Mutex : nbAttenteAtterrissageLow_mutex\n");
			break;

		default :
			break;
	}
}

void updateNbAttenteDecollage(bool incrementation) {
	OS_ERR err;
	CPU_TS ts;

	OSMutexPend(&nbAttenteDecollage_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
	errMsg(err, "erreur Pend Mutex: nbAttenteDecollage_mutex\n");

	if (incrementation) {
		nbAttenteDecollage++;
	} else {
		nbAttenteDecollage--;
	}

	OSMutexPost(&nbAttenteDecollage_mutex, OS_OPT_POST_NONE, &err);
	errMsg(err, "erreur Post Mutex: nbAttenteDecollage_mutex\n");
}

void enAttenteTerminalLibre(Avion* avion) {
	OS_ERR err;
	CPU_TS ts;
	OSMutexPend(&terminalAvailable_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
	errMsg(err, "erreur Pend Mutex: terminalAvailable_mutex\n");


	if(terminauxLibres()) {
		if (terminal2 == 1) {
		terminal2--;
		OSQPost(&Q_terminal1, avion, sizeof(Avion), OS_OPT_POST_FIFO, &err);
		errMsg(err, "erreur Post Queue : Q_terminal1\n");
		safe_print_int("[ATTERRISSAGE] Terminal libre num %d obtenu\n", 2);
		}
		else if (terminal1 == 1) {
			terminal1--;
			OSQPost(&Q_terminal0, avion, sizeof(Avion), OS_OPT_POST_FIFO, &err);
			errMsg(err, "erreur Post Queue : Q_terminal0\n");
			safe_print_int("[ATTERRISSAGE] Terminal libre num %d obtenu\n", 1);

		}
	}
	else {
		safe_print("[ATTERRISSAGE] Aucun terminal n'est libre.\n");
	}

	OSMutexPost(&terminalAvailable_mutex, OS_OPT_POST_NONE, &err);
	errMsg(err, "erreur Post Mutex: terminalAvailable_mutex\n");
}

bool terminauxLibres() {
	return terminal1 || terminal2;
}

void terminalSeLibere(int num) {
	OS_ERR err;
	CPU_TS ts;
	if(num == 1) {
		if(terminal1 ==0) {
			OSMutexPend(&terminal1_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
			errMsg(err, "erreur Pend Mutex: terminal1_mutex\n");
			terminal1++;
			OSMutexPost(&terminal1_mutex, OS_OPT_POST_NONE, &err);
			errMsg(err, "erreur Post Mutex: terminal1_mutex\n");
		}
	} else if(num ==2) {
		if(terminal2 ==0) {
			OSMutexPend(&terminal2_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
			errMsg(err, "erreur Pend Mutex: terminal2_mutex\n");
			terminal2++;
			OSMutexPost(&terminal2_mutex, OS_OPT_POST_NONE, &err);
			errMsg(err, "erreur Post Mutex: terminal2_mutex\n");
		}
	}
}


