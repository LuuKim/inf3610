/*
*********************************************************************************************************
*                                                 uC/OS-II
*                                          The Real-Time Kernel
*                                               PORT Windows
*
*
*            		          	Arnaud Desaulty, Frederic Fortier, Eva Terriault
*                                  Ecole Polytechnique de Montreal, Qc, CANADA
*                                                  08/2017
*
* File : simulateur.c
*
*********************************************************************************************************
*/

// Main include of ï¿½C-II
#include "simAvion.h"

#include "bsp_init.h"
#include <stdlib.h>
#include <stdbool.h>
#include <xil_printf.h>
#include <xgpio.h>
///////////////////////////////////////////////////////////////////////////////////////
//								Routines d'interruptions
///////////////////////////////////////////////////////////////////////////////////////



/**
* Ecrivez les routines d'interruption des deux fit_timer (celui qui s'execute a chaque seconde et celui qui s'execute a chaque 3 secondes) et du gpio
*/

void handlerTimer1(void* InstancePtr) {
	OS_ERR err;
	CPU_TS ts;
	OSSemPend(&generation_to_timer1_sem, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
	errMsg(err, "error Pend Sem : generation_to_timer1_sem");

	xil_printf("timer1");

	OSSemPost(&timer1_to_atterrissage_sem, OS_OPT_POST_1, &err);
	errMsg(err, "error Post Sem : timer1_to_atterrissage_sem");
}

void handlerTimer2(void* InstancePtr) {
	xil_printf("timer2");

}

void handlerGpio(void* InstancePtr) {
	xil_printf("gpio");
	XGpio_InterruptClear(&gpSwitch, XGPIO_IR_MASK);
	/*
	 * https://github.com/Xilinx/embeddedsw/blob/master/XilinxProcessorIPLib/drivers/gpio/examples/xgpio_intr_tapp_example.c
	 */
}

/*
*********************************************************************************************************
*                                                  MAIN
*********************************************************************************************************
*/

int main(void* arg)
{

    OS_ERR  os_err;
    UCOS_LowLevelInit();

    CPU_Init();
    Mem_Init();
    OSInit(&os_err);
	create_application();

    init_interrupt();
    connect_axi();
    OSStart(&os_err);

    return 0;
}

void create_application() {
	int error;

	error = create_tasks();
	if (error != 0)
		xil_printf("Error %d while creating tasks\n", error);

	error = create_events();
	if (error != 0)
		xil_printf("Error %d while creating events\n", error);
}

int create_tasks() {
	// Stacks
	OS_ERR  os_err;
	static CPU_STK generationStk[TASK_STK_SIZE]; //Stack of each task
	static CPU_STK atterrissage0Stk[TASK_STK_SIZE];
	static CPU_STK terminal0Stk[TASK_STK_SIZE];
	static CPU_STK terminal1Stk[TASK_STK_SIZE];
	static CPU_STK decollageStk[TASK_STK_SIZE];
	static CPU_STK statistiquesStk[TASK_STK_SIZE];
	static CPU_STK verificationStk[TASK_STK_SIZE];


	static OS_TCB generationTCB;
	static OS_TCB atterrissageTCB;
	static OS_TCB terminal0TCB;
	static OS_TCB terminal1TCB;
	static OS_TCB decollageTCB;
	static OS_TCB statistiquesTCB;
	static OS_TCB verificationTCB;


	/**
	* TODO : Creer les differentes taches
	*/
	OSTaskCreate(&generationTCB, "generation", generation, 0, GENERATION_PRIO, &generationStk[0], TASK_STK_SIZE/2, TASK_STK_SIZE, 0, 0, (void*) 0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &os_err);
	errMsg(os_err, "error while creating the task generation");

	OSTaskCreate(&atterrissageTCB, "atterrissage", atterrissage, 0, ATTERRISSAGE_PRIO, &atterrissage0Stk[0], TASK_STK_SIZE/2, TASK_STK_SIZE, 0, 0, (void*) 0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &os_err);
	errMsg(os_err, "error while creating the task atterrissage");

	OSTaskCreate(&terminal0TCB, "terminal0", terminal, 0, TERMINAL0_PRIO, &terminal0Stk[0], TASK_STK_SIZE/2, TASK_STK_SIZE, 0, 0, (void*) 0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &os_err);
	errMsg(os_err, "error while creating the task terminal0");

	OSTaskCreate(&terminal1TCB, "terminal1", terminal, 0, TERMINAL1_PRIO, &terminal1Stk[0], TASK_STK_SIZE/2, TASK_STK_SIZE, 0, 0, (void*) 0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &os_err);
	errMsg(os_err, "error while creating the task terminal1");

	OSTaskCreate(&decollageTCB, "decollage", decollage, 0, DECOLLAGE_PRIO, &decollageStk[0], TASK_STK_SIZE/2, TASK_STK_SIZE, 0, 0, (void*) 0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &os_err);
	errMsg(os_err, "error while creating the task decollage");

	OSTaskCreate(&statistiquesTCB, "statistiques", statistiques, 0, STATISTIQUES_PRIO, &statistiquesStk[0], TASK_STK_SIZE/2, TASK_STK_SIZE, 0, 0, (void*) 0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &os_err);
	errMsg(os_err, "error while creating the task statistiques");

	OSTaskCreate(&verificationTCB, "verification", verification, 0, VERIFICATION_PRIO, &verificationStk[0], TASK_STK_SIZE/2, TASK_STK_SIZE, 0, 0, (void*) 0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &os_err);
	errMsg(os_err, "error while creating the task verification");

	return 0;
}

int create_events() {
	OS_ERR err;


	/**
	* TODO : Creer les mutex, files, semaphores que vous utilisez dans ce devoir
	*/

	//Mutex
	OSMutexCreate(&print_mutex, "print_mutex", &err);
	errMsg(err, "error while creating mutex print_mutex");

	OSMutexCreate(&malloc_mutex, "malloc_mutex", &err);
	errMsg(err, "error while creating mutex malloc_mutex");

	OSMutexCreate(&free_mutex, "free_mutex", &err);
	errMsg(err, "error while creating mutex free_mutex");

	//Sem
	OSSemCreate(&timer1_to_atterrissage_sem, "timer1_to_atterrissage_sem", 0, &err);
	errMsg(err, "error while creating timer1_to_atterrissage_sem");

	OSSemCreate(&atterrissage_to_terminal_sem, "atterrissage_to_terminal_sem", 0, &err);
	errMsg(err, "error while creating atterrissage_to_terminal_sem");

	//Queue
	OSQCreate(&Q_high_priority, "Q_high_priority", sizeof(Avion), &err);
	errMsg(err, "error while creating the queue Q_high_priority");

	OSQCreate(&Q_medium_priority, "Q_medium_priority", sizeof(Avion), &err);
	errMsg(err, "error while creating the queue Q_medium_priority");

	OSQCreate(&Q_low_priority, "Q_low_priority", sizeof(Avion), &err);
	errMsg(err, "error while creating the queue Q_low_priority");

	//flag
	OSFlagCreate(&flags, "flag", (OS_FLAGS)TERMINAL0_READY, &err);
	errMsg(err, "error while creating Flag flag");

	return 0;
}

/*
*********************************************************************************************************
*                                            TASK FUNCTIONS
*********************************************************************************************************
*/
void generation(void* data) {
	OS_ERR err;
	int nbAvionsCrees = 0;
	safe_print("Tache generation lancee\n");
	int skipGen = 0;
	int seed = 34;
	while (DEF_ON) {

		/*TODO: Synchronisation unilaterale timer 1s*/
		OSSemPost(&generation_to_timer1_sem, OS_OPT_POST_1, &err);
		errMsg(err, "error Post Sem : generation_to_timer1_sem");

		srand(seed);
		skipGen = rand() % 5; //On saute la generation 1 fois sur 6
		if (skipGen != 0){
			Avion* avion;

			safe_malloc(avion);

			avion->id = nbAvionsCrees;
			remplirAvion(avion);

			safe_free(avion);

			nbAvionsCrees++;

			/*TODO: Envoi des avions dans les files appropriees*/
			switch (priority(avion->retard)) {
				case 0 :
					OSQPost(&Q_high_priority, avion, sizeof(Avion), OS_OPT_POST_FIFO, &err);
					errMsg(err, "error Post Queue : Q_high_priority");
					break;

				case 1 :
					OSQPost(&Q_medium_priority, avion, sizeof(Avion), OS_OPT_POST_FIFO, &err);
					errMsg(err, "error Post Queue : Q_medium_priority");
					break;

				case 2 :
					OSQPost(&Q_low_priority, avion, sizeof(Avion), OS_OPT_POST_FIFO, &err);
					errMsg(err, "error Post Queue : Q_low_priority");
					break;

				default :
					safe_print("Impossible to determine the priority of the plane");
			}
		}
		else{
			safe_print("Generation avion a ete skippe!\n");
		}
		seed++;
	}
}

void atterrissage(void* data)
{
	OS_ERR err;
	CPU_TS ts;
	OS_MSG_SIZE msg_size;

	Avion* avion = NULL;
	safe_print("Tache atterrissage lancee\n");
	while (DEF_ON) {
		OSSemPend(&timer1_to_atterrissage_sem, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
		errMsg(err, "error Pend Sem : timer1_to_atterrissage_sem");

		/*TODO: Mise en attente des 3 files en fonction de leur priorité*/
		avion = (Avion*)OSQPend(&Q_high_priority, 0, OS_OPT_PEND_BLOCKING, &msg_size, &ts, &err);
		errMsg(err, "error Pend Queue : Q_high_priority");

		avion = (Avion*)OSQPend(&Q_medium_priority, 0, OS_OPT_PEND_BLOCKING, &msg_size, &ts, &err);
		errMsg(err, "error Pend Queue : Q_medium_priority");

		avion = (Avion*)OSQPend(&Q_low_priority, 0, OS_OPT_PEND_BLOCKING, &msg_size, &ts, &err);
		errMsg(err, "error Pend Queue : Q_low_priority");


		safe_print("[ATTERRISSAGE] Debut atterrissage\n");
		OSTimeDly(150, OS_OPT_TIME_DLY, &err); //Temps pour que l'avion atterrisse
		safe_print("[ATTERRISSAGE] Attente terminal libre\n");

		/*TODO: Mise en attente d'un terminal libre (mecanisme a votre choix)*/
		/*OSSemPend(&atterrissage_to_terminal_sem, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
		errMsg(err, "error Pend Sem : atterrissage_to_terminal_sem");*/

        //safe_print("[ATTERRISSAGE] Terminal libre num %d obtenu\n", ...);
 
        /*TODO: Envoi de l'avion au terminal choisi (mecanisme de votre choix)*/
		/*OSSemPost(&atterrissage_to_terminal_sem, OS_OPT_POST_1, &err);
		errMsg(err, "error Post Sem : atterrissage_to_terminal_sem");*/
		}
	}

void terminal(void* data)
{
	OS_ERR err;
	CPU_TS ts;
	OS_MSG_SIZE msg_size;
	int numTerminal = (int*)data;
	Avion* avion = NULL;

	//a modifier
	xil_printf("TERMINAL %d: Tache lancee\n", numTerminal);

	while (DEF_ON) {

		/*TODO: Mise en attente d'un avion venant de la piste d'atterrissage*/
		xil_printf("TERMINAL %d: Obtention avion\n", numTerminal);

		//Attente pour le vidage, le nettoyage et le remplissage de l'avion
		OSTimeDly(160, OS_OPT_TIME_DLY, &err);

		remplirAvion(avion);

		/*TODO: Envoi de l'avion pour le piste de decollage*/
		xil_printf("[TERMINAL %d] Liberation avion\n", numTerminal);
 
        /*TODO: Notifier que le terminal est libre (mecanisme de votre choix)*/
	}

}

void decollage(void* data)
{
	OS_ERR err;
	CPU_TS ts;
	OS_MSG_SIZE msg_size;

	Avion* avion = NULL;
	xil_printf("Tache decollage lancee\n");

	while (DEF_ON) {
		/*TODO: Mise en attente d'un avion pret pour le decollage*/
		OSTimeDly(30, OS_OPT_TIME_DLY, &err); //Temps pour que l'avion decolle
		xil_printf("DECOLLAGE: decollage avion\n");

        /*TODO: Destruction de l'avion*/
	}
}


void statistiques(void* data){
	OS_ERR err;
	CPU_TS ts;
    xil_printf("[STATISTIQUES] Tache lancee\n");

	while(DEF_ON){
        /*TODO: Synchronisation unilaterale switches*/
		xil_printf("\n------------------ Affichage des statistiques ------------------\n");

		/*TODO: Obtenir statistiques pour les files d'atterrissage*/
        /*xil_printf("Nb d'avions en attente d'atterrissage de type High : %d\n", ...);
        xil_printf("Nb d'avions en attente d'atterrissage de type Medium : %d\n", ...);
        xil_printf("Nb d'avions en attente d'atterrissage de type Low : %d\n", ...);*/
 
        /*TODO: Obtenir statistiques pour la file de decollage*/
        //xil_printf("Nb d'avions en attente de decollage : %d\n", ...);
 
        /*TODO: Obtenir statut des terminaux*/
        xil_printf("Terminal 0 ");
        int statutTerm0 = 0; /*A modifier (simplement un exemple d'affichage pour vous aider)*/
        (statutTerm0 == 0) ? xil_printf("OCCUPE\n") : xil_printf("LIBRE\n");
 
        xil_printf("Terminal 1 ");
        int statutTerm1 = 0; /*A modifier (simplement un exemple d'affichage pour vous aider)*/
        (statutTerm1 == 0) ? xil_printf("OCCUPE\n") : xil_printf("LIBRE\n");
	}
}

void verification(void* data){
	OS_ERR err;
	CPU_TS ts;
	xil_printf("[VERIFICATION] Tache lancee\n");
    while(1){
        /*TODO: Synchronisation unilaterale avec timer 3s*/
        if (stopSimDebordement){
            /*TODO: Suspension de toutes les taches de la simulation*/
        }
    }
}
void remplirAvion(Avion* avion) {
	OS_ERR err;

	srand(OSTimeGet(&err));
	avion->retard = rand() % BORNE_SUP_HIGH;
    avion->origine = rand() % NB_AEROPORTS;
    do { avion->destination = rand() % NB_AEROPORTS; } while (avion->origine == avion->destination);
    //safe_print("Avion retard = %d\n", avion->retard);
    //safe_print("Avion origine = %d\n", avion->origine);
    //safe_print("Avion destination = %d\n", avion->destination);
}


int priority(int retard) {
	if(retard >= 40 && retard <= 60) {
		return 0;
	} else if(retard >= 20 && retard <= 39) {
		return 1;
	} else if(retard < 20) {
		return 2;
	}
	return -1;
}


void safe_print(char* message) {
	OS_ERR err;
	CPU_TS ts;

	OSMutexPend(&print_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
	errMsg(err, "error Pend Mutex : print_mutex");

	xil_printf(message);

	OSMutexPost(&print_mutex, OS_OPT_POST_NONE, &err);
	errMsg(err, "error Post Mutex : print_mutex");
}

void safe_malloc(Avion* avion) {
	OS_ERR err;
	CPU_TS ts;

	OSMutexPend(&malloc_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
	errMsg(err, "error Pend Mutex : malloc_mutex");

	avion = malloc(sizeof(Avion));

	OSMutexPost(&malloc_mutex, OS_OPT_POST_NONE, &err);
	errMsg(err, "error Post Mutex : malloc_mutex");

}

void safe_free(Avion* avion) {
	OS_ERR err;
	CPU_TS ts;

	OSMutexPend(&free_mutex, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
	errMsg(err, "error Pend Mutex : free_mutex");

	free(avion);

	OSMutexPost(&free_mutex, OS_OPT_POST_NONE, &err);
	errMsg(err, "error Post Mutex : free_mutex");

}

void errMsg(OS_ERR err, char* errMsg)
{
	if (err != OS_ERR_NONE)
	{
		safe_print(errMsg);
		exit(1);
	}
}
