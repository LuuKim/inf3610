/*
 * tourControleAvions.h
 *
 *  Created on: 14 sept. 2018
 *      Author: evter
 */

#ifndef SRC_SIMAVION_H_
#define SRC_SIMAVION_H_

#include <os.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>

 /*
 *********************************************************************************************************
 *                                              CONSTANTS
 *********************************************************************************************************
 */

#define TASK_STK_SIZE       16384            // Size of each task's stacks (# of WORDs)

#define	VERIFICATION_PRIO	3				// Defining Priority of each task
#define	GENERATION_PRIO		4
#define	STATISTIQUES_PRIO	5
#define	ATTERRISSAGE_PRIO	6
#define	TERMINAL0_PRIO		7
#define	TERMINAL1_PRIO		8
#define	DECOLLAGE_PRIO		9

 //Intervalles criteres de retard
#define BORNE_INF_LOW      	0
#define BORNE_SUP_LOW      	2
#define BORNE_INF_MEDIUM   	3
#define BORNE_SUP_MEDIUM  	5
#define BORNE_INF_HIGH     	6
#define BORNE_SUP_HIGH     	8

#define HIGH_PRIORITY     	0
#define MEDIUM_PRIORITY    	1
#define LOW_PRIORITY     	2

#define TERMINAL_1	     	1
#define TERMINAL_2	     	2

/*
*********************************************************************************************************
*                                             VARIABLES
*********************************************************************************************************
*/
CPU_STK           generationStk[TASK_STK_SIZE]; //Stack of each task
CPU_STK           decollageStk[TASK_STK_SIZE]; //Stack of each task
CPU_STK           atterrissageStk[TASK_STK_SIZE];
CPU_STK           terminal0Stk[TASK_STK_SIZE];
CPU_STK           terminal1Stk[TASK_STK_SIZE];
CPU_STK           terminal2Stk[TASK_STK_SIZE];
CPU_STK           terminal3Stk[TASK_STK_SIZE];

OS_TCB generationTCB;
OS_TCB atterrissageTCB;
OS_TCB terminal0TCB;
OS_TCB terminal1TCB;
OS_TCB decollageTCB;
OS_TCB statistiquesTCB;
OS_TCB verificationTCB;

/*
*********************************************************************************************************
*                                               QUEUES
*********************************************************************************************************
*/
OS_Q Q_high_priority;
OS_Q Q_medium_priority;
OS_Q Q_low_priority;

OS_Q Q_terminal0;
OS_Q Q_terminal1;

OS_Q Q_decollage;


/*
*********************************************************************************************************
*                                              FLAGS
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                              SEMAPHORES
*********************************************************************************************************
*/

OS_SEM timer1_to_generation_sem;
OS_SEM timer2_to_verification_sem;
OS_SEM gpio_to_statistiques_sem;
OS_SEM atterrissage_to_terminal_sem;
OS_SEM terminal_to_atterrissage_sem;



/*
*********************************************************************************************************
*                                             ENUMERATIONS
*********************************************************************************************************
*/

enum Aeroport { YUL, YYZ, YVR, PEK, DBX, LGA, HND, LAX, CDG, AMS, NB_AEROPORTS };



/*
*********************************************************************************************************
*                                             STRUCTURES
*********************************************************************************************************
*/

typedef struct Avion {
	int id;
	int retard;
	enum Aeroport origine;
	enum Aeroport destination;
} Avion;

/*
*********************************************************************************************************
*                                             SHARED VAIRABLES
*********************************************************************************************************
*/
bool stopSimDebordement;
volatile int terminal1 = 1;
volatile int terminal2 = 1;
volatile int nbAttenteAtterrissageHigh = 0;
volatile int nbAttenteAtterrissageMedium = 0;
volatile int nbAttenteAtterrissageLow = 0;
volatile int nbAttenteDecollage = 0;

OS_MUTEX print_mutex;
OS_MUTEX malloc_mutex;
OS_MUTEX free_mutex;
OS_MUTEX terminalAvailable_mutex;
OS_MUTEX terminal1_mutex;
OS_MUTEX terminal2_mutex;
OS_MUTEX nbAttenteAtterrissageHigh_mutex;
OS_MUTEX nbAttenteAtterrissageMedium_mutex;
OS_MUTEX nbAttenteAtterrissageLow_mutex;
OS_MUTEX nbAttenteDecollage_mutex;
OS_MUTEX debordement_mutex;
OS_MUTEX affichage_mutex;


/*
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*/

void create_application();
int create_tasks();
int create_events();

void	generation(void* data);
void	atterrissage(void* data);
void    terminal(void* data);
void    decollage(void* data);
void	remplirAvion(Avion* avion);
void	statistiques(void* data);
void 	verification(void* data);

void safe_print(char* message);
void safe_print_int(char* message, int i);
void safe_malloc(Avion* avion);
void safe_free(Avion* avion);

int priority(int retard);

void errMsg(OS_ERR err, char* errMsg);
void qIsFull(OS_ERR err);
void updateNbAttenteAtterrissage(int type, bool incrementation);
void enAttenteTerminalLibre(Avion* avion);
void updateNbAttenteDecollage(bool incrementation);

bool terminauxLibres();
void terminalSeLibere(int num);

#endif /* SRC_SIMAVION_H_ */
