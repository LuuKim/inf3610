Micrium uC/OS Repository v1.43

Installation and usage instructions can be found at https://doc.micrium.com/display/UCOSXSDK/uCOS+Xilinx+SDK+Repository+Documentation+Home

Requirements
    Xilinx SDK v2017.4

Supported Software
    uC/CAN        v2.42.00
    uC/Clk        v3.09.03
    uC/Common     v1.01.00
    uC/CPU        v1.31.02
    uC/CRC        v1.09.01
    uC/DHCPc      v2.10.01
    uC/DNSc       v2.00.04
    uC/FS         v4.07.03
    uC/HTTP       v3.00.03
    uC/LIB        v1.38.02
    uC/MQTT       v1.01.00
    uC/OS-II      v2.92.14
    uC/OS-III     v3.06.02
    uC/Shell      v1.03.01
    uC/TCPIP      v3.04.03
    uC/TELNETs    v1.05.02
    uC/USB-Device v4.05.02
    uC/USB-Host   v3.41.04